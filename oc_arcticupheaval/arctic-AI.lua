
WaterLandings = 
{
	Easy =
	{
	{"3tnk","3tnk","v2rl","e1"},		--Ready
	{"3tnk","3tnk","v2rl","e1"},
	{"3tnk","3tnk","v2rl","e1"},
	{"3tnk","3tnk","v2rl","e1"},
	{"3tnk","3tnk","v2rl","e1"}
	},
	
	Normal =
	{
	{"3tnk","3tnk","v2rl","e1"},
	{"3tnk","3tnk","v2rl","e1"},
	{"3tnk","3tnk","v2rl","e1"},
	{"3tnk","3tnk","v2rl","e1"},
	{"3tnk","3tnk","v2rl","e1"}
	},
	
	Hard =
	{
	{"3tnk","3tnk","e1","e1","e1"}, 
	{"3tnk","3tnk","e1","e1","e1"},
	{"3tnk","3tnk","e1","e1","e2"},		--Ready
	{"ttnk","ttnk","3tnk","e4","e4"},	--Ready
	{"4tnk","4tnk","3tnk","e1","e2"}	--Ready
	}
}

LstBaseAtkEntries =
{
	LstBaseAtkEntry1,
	LstBaseAtkEntry2,
	LstBaseAtkEntry3,
	LstBaseAtkEntry4
}

LstBaseAtkDestinies =
{
	LstBaseAtkDst1,
	LstBaseAtkDst2,
	LstBaseAtkDst3,
	LstBaseAtkDst4,
	LstBaseAtkDst5,
	LstBaseAtkDst6,
	LstBaseAtkDst7,
	LstBaseAtkDst8,
	LstBaseAtkDst9
}

--[[Submarine Blockade Locations]]
BlockadeSubSpawn = SubBlockadeEntry.Location
BlockadeSubDsts = 
	{
		SubBlockadeDst1.Location, 
		SubBlockadeDst2.Location,
		SubBlockadeDst3.Location,
		SubBlockadeDst4.Location,
		SubBlockadeDst5.Location
	}
BlockadeVessels = {"ss.defend","ss.defend","ss.defend","ss.defend","ss.defend"}
BlockadeActive = false
--[[Submarine Blockade Locations]]

SovietLandInfantryTypes = { "e1", "e2" }
InfLandAttack = { }
InfLandAtkCount = 11

SovietLandVehicleTypes = { "3tnk", "ftrk", "3tnk" }
ArmorLandAttack = { }
ArmorLandAtkCount = 2

SovietAircraftTypes = { "yak", "mig" }
PlaneAttack = { }

InlandAtkPath =
{
	{MainlandAtkPath1, MainlandAtkPath2, MainlandAtkPath3}
}

--Air
--[[			]]
OoMAir = true

AtkAircraftEntry = {AirAtkEntry1, AirAtkEntry2, AirAtkEntry3}
AtkAircraftDelay = DateTime.Minutes(2)

ParadropInitialDelay = 300
ParadropDestinies = 
{ 
	{ParatroopLz1, 24}, 
	{ParatroopLz2, -16}, 
	{ParatroopLz3, -32}, 
	{ParatroopLz4, -32},
	{ParatroopLz5, -32}
}

LstBaseAtks =
{
LstBaseAtkEntry1,
LstBaseAtkEntry2,
LstBaseAtkEntry3,
LstBaseAtkEntry4
}

------------------------------
---SUBMARINE BLOCKADE START---
------------------------------
SendSubBlockade = function()
	if BlockadeActive == false then
		BlockadeActive = true
		local subs = Reinforcements.Reinforce(enemy, BlockadeVessels, {BlockadeSubSpawn, BlockadeSubSpawn+CVec.New(0,-4)}, 10)
		local count = 1
	
		Utils.Do(subs, function(submarine)
			submarine.AttackMove(BlockadeSubDsts[count])
			count = count+1
		end)
		
		Trigger.OnAllKilled(subs, function()
			BlockadeActive = false
		end)
	end

	Trigger.AfterDelay(DateTime.Seconds(20), function()
		SendSubBlockade()
	end)
end
------------------------------
----SUBMARINE BLOCKADE END----
------------------------------

------------------------------
-----ATTACK MANAGER START-----
------------------------------
AvailableEnemyForces = function()
	if TotalEnemyForce > 0 then
		return true
	else
		TotalEnemyForce = 0
		Media.Debug("No more enemy forces")
		return false
	end
end

OperateEnemyForces = function(value)
	TotalEnemyForce = TotalEnemyForce + value
end
------------------------------
------ATTACK MANAGER END------
------------------------------

------------------------------
-------PARADROP START---------
------------------------------
SetParatroopersDelay = function()	
	local countAflds = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.Owner == enemy and actor.Type == "afld"
	end)
	
	local delay = ParadropInitialDelay-#countAflds*20
	
	if delay < 60 then
		delay = 60
	end
	
	if AvailableEnemyForces() then
		Media.Debug("Sending Paradrop - delay: " .. tostring(delay))
		SendParatroopers(delay)
		OperateEnemyForces(-50)
	else
		return
	end
end

SendParatroopers = function(delay)
	local powerproxy = Actor.Create("powerproxy.paratroopers", false, { Owner = enemy })
	
	local paradropData = Utils.Random(ParadropDestinies)
	local destiny = paradropData[1]
	local facing = paradropData[2]

	local plane = powerproxy.ActivateParatroopers(destiny.CenterPosition, 128 + facing)
	
	Utils.Do(plane, function(a)
		Trigger.OnPassengerExited(a, function(t, p)
			IdleHunt(p)
		end)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(delay), function()
		SetParatroopersDelay()
	end)
end
------------------------------
--------PARADROP END----------
------------------------------


IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end

BuildBase = function(buildings, fact)
	for i,v in ipairs(buildings) do
		if not v.exists then
			
			BuildBuilding(v, fact, buildings)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		BuildBase(buildings, fact)
	end)
end

BuildBuilding = function(building, fact, allbuildings)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if fact.IsDead or fact.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = fact.Location+CVec.New(1,1) + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), function()
			BuildBase(allbuildings, fact)
		end)
	end)
end

ProduceInfantry = function(producer, owner, paths, col)
	if not producer.exists --[[or producer.Owner ~= owner]] then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		--Media.Debug("ProduceInfantry - Exit")
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(SovietLandInfantryTypes) }
	local Path = Utils.Random(paths)
	owner.Build(toBuild, function(unit)
		col[#col + 1] = unit[1]
		if #col >= InfAtkCount then
			SendUnits(col, Path)
			col = { }
			Trigger.AfterDelay(DateTime.Seconds(30), function()
				ProduceInfantry(producer, owner, paths, col)
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceInfantry(producer, owner, paths, col)
			end)
		end
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

