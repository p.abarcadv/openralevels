
if Map.LobbyOption("difficulty") == "easy" then
	--
	HardMSub1.Destroy()
	HardMSub2.Destroy()
	
	DelayAIStart = DateTime.Seconds(120)
	
	SupplyConvoy = {"truk","truk","truk","truk"}
	ReinforcingConvoy = {"e1","e1","1tnk","2tnk","2tnk"}
	
	StartingCash = 12000
	
	--For secondary objective
	sentLSTs = 15
	LstNotToBeDestroyed = 5
	X = 5
	
	TotalEnemyForce = 5000
	
elseif Map.LobbyOption("difficulty") == "normal" then
	--
	DelayAIStart = DateTime.Seconds(60)
	
	SupplyConvoy = {"truk","truk","truk"}
	ReinforcingConvoy = {"e1","e1","1tnk","jeep","2tnk"}
	
	StartingCash = 9000

	--For secondary objective
	sentLSTs = 12
	LstNotToBeDestroyed = 2
	X = 5
	
	TotalEnemyForce = 5000
	
elseif Map.LobbyOption("difficulty") == "hard" then
	--
	DelayAIStart = DateTime.Seconds(10)
	
	SupplyConvoy = {"truk","truk","truk"}
	ReinforcingConvoy = {"e1","e1","1tnk","jeep","1tnk"}
	
	StartingCash = 6000
	
	--For secondary objective
	sentLSTs = 9
	LstNotToBeDestroyed = 0
	X = "any"
	
	TotalEnemyForce = 5000
end

--Allied Convoys
ReinforceType = "money"	--money or units
AlliedWaterEntryPath = {SupplyEntry.Location, SupplyWy.Location, SupplyDst.Location}
AlliedWaterExitPath = {SupplyWy.Location, SupplyDst.Location}
InsertionLst = "lst.in"
AlliedConvoyDelay = DateTime.Minutes(3)
DestroyedLSTs = 0

--Enemy MCVs
LstMcvPaths =
{
	{LstMcvEntry1.Location, LstMcvDst1.Location},
	{LstMcvEntry2.Location, LstMcvDst2.Location},
	{LstMcvEntry3.Location, LstMcvDst3.Location},
	{LstMcvEntry4.Location, LstMcvDst4.Location},
	{LstMcvEntry5.Location, MainlandLstDst1.Location} --Mainland is different
}

McvDeployPos =
{
	McvDeployPoint1,
	McvDeployPoint2,
	McvDeployPoint3,
	McvDeployPoint4,
	MainlandMcvDst1
}

MCVEscorts =
{
	{"mcv","4tnk","4tnk","e1","e1"},
	{"mcv","4tnk","4tnk","e1","e1"},
	{"mcv","4tnk","4tnk","e1","e1"},
	{"mcv"},
	{"mcv","4tnk","4tnk","e1","e1"}
}

SendWaterConvoy = function()
	if sentLSTs < 12 then
		Media.PlaySpeechNotification(player, "AlliedReinforcementsArrived")
		local x = {}
		
		if ReinforceType == "money" then
			ReinforceType = "units"
			x = SupplyConvoy
		else
			ReinforceType = "money"
			x = ReinforcingConvoy
		end
		
		local lst = Reinforcements.ReinforceWithTransport(player, InsertionLst, x, AlliedWaterEntryPath, AlliedWaterExitPath)[1]
		
		Trigger.OnKilled(lst, function()
			if lst.PassengerCount > 0 then
				DestroyedLSTs = DestroyedLSTs + 1
				Media.Debug(tostring(DestroyedLSTs))
			end
		end)
		
		Trigger.AfterDelay(AlliedConvoyDelay, function()
			SendWaterConvoy()
		end)
	end
end



Extension = { WVec.New(-1024*5, -1024*5, 0), WVec.New (1024*5,1024*5, 0) }

--------------------------------------
---MANAGE ENEMY BASE CREATION START---
--------------------------------------
ManageMCVs = function(units, pos)
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		SendMCVs(MCVEscorts[1], LstMcvPaths[1], McvDeployPos[1], Base1Buildings)
		
		Trigger.AfterDelay(DateTime.Minutes(2), function()
			SendMCVs(MCVEscorts[2], LstMcvPaths[2], McvDeployPos[2], Base2Buildings)
			
			Trigger.AfterDelay(DateTime.Minutes(2), function()
				SendMCVs(MCVEscorts[3], LstMcvPaths[3], McvDeployPos[3], Base3Buildings)
				
				Trigger.AfterDelay(DateTime.Minutes(2), function()
					SendMCVs(MCVEscorts[4], LstMcvPaths[4], McvDeployPos[4], Base4Buildings)
		
				end)
			end)
		end)
	end)

	SendMCVs(MCVEscorts[5], LstMcvPaths[5], McvDeployPos[5], Base5Buildings)
end


SendMCVs = function(group, path, deployPoint, buildings)
	local mcv = Reinforcements.ReinforceWithTransport(enemy, InsertionLst, group, path, {path[1]})[2][1]
	
	Trigger.OnAddedToWorld(mcv, function()
		if not mcv.IsDead then
			mcv.Move(deployPoint.Location)
			mcv.Deploy()
			
			Trigger.OnEnteredFootprint({deployPoint.Location}, function(a, id)
				if a.Owner == enemy and a.Type == "mcv" then
					Trigger.RemoveFootprintTrigger(id)
					Trigger.AfterDelay(DateTime.Seconds(5), function()
						local fact = Map.ActorsInBox(deployPoint.CenterPosition+Extension[1], deployPoint.CenterPosition+Extension[2], function(actor)
							return actor.Type == "fact" and (actor.Owner == enemy or actor.Owner == badguy) end)
						if #fact > 0 then
							BuildBase(buildings, fact[1])
						end
					end)
				end
			end)
		end
	end)
end
--------------------------------------
----MANAGE ENEMY BASE CREATION END----
--------------------------------------

DefineSubs = function()
	local subs = Utils.Where(enemy.GetActors(), function(subm)
		return subm.Type == "ss" and subm.Owner == enemy
	end)
	Utils.Do(subs, function(a)
		if not a.IsDead then
			local stance = Utils.Random({"AttackAnything", "Defend"})
			a.Stance = stance
		end
	end)
end


InfRightAttack = { }
InfAtkCount = 7

RunAIActivities = function()
	ManageMCVs()
	SetParatroopersDelay()
	
	ProduceInfantry(Base5Barr, enemy, InlandAtkPath, InfRightAttack)
	
end

Tick = function()
	if enemy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(DenySoviets)
	end
	
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(DenyAllies)
	end
	
	if DestroyedLSTs > LstNotToBeDestroyed then
		player.MarkFailedObjective(ProtectConvoy)
	end
	
	--FOR DEBUG--
	enemy.Cash = 10000
	UserInterface.SetMissionText("Enemy force count: " .. tostring(TotalEnemyForce), HSLColor.White)
	-------------
end

InitTriggers = function()
	Camera.Position = PCyard.CenterPosition
	player.Cash = StartingCash
	
	DefineSubs()
	Trigger.AfterDelay(DelayAIStart, function()
		RunAIActivities()
	end)
	
	Trigger.AfterDelay(DateTime.Minutes(5), function()
		SendWaterConvoy()
		Trigger.AfterDelay(DateTime.Minutes(5), function()
			SendSubBlockade()
		end)
	end)
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DenySoviets = player.AddPrimaryObjective("Deny any soviet incursion in the arctic.") --x
	ProtectConvoy = player.AddSecondaryObjective("Prevent the destruction of " .. X .. " supply convoys.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

SetupMood = function()
	Media.PlayMusic("gloom")
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	
	Media.Debug(Map.LobbyOption("difficulty"))
	
	InitTriggers()
	InitObjectives()
	SetupMood()
end