--------------
----BASE 1----
--------------
--Units
Base1Mcv	= {type = "powr", pos = CVec.New(2,1)	, cost = 300, 	exists = false}
Base14tnk 	= {type = "powr", pos = CVec.New(2,1)	, cost = 300, 	exists = false}
Base14tnk	= {type = "powr", pos = CVec.New(2,1)	, cost = 300, 	exists = false}
Base1E1 	= {type = "powr", pos = CVec.New(2,1)	, cost = 300, 	exists = false}
Base1E1 	= {type = "powr", pos = CVec.New(2,1)	, cost = 300, 	exists = false}
--Buildings
Base1Power1 = {type = "powr", pos = CVec.New(2,1)	, cost = 300, 	exists = false}
Base1Power2 = {type = "apwr", pos = CVec.New(2,-2)	, cost = 500, 	exists = false}
Base1Power3 = {type = "powr", pos = CVec.New(5,-2)	, cost = 300, 	exists = false}
Base1Power4 = {type = "apwr", pos = CVec.New(9,-2)	, cost = 500, 	exists = false}
Base1Power5 = {type = "apwr", pos = CVec.New(9,-5)	, cost = 500, 	exists = false}

Base1Pen1 	= {type = "spen", pos = CVec.New(-3,-6)	, cost = 1000, 	exists = false}
Base1Pen2 	= {type = "spen", pos = CVec.New(6,-10)	, cost = 1000, 	exists = false}

Base1Afld1 	= {type = "afld", pos = CVec.New(-12,4)	, cost = 500, 	exists = false}
Base1Afld2 	= {type = "afld", pos = CVec.New(-6,6)	, cost = 500, 	exists = false}

Base1Hpad1 	= {type = "hpad", pos = CVec.New(-13,6)	, cost = 500, 	exists = false}
Base1Hpad2 	= {type = "hpad", pos = CVec.New(-10,7)	, cost = 500, 	exists = false}
Base1Hpad3 	= {type = "hpad", pos = CVec.New(-7,8)	, cost = 500, 	exists = false}

Base1Sam1	= {type = "sam", pos = CVec.New(-9,4)	, cost = 1000, 	exists = false}
Base1Sam2	= {type = "sam", pos = CVec.New(-14,9)	, cost = 1000, 	exists = false}
Base1Sam3	= {type = "sam", pos = CVec.New(-8,11)	, cost = 1000, 	exists = false}

Base1Tesla1	= {type = "tsla", pos = CVec.New(-14,10), cost = 1500, 	exists = false}
Base1Tesla2	= {type = "tsla", pos = CVec.New(-7,12)	, cost = 1500, 	exists = false}
--------------
----BASE 2----
--------------
--Units

--Buildings
Base2Power1 = {type = "powr", pos = CVec.New(4,3)	, cost = 300, 	exists = false}
Base2Power2 = {type = "apwr", pos = CVec.New(-4,4)	, cost = 500, 	exists = false}
Base2Power3 = {type = "apwr", pos = CVec.New(-1,4)	, cost = 500, 	exists = false}
Base2Power4 = {type = "powr", pos = CVec.New(2,4)	, cost = 300, 	exists = false}
Base2Power5 = {type = "apwr", pos = CVec.New(-1,-7)	, cost = 500, 	exists = false}
Base2Power6 = {type = "apwr", pos = CVec.New(2,-6)	, cost = 500, 	exists = false}
Base2Power7 = {type = "apwr", pos = CVec.New(5,-6)	, cost = 500, 	exists = false}
Base2Power8 = {type = "apwr", pos = CVec.New(-1,-4)	, cost = 500, 	exists = false}
Base2Power9 = {type = "apwr", pos = CVec.New(2,-3)	, cost = 500, 	exists = false}
Base2Power10= {type = "powr", pos = CVec.New(5,-3)	, cost = 300, 	exists = false}

Base2Barr 	= {type = "barr", pos = CVec.New(-7,-4)	, cost = 500, 	exists = false}
Base2Kenn 	= {type = "kenn", pos = CVec.New(-2,-2)	, cost = 150, 	exists = false}
Base2Weap 	= {type = "weap", pos = CVec.New(-8,0)	, cost = 2000, 	exists = false}

Base2Fix	= {type = "fix", pos = CVec.New(-10,-4)	, cost = 1000, 	exists = false}

Base2Dome	= {type = "dome", pos = CVec.New(5,0)	, cost = 1000, 	exists = false}
Base2STek 	= {type = "stek", pos = CVec.New(2,0)	, cost = 2000, 	exists = false}

Base2Afld1 	= {type = "afld", pos = CVec.New(19,8)	, cost = 500, 	exists = false}
Base2Afld2 	= {type = "afld", pos = CVec.New(22,8)	, cost = 500, 	exists = false}
Base2Afld3 	= {type = "afld", pos = CVec.New(19,10)	, cost = 500, 	exists = false}
Base2Afld4 	= {type = "afld", pos = CVec.New(22,10)	, cost = 500, 	exists = false}

Base2SPen1 	= {type = "spen", pos = CVec.New(-19,-6), cost = 1000, 	exists = false}
Base2SPen2 	= {type = "spen", pos = CVec.New(-16,-1), cost = 1000, 	exists = false}
Base2SPen3 	= {type = "spen", pos = CVec.New(16,-5)	, cost = 1000, 	exists = false}
Base2SPen4 	= {type = "spen", pos = CVec.New(22,-4)	, cost = 1000, 	exists = false}
Base2SPen5 	= {type = "spen", pos = CVec.New(22,1)	, cost = 1000, 	exists = false}

Base2Tesla1 = {type = "tsla", pos = CVec.New(-14,-4), cost = 1500, 	exists = false}
Base2Tesla2 = {type = "tsla", pos = CVec.New(-9,4)	, cost = 1500, 	exists = false}
Base2Tesla3 = {type = "tsla", pos = CVec.New(-3,8)	, cost = 1500, 	exists = false}
Base2Tesla4 = {type = "tsla", pos = CVec.New(5,6)	, cost = 1500, 	exists = false}
Base2Tesla5 = {type = "tsla", pos = CVec.New(15,1)	, cost = 1500, 	exists = false}
Base2Tesla6 = {type = "tsla", pos = CVec.New(16,8)	, cost = 1500, 	exists = false}
Base2Tesla7 = {type = "tsla", pos = CVec.New(23,12)	, cost = 1500, 	exists = false}
Base2Tesla8 = {type = "tsla", pos = CVec.New(29,6)	, cost = 1500, 	exists = false}
Base2Tesla9 = {type = "tsla", pos = CVec.New(35,-3)	, cost = 1500, 	exists = false}

Base2Sam1 = {type = "sam", pos = CVec.New(-13,-4)	, cost = 1000, 	exists = false}
Base2Sam2 = {type = "sam", pos = CVec.New(-9,5)		, cost = 1000, 	exists = false}
Base2Sam3 = {type = "sam", pos = CVec.New(1,7)		, cost = 1000, 	exists = false}
Base2Sam4 = {type = "sam", pos = CVec.New(6,-7)		, cost = 1000, 	exists = false}
Base2Sam5 = {type = "sam", pos = CVec.New(16,7)		, cost = 1000, 	exists = false}
Base2Sam6 = {type = "sam", pos = CVec.New(21,12)	, cost = 1000, 	exists = false}
Base2Sam7 = {type = "sam", pos = CVec.New(28,7)		, cost = 1000, 	exists = false}
Base2Sam8 = {type = "sam", pos = CVec.New(33,-3)	, cost = 1000, 	exists = false}
--------------
----BASE 3----
--------------
--Units

--Buildings
Base3Power1 = {type = "powr", pos = CVec.New(9,1)	, cost = 300, 	exists = false}
Base3Power2 = {type = "apwr", pos = CVec.New(8,-5)	, cost = 500, 	exists = false}
Base3Power3 = {type = "apwr", pos = CVec.New(11,-6)	, cost = 500, 	exists = false}
Base3Power4 = {type = "apwr", pos = CVec.New(11,-3)	, cost = 500, 	exists = false}
Base3Power5 = {type = "apwr", pos = CVec.New(11,0)	, cost = 500, 	exists = false}
Base3Power6 = {type = "apwr", pos = CVec.New(11,3)	, cost = 500, 	exists = false}
Base3Power7 = {type = "apwr", pos = CVec.New(8,4)	, cost = 500, 	exists = false}
Base3Power8 = {type = "apwr", pos = CVec.New(11,6)	, cost = 500, 	exists = false}

Base3Barr 	= {type = "barr", pos = CVec.New(3,4)	, cost = 500, 	exists = false}
Base3Weap 	= {type = "weap", pos = CVec.New(4,0)	, cost = 1500, 	exists = false}
Base3Kenn 	= {type = "kenn", pos = CVec.New(8,1)	, cost = 150, 	exists = false}

Base3Hpad1 	= {type = "hpad", pos = CVec.New(0,4)	, cost = 500, 	exists = false}
Base3Hpad2 	= {type = "hpad", pos = CVec.New(12,10)	, cost = 500, 	exists = false}
Base3Afld1 	= {type = "afld", pos = CVec.New(-3,-4)	, cost = 500, 	exists = false}
Base3Afld2 	= {type = "afld", pos = CVec.New(-3,-6)	, cost = 500, 	exists = false}

Base3Pen1 	= {type = "spen", pos = CVec.New(-6,11)	, cost = 1000, 	exists = false}
Base3Pen2 	= {type = "spen", pos = CVec.New(2,10)	, cost = 1000, 	exists = false}
Base3Pen3 	= {type = "spen", pos = CVec.New(6,14)	, cost = 1000, 	exists = false}
Base3Pen4 	= {type = "spen", pos = CVec.New(-8,16)	, cost = 1000, 	exists = false}
Base3Pen5 	= {type = "spen", pos = CVec.New(-1,19)	, cost = 1000, 	exists = false}

Base3Fix 	= {type = "fix", pos = CVec.New(8,-2)	, cost = 1500, 	exists = false}

Base3Dome 	= {type = "dome", pos = CVec.New(1,-5)	, cost = 1500, 	exists = false}
Base3Stek 	= {type = "stek", pos = CVec.New(5,-4)	, cost = 1500, 	exists = false}

Base3Tesla1	= {type = "tsla", pos = CVec.New(-5,-1)	, cost = 1500, 	exists = false}
Base3Tesla2	= {type = "tsla", pos = CVec.New(-14,8)	, cost = 1500, 	exists = false}
Base3Tesla3	= {type = "tsla", pos = CVec.New(5,6)	, cost = 1500, 	exists = false}

Base3Sam1	= {type = "sam", pos = CVec.New(-4,-7)	, cost = 1000, 	exists = false}
Base3Sam2	= {type = "sam", pos = CVec.New(-3,0)	, cost = 1000, 	exists = false}
Base3Sam3	= {type = "sam", pos = CVec.New(-12,9)	, cost = 1000, 	exists = false}
Base3Sam4	= {type = "sam", pos = CVec.New(12,-9)	, cost = 1000, 	exists = false}
Base3Sam5	= {type = "sam", pos = CVec.New(5,7)	, cost = 1000, 	exists = false}
Base3Sam6	= {type = "sam", pos = CVec.New(12,14)	, cost = 1000, 	exists = false}
--------------
----BASE 4----
--------------
--Units

--Buildings
Base4Power1 = {type = "powr", pos = CVec.New(-2,-12), cost = 300, 	exists = false}
Base4Power2 = {type = "powr", pos = CVec.New(5,-12)	, cost = 300, 	exists = false}
Base4Power3 = {type = "apwr", pos = CVec.New(-4,-15), cost = 500, 	exists = false}
Base4Power4 = {type = "apwr", pos = CVec.New(-1,-15), cost = 500, 	exists = false}
Base4Power5 = {type = "apwr", pos = CVec.New(2,-15)	, cost = 500, 	exists = false}
Base4Power6 = {type = "apwr", pos = CVec.New(5,-15)	, cost = 500, 	exists = false}

Base4Barr 	= {type = "barr", pos = CVec.New(1,-12)	, cost = 500, 	exists = false}

Base4Tesla1 = {type = "tsla", pos = CVec.New(-5,-9)	, cost = 1500, 	exists = false}
Base4Tesla2 = {type = "tsla", pos = CVec.New(9,-11)	, cost = 1500, 	exists = false}
Base4Tesla3 = {type = "tsla", pos = CVec.New(-3,3)	, cost = 1500, 	exists = false}
Base4Tesla4 = {type = "tsla", pos = CVec.New(4,3)	, cost = 1500, 	exists = false}

Base4Sam1 	= {type = "sam", pos = CVec.New(-6,-16)	, cost = 500, 	exists = false}
Base4Sam2 	= {type = "sam", pos = CVec.New(-1,-9)	, cost = 500, 	exists = false}
Base4Sam3 	= {type = "sam", pos = CVec.New(7,-17)	, cost = 500, 	exists = false} 
Base4Sam4 	= {type = "sam", pos = CVec.New(0,3)	, cost = 500, 	exists = false}
--------------
----BASE 5----
--------------
--Units

--Buildings
Base5Power1 = {type = "powr", pos = CVec.New(4,1)	, cost = 300, 	exists = true}
Base5Power2 = {type = "powr", pos = CVec.New(9,4)	, cost = 300, 	exists = true}
Base5Power3 = {type = "apwr", pos = CVec.New(0,9)	, cost = 500, 	exists = true}
Base5Power4 = {type = "apwr", pos = CVec.New(3,12)	, cost = 500, 	exists = true}
Base5Power5 = {type = "apwr", pos = CVec.New(6,12)	, cost = 500, 	exists = true}
Base5Power6 = {type = "apwr", pos = CVec.New(9,12)	, cost = 500, 	exists = true}
Base5Power7 = {type = "apwr", pos = CVec.New(12,11)	, cost = 500, 	exists = true}

Base5Barr	= {type = "barr", pos = CVec.New(-4,-1)	, cost = 500, 	exists = true}
Base5Weap	= {type = "weap", pos = CVec.New(3,4)	, cost = 2000, 	exists = false}
Base5Kenn	= {type = "kenn", pos = CVec.New(-1,3)	, cost = 150, 	exists = true}

Base5Spen1	= {type = "spen", pos = CVec.New(13,-5)	, cost = 1000, 	exists = false}
Base5Spen2	= {type = "spen", pos = CVec.New(18,-2)	, cost = 1000, 	exists = false}

Base5Dome	= {type = "dome", pos = CVec.New(6,1)	, cost = 1000, 	exists = false}
Base5STek	= {type = "stek", pos = CVec.New(11,4)	, cost = 1000, 	exists = false}

Base5Ftur1	= {type = "ftur", pos = CVec.New(-8,3)	, cost = 500, 	exists = true}
Base5Ftur2	= {type = "ftur", pos = CVec.New(-8,8)	, cost = 500, 	exists = true}
Base5Tesla1	= {type = "tsla", pos = CVec.New(-6,2)	, cost = 1000, 	exists = true}
Base5Tesla2	= {type = "tsla", pos = CVec.New(-6,9)	, cost = 1000, 	exists = true}
Base5Tesla3	= {type = "tsla", pos = CVec.New(20,4)	, cost = 1000, 	exists = true}
Base5Tesla4	= {type = "tsla", pos = CVec.New(6,4)	, cost = 1000, 	exists = true}
Base5Sam1	= {type = "sam"	, pos = CVec.New(-4,-3)	, cost = 500, 	exists = true}
Base5Sam2	= {type = "sam"	, pos = CVec.New(-4,11)	, cost = 500, 	exists = true}

Base1Buildings =
{
Base1Power1,
Base1Power2,
Base1Power3,
Base1Power4,
Base1Power5,
Base1Pen1,
Base1Pen2,
Base1Afld1,
Base1Afld2,
Base1Hpad1,
Base1Hpad2,
Base1Hpad3,
Base1Sam1,
Base1Sam2,
Base1Sam3,
Base1Tesla1,
Base1Tesla2
}

Base2Buildings =
{
Base2Power1,
Base2Power2,
Base2Power3,
Base2Power4,
Base2Power5,
Base2Power6,
Base2Power7,
Base2Power8,
Base2Power9,
Base2Power10,
Base2Barr,
Base2Kenn,
Base2Weap,
Base2Afld1,
Base2Afld2,
Base2Afld3,
Base2Afld4,
Base2Fix,
Base2Dome,
Base2STek,
Base2SPen1,
Base2SPen2,
Base2SPen3,
Base2SPen4,
Base2SPen5,
Base2Tesla1,
Base2Tesla2,
Base2Tesla3,
Base2Tesla4,
Base2Tesla5,
Base2Tesla6,
Base2Tesla7,
Base2Tesla8,
Base2Tesla9,
Base2Sam1,
Base2Sam2,
Base2Sam3,
Base2Sam4,
Base2Sam5,
Base2Sam6,
Base2Sam7,
Base2Sam8
}

Base3Buildings =
{
Base3Power1,
Base3Power2,
Base3Power3,
Base3Power4,
Base3Power5,
Base3Power6,
Base3Power7,
Base3Power8,
Base3Barr,
Base3Weap,
Base3Kenn,
Base3Hpad1,
Base3Hpad2,
Base3Afld1,
Base3Afld2,
Base3Fix,
Base3Dome,
Base3Stek,
Base3Tesla1,
Base3Tesla2,
Base3Tesla3,
Base3Pen1,
Base3Pen2,
Base3Pen3,
Base3Pen4,
Base3Pen5,
Base3Sam1,
Base3Sam2,
Base3Sam3,
Base3Sam4,
Base3Sam5,
Base3Sam6
}

Base4Buildings =
{
Base4Power1,
Base4Power2,
Base4Power3,
Base4Power4,
Base4Power5,
Base4Power6,
Base4Barr,
Base4Tesla1,
Base4Tesla2,
Base4Tesla3,
Base4Tesla4,
Base4Sam1,
Base4Sam2,
Base4Sam3,
Base4Sam4
}

Base5Buildings =
{
Base5Power1,
Base5Power2,
Base5Power3,
Base5Power4,
Base5Power5,
Base5Power6,
Base5Power7,
Base5Barr,
Base5Kenn,
Base5Weap,
Base5Dome,
Base5STek,
Base5Spen1,
Base5Spen2,
Base5Ftur1,
Base5Ftur2,
Base5Tesla1,
Base5Tesla2,
Base5Tesla3,
Base5Tesla4,
Base5Sam1,
Base5Sam2
}

Trigger.OnKilled(InlandPower1, function()
	Base5Power1.exists=false
end)

Trigger.OnKilled(InlandPower2, function()
	Base5Power2.exists=false
end)

Trigger.OnKilled(InlandPower3, function()
	Base5Power3.exists=false
end)

Trigger.OnKilled(InlandPower4, function()
	Base5Power4.exists=false
end)

Trigger.OnKilled(InlandPower5, function()
	Base5Power5.exists=false
end)

Trigger.OnKilled(InlandPower6, function()
	Base5Power6.exists=false
end)

Trigger.OnKilled(InlandPower7, function()
	Base5Power7.exists=false
end)

Trigger.OnKilled(InlandBarr, function()
	Base5Barr.exists=false
end)

Trigger.OnKilled(InlandKenn, function()
	Base5Kenn.exists=false
end)

Trigger.OnKilled(InlandFtur1, function()
	Base5Ftur1.exists=false
end)

Trigger.OnKilled(InlandFtur2, function()
	Base5Ftur2.exists=false
end)

Trigger.OnKilled(InlandSam1, function()
	Base5Sam1.exists=false
end)

Trigger.OnKilled(InlandSam2, function()
	Base5Sam2.exists=false
end)

Trigger.OnKilled(InlandTesla1, function()
	Base5Tesla1.exists=false
end)

Trigger.OnKilled(InlandTesla2, function()
	Base5Tesla2.exists=false
end)

Trigger.OnKilled(InlandTesla3, function()
	Base5Tesla3.exists=false
end)

Trigger.OnKilled(InlandTesla4, function()
	Base5Tesla4.exists=false
end)
--[[]]