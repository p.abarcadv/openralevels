TimerTicks = DateTime.Seconds(5) --first convoy (gotta chech the time diferences between games) Should be like 5 mins instead of 20 misn

                                    --0:12 a 5:01
                                    --20 mins mean 4:50 irl seconds (approximate to 5 mins
									
ticked = TimerTicks

InititalAttackers = { firstattackera1,firstattackera2, firstattackera3, firstattackerb1, firstattackerb2, firstattackerb3 }


EscapeRoutes = { SouthExit, EastExit, NorthEastExit } 


--CONVOY UNITS NECESSARY
NuclearConvoy = { "truk.nuclear", "truk.nuclear", "truk.nuclear" }

SouthConvoy = { SouthEntrance.Location, SouthEntrance.Location } --First entry
WestConvoy = { WestEntrance.Location, WestPath1.Location } --Second and Fourth entry
NorthWestConvoy = { NorthWestEntrance.Location, NorthEastExit.Location } --Third Entry
NorthLeftConvoy = { NorthLeftEntrance.Location, EastExit.Location } --Fifth Entry
SouthWestEntry = { SouthWestEntrance.Location, SouthExit.Location } --Sixth Entry
NorthRightConvoy = { NorthRightEntrance.Location, NorthEastExit.Location } --Seventh Entry

--PATHS
Path1 = {SouthEntrance.Location, SouthPath2.Location, SouthPath4.Location, SouthExitPath.Location, SouthExit.Location} --SouthPath
Path2 = {WestEntrance.Location, WestPath1.Location, WestPath2.Location, WestPath4.Location, waypoint7.Location, EastExit.Location}
Path3 = {NorthWestEntrance.Location, NorthWestPath1.Location, WestPath1.Location, SouthExit.Location}
Path4 = {}

CurrPath = { }

EntryWest = { WestEntrance.Location, WestPath1.Location } --Second and Fourth entry

SouthFlare = SouthExit.Location + CVec.New(0,-3)
EastFlare = EastExit.Location + CVec.New(-3,0)
NorthEastFlare = NorthEastExit.Location + CVec.New(-3,3)


CreateConvoySignal = function(flareLoc)
	local flareCam = Actor.Create("camera.small", true, { Owner = player, Location = flareLoc })
	local flare = Actor.Create("flare", true, { Owner = flare, Location = flareLoc })
	Trigger.AfterDelay(DateTime.Seconds(40), function() 
		flare.Destroy() 
		flareCam.Destroy()
	end)
end




PassingUnits = 
    {
        { "3tnk", "3tnk", "e1", "e1" }, --1
        { "3tnk", "3tnk", "e2", "e2" }, --2
        { "ttnk", "ttnk", "e2", "e2" }, --3
        { "ttnk", "ttnk", "e1", "e1" }, --4
        { "ttnk", "3tnk", "e1", "e1" }, --5
        { "4tnk", "4tnk", "ttnk", "ttnk" }, --6
        { "e2","e2","e2","e2" }, --7
        { "e1","e1","e1","e1" } --8
    }

AttackingUnits =
    {
        {"e2","e2","e2","e2"},
        {"e1","e1","e1","e1"},
        {"e1", "e1", "3tnk", "3tnk"},
        {"e2", "e2", "3tnk", "3tnk"}
    }

GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end

			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)

				if bool then
					stop = true

					i = i + 1
					if i > #waypoints then
						i = 1
					end

					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end


MoveThroughMap = function(units, movepath)
    
	local totalPoints = #movepath
	local radius = WDist.FromCells(3)
	
	
    Utils.Do(units, function(unit) --all units do
		unit.Move(movepath[1])
		Trigger.OnIdle(unit, function()
			if totalPoints >= 2 then
				if unit.Location == movepath[1] then 
					unit.Move(movepath[2])
				
				end
			end
			if totalPoints >= 3 then
				if unit.Location == movepath[2] then 
					unit.Move(movepath[3])
					
				end
			end
			if totalPoints >= 4 then
				if unit.Location == movepath[3] then
					unit.Move(movepath[4])
					
				end
			end
			if totalPoints >= 5 then
				if unit.Location == movepath[4] then
					unit.Move(movepath[5])		
						
				end
			end
			if totalPoints >= 6 then
				if unit.Location == movepath[5] then
					unit.Move(movepath[6])	
				end
			end
			if totalPoints >= 7 then
				if unit.Location == movepath[6] then
					unit.Move(movepath[7])	
				end
			end
			
			if unit.Location == movepath[totalPoints] then
				unit.Destroy()
			end
		end)
    end)
end

SendUnits = function(units, waypoints, path)
    local ArrivingUnits  = Reinforcements.Reinforce(enemy, units, waypoints)
    --Trigger.AfterDelay(DateTime.Seconds(5), function() 
        MoveThroughMap(ArrivingUnits, path)
    --end)
end

SendConvoy = function(waypoints, path)
    Media.PlaySpeechNotification(player, "ConvoyApproaching")
    Trigger.AfterDelay(DateTime.Seconds(2), function()
        local convoy = Reinforcements.Reinforce( enemy, NuclearConvoy, waypoints)
		MoveThroughMap(convoy, path) 
	end)
end

PrepareConvoy = function()
    --Trigger.AfterDelay(DateTime.Seconds(10), function() SendConvoy(SouthConvoy, Path1)  end) --1
    Trigger.AfterDelay(DateTime.Seconds(50), function() SendConvoy(WestConvoy, Path2)  end) -- 2
    --Trigger.AfterDelay(DateTime.Seconds(90), function() SendConvoy(NorthWestConvoy)  end) -- 3 
    --Trigger.AfterDelay(DateTime.Seconds(130), function() SendConvoy(WestConvoy)  end) -- 4
    --Trigger.AfterDelay(DateTime.Seconds(170), function() SendConvoy(NorthLeftConvoy) end) -- 5
    --Trigger.AfterDelay(DateTime.Seconds(210), function() SendConvoy(SouthWestEntry) end) -- 6
    --Trigger.AfterDelay(DateTime.Seconds(250), function() SendConvoy(NorthRightConvoy) end) -- 7
    SendUnits(PassingUnits[1], SouthConvoy, Path1)
	SendUnits(PassingUnits[2], WestConvoy, Path2)
	Trigger.AfterDelay(DateTime.Seconds(5), function() SendUnits(PassingUnits[2], WestConvoy, Path2) end)

end

FinishTimer = function()
	timerEnded = true
	for i = 0, 5, 1 do
		local c = TimerColor
		if i % 2 == 0 then
			c = HSLColor.White
		end
        Trigger.AfterDelay(DateTime.Seconds(i), function() UserInterface.SetMissionText("Time has ended.", c) end)
	end
	Trigger.AfterDelay(DateTime.Seconds(6), function() UserInterface.SetMissionText("") end)
end

Tick = function()
    if ticked > 0 then
        UserInterface.SetMissionText("Convoy arrives in " .. Utils.FormatTime(ticked), TimerColor)
        ticked = ticked - 1
    elseif ticked == 0 then
        FinishTimer()
        ticked = ticked - 1
        Trigger.AfterDelay(DateTime.Seconds(1), function() 
            PrepareConvoy() 
            player.MarkCompletedObjective(WaitConvoy)
        end)
    end
	
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(DenyAllies)
	end
	
end

InitTriggers = function()
    
	CreateConvoySignal(SouthFlare)
	
	
	player.Cash = 2000
	
    Camera.Position = DefaultCameraPosition.CenterPosition
    
    --CONVOY ESCAPE ROUTES
    Utils.Do(EscapeRoutes, function(actor)
        Trigger.OnEnteredProximityTrigger(actor.CenterPosition, WDist.FromCells(5), function(discoverer, id)   
            if discoverer.Owner == enemy and discoverer.Type == "truk.nuclear" then
                enemy.MarkCompletedObjective(DenyAllies)
            end
        end)
    end)
	
    Trigger.AfterDelay(0, function()
        local bridges = Utils.Where(Map.ActorsInWorld, function(actor) return actor.Type == "bridge1" or actor.Type == "bridge2" end)
        ExplodingBridgeA = bridges[1]--NorthWest Bridge
        ExplodingBridgeB = bridges[2]--North Bridge
        ExplodingBridgeC = bridges[3]--SouthEast Bridge
    end)
    
    
    Trigger.OnKilled(BridgeABarrel2, function()
        if not ExplodingBridgeA.IsDead then
            ExplodingBridgeA.Kill()
        end
    end)
    
    Trigger.OnKilled(BridgeBBarrel3, function()
        if not ExplodingBridgeB.IsDead then
            ExplodingBridgeB.Kill()
        end
    end)
    
    Trigger.OnKilled(BridgeBBarrel6, function()
        if not ExplodingBridgeB.IsDead then
            ExplodingBridgeB.Kill()
        end
    end)
  
    Trigger.OnKilled(BridgeCBarrel2, function()
        if not ExplodingBridgeC.IsDead then
            ExplodingBridgeC.Kill()
        end
    end)
    
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Utils.Do(InititalAttackers, function(unit)
			IdleHunt(unit)
		end)
	end)
	
end

IdleHunt = function(unit)
	if not unit.IsDead then
		Trigger.OnIdle(unit, unit.Hunt)
	end
end

InitObjectives = function()
    Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	WaitConvoy = player.AddPrimaryObjective("Wait Convoy")
    StopConvoy = player.AddPrimaryObjective("Stop the convoy.")
	DenyAllies = enemy.AddPrimaryObjective("Eliminate all allied presence in the area.")
	

	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)

end

WorldLoaded = function()
    player = Player.GetPlayer("Greece")
    enemy = Player.GetPlayer("USSR")
	flare = Player.GetPlayer("England")

    TimerColor = player.Color

    InitObjectives()
    InitTriggers()

end