
if Map.LobbyOption("difficulty") == "easy" then
	Extractions = 1
	CompanionType = "e3"
	SpyType = "spy"
	remainingTime = DateTime.Minutes(7)
elseif Map.LobbyOption("difficulty") == "normal" then
	Extractions = 1
	CompanionType = "e3"
	SpyType = "spy.england"
	remainingTime = DateTime.Minutes(6)
elseif Map.LobbyOption("difficulty") == "hard" then
	Extractions = 2
	CompanionType = "jeep"
	SpyType = "spy.england"
	remainingTime = DateTime.Minutes(3)
end

timerstarted = false

SpyFootPrintTriggers = 
	{
		CPos.New(85, 79)
	}

FirstCameraBatchTrigger = 
	{
		CPos.New(83,74), CPos.New(84,74), CPos.New(85,74), CPos.New(86,74), CPos.New(87,74), 
		CPos.New(88,74), CPos.New(89,74), CPos.New(92,74), CPos.New(93,74), CPos.New(94,74), 
		CPos.New(95,74), CPos.New(96,74)
	}

SecondCameraBatchTrigger = 
	{ 
		CPos.New(64,52), CPos.New(64,51), CPos.New(65,51), 
		CPos.New(66,51), CPos.New(67,51), CPos.New(68,51)
	}

ThridCameraBatchTrigger =
	{
		CPos.New(100,38), CPos.New(101,38), CPos.New(102,38)
	}

FourthCameraBatchTrigger = 
	{ 
		CPos.New(68,38), CPos.New(68,37), CPos.New(69,37), CPos.New(70,37),
		CPos.New(71,37), CPos.New(72,37), CPos.New(73,37), CPos.New(74,37),
		CPos.New(75,37), CPos.New(76,37), CPos.New(77,37), CPos.New(77,35),
		CPos.New(80,28), CPos.New(80,27), CPos.New(80,26), CPos.New(80,25),
		CPos.New(80,24)
	} 
	
FifthCameraBatchTrigger = 
	{ 
		CPos.New(68,17), CPos.New(67,17), CPos.New(67,16), CPos.New(67,15),
		CPos.New(68,10), CPos.New(68,9)
	} 

GreeceReinforcements1Trigger = 
	{ 
		CPos.New(68,38), CPos.New(68,37), CPos.New(69,37), CPos.New(70,37),
		CPos.New(71,37), CPos.New(72,37), CPos.New(73,37), CPos.New(74,37),
		CPos.New(75,37), CPos.New(76,37), CPos.New(77,37), CPos.New(77,35),
		CPos.New(80,28), CPos.New(80,27), CPos.New(80,26), CPos.New(80,25),
		CPos.New(80,24)
	} 
	
GreeceReinforcements2Trigger = 
	{ 
		CPos.New(93,27), CPos.New(93,28), CPos.New(93,29)
	} 

GreeceReinforcements2Units = { "e1", "e3", "e3" }
GreeceReinforcements2Medi = { "medi" }

GreeceReinforcements2Path = { GreeceReinforce2Spawn.Location, GreeceReinforce2Dst.Location}

USSRBaseReinforcements = {"e1", "e1", "e1"}
USSRSouthReinforcements = {"dog", "dog","dog", "dog", "dog", "e2", "e2" }


ReinforceBarrPath = {BaseReinforceSpawn.Location, BaseReinforceDst.Location}
ReinforceSouthPath = {SouthReinforceSpawn.Location, SouthReinforceDst.Location}

USSRBase = { BasePower1, BasePower2, BaseTech, BaseBarr, BaseBarl1, BaseBarl2, BaseBarl3, BaseBarl4 }

BarricadeActors = { BarricadeInf1, BarricadeInf2, BarricadeSbag }

CheckpointUnits = 
{
	CheckpointInf1, CheckpointInf2, CheckpointInf3
}

Hunters = { Hunter1, Hunter2, Hunter3, Hunter4, Hunter5, Hunter6 }

GreeceReinforcementsArrived = false



--[[
EnemyReinforcements1SpyHideout2 = { "e1", "e3" }
EnemyReinforcements2SpyHideout2 = { "e3", "e3" }
ExtractionHeliType = "tran"
ExtractionPath = { HelicopterSpawn.Location, HelicopterGoal.Location }
Farmers = { Farmer1, Farmer2, Farmer3 }
BarrierSoldiers = { BarrierSoldier1, BarrierSoldier2, BarrierSoldier3, BarrierSoldier4, BarrierSoldier5, BarrierSoldier6 }
RedBuildings = { RedBuildung1, RedBuilding2, RedBuilding3 }
BaseBuildings1 = { BaseBarrel1, BaseBarrel2, BaseBuilding1, BaseBuilding2 }
BaseBuildings2 = { BaseBuilding3, BaseBuilding4, BaseBuilding5, BaseBuilding6 }

SpyHideout1Trigger = 
{ 
CPos.New(84, 45), CPos.New(85, 45), CPos.New(86, 45), CPos.New(87, 45), CPos.New(88, 45), 
CPos.New(89, 45), CPos.New(90, 45) 
}

SpyHideout2PathTrigger = 
{ 
CPos.New(70, 61), CPos.New(70, 62), CPos.New(70, 63), CPos.New(70, 64), CPos.New(70, 65) 
}
SpyHideout2Trigger = 
{ 
CPos.New(50, 63), CPos.New(50, 64), CPos.New(50, 65), CPos.New(50, 66), 
CPos.New(50, 67), CPos.New(50, 68), CPos.New(50, 69) 
}
SpyTransport1CheckpointTrigger = { CPos.New(31, 65) }
SpyTransport2CheckpointTrigger = { CPos.New(47, 51) }
Barrier1Trigger = 
{ 
CPos.New(59,57), CPos.New(60,57), CPos.New(61,57), CPos.New(62,57), 
CPos.New(63,57), CPos.New(64,57), CPos.New(65,57), CPos.New(66,57), 
CPos.New(67,57), CPos.New(68,57) 
}
Barrier2Trigger = 
{ 
CPos.New(63, 47), CPos.New(64, 47), CPos.New(65, 47), CPos.New(66, 47), 
CPos.New(67, 47), CPos.New(68, 47) 
}
SpyHideout3Trigger = 
{ 
CPos.New(58, 45), CPos.New(58, 46), CPos.New(58, 47) 
}
RTrapTrigger = 
{ 
CPos.New(46, 34), CPos.New(47, 35), CPos.New(48, 36), 
CPos.New(48, 37), CPos.New(48, 38), CPos.New(48, 39) 
}
SpyHideout4Trigger = 
{ 
CPos.New(41, 34), CPos.New(41, 35), CPos.New(41, 36), 
CPos.New(41, 37), CPos.New(41, 38) 
}
]]


PlayerReinforcementsBase = function()
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		Reinforcements.Reinforce( player, USSRBaseReinforcements, ReinforceBarrPath, 1)
	end)
end

PlayerReinforcementsSouth = function()
	
	local units = Reinforcements.Reinforce( player, USSRSouthReinforcements, ReinforceSouthPath, 0)
	
end


IntroSequence = function()
	Actor.Create("camera", true, { Owner = player, Location = StartCamA.Location })
	Actor.Create("camera", true, { Owner = player, Location = StartCamB.Location })
	Actor.Create("camera", true, { Owner = player, Location = StartCamC.Location })
	Actor.Create("camera", true, { Owner = player, Location = StartCamD.Location })
	Actor.Create("camera", true, { Owner = player, Location = StartCamE.Location })
	Actor.Create("camera", true, { Owner = player, Location = StartCamF.Location })
	
	TheSpy.DisguiseAsType("e1", player)
	
	if not TheSpy.IsDead then
		CompanionUnit.Move(MoveCompanionPos.Location)
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySoundNotification(player, "sking")
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(2), function() 
			if SpyType == "spy.england" then
				TheSpy.Attack(AttackedBySpy, false, true)
			end
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(3), function() 
			TheSpy.Move(MoveSpy1.Location)
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(4), function() 
			Media.PlaySoundNotification(player, "AlertBuzzer")
			TheSpy.DisguiseAsType("e1", player)
			TheSpy.Move(MoveSpy2.Location)
			
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(7), function()
			if not CompanionUnit.IsDead and not AttackedByCompanion.IsDead then
				CompanionUnit.Attack(AttackedByCompanion, false, true)
			end
		end)
		
		Trigger.OnKilled(BaseFtur, function()
			CompanionUnit.Move(MoveSpy2.Location + CVec.New(0,-1))
			Trigger.AfterDelay(DateTime.Seconds(1), function()
				Utils.Do(USSRBase, function(unit)
					unit.Kill()
				end)
			end)
		end)

		Trigger.AfterDelay(DateTime.Seconds(6), function()
			Media.PlaySpeechNotification(player, "ExplosiveChargePlaced")
		end)

		Trigger.AfterDelay(DateTime.Seconds(6), function() 
			PlayerReinforcementsBase()
			timerstarted = true
			UnitsArrived = true
		end)
	end
end

--[[Spy Movement Waypoint Path 3]]
--
--Non-hard - Spy leaves center village and moves towards the barricade and then to the beach.
--Hard - Spy leaves center village, jumps into the jeep and is transported to the second barricade. Then he moves to the beach
Hideout2Escape = function()
	if not TheSpy.IsDead and not Escape3 then
		Escape3 = true
		Hideout2.UnloadPassengers()
		if Map.LobbyOption("difficulty") ~= "hard" then
			Trigger.AfterDelay(DateTime.Seconds(1), function()
				TheSpy.Move(SpyFinalWaypoint.Location)
			end)
		else
			Trigger.AfterDelay(DateTime.Seconds(5), function()
				TheSpy.EnterTransport(CompanionUnit)
				Trigger.AfterDelay(DateTime.Seconds(3), function()
					CompanionUnit.Move(MoveSpy6.Location + CVec.New(-2,-2))
					Trigger.AfterDelay(DateTime.Seconds(6), function()
						CompanionUnit.UnloadPassengers()
						Trigger.AfterDelay(DateTime.Seconds(1), function()
							CompanionUnit.Move(MoveSpy6.Location)
							Trigger.AfterDelay(DateTime.Seconds(3), function()
								CompanionUnit.Move(MoveSpy6.Location + CVec.New(-1,1))
							end)
						end)
					end)
				end)
			end)
		end
	end
end

--[[Spy Movement Waypoint Path 2]]
--
--
--
Hideout1Escape = function()
	if not TheSpy.IsDead and not Escape2 then
		Escape2 = true
		Hideout1.UnloadPassengers()
		if Map.LobbyOption("difficulty") ~= "hard" then
			Trigger.AfterDelay(DateTime.Seconds(1), function()
				TheSpy.Move(MoveSpy5.Location)
				TheSpy.EnterTransport(Hideout2)
			end)
		else
			Trigger.AfterDelay(DateTime.Seconds(1), function()
				TheSpy.EnterTransport(CompanionUnit)
				Trigger.AfterDelay(DateTime.Seconds(3), function()
					CompanionUnit.Move(MoveSpy5.Location)
					Trigger.AfterDelay(DateTime.Seconds(7), function()
						CompanionUnit.UnloadPassengers()
						Trigger.AfterDelay(DateTime.Seconds(3), function()
							CompanionUnit.Move(MoveSpy5.Location + CVec.New(-1,1))
							TheSpy.EnterTransport(Hideout2)
						end)
					end)
				end)
			end)
		end
	end
end

--[[Spy Movement Waypoint Path 1]]
--
--Non-hard - 
--Hard -
InitialEscape = function()
	if Map.LobbyOption("difficulty") ~= "hard" then
		TheSpy.Move(MoveSpy4.Location)
		TheSpy.EnterTransport(Hideout1)
	else 
		TheSpy.EnterTransport(CompanionUnit)
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			CompanionUnit.Move(MoveSpy4.Location)
			CompanionUnit.Move(Hideout1.Location + CVec.New(-2,2))
			Trigger.AfterDelay(DateTime.Seconds(6), function()
				CompanionUnit.UnloadPassengers()
				Trigger.AfterDelay(DateTime.Seconds(4), function()
					TheSpy.EnterTransport(Hideout1)
					CompanionUnit.Move(Hideout1.Location + CVec.New(-3,3))
					Trigger.AfterDelay(DateTime.Seconds(4), function()
						TheSpy.EnterTransport(Hideout1)
					end)
				end)
			end)
		end)
	end
end


Escape1 = false
Escape2 = false
Escape3 = false

--[[Spy Movement Waypoint 1]]
Trigger.OnEnteredFootprint({MoveSpy2.Location}, function(a, id)
	if not TheSpy.IsDead and not Escape1 and a.Owner == enemy then
		Trigger.RemoveFootprintTrigger(id)
		Escape1 = true
		InitialEscape()
	end
end)

BlowBridge = function()	
	Trigger.AfterDelay(DateTime.Seconds(6), function()
		if not BridgeBazooka.IsDead then
			BridgeBazooka.Attack(BridgeBarrels, false, true)
		end
	end)
end

Trigger.OnKilled(BridgeBarrels, function()
	if not ExplodingBridge.IsDead then
		ExplodingBridge.Kill()
	end
end)

--[[
Trigger.OnEnteredFootprint({MoveSpy4.Location}, function(a, id)
	Trigger.RemoveFootprintTrigger(id)
	if not TheSpy.IsDead and a == TheSpy then
		TheSpy.Move(MoveSpy5.Location)
	end
end)
	]]
	
	
	
	--[[
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		Utils.Do(BaseBuildings1, function(actor)
			if not actor.IsDead then
				actor.Kill()
			end
		end)
	end)
	Trigger.AfterDelay(DateTime.Seconds(6), function()
		Utils.Do(BaseBuildings2, function(actor)
			if not actor.IsDead then
				actor.Kill()
			end
		end)
		if not RSoldier1.IsDead then
			RSoldier1.Move(SpyWaypoint2.Location)
		end
		if not RSoldier2.IsDead then
			RSoldier2.Move(SpyWaypoint2.Location)
		end
	end)
	Trigger.AfterDelay(DateTime.Seconds(8), function()
		Dogs = Reinforcements.Reinforce(player, USSRReinforcements1, { ReinforcementSpawn.Location, ReinforcementGoal.Location }, 0)
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		timerstarted = true
	end)
	Trigger.AfterDelay(DateTime.Seconds(9), function()
		Media.PlaySoundNotification(player, "AlertBleep")
	end)
		Trigger.AfterDelay(DateTime.Seconds(10), function()
		Media.PlaySpeechNotification(player, "TimerStarted")
	end)
	]]


SendUSSRParadrops = function()
	paraproxy = Actor.Create("powerproxy.paratroopers", false, { Owner = player })
	paraproxy.SendParatroopersFrom(ParadropDst.Location, ParadropLZ.Location)
	paraproxy.Destroy()
end

--[[
SpyFinalSequency = function()
	if not SpyHideout4.IsDead then
		SpyHideout4.UnloadPassengers()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			if not TheSpy.IsDead then
				TheSpy.Move(SpyGoal.Location)
			end
		end)
	end
end

SpyHelicopterEscape = function()
	if not spyHelicopterEscape then
		spyHelicopterEscape = true
		SpyFinalSequency()
		Actor.Create("camera", true, { Owner = player, Location = CameraFinalArea.Location })
		ExtractionHeli = Reinforcements.ReinforceWithTransport(greece, ExtractionHeliType, nil, ExtractionPath)[1]
		local exitPos = CPos.New(ExtractionPath[1].X, ExtractionPath[2].Y)
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			if not TheSpy.IsDead and not ExtractionHeli.IsDead then
				TheSpy.EnterTransport(ExtractionHeli)
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(7), function()
			if not ExtractionHeli.IsDead then
				ExtractionHeli.Move(HelicopterEscape.Location)
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(12), function()
			enemy.MarkCompletedObjective(alliedObjective)
		end)
	end
end

Trigger.OnAllKilled(Farmers, function()
	Reinforcements.Reinforce(player, USSRReinforcementsFarm, { FarmSpawn.Location, FarmArea.Location }, 0)
	player.MarkCompletedObjective(sovietObjective2)
end)

Trigger.OnAllKilled(RedBuildings, function()
	player.MarkCompletedObjective(sovietObjective3)
end)

Trigger.OnAnyKilled(BarrierSoldiers, function()
	if barrier1Trigger then
		Utils.Do(BarrierSoldiers, function(actor)
			if not actor.IsDead then
				Trigger.OnIdle(actor, actor.Hunt)
			end
		end)
	end
end)

Trigger.OnEnteredFootprint(SpyHideout1Trigger, function(a, id)
	if not spyHideout1Trigger and a.Owner == player then
		spyHideout1Trigger = true
		Trigger.RemoveFootprintTrigger(id)
		Actor.Create("camera", true, { Owner = player, Location = SpyHideout1.Location })
		if not TheSpy.IsDead and not SpyHideout1.IsDead then
			TheSpy.EnterTransport(SpyHideout1)
		end
	end
end)

Trigger.OnEnteredFootprint(SpyHideout2PathTrigger, function(a, id)
	if not spyHideout2PathTrigger and a.Owner == player then
		spyHideout2PathTrigger = true
		Trigger.RemoveFootprintTrigger(id)
		Actor.Create("camera", true, { Owner = player, Location = CameraSpyVillage.Location })
		Actor.Create("camera", true, { Owner = player, Location = CameraVillage.Location })
		if not TheSpy.IsDead and not SpyHideout2.IsDead then
			TheSpy.EnterTransport(SpyHideout2)
		end
	end
end)

Trigger.OnEnteredFootprint(SpyHideout2Trigger, function(a, id)
	if not spyHideout2Trigger and a.Owner == player then
		spyHideout2Trigger = true
		SpyGuards1 = Reinforcements.Reinforce(greece, EnemyReinforcements1SpyHideout2, { EnemyReinforcements1.Location, EnemyReinforcements1Goal.Location }, 0)
		SpyGuards2 = Reinforcements.Reinforce(greece, EnemyReinforcements2SpyHideout2, { EnemyReinforcements2.Location, EnemyReinforcements2Goal.Location }, 0)
		Utils.Do(SpyGuards1, function(actor)
			if not actor.IsDead then
				Trigger.OnIdle(actor, actor.Hunt)
			end
		end)
		Utils.Do(SpyGuards2, function(actor)
			if not actor.IsDead then
				Trigger.OnIdle(actor, actor.Hunt)
			end
		end)
		if not SpyHideout2.IsDead and not Transport.IsDead then
			SpyHideout2.UnloadPassengers()
			Transport.Move(TransportPath1Water.Location)
		end
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			if not TheSpy.IsDead then
				TheSpy.Move(TransportPath1.Location)
				TheSpy.EnterTransport(Transport)
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(7), function()
			SendUSSRParadrops()
			Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		end)
	end
end)

Trigger.OnEnteredFootprint(SpyTransport1CheckpointTrigger, function(a, id)
	if not spyTransport1CheckpointTrigger and a.Owner == enemy then
		spyTransport1CheckpointTrigger = true
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Actor.Create("camera", true, { Owner = player, Location = CameraWater1.Location })
			Actor.Create("camera", true, { Owner = player, Location = CameraWater2.Location })
			Actor.Create("camera", true, { Owner = player, Location = CameraWater3.Location })
			Actor.Create("camera", true, { Owner = player, Location = CameraWater4.Location })
			Actor.Create("camera", true, { Owner = player, Location = CameraWater5.Location })
			Actor.Create("camera", true, { Owner = player, Location = CameraWater6.Location })
			Actor.Create("camera", true, { Owner = player, Location = TransportPath2.Location })
			if not Transport.IsDead then
				Transport.Wait(25)
				Transport.Move(TransportPath2Water.Location)
			end
		end)
	end
end)

Trigger.OnEnteredFootprint(SpyTransport2CheckpointTrigger, function(a, id)
	if not spyTransport2CheckpointTrigger and a.Owner == greece then
		spyTransport2CheckpointTrigger = true
		Transport.UnloadPassengers()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			if not TheSpy.IsDead then
				if not Hideout3Barrel.IsDead then
					TheSpy.EnterTransport(SpyHideout3)
				elseif not SpyHideout4.IsDead then
					TheSpy.EnterTransport(SpyHideout4)
				else
					TheSpy.Move(SpyGoal.Location)
				end
			end
		end)
	end
end)

Trigger.OnEnteredFootprint(Barrier1Trigger, function(a, id)
	if not barrier1Trigger and a.Owner == player then
		barrier1Trigger = true
		Actor.Create("camera", true, { Owner = player, Location = CameraBarrier.Location })
	end
end)

Trigger.OnEnteredFootprint(Barrier2Trigger, function(a, id)
	if not barrier2Trigger and a.Owner == player then
		barrier2Trigger = true
		Actor.Create("camera", true, { Owner = player, Location = CameraSpyHideout31.Location })
		Actor.Create("camera", true, { Owner = player, Location = CameraSpyHideout32.Location })
		Actor.Create("camera", true, { Owner = player, Location = CameraSpyHideout33.Location })
	end
end)

Trigger.OnEnteredFootprint(SpyHideout3Trigger, function(a, id)
	if not spyHideout3Trigger and a.Owner == player then
		spyHideout3Trigger = true
		if Map.LobbyOption("difficulty") ~= "hard" then
			Reinforcements.Reinforce(player, USSRReinforcements2, { ReinforcementSpawn.Location, CameraSpyHideout33.Location }, 0)
			Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		end
	end
end)

Trigger.OnEnteredFootprint(RTrapTrigger, function(a, id)
	if not rTrapTrigger and a.Owner == player then
		rTrapTrigger = true
		Actor.Create("camera", true, { Owner = player, Location = CameraFinalArea.Location })
		if not RSoldier3.IsDead and not RSoldierTrap.IsDead then
			RSoldier3.Attack(RSoldierTrap)
		end
		if not RSoldier4.IsDead and not RSoldierTrap.IsDead then
			RSoldier4.Attack(RSoldierTrap)
		end
	end
end)

Trigger.OnEnteredFootprint(SpyHideout4Trigger, function(a, id)
	if not spyHideout4Trigger and a.Owner == player then
		spyHideout4Trigger = true
		SpyFinalSequency()
		Actor.Create("camera", true, { Owner = player, Location = HelicopterGoal.Location })
	end
end)



Trigger.OnKilled(Hideout1Barrel, function()
		if not Hideout1PBox.IsDead then
			Hideout1PBox.Kill()
		end
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			if not SpyHideout1.IsDead then
				SpyHideout1.UnloadPassengers()
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			if not TheSpy.IsDead then
				TheSpy.Move(SpyWaypoint3.Location)
				TheSpy.Move(SpyWaypoint4.Location)
			end
		end)
		if not RSoldier1.IsDead then
			RSoldier1.Move(RStandoff.Location)
		end
		if not RSoldier2.IsDead then
			RSoldier2.Move(RStandoff.Location)
		end
end)
]]

--[[
Tick = function()
	Trigger.AfterDelay(DateTime.Seconds(12), function()
		if player.HasNoRequiredUnits() then
			enemy.MarkCompletedObjective(alliedObjective)
		end
	end)
	if not SpyHideout4.IsDead and SpyHideout4.HasPassengers then
		spyReachedHideout4 = true
	end
	

end
]]

Trigger.OnEnteredFootprint(GreeceReinforcements2Trigger, function(a, id)
	if a.Owner == player then
		if not GreeceReinforcementsArrived then
			GreeceReinforcementsArrived = true
			local units = Reinforcements.Reinforce( greece, GreeceReinforcements2Units, GreeceReinforcements2Path, 0)
			local medi = Reinforcements.Reinforce( greece, GreeceReinforcements2Medi, GreeceReinforcements2Path, 0)
		
			GreeceReinforce2 = units
			GreeceMedi2 = medi[1]
		end
		Trigger.RemoveFootprintTrigger(id)
		
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			if not GreeceMedi2.IsDead and not GreeceReinforce2[1].IsDead then
				GreeceMedi2.Guard(GreeceReinforce2[1])
			end
			Utils.Do(GreeceReinforce2, function(unit)
				IdleHunt(unit)
			end)
		end)
	end
end)

--
Trigger.OnEnteredFootprint(FirstCameraBatchTrigger, function(a, id)
	if a.Owner == enemy or a.Owner == greece then
		Actor.Create("camera", true, { Owner = player, Location = CamPath1.Location })
		--Actor.Create("camera", true, { Owner = player, Location = CamPath3.Location })
	end
end)
--


Trigger.OnEnteredFootprint(FirstCameraBatchTrigger, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		--Actor.Create("camera", true, { Owner = player, Location = CamPath2.Location })
		Actor.Create("camera", true, { Owner = player, Location = CamPath3.Location })
		Hideout1Escape()
	end
end)

--Bridge camera zone camera triggers
--[[
Trigger.OnEnteredFootprint({MoveSpy5.Location}, function(a, id)
	if a.Owner == greece and not CompanionUnit.IsDead then
		--CompanionUnit.UnloadPassengers()
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			local flare = Actor.Create("flare", true, { Owner = enemy, Location = MoveSpy5.Location - CVec.New(0, -2) })
			
			Trigger.AfterDelay(DateTime.Seconds(60), function()
				flare.Destroy()
			end)
		end)
	end
end)
]]
Trigger.OnEnteredFootprint(SecondCameraBatchTrigger, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		Actor.Create("camera", true, { Owner = player, Location = CamPath4.Location })
		Actor.Create("camera", true, { Owner = player, Location = CamPath5.Location })
		BlowBridge()
		Hideout2Escape()
	end
end)

Trigger.OnEnteredFootprint(ThridCameraBatchTrigger, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		Actor.Create("camera", true, { Owner = player, Location = CamPath6.Location })
		Actor.Create("camera", true, { Owner = player, Location = CamPath7.Location })
	end
end)

Trigger.OnEnteredFootprint(FourthCameraBatchTrigger, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		Actor.Create("camera", true, { Owner = player, Location = CamPath8.Location })
	end
end)

Trigger.OnEnteredFootprint(FifthCameraBatchTrigger, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		Actor.Create("camera", true, { Owner = player, Location = CamPath9.Location })
	end
end)

Trigger.OnEnteredFootprint(SpyFootPrintTriggers, function(a, id)
	Trigger.RemoveFootprintTrigger(id)
end)



--JEEP MOVEMENT SEQUENCE
--[[
Trigger.OnEnteredFootprint({MoveSpy4.Location}, function(a, id)
	if a == CompanionUnit then
		if not CompanionUnit.IsDead then
			CompanionUnit.Move(MoveSpy5.Location)
		end
		
	end
end)
]]


Tick = function()
	
	--[[TIME ALERTS]]
	if remainingTime == DateTime.Minutes(5) and Map.LobbyOption("difficulty") ~= "hard" then
		Media.PlaySpeechNotification(player, "WarningFiveMinutesRemaining")
	end	
	
	if remainingTime == DateTime.Minutes(4) and Map.LobbyOption("difficulty") ~= "hard" then
		Media.PlaySpeechNotification(player, "WarningFourMinutesRemaining")
	end
	
	if remainingTime == DateTime.Minutes(3) and Map.LobbyOption("difficulty") ~= "hard" then
		Media.PlaySpeechNotification(player, "WarningThreeMinutesRemaining")
	end
	
	if remainingTime == DateTime.Minutes(2) then
		Media.PlaySpeechNotification(player, "WarningTwoMinutesRemaining")
	end
	
	if remainingTime == DateTime.Minutes(1) then
		Media.PlaySpeechNotification(player, "WarningOneMinuteRemaining")
	end
	--[[TIME ALERTS]]
	
	
	if remainingTime > 0 and timerstarted then
		UserInterface.SetMissionText("Time Remaining: " .. Utils.FormatTime(remainingTime), player.Color)
		remainingTime = remainingTime - 1
	elseif remainingTime == 0 then
		UserInterface.SetMissionText("")
		enemy.MarkCompletedObjective(DenySoviets)
	end
	
	
	Trigger.AfterDelay(DateTime.Seconds(12), function()
		if player.HasNoRequiredUnits() then
			enemy.MarkCompletedObjective(DenySoviets)
		end
	end)
	
end

IdleHunt = function(actor)
	Trigger.OnIdle(actor, function(a)
		if a.IsInWorld then
			a.Hunt()
		end
	end)
end

IntiTriggers = function()

	Camera.Position = StartingView.CenterPosition
	
	--Referencing bridge
	Trigger.AfterDelay(0, function()
		local bridges = Utils.Where(Map.ActorsInWorld, function(actor) return actor.Type == "bridge1" or actor.Type == "bridge2" end)
		
		ExplodingBridge = bridges[1]
	end)
	
	--Creation of SpyType
	CompanionUnit = Actor.Create(CompanionType, true, {Owner = greece, Location = CompanionStartPos.Location}) 
	TheSpy = Actor.Create(SpyType, true, { Owner = enemy, Location = SpyStart.Location })
	CompanionUnit.Stance = "Defend"
	
	--Remove non-essential personel
	if Map.LobbyOption("difficulty") == "easy" then
		AttackedBySpy.Destroy()
	end
	
	Trigger.OnKilled(Checkpoint2Barl, function()
		if not Checkpoint2Medic.IsDead then
			Checkpoint2Medic.Kill()
		end
		if not CompanionUnit.IsDead and CompanionUnit.Location == MoveSpy6.Location then
			CompanionUnit.Kill()
		end
	end)
	
	Utils.Do(CheckpointUnits, function(actors)
		Trigger.OnDamaged(actors, function()
			Utils.Do(Hunters, function(unit)
				if not unit.IsDead then
					IdleHunt(unit) 
				end
			end)
		end)
	end)

	Trigger.OnKilled(AttackedByCompanion, function()
		if not BaseFtur.IsDead then
			BaseFtur.Kill()
		end
	end)

	Trigger.OnKilled(TheSpy, function()
		player.MarkCompletedObjective(KillSpy)
	end)
	
	Trigger.OnKilled(BarricadeBarl, function()
		Utils.Do(BarricadeActors, function(unit)
			if not unit.IsDead then
				unit.Kill()
			end
		end)
		
		player.MarkCompletedObjective(ClearBarricade)
		PlayerReinforcementsSouth()
	end)
	
end

UnitsArrived = false

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
	
	KillSpy = player.AddPrimaryObjective("Kill the enemy spy.")
	ClearBarricade = player.AddSecondaryObjective("Clear the south barricade for reinforcements.")
	KillAA = player.AddSecondaryObjective("Clear area of anti-air capabality to ensure reinforcements.")
	DenySoviets = enemy.AddPrimaryObjective("Deny the soviets.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("England")
	greece = Player.GetPlayer("Greece")
	
	IntiTriggers()
	InitObjectives()
	
	IntroSequence()
end