
EnemyEngineers = { ObjEngineer1, ObjEngineer2, ObjEngineer3 }
ProductionUnits = { "e1", "e1", "e2" }
ProductionBuildings = { BaseABarracks, BaseBBarracks, HiddenBarracks }

FirstUSSRBase = 
	{ 
		BaseA07, BaseA14, BaseAFlame, BaseA26, BaseABarracks, 
		BaseA28, BaseA29, BaseA44, BaseA45, BaseA47, BaseA48, 
		BaseA49, BaseA52, BaseA53, BaseA54, BaseA55, BaseA57, 
		BaseA58 
	}
	
FirstUSSRStructures =
	{
		BaseA26, BaseABarracks, BaseA28, BaseA29, BaseA07, BaseASilo
	}

SecondUSSRBase = 
	{ 
		USSRBaseB01, USSRBaseB02, USSRBaseB03, USSRBaseB04, USSRBaseB05, USSRBaseB06, USSRBaseB07, 
		USSRBaseB08, USSRBaseB09, BaseBBarracks, BaseBFlame, BaseBInf01, BaseBInf02, BaseBInf03, 
		BaseBInf04, BaseBInf05, BaseBInf06, BaseBInf07, BaseBGuard02 
	}
	
SecondUSSRStructures =
	{
		USSRBaseB07, USSRBaseB08, USSRBaseB09, USSRBaseB06,
		USSRBaseB05, BaseBFlame, BaseBBarracks, USSRBaseB04,
		USSRBaseB03, USSRBaseB02
	}

SecondUSSRBaseDogs =
    {
        BaseBGuard07, BaseBInf08
    }

Prisoners = { PrisonedEngi, PrisonedEngi2, PrisonedMedi }

CameraTriggerAreaBaseA = 
	{ 
	CPos.New(91,45), CPos.New(91,46), CPos.New(91,47), 
	CPos.New(91,48), CPos.New(91,49), CPos.New(86,52), 
	CPos.New(87,52), CPos.New(88,52), CPos.New(89,34), 
    CPos.New(89,35), CPos.New(89,36) 
	}
	
CameraTriggerAreaBaseB = 
	{
    CPos.New(41,74), CPos.New(42,74), CPos.New(43,74), 
    CPos.New(43,74), CPos.New(41,74), CPos.New(44,73),
    CPos.New(45,73), CPos.New(45,72), CPos.New(46,72),
    CPos.New(47,72), CPos.New(41,74), CPos.New(47,71),
    CPos.New(47,70),
    
    CPos.New(47,57), CPos.New(47,56), CPos.New(47,55),
    CPos.New(47,54), CPos.New(47,53), CPos.New(47,52),
    CPos.New(47,51), CPos.New(47,50), CPos.New(47,49),
    CPos.New(47,48)
	}
	
ReinforcementsTriggerArea1 = { CPos.New(58,42), CPos.New(59,42), CPos.New(60,42), CPos.New(59,41), CPos.New(58,41) }

ReinforcementsTriggerArea2 = 
	{ 
	CPos.New(32,41), CPos.New(33,41), CPos.New(34,41), CPos.New(31,42), CPos.New(32,42),
	CPos.New(33,42), CPos.New(34,42), CPos.New(35,42), CPos.New(35,41), CPos.New(35,40) 
	}
	
USSRTankReinforcementsA = { "3tnk", "3tnk" }
USSRTankReinforcementsB = { "3tnk", "3tnk", "3tnk" }
HiddenUSSRBase = { hiddenBase5, hiddenBase6 }
tankReinforcementsAPath = { tankReinforcementsA1.Location, tankReinforcementsA2.Location, tankReinforcementsA3.Location }
tankReinforcementsBPath = { tankReinforcementsB1.Location, tankReinforcementsB2.Location, tankReinforcementsB3.Location }

PatrolA = { PatrolA1 }
PatrolB = { PatrolB1, PatrolB2 }
PatrolC = { PatrolC1, PatrolC2, PatrolC3 }
PatrolD = { PatrolD1, PatrolD2 }
PatrolE = { PatrolE1, PatrolE2, PatrolE3, PatrolE4 }

PatrolAPath = { PatrolAWay1.Location, PatrolAWay2.Location }
PatrolBPath = { PatrolBWay1.Location, PatrolBWay2.Location }
PatrolCPath = { PatrolCWay1.Location, PatrolCWay2.Location }
PatrolDPath = { PatrolDWay1.Location, PatrolDWay2.Location }
PatrolEPath = { PatrolEWay1.Location, PatrolEWay2.Location, PatrolEWay3.Location, PatrolEWay2.Location }

if Map.LobbyOption("difficulty") == "easy" then
	TanyaType = "e7"
else
	TanyaType = "e7.noautotarget"
end

GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end

			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)

				if bool then
					stop = true

					i = i + 1
					if i > #waypoints then
						i = 1
					end

					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end

ActivatePatrols = function()
	GroupPatrol(PatrolA, PatrolAPath, DateTime.Seconds(6))
	GroupPatrol(PatrolB, PatrolBPath, DateTime.Seconds(6))
	GroupPatrol(PatrolC, PatrolCPath, DateTime.Seconds(6))
    GroupPatrol(PatrolD, PatrolDPath, DateTime.Seconds(6))
    GroupPatrol(PatrolE, PatrolEPath, DateTime.Seconds(3))
    
	local units = Map.ActorsInBox(Map.TopLeft, Map.BottomRight, function(self) return self.Owner == soviets and self.HasProperty("AutoTarget") end)
	Utils.Do(units, function(unit)
		unit.Stance = "Defend"
	end)
end

InitTriggers = function()
    
    Trigger.OnAnyKilled(Prisoners, function() player.MarkFailedObjective(FreePrisoners) end)

    Trigger.AfterDelay(0, function()
		local bridges = Utils.Where(Map.ActorsInWorld, function(actor) return actor.Type == "bridge1" or actor.Type == "bridge2" end)
		
		ExplodingBridgeA = bridges[2]
        ExplodingBridgeB = bridges[1]
        
		Trigger.OnAllKilled(bridges, function()
			player.MarkCompletedObjective(FreePrisoners)
			player.MarkCompletedObjective(TanyaSurvive)
			player.MarkCompletedObjective(DestroyBridges)
		end)
		
	end)
    
    Trigger.OnKilled(barrelBridgeA, function()
		if not ExplodingBridgeA.IsDead then ExplodingBridgeA.Kill() end
		Trigger.AfterDelay(DateTime.Seconds(1), SendUSSRTankReinforcementsA)
		reinforcementsTriggered1 = true
	end)

    Trigger.OnKilled(barrelBridgeB, function()
		if not ExplodingBridgeB.IsDead then ExplodingBridgeB.Kill() end
		Trigger.AfterDelay(DateTime.Seconds(1), SendUSSRTankReinforcementsB)
		reinforcementsTriggered2 = true
	end)

    Utils.Do(FirstUSSRBase, function(unit)
		Trigger.OnDamaged(unit, function()          
			if not FirstBaseAlert then
				FirstBaseAlert = true
				if not baseACam then
					baseACam = Actor.Create("camera", true, { Owner = player, Location = BaseACam.Location })
                end
				Utils.Do(FirstUSSRBase, function(unit)
					if unit.HasProperty("Move") then
						IdleHunt(unit)
					end
				end)
				for i = 0, 2 do
					Trigger.AfterDelay(DateTime.Seconds(i), function()
						Media.PlaySoundNotification(player, "AlertBuzzer")
					end)
				end
				ProduceUnits(ProductionBuildings[1], Utils.RandomInteger(3, 5))
			end
		end)
	end)
    
    Trigger.OnAllRemovedFromWorld(FirstUSSRStructures, function() 
		if baseACam then baseACam.Destroy() end
    end)
	
	Trigger.OnAllRemovedFromWorld(SecondUSSRStructures, function() 
		
        if baseBCam1 then baseBCam1.Destroy() 
            --Trigger.AfterDelay(DateTime.Seconds(1), function() Media.PlaySoundNotification(player, "AlertBuzzer") end)
        end
		if baseBCam2 then baseBCam2.Destroy() 
            --Trigger.AfterDelay(DateTime.Seconds(3), function() Media.PlaySoundNotification(player, "AlertBuzzer") end)
        end
        if baseBCam3 then baseBCam3.Destroy() 
            --Trigger.AfterDelay(DateTime.Seconds(5), function() Media.PlaySoundNotification(player, "AlertBuzzer") end)
        end
    end)
    
    Utils.Do(SecondUSSRBase, function(unit)
		Trigger.OnDamaged(unit, function()
			if not SecondBaseAlert then
				SecondBaseAlert = true
				if not baseBCam1 or not baseBCam2 or not baseBCam3 then 
					baseBCam1 = Actor.Create("camera", true, { Owner = player, Location = BaseBCam1.Location })
                    baseBCam2 = Actor.Create("camera", true, { Owner = player, Location = BaseBCam2.Location })
                    baseBCam3 = Actor.Create("camera", true, { Owner = player, Location = BaseBCam3.Location })
				end
                Utils.Do(SecondUSSRBase, function(unit)
					if unit.HasProperty("Move") then
						IdleHunt(unit)
					end
				end)
                
                --[[ NO HARD MODE YET ]]
                if Map.LobbyOption("difficulty") == "hard" then
                    Utils.Do(SecondUSSRBaseDogs, function()
                        if unit.HasProperty("Move") then
                            IdleHunt(unit)
                        end
                    end)
                end
                    
				for i = 0, 2 do
					Trigger.AfterDelay(DateTime.Seconds(i), function()
						Media.PlaySoundNotification(player, "AlertBuzzer")
					end)
				end
				ProduceUnits(ProductionBuildings[2], Utils.RandomInteger(5, 7))
			end
		end)
	end)
    
    Trigger.OnEnteredFootprint(CameraTriggerAreaBaseA, function(a, id)
		if a.Owner == player and not baseCameraA then
			Trigger.RemoveFootprintTrigger(id)
			baseACam = Actor.Create("camera", true, { Owner = player, Location = BaseACam.Location })
		end
	end)
    
    Trigger.OnEnteredFootprint(CameraTriggerAreaBaseB, function(a, id)
		if a.Owner == player and not baseBCam1 and not baseBCam2 and not baseBCam3 then
			Trigger.RemoveFootprintTrigger(id)
			baseBCam1 = Actor.Create("camera", true, { Owner = player, Location = BaseBCam1.Location })
            baseBCam2 = Actor.Create("camera", true, { Owner = player, Location = BaseBCam2.Location })
            baseBCam3 = Actor.Create("camera", true, { Owner = player, Location = BaseBCam3.Location })
		end
	end)
	
	Trigger.OnAllKilled(EnemyEngineers, function()player.MarkCompletedObjective(KillEngineers)end)
    
    Trigger.OnEnteredFootprint(ReinforcementsTriggerArea1, function(a, id)
		if a.Owner == player and not reinforcementsTriggered1 then
			reinforcementsTriggered1 = true
			Trigger.RemoveFootprintTrigger(id)
			Trigger.AfterDelay(DateTime.Seconds(1), function() SendUSSRTankReinforcementsA() end)
		end
	end)
    
    Trigger.OnEnteredFootprint(ReinforcementsTriggerArea2, function(a, id)
		if a.Owner == player and not reinforcementsTriggered2 then
			reinforcementsTriggered2 = true
			Trigger.RemoveFootprintTrigger(id)
			Trigger.AfterDelay(DateTime.Seconds(1), function() SendUSSRTankReinforcementsB() end)
		end
	end)
    --[[
	]]
end


SendUSSRTankReinforcementsA = function()
	local camera = Actor.Create("camera", true, { Owner = player, Location = bridgeACam.Location })
    local ussrUnits = Reinforcements.Reinforce(ussr, USSRTankReinforcementsA, tankReinforcementsAPath)
    Trigger.OnAllRemovedFromWorld(ussrUnits, function()
        Trigger.AfterDelay(DateTime.Seconds(3), function()
            if not	camera.IsDead then
			    camera.Destroy()
            end
		end)
	end)
end

SendUSSRTankReinforcementsB = function()
	local camera = Actor.Create("camera", true, { Owner = player, Location = bridgeBCam.Location })
    local ussrUnits = Reinforcements.Reinforce(ussr, USSRTankReinforcementsB, tankReinforcementsBPath)
    Trigger.OnAllRemovedFromWorld(ussrUnits, function()
        Trigger.AfterDelay(DateTime.Seconds(3), function()
            if not	camera.IsDead then
			    camera.Destroy()
            end
		end)
	end)
end

BaseBFlameDead = function()
	Trigger.OnKilled(barrelfturB, function()
			if not BaseBFlame.IsDead then BaseBFlame.Kill() end
	end)
end

IdleHunt = function(actor)
	Trigger.OnIdle(actor, function(a)
		if a.IsInWorld then
			a.Hunt()
		end
	end)
end

ProduceUnits = function(factory, count)
	if ussr.IsProducing("e1") then
		Trigger.AfterDelay(DateTime.Seconds(5), function() ProduceUnits(factory, count) end)
		return
	end

	local units = { }
	for i = 0, count, 1 do
		local type = Utils.Random(ProductionUnits)
		units[i] = type
	end

	if not factory.IsDead then
		factory.IsPrimaryBuilding = true
		ussr.Build(units, function(soldiers)
			Utils.Do(soldiers, function(unit) IdleHunt(unit) end)
		end)
	end
end

SendAlliedUnits = function()
    Camera.Position = StartingCamera.CenterPosition
    
    Bazooka1 = Actor.Create("e3", true, { Owner = player, Location = StartingCamera.Location })
    Bazooka2 = Actor.Create("e3", true, { Owner = player, Location = StartingCamera.Location })
	Tanya = Actor.Create(TanyaType, true, { Owner = player, Location = StartingCamera.Location, Facing = 128 })
    
    Bazooka1.Stance = "Defend"
    Bazooka2.Stance = "Defend"

	if ChangeStance then
		Tanya.Stance = "HoldFire"
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Media.DisplayMessage("According to the rules of engagement I need your explicit orders to fire, Commander!", "Tanya")
		end)
	end

	Tanya.Move( CPos.New(98,76))
	Bazooka1.Move( CPos.New(96,75))
    Bazooka2.Move( CPos.New(100,77))
    InsertionLst.Wait(DateTime.Seconds(2))
	InsertionLst.Move(CPos.New(75,110))
	InsertionLst.Destroy()

	Trigger.OnKilled(Tanya, function() player.MarkFailedObjective(TanyaSurvive) end)
end

Trigger.OnCapture(BaseASilo, function() 
		player.Cash = player.Cash + Utils.RandomInteger(1200, 1300)
end)

Trigger.OnCapture(USSRRadar, function()
	largeCameraA = Actor.Create("camera.VLarge", true, { Owner = player, Location = LargeCam1.Location })
	largeCameraB = Actor.Create("camera.VLarge", true, { Owner = player, Location = LargeCam2.Location })
	largeCameraC = Actor.Create("camera.VLarge", true, { Owner = player, Location = LargeCam3.Location })
	largeCameraD = Actor.Create("camera.VLarge", true, { Owner = player, Location = LargeCam4.Location })

end)

Trigger.OnRemovedFromWorld(USSRRadar, function()
	if largeCameraA and largeCameraA.IsInWorld then largeCameraA.Destroy() end
	if largeCameraB and largeCameraB.IsInWorld then largeCameraB.Destroy() end
	if largeCameraC and largeCameraC.IsInWorld then largeCameraC.Destroy() end
	if largeCameraD and largeCameraD.IsInWorld then largeCameraD.Destroy() end
end)


Tick = function()
	
		
end


InitObjectives = function()
    Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DestroyBridges = player.AddPrimaryObjective("Destroy all bridges.")
	TanyaSurvive = player.AddPrimaryObjective("Tanya must survive.")
	FreePrisoners = player.AddSecondaryObjective("Free all Allied soldiers and keep them alive.")
    KillEngineers = player.AddSecondaryObjective("Kill all Soviet engineers in the area.")
	ussr.AddPrimaryObjective("Bridges must not be destroyed.")

	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionFailed")
		end)
	end)
	Trigger.OnPlayerWon(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionAccomplished")
		end)
	end)
end


WorldLoaded = function()
	
	player = Player.GetPlayer("Greece")
	ussr = Player.GetPlayer("USSR")
	
	ussr.Cash = 5000
	
	Trigger.AfterDelay(0, function() player.Resources = player.ResourceCapacity * 0.75 end)
    Trigger.AfterDelay(0, function() ussr.Resources = ussr.ResourceCapacity * 0.75 end)
	
	
	
	InitObjectives()
	InitTriggers()
	
	SendAlliedUnits()
    ActivatePatrols()
	
	BaseBFlameDead()
    
end