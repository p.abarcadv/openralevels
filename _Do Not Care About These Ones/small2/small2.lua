
ArriveToLoc = function()
	player1.MarkCompletedObjective(BreakWallP1)
	player2.MarkCompletedObjective(BreakWallP2)
end

Tick = function()
  CPos.Zero
end

InitTrigger = function()
  Camera.Position = StartingCameraLocP1
  Camera.Position = StartingCameraLocP2
end

InitObjectives = function()

	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	BreakWallP1 = player1.AddPrimaryObjective("Down every convoy that approaches your position.")
	BreakWallP2 = player2.AddSecondaryObjective("Avoid attacking inhabitants of the area.")
	
	SovObjective =  enemy.AddPrimaryObjective("Eliminate all allied presence in the area.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)

end

WorldLoaded = function()
	enemy = Player.Get("USSR")
	player1 = Player.Get("Greece")
	player2 = Player.Get("England")
	
	InitTriggers()
	InitObjectives()

end

