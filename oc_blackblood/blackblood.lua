if Map.LobbyOption("difficulty") == "easy" then
	OoMAtksWait = DateTime.Minutes(3)
	totalReinforcements = 12
elseif Map.LobbyOption("difficulty") == "normal" then
	OoMAtksWait = DateTime.Minutes(2.5)
	totalReinforcements = 9
else
	OoMAtksWait = DateTime.Minutes(2)
	totalReinforcements = 6
end

StartInterface = false

CashToAchieve = 100000
count = 0

StartingMovingUnits = {StartUnit2, StartUnit3, StartUnit4, StartMCV, StartUnit6, StartUnit7, StartUnit8, StartUnit9}

ReinforcementsPath =
	{
	{ReinforcePos1.Location, ReinforceDst1.Location}, 
	{ReinforcePos2.Location, ReinforceDst2.Location},
	{ReinforcePos3.Location, ReinforceDst3.Location}
	}

ReinforcePoint1 = { ReinforcePos1.Location, ReinforceDst1.Location }
ReinforcePoint2 = { ReinforcePos2.Location, ReinforceDst2.Location }
ReinforcePoint3 = { ReinforcePos3.Location, ReinforceDst3.Location }

CampPositions = 
	{
	OutVec1.Location, OutVec2.Location, OutVec3.Location, OutVec4.Location, OutVec5.Location, 
	OutVec6.Location, OutVec7.Location, OutVec8.Location, OutVec9.Location
	}

FlavorMove = function()
	Utils.Do(StartingMovingUnits, function(unit)
		unit.Move(unit.Location + CVec.New(0,-3))
	end)
	Reinforcements.Reinforce(player, {"e1", "e1", "e2"}, {ReinforcePos1.Location, ReinforcePos1.Location + CVec.New(0,-2)})
	Reinforcements.Reinforce(player, {"e1", "e1", "e2"}, {ReinforcePos3.Location, ReinforcePos3.Location + CVec.New(0,-2)})
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Reinforcements.Reinforce(player, {"v2rl"}, {ReinforcePos1.Location, ReinforcePos1.Location + CVec.New(1,-2)})
		Reinforcements.Reinforce(player, {"v2rl"}, {ReinforcePos3.Location, ReinforcePos3.Location + CVec.New(-1,-2)})
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Reinforcements.Reinforce(player, {"e6", "e6"}, {ReinforcePos1.Location, ReinforcePos1.Location + CVec.New(0,-1)})
		Reinforcements.Reinforce(player, {"e6", "e6"}, {ReinforcePos3.Location, ReinforcePos3.Location + CVec.New(0,-1)})
	end)
end

ActivateAI = function()
	BuildBase()
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		ProduceTanks()
		ProduceInfantry()
		ProduceHeli()
	end)

	Trigger.AfterDelay(DateTime.Seconds(5), function()
		TranEntry()
	end)

	Trigger.AfterDelay(DateTime.Minutes(1), function()
		HeliAttack()
	end)
end

ReinforcingTeams =
	{
	{ "ttnk", "e1", "e1" },
	{ "3tnk", "e2", "e2", },
	{ "v2rl","e4", "e4"},
	{ "apc.sovloaded", "e1", "e1" },
	{ "ftrk", "e1", "e1",}
	}

ConstantReinforcements = function()
	local units = Utils.Random(ReinforcingTeams, function() end)
	local path = Utils.Random(ReinforcementsPath, function() end)
	count = count + 1
	
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	Reinforcements.Reinforce(player, units, path)
	
	if count <= totalReinforcements then
		Trigger.AfterDelay(DateTime.Seconds(90),  function()
			ConstantReinforcements() 
		end)
	end
end


CheckCredits = function()
	if player.Cash >= CashToAchieve then
		ProtectArea = player.AddPrimaryObjective("Defend the area of allied counter-offensive.")
		player.MarkCompletedObjective(AcquireMoney)
	else
		Trigger.AfterDelay(DateTime.Seconds(1), CheckCredits)
	end
end



IdleEngi = function(unit)
	if unit.IsIdle and not unit.IsDead then
		
	end
end

Cams = {CamMap1, CamMap2, CamMap3, CamMap4, CamMap5, CamMap6, CamMap7, CamMap8, CamMap9, CamMap10, CamMap11}

DebugCameras = function()
	Utils.Do(Cams, function(cam)
		if not cam.IsDead then
			cam.Owner = player
		end
	end)
end

countedOils = 0
countedOilsMax = -1

CheckOils = function()
	
	TimerColor = HSLColor.Red
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		TimerColor = HSLColor.White
	end)
end

CountOils = function()
	local actors = 
		Map.ActorsInBox(TopFirst.CenterPosition, BotLast.CenterPosition, function(actor)		
		return actor.Type == "oilb" end)
		
		if countedOilsMax <= 0 then
			countedOilsMax = #actors
		end
		countedOils = #actors
		
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			CountOils()
		end)
end

Tick = function()
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(AlliedObj)
	end
	
	if EAtek.IsDead or EAtek.Owner ~= enemy then
		if DisabledAtek == false then
			DisabledAtek = true
			player.MarkCompletedObjective(DisruptComms)
			OoMAtkAmount = OoMAtkAmount - 2 --probably works infinetely
		end
	end
	
	if EHpad1.IsDead or EHpad1.Owner ~= enemy then
		if EHpad2.IsDead or EHpad2.Owner ~= enemy then
			if AirInfReinforce == true then
				AirInfReinforce = false
				player.MarkCompletedObjective(DisableHpads)
			end
		end
	end
	
	if StartInterface == true then
		UserInterface.SetMissionText("Total Oil Derricks in area: " .. countedOils .. "/" .. countedOilsMax, TimerColor)
	end
end

InitTriggers = function()
	Camera.Position = StartingPos.CenterPosition
	player.Cash = 10000
	
	Trigger.AfterDelay(OoMAtksWait, function()
		StartOutOfMapAtks()
	end)
	
	CheckCredits()
	FlavorMove()
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		ActivateAI()
	end)
	
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		StartInterface = true
		CountOils()
	end)
	
	Trigger.AfterDelay(DateTime.Minutes(1), function()
		ConstantReinforcements()
	end)
	
	if player.Cash >= CashToAchieve then
		player.MarkCompletedObjective(AcquireMoney)
	end
	
	TimerColor = HSLColor.White
	
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	AcquireMoney = player.AddPrimaryObjective("Accumulate 100.000 Credits.")
	DisruptComms = player.AddSecondaryObjective("Destroy or capture the Allied Tech Center to \ndisrupt enemy reinforcements.")
	DisableHpads = player.AddSecondaryObjective("Destroy the helipads to prevent enemy \nreinforcements via air.")
	AlliedObj =  enemy.AddPrimaryObjective("Deny the soviets.")
	

	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	neutral = Player.GetPlayer("Neutral")

	InitObjectives()
	InitTriggers()
	
	--DebugCameras()
end