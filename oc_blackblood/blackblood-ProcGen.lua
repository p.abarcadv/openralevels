
--NEEDS MASSIVE REFACTORING

--Procedural block start
--Only uneven numbers over 1

if Map.LobbyOption("difficulty") == "easy" then
	PossiblePower = {"simple", "light", "heavy", "none"}								--25% Each
	PossibleComp = {"AntiTank", "AntiInf", "Rounded"}
	PossibleDimensions = { 5, 9}														--50% 5, 50% 9
elseif Map.LobbyOption("difficulty") == "normal" then
	PossiblePower = {"simple", "light", "light", "heavy", "heavy", "none"}				--25% Each
	PossibleComp = {"AntiTank", "AntiInf", "Rounded"}
	PossibleDimensions = { 5, 5, 5, 9, 9, 9, 9, 9, 9, 9}								--30% 5, 70% 9
else
	PossiblePower = {"simple", "light", "light", "heavy", "heavy", "heavy"}				--25% Each
	PossibleComp = {"AntiTank", "AntiInf", "Rounded"}
	PossibleDimensions = { 5, 9, 9, 9, 9}												--20% 5, 80% 9
end

--[[
PossiblePower = {"simple", "light", "heavy", "none"}
PossibleComp = {"AntiTank", "AntiInf", "Rounded"}
PossibleDimensions = { 5, 9, 9, 9, 9}
]]

CampLocations =
{
OutVec1.Location, OutVec2.Location, OutVec3.Location, OutVec4.Location, OutVec5.Location, OutVec6.Location,
OutVec7.Location, OutVec8.Location, OutVec9.Location, OutVec10.Location, OutVec11.Location, OutVec12.Location,
OutVec13.Location
}

Trigger.AfterDelay(DateTime.Seconds(0), function()
	ProceduralRandomizer()
end)

ProceduralRandomizer = function()
	for i = 1, #CampLocations, 1 do
		local value_x = Utils.Random(PossibleDimensions)
		local value_y = Utils.Random(PossibleDimensions)
		
		ProceduralInit(CampLocations[i], value_x, value_y)
	end
end

ProceduralInit = function(pos, w, h)
	local area = (w * h) - 1
	local start_x = ( (w - 1) / 2) * -1
	local start_y = ( (h - 1 ) / 2)* -1
	local origin = CVec.New(start_x, start_y)
	
	CampCreation(pos, area, origin, w)
end

--TYPE OF CAMPS
--(Size)
--5x5
--5x9
--9x5
--9x9
----(Pattern)
----Romboid
----Squared
------(Power)
------Heavy
------Light
------Simple
------None
--------(Type)
--------Anti-Inf
--------Anti-Tank
--------Rounded
--------Random*

CampCreation = function(pos, area, origin, limit)
	local x_pos = 0
	local y_pos = 0
	
	local campPower = Utils.Random(PossiblePower)
	local campComp = Utils.Random(PossibleComp)
	local oilPattern = Utils.Random({"Squared", "Romboid"})
	local wall = Utils.Random({"sbag"})
	
	for i = 0, area, 1 do
		local start = origin
		local iterator = CVec.New(x_pos, y_pos)
		local misc = Utils.Random({"barl", "brl3"})
		
		
		local wall_pos = {}
		
		local defence = Utils.Random({"pbox.nopower", "gun.nopower"})
		local defence_pos = {}
		
		local defence_AA = "agun.nopower"
		local defence_AA_pos = {}
		
		local unit = Utils.Random({"e1", "e3", "jeep", "arty", "1tnk", "2tnk", "e1.autotarget", "e3.autotarget", "jeep", "arty.autotarget", "1tnk.autotarget", "2tnk.autotarget"})
		local infunit = Utils.Random({"e1", "e3", "e1.autotarget", "e3.autotarget"})
		local unit_pos = {}
		local facing = Utils.Random({North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest})
		
		--Walls
		if campPower == "light" or campPower == "heavy" then
			if
			(x_pos == 0 and y_pos == 0) or (x_pos == 1 and y_pos == 0) or
			(x_pos == 2 and y_pos == 0) or (x_pos == 6 and y_pos == 0) or
			(x_pos == 7 and y_pos == 0) or (x_pos == 8 and y_pos == 0) or
			(x_pos == 0 and y_pos == 1) or (x_pos == 8 and y_pos == 1) or
			(x_pos == 0 and y_pos == 2) or (x_pos == 8 and y_pos == 2) or
			(x_pos == 0 and y_pos == 6) or (x_pos == 8 and y_pos == 6) or
			(x_pos == 0 and y_pos == 7) or (x_pos == 8 and y_pos == 7) or
			(x_pos == 0 and y_pos == 8) or (x_pos == 1 and y_pos == 8) or
			(x_pos == 2 and y_pos == 8) or (x_pos == 6 and y_pos == 8) or
			(x_pos == 7 and y_pos == 8) or (x_pos == 8 and y_pos == 8)
			then
				Actor.Create(wall, true, { Owner = enemy, Location = (pos + start + iterator)  })
			end
		end
		
		--Defences
		if campPower == "heavy" then
			if 
			(x_pos == 3 and y_pos == 0) or
			(x_pos == 8 and y_pos == 3) or
			(x_pos == 0 and y_pos == 5) or
			(x_pos == 5 and y_pos == 8)
			then
				if campComp == "AntiTank" then
					Actor.Create("gun", true, { Owner = enemy, Location = (pos + start + iterator)  })
				elseif campComp == "AntiInf" then
					Actor.Create("pbox", true, { Owner = enemy, Location = (pos + start + iterator)  })
				else
					Actor.Create(defence, true, { Owner = enemy, Location = (pos + start + iterator)  })
				end
			end
		end
		
		--Gap
		if campPower == "heavy" then
			if area >= 80 then
				if 
				(x_pos == 4 and y_pos == 4)
				then
					Actor.Create("gap.nopower", true, { Owner = enemy, Location = (pos + start + iterator)  })
				end
			end
		end
		
		--Units
		if campPower == "heavy" then
			if 
			(x_pos == 3 and y_pos == 1) or (x_pos == 5 and y_pos == 3) or
			(x_pos == 3 and y_pos == 4) or (x_pos == 5 and y_pos == 4) or
			(x_pos == 1 and y_pos == 5) or (x_pos == 4 and y_pos == 5)
			then
				Actor.Create(unit, true, { Owner = enemy, Location = (pos + start + iterator), Angle = facing })
			end
		elseif campPower == "light" then
			if
			(x_pos == 3 and y_pos == 1) or (x_pos == 3 and y_pos == 4) or 
			(x_pos == 5 and y_pos == 4) or (x_pos == 1 and y_pos == 5) or 
			(x_pos == 2 and y_pos == 5) or (x_pos == 4 and y_pos == 5)
			then
				Actor.Create(unit, true, { Owner = enemy, Location = (pos + start + iterator), Angle = facing })
			end
		else --only inf
			if campPower == "simple" then
				if oilPattern == "Squared" then
					if
					(x_pos == 4 and y_pos == 1) or (x_pos == 1 and y_pos == 3) or 
					(x_pos == 4 and y_pos == 1) or (x_pos == 7 and y_pos == 3) or 
					(x_pos == 4 and y_pos == 6) or (x_pos == 4 and y_pos == 7) or
					(x_pos == 5 and y_pos == 6) or (x_pos == 4 and y_pos == 7)
					then
						Actor.Create(infunit, true, { Owner = enemy, Location = (pos + start + iterator), Angle = facing })
					end
				else
					if
					(x_pos == 3 and y_pos == 1) or (x_pos == 1 and y_pos == 2) or 
					(x_pos == 6 and y_pos == 1) or (x_pos == 7 and y_pos == 2) or 
					(x_pos == 1 and y_pos == 6) or (x_pos == 2 and y_pos == 7) or
					(x_pos == 6 and y_pos == 6) or (x_pos == 6 and y_pos == 7)
					then
						Actor.Create(infunit, true, { Owner = enemy, Location = (pos + start + iterator), Angle = facing })
					end
				end
			end
		end
		
		--AA Defences
		if campPower == "light" or campPower == "heavy" then
			if oilPattern == "Romboid" then
				if 
				(x_pos == 1 and y_pos == 1) or (x_pos == 7 and y_pos == 7)
				then
					Actor.Create(defence_AA, true, { Owner = enemy, Location = (pos + start + iterator)  })
				end
			else
				if 
				(x_pos == 2 and y_pos == 4) or (x_pos == 6 and y_pos == 4)
				then
					Actor.Create(defence_AA, true, { Owner = enemy, Location = (pos + start + iterator)  })
				end
			end
		end
		
		--Oils
		if oilPattern == "Squared" then
			if 
			(x_pos == 1 and y_pos == 1) or (x_pos == 6 and y_pos == 1) or
			(x_pos == 1 and y_pos == 6) or (x_pos == 6 and y_pos == 6)
			then
				local oil = Actor.Create("oilb", true, { Owner = neutral, Location = (pos + start + iterator), Angle = facing })
			end
		else
			if 
			(x_pos == 4 and y_pos == 1) or (x_pos == 1 and y_pos == 3) or
			(x_pos == 6 and y_pos == 4) or (x_pos == 3 and y_pos == 6)
			then
				local oil = Actor.Create("oilb", true, { Owner = neutral, Location = (pos + start + iterator) })
			end
		end
		
		--Misc
		if oilPattern == "Squared" then
			if 
				(x_pos == 3 and y_pos == 2) or (x_pos == 5 and y_pos == 1) or
				(x_pos == 3 and y_pos == 6) or (x_pos == 5 and y_pos == 5)
			then
				local barrel = Actor.Create(misc, true, { Owner = enemy, Location = (pos + start + iterator) })
			end
		else
			if 
				(x_pos == 2 and y_pos == 1) or (x_pos == 7 and y_pos == 1) or
				(x_pos == 2 and y_pos == 6) or (x_pos == 5 and y_pos == 6)
			then
				local barrel = Actor.Create(misc, true, { Owner = enemy, Location = (pos + start + iterator) })
			end
		end
		
		ProcGenOils = {}
		table.insert(ProcGenOils, oil)
		
		x_pos = x_pos + 1
		if x_pos == limit then
			x_pos = 0
			y_pos = y_pos + 1
		end
	end
end

--Procedural block end
--Values for generation(?)
MedTank = 8
LightTank = 6
Apc = 5
Jeep = 3
Arty = 9
RifleInf = 3
RocketInf = 1
Gun = 9
Pbox = 6
Hbox = 7
Aagun = 9

--Wall Pos Check
CheckLocWall = function(x, y)
	if x == 0 then
		return true
	end
end

--Oil Pos Check
CheckLocDef = function(x, y)
	if x == 0 then
		return true
	end
end

CheckLocTank = function(x, y)
	if x == 0 then
		return true
	end
end

CheckLocInf = function(x, y)
	if x == 0 then
		return true
	end
end

CheckLocAA = function(x, y)
	if x == 0 then
		return true
	end
end

CheckLocOil = function(x, y)
	if x == 0 then
		return true
	end
end
