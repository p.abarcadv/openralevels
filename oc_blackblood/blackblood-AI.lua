if Map.LobbyOption("difficulty") == "easy" then
	OoMAtkAmount = 5
	RepeatDelay = 3.5
elseif Map.LobbyOption("difficulty") == "normal" then
	OoMAtkAmount = 8
	RepeatDelay = 3
else
	OoMAtkAmount = 11
	RepeatDelay = 2.25
end

OoMAtk = {}
OoMAtkTypes = {"2tnk", "1tnk", "jeep", "arty"}

VehiclesBaseAttack = {}
InfantryBaseAttack = {}

InfantryBaseTypes = {"e1", "e1", "e3"}
VehiclesBaseTypes = {"jeep", "1tnk", "1tnk", "2tnk", "2tnk", "arty"}
HeliBaseTypes = "heli.autotarget"

AirInfReinforce = true

AttackHelis = {"heli.outofmap", "heli.outofmap", "heli.outofmap"}
AttackHelisEntry = {HeliEntry1.Location, HeliEntry2.Location, HeliEntry3.Location, HeliEntry4.Location, HeliEntry5.Location}

AttackPaths = 
{
	{EnemyAtkPathA1, EnemyAtkPathA2, EnemyAtkPathA3},
	{EnemyAtkPathB1, EnemyAtkPathB2},
	{EnemyAtkPathC1, EnemyAtkPathC2}
}



InfantryWaves =
{
"e1", "e3", "e1", "e3", "e1", "e3", "e1", "e3"
}

originPoint = {1, 2, 3, 4}

APCPath = { EngiApcPath.Location, EngiApcPath.Location }

OoMAtkRoutesA =
{
{ OutofMapAtkA1.Location,  DstOfAtkA1.Location},
{ OutofMapAtkA2.Location,  DstOfAtkA2.Location},
{ OutofMapAtkA3.Location,  DstOfAtkA3.Location}
}
OoMAtkRoutesB =
{
{ OutofMapAtkB1.Location,  DstOfAtkB1.Location},
{ OutofMapAtkB2.Location,  DstOfAtkB2.Location},
{ OutofMapAtkB3.Location,  DstOfAtkB3.Location}
}
OoMAtkRoutesC =
{
{ OutofMapAtkC1.Location,  DstOfAtkC1.Location},
{ OutofMapAtkC2.Location,  DstOfAtkC2.Location},
{ OutofMapAtkC3.Location,  DstOfAtkC3.Location}
}
OoMAtkRoutesD =
{
{ OutofMapAtkD1.Location,  DstOfAtkD1.Location},
{ OutofMapAtkD2.Location,  DstOfAtkD2.Location},
{ OutofMapAtkD3.Location,  DstOfAtkD3.Location}
}

BaseAttackableArea =
	{
	CVec.New(0,1), CVec.New(0,2), CVec.New(0,3), CVec.New(0,-1), CVec.New(0,-2), CVec.New(0,-3),
	CVec.New(1,0), CVec.New(2,0), CVec.New(3,0), CVec.New(-1,0), CVec.New(-2,0), CVec.New(-3,0),
	CVec.New(1,1), CVec.New(2,2), CVec.New(3,3), CVec.New(-1,-1), CVec.New(-2,-2), CVec.New(-3,-3),
	CVec.New(-1,1), CVec.New(-2,2), CVec.New(-3,3), CVec.New(1,-1), CVec.New(2,-2), CVec.New(3,-3)
	}

StartOutOfMapAtks = function()
	local amount = OoMAtkAmount
	local delay = RepeatDelay
	local origin = Utils.Random(originPoint, function() end)
	OutOfMapAtk(amount, delay, origin)
end

--Refactor
OutOfMapAtk = function(amount, delay, origin)
	for i = 1, amount, 1 do
		local unit = Utils.Random(OoMAtkTypes, function() end)
		if origin == 1 then
			entryPoint = Utils.Random(OoMAtkRoutesA, function() end)
		elseif origin == 2 then
			entryPoint = Utils.Random(OoMAtkRoutesB, function() end)
		elseif origin == 3 then
			entryPoint = Utils.Random(OoMAtkRoutesC, function() end)
		else
			entryPoint = Utils.Random(OoMAtkRoutesD, function() end)
		end
		local atkforce = Reinforcements.Reinforce(enemy, {unit}, entryPoint)[1]
		table.insert(OoMAtk, atkforce)
	end
	Trigger.AfterDelay(DateTime.Seconds(20), function()
		SendAttack(OoMAtk)
		OoMAtk = {}
	end)
	Trigger.AfterDelay(DateTime.Minutes(RepeatDelay), function()
		StartOutOfMapAtks()
	end)
end

SendAttack = function(units)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			IdleHunt(unit)
		end
	end)
end

Trigger.AfterDelay(DateTime.Seconds(2), function() SendApc() end)

SendApc = function(path)
	local units = Reinforcements.ReinforceWithTransport(enemy, "apc", {"e6", "e6", "e6", "e6", "e3"}, {EngiApcPath.Location, OutVec1.Location})[2]
	
	--Utils.Do(units, Capture(OilInit1))

end

DeployWhenHurt = function(unit)
	Trigger.OnDamaged(unit, function()
		if unit.Type == "apc.loaded" and unit.PassengerCount ~= 0 and unit.Owner == enemy then
			unit.Stop()
			unit.UnloadPassengers()
		end
	end)
end

IdleHunt = function(unit) if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end end

--Refactor this
TranEntry = function()
	local group1 = Reinforcements.ReinforceWithTransport(enemy, "tran.insertion", {"e1", "e1", "e1", "e3", "e3"}, {HeliEntry1.Location, TranArrival1.Location}, {HeliEntry1.Location})[2]
			Utils.Do(group1, IdleHunt)
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		local group2 = Reinforcements.ReinforceWithTransport(enemy, "tran.insertion", {"e1", "e1", "e1", "e3", "e3"}, {HeliEntry1.Location, TranArrival2.Location}, {HeliEntry1.Location})[2]
		Utils.Do(group2, IdleHunt)
	end)
	
	Trigger.AfterDelay(DateTime.Minutes(2.5), function()
		if AirInfReinforce == true then
			TranEntry()
		end
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if EFact.IsDead or EFact.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EFactLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
				
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function()
	if not Tent.exists and Tent.Owner ~= enemy  then
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(InfantryBaseTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		
		InfantryBaseAttack[#InfantryBaseAttack + 1] = unit[1]
		if #InfantryBaseAttack >= 10 then
			SendUnits(InfantryBaseAttack, Path)
			InfantryBaseAttack = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceTanks = function()
	if not Weap.exists then
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(VehiclesBaseTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		VehiclesBaseAttack[#VehiclesBaseAttack + 1] = unit[1]

		if #VehiclesBaseAttack >= 6 then
			SendUnits(VehiclesBaseAttack, Path)
			VehiclesBaseAttack = { }
			Trigger.AfterDelay(DateTime.Minutes(1), ProduceTanks)
		else
			Trigger.AfterDelay(delay, ProduceTanks)
		end
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

ProduceHeli = function()
	if EHpad1.IsDead and EHpad2.IsDead then
		return
	end
	
	if Heli1.exists == false then
		Trigger.AfterDelay(DateTime.Seconds(20), function()
			Actor.Create(HeliBaseTypes, true, { Owner = enemy, Location = EHpad1.Location })
			Heli1.exists = true
		end)
	end
	
	if Heli2.exists == false then
		Trigger.AfterDelay(DateTime.Seconds(20), function()
			Actor.Create(HeliBaseTypes, true, { Owner = enemy, Location = EHpad2.Location })
			Heli2.exists = true
		end)
	end
	
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		ProduceHeli()
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

HeliAttack = function()
	local way = Utils.Random(AttackHelisEntry)
	local delay = Utils.RandomInteger(300,350)
	local attackersAir = Reinforcements.Reinforce(enemy, AttackHelis, {way, StartingPos.Location})
	local attackArea = Utils.Random(BaseAttackableArea)
	
	Utils.Do(attackersAir, function(a)
		a.AttackMove(StartingPos.Location + attackArea)
		IdleHeliHunt(a, way)
	end)

	Trigger.AfterDelay(DateTime.Seconds(delay), function()
		HeliAttack()
	end)
end

Trigger.OnEnteredProximityTrigger(HeliEntry1.CenterPosition, WDist.FromCells(5), function(a, id)
	if a.Type == "heli.outofmap" and a.AmmoCount() == 0 then
		a.Destroy()
	end
end)

Trigger.OnEnteredProximityTrigger(HeliEntry2.CenterPosition, WDist.FromCells(5), function(a, id)
	if a.Type == "heli.outofmap" and a.AmmoCount() == 0	 then
		a.Destroy()
	end
end)

--[[REMOVE EMPTIED HELIS (SW-CORNER, BASE)]]
Trigger.OnEnteredProximityTrigger(HeliEntry3.CenterPosition, WDist.FromCells(5), function(a, id)
	if a.Type == "heli.outofmap" and a.AmmoCount() == 0	 then
		a.Destroy()
	end
end)

--[[REMOVE EMPTIED HELIS (SW-CORNER, BASE)]]
Trigger.OnEnteredProximityTrigger(HeliEntry4.CenterPosition, WDist.FromCells(5), function(a, id)
	if a.Type == "heli.outofmap" and a.AmmoCount() == 0	 then
		a.Destroy()
	end
end)

--[[REMOVE EMPTIED HELIS (SW-CORNER, BASE)]]
Trigger.OnEnteredProximityTrigger(HeliEntry5.CenterPosition, WDist.FromCells(5), function(a, id)
	if a.Type == "heli.outofmap" and a.AmmoCount() == 0	 then
		a.Destroy()
	end
end)

IdleHeliHunt = function(unit, way) 
	if not unit.IsDead then
		Trigger.OnIdle(unit, function()
			if unit.AmmoCount() == 0 then
				unit.Move(way)
			else
				unit.Hunt() 
			end
		end)
	end
end
