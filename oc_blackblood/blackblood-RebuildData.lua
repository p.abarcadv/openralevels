
Gun1 = {type = "gun", pos = CVec.New(15,-3), cost = 800, exists = true}
Gun2 = {type = "gun", pos = CVec.New(15,1), cost = 800, exists = true}
Gun3 = {type = "gun", pos = CVec.New(-3,11), cost = 800, exists = true}

Pbox1 = {type = "pbox", pos = CVec.New(15,-4), cost = 650, exists = true}
Pbox2 = {type = "pbox", pos = CVec.New(15,2), cost = 650, exists = true}
Pbox3 = {type = "pbox", pos = CVec.New(5,12), cost = 650, exists = true}

Aagun1 = {type = "agun", pos = CVec.New(-4,8), cost = 900, exists = true}
Aagun2 = {type = "agun", pos = CVec.New(11,8), cost = 900, exists = true}
Aagun3 = {type = "agun", pos = CVec.New(1,-3), cost = 900, exists = true}
Aagun4 = {type = "agun", pos = CVec.New(11,-6), cost = 900, exists = true}

Weap = {type = "weap", pos = CVec.New(6,-5), cost = 2000, exists = true}
Tent = {type = "tent", pos = CVec.New(5,1), cost = 300, exists = true}

Power1 = {type = "apwr", pos = CVec.New(-3,-4), cost = 500, exists = true}
Power2 = {type = "apwr", pos = CVec.New(-6,-3), cost = 500, exists = true}
Power3 = {type = "powr", pos = CVec.New(-3,-1), cost = 300, exists = true}
Power4 = {type = "apwr", pos = CVec.New(-6,1), cost = 500, exists = true}
Power5 = {type = "apwr", pos = CVec.New(-6,4), cost = 500, exists = true}
Power6 = {type = "apwr", pos = CVec.New(-3,-15), cost = 500, exists = true}
Power7 = {type = "apwr", pos = CVec.New(-6,-17), cost = 500, exists = true}
Power8 = {type = "apwr", pos = CVec.New(-3,-18), cost = 500, exists = true}
Power9 = {type = "powr", pos = CVec.New(8,-19), cost = 300, exists = true}
Fix = {type = "fix", pos = CVec.New(2,-10), cost = 1000, exists = true}

Dome = {type = "dome", pos = CVec.New(6,-19), cost = 1400, exists = true}

Heli1 = {type = "heli", cost = 2000, exists = true}
Heli2 = {type = "heli", cost = 2000, exists = true}

--Ordered
BaseBuildings =
{
Power3,
Tent,
Power9,
Weap,
Dome,
Power1,
Aagun3,
Power2,
Gun1,
Gun2,
Pbox1,
Pbox2,
Pbox3,
Gun3,
Aagun1,
Fix,
Aagun2,
Aagun4,
Power4,
Power5,
Power6,
Power7,
Power8
}

BaseHelis = {Heli1, Heli2}

Trigger.OnKilled(EGun1, function()
	Gun1.exists = false
end)

Trigger.OnKilled(EGun2, function()
	Gun2.exists = false
end)

Trigger.OnKilled(EGun3, function()
	Gun3.exists = false
end)

Trigger.OnKilled(EPbox1, function()
	Pbox1.exists = false
end)

Trigger.OnKilled(EPbox2, function()
	Pbox2.exists = false
end)

Trigger.OnKilled(EPbox3, function()
	Pbox3.exists = false
end)

Trigger.OnKilled(EAagun1, function()
	Aagun1.exists = false
end)

Trigger.OnKilled(EAagun2, function()
	Aagun2.exists = false
end)

Trigger.OnKilled(EAagun3, function()
	Aagun3.exists = false
end)

Trigger.OnKilled(EAagun4, function()
	Aagun4.exists = false
end)

Trigger.OnKilled(EPower1, function()
	Power1.exists = false
end)

Trigger.OnKilled(EPower2, function()
	Power2.exists = false
end)

Trigger.OnKilled(EPower3, function()
	Power3.exists = false
end)

Trigger.OnKilled(EPower4, function()
	Power4.exists = false
end)

Trigger.OnKilled(EPower5, function()
	Power5.exists = false
end)

Trigger.OnKilled(EPower6, function()
	Power6.exists = false
end)

Trigger.OnKilled(EPower7, function()
	Power7.exists = false
end)

Trigger.OnKilled(EPower8, function()
	Power8.exists = false
end)

Trigger.OnKilled(EPower9, function()
	Power9.exists = false
end)

Trigger.OnKilled(EWeap, function()
	Weap.exists = false
end)

Trigger.OnKilled(ETent, function()
	Tent.exists = false
end)

Trigger.OnKilled(EDome, function()
	Dome.exists = false
end)

Trigger.OnKilled(EFix, function()
	Fix.exists = false
end)

Trigger.OnKilled(EHeli1, function()
	Heli1.exists = false
end)

Trigger.OnKilled(EHeli2, function()
	Heli2.exists = false
end)

