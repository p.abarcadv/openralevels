

if Map.LobbyOption("difficulty") == "easy" then
	remainingTime = DateTime.Seconds(630)
    SpyType = "spy"
    SpyTypeAfterObj = "spy.afterdome"
    AddedTime = DateTime.Minutes(15)
elseif Map.LobbyOption("difficulty") == "normal" then
	remainingTime = DateTime.Seconds(150)
    SpyType = "spy"
    SpyTypeAfterObj = "spy.afterdome"
    AddedTime = DateTime.Minutes(12)
else 
    remainingTime = DateTime.Seconds(90)
    SpyType = "spy.unarmed"
    SpyTypeAfterObj = "spy.afterdomeunarmed"
    AddedTime = DateTime.Minutes(10)
end

InfiltratedSquads = 
	{ 
	SquadM = { "medi", "e3.defendstance", "e3.defendstance", "e1.defendstance", "e1.defendstance" },
	SquadE1 = { "e1.defendstance", "e1.defendstance", "e1.defendstance", "e3.defendstance", "e3.defendstance" },
	SquadE3 = { "e3.defendstance", "e3.defendstance", "e3.defendstance", "e1.defendstance", "e1.defendstance" }
	}

removedCamAbyRadar = false
removedCamBbyRadar = false
removedCamCbyRadar = false
removedCamMbyRadar = false

removedCamAbyPower = false
removedCamBbyPower = false
removedCamCbyPower = false
removedCamMbyPower = false

cameraA_created = false
cameraB_created = false
cameraC_created = false
cameraM_created = false

destroyedPowerA = false
destroyedPowerB = false
destroyedPowerC = false
destroyedPowerM = false

timerStarted = true
addtime = false
AfterInfDome = false
TranPathA = { TranInsertion1.Location, CPos.New(12,32) }
TranPathB = { TranInsertion2.Location, Flare.Location }
TranPathC = { TranInsertion3.Location, MainBaseCam.Location }
Convoy = { "truk.supplies" }
PConvoy = { "truk.mission" }
Tran = "tran.insertion"
ObjSamsA = { ObjSamA1, ObjSamA2, ObjSamA3 }
ObjSamsB = { ObjSamB1, ObjSamB2, ObjSamB3 }
ObjSamsC = { ObjSamC1, ObjSamC2, ObjSamC3, ObjSamC4 }
ObjSams = { ObjSamA1, ObjSamA2, ObjSamA3, ObjSamB1, ObjSamB2, ObjSamB3, ObjSamC1, ObjSamC2, ObjSamC3, ObjSamC4 }

PatrolA = { PatrolA00, PatrolA01, PatrolA02, PatrolA03 }
PatrolB = { PatrolB00, PatrolB01, PatrolB02 }
PatrolC = { PatrolC00, PatrolC01, PatrolC02, PatrolC03, PatrolC04 }
PatrolAPath = { PatrolAWay3.Location, PatrolAWay1.Location, PatrolAWay2.Location, PatrolAWay1.Location }
PatrolBPath = { PatrolBWay1.Location, PatrolBWay2.Location }
PatrolCPath = { PatrolCWay1.Location, PatrolCWay2.Location }

InfiltrationPoint = { InfiltrationSpawn.Location, InfiltrationTarget.Location }

ConvoyPathA = { ConvoyStart.Location, ConvoyWay01.Location, ConvoyWay02.Location, ConvoyEndA.Location } 
ConvoyPathB = { ConvoyStart.Location, ConvoyWay01.Location, ConvoyWay02.Location, ConvoyEndB.Location }
ConvoyPathC = { ConvoyStart.Location, ConvoyWay01.Location, ConvoyWay02.Location, ConvoyEndC.Location }
AlliedConvoyPath = { ConvoyStart.Location, InfiltrationSpawn.Location  }

RadardomeVisionTrigger = { CPos.New(36,8), CPos.New(41,14), CPos.New(42,14), CPos.New(42,15), CPos.New(43,15), CPos.New(44,15), CPos.New(45,15), CPos.New(45,14), CPos.New(46,14) }

BaseATotalPower = { BaseAPower00, BaseAPower01, BaseAPower02, BaseAPower03 }
BaseBTotalPower = { BaseBPower00, BaseBPower01, BaseBPower02 }
BaseCTotalPower = { BaseCPower00, BaseCPower01, BaseCPower02, BaseCPower03 }
BaseMTotalPower = { MainBasePower02, MainBasePower03, MainBasePower04, MainBasePower04, MainBasePower05, MainBasePower06, MainBasePower07, MainBasePower08, MainBasePower09 }
            
PowerSupply = 
{ 
    BaseAPower00, BaseAPower01, BaseAPower02, BaseAPower03, BaseBPower00, BaseBPower01, 
    BaseBPower02, BaseCPower00, BaseCPower01, BaseCPower02, BaseCPower03, 
    MainBasePower02, MainBasePower03, MainBasePower04, MainBasePower04, MainBasePower05, 
    MainBasePower06, MainBasePower07, MainBasePower08, MainBasePower09 
}


SetupAlliedUnits = function()
	
    Spy = Actor.Create(SpyType, true, { Owner = player, Location = EnglandSpy.Location, Facing = 128 })
	Camera.Position = StartingCamera1.CenterPosition
	
	Trigger.AfterDelay(DateTime.Seconds(12), function()USSR00.AttackMove(SpyReturn.Location) end)
    
	StartingCam = Actor.Create("camera", true, { Owner = player, Location = StartingCamera1.Location })
    
    Trigger.OnKilled(Spy, function() 
        enemy.MarkCompletedObjective(StopAllies) 
    end) 

end

Trigger.OnAllKilled(PowerSupply, function()
	player.MarkCompletedObjective(destroyPowerplants)
	player.MarkCompletedObjective(spySurvive)
end)

Trigger.OnAllKilled(ObjSamsA, function()
	ExtraSquadInsertion(InfiltratedSquads["SquadE1"], TranPathA)
end)
    
Trigger.OnAllKilled(ObjSamsB, function()
	ExtraSquadInsertion(InfiltratedSquads["SquadM"], TranPathB)
end)

Trigger.OnAllKilled(ObjSamsC, function()
	ExtraSquadInsertion(InfiltratedSquads["SquadE3"], TranPathC)
end)

Trigger.OnAllKilled(ObjSams, function()
	player.MarkCompletedObjective(destroySams)
end)

ExtraSquadInsertion = function(units, path)
	local flare = Actor.Create("flare", true, { Owner = player, Location = path[2] })
	
	Trigger.AfterDelay(DateTime.Seconds(1), function() 
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		Reinforcements.ReinforceWithTransport(player, Tran, units, path, { path[1] })		
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(22), flare.Destroy) 
end

ActivatePatrols = function()
	GroupPatrol(PatrolA, PatrolAPath, DateTime.Seconds(3))
	GroupPatrol(PatrolB, PatrolBPath, DateTime.Seconds(2))
    GroupPatrol(PatrolC, PatrolCPath, DateTime.Seconds(5))
end

GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end

			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)

				if bool then
					stop = true

					i = i + 1
					if i > #waypoints then
						i = 1
					end

					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end

SendConvoy = function()
	trucks1 = Reinforcements.Reinforce(enemy, Convoy, ConvoyPathA, 20 )
    Trigger.AfterDelay(DateTime.Seconds(1), function() 
		trucks2 = Reinforcements.Reinforce(enemy, Convoy, ConvoyPathB, 20 ) 
	end)
    Trigger.AfterDelay(DateTime.Seconds(2), function() 
		trucks3 = Reinforcements.Reinforce(enemy, Convoy, ConvoyPathC, 20 ) 
	end)
    
    Trigger.AfterDelay(DateTime.Seconds(3), function() 
		spytruck = Reinforcements.Reinforce(ussrradar, PConvoy, AlliedConvoyPath, 20) 
	end)
 
    Trigger.AfterDelay(DateTime.Seconds(9), function()
        Reinforcements.Reinforce(player, InfiltratedSquads["SquadM"], InfiltrationPoint, 5)
		spytruck.Owner = neutral
    end)

end

AddRadar = function()
	alliedDome = Actor.Create("dome", true, { Owner = player, Location = OffmapRadar.Location })
	alliedPower = Actor.Create("powr", true, { Owner = player, Location = OffmapPower.Location })
end

AddTeslaCams = function()
	vision_tesla1 = Actor.Create("camera.tesla", true, { Owner = player, Location = TeslaCam00.Location })
	vision_tesla2 = Actor.Create("camera.tesla", true, { Owner = player, Location = TeslaCam01.Location })
	vision_tesla3 = Actor.Create("camera.tesla", true, { Owner = player, Location = TeslaCam02.Location })
	vision_tesla4 = Actor.Create("camera.tesla", true, { Owner = player, Location = TeslaCam03.Location })
	vision_tesla5 = Actor.Create("camera.tesla", true, { Owner = player, Location = TeslaCam04.Location })
end

CheckPowerPlants = function(buildings)
	local bool = Utils.Any(buildings, function(actor) return not actor.IsDead end)	
	return bool
end

Trigger.OnAllKilled(BaseATotalPower, function() 
	if Map.LobbyOption("difficulty") == "easy" then
		if BaseACamera then
			BaseACamera.Destroy()
		end
	end
end)
   
Trigger.OnAllKilled(BaseBTotalPower, function()
	destroyedPowerB = true
	if cameraB_created == true then
		if removedCamBbyRadar == false then 
			BaseBCamera.Destroy()
			removedCamBbyPower = true
		end
	end
end)

Trigger.OnAllKilled(BaseCTotalPower, function()
	destroyedPowerC = true
	if cameraC_created == true then
		if removedCamCbyRadar == false then 
			BaseCCamera.Destroy()
			removedCamCbyPower = true
		end
	end
end)

Trigger.OnAllKilled(BaseMTotalPower, function()
	destroyedPowerM = true
	if cameraM_created == true then
		if removedCamMbyRadar == false then
			MainBaseCamera.Destroy() 
			removedCamMbyPower = true
		end
	end
end)

Trigger.OnEnteredFootprint(RadardomeVisionTrigger, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id) 
		DomeCam = Actor.Create("camera.base", true, { Owner = player, Location = RadardomeVision.Location }) 
	end
end)

Trigger.OnInfiltrated(Radardome, function()
	Trigger.ClearAll(Spy)
	player.MarkCompletedObjective(infRadardome)

    AfterInfDome = true    
    AddTime()
    

	Trigger.AfterDelay(DateTime.Seconds(1), function()
		if Map.LobbyOption("difficulty") == "easy" then
			AddRadar()
			AddTeslaCams()
		end
        
		if Map.LobbyOption("difficulty") ~= hard and CheckPowerPlants(BaseATotalPower) then
			BaseACamera = Actor.Create("camera.base", true, { Owner = player, Location = BaseACam.Location })
		end
		
		if Map.LobbyOption("difficulty") ~= hard and CheckPowerPlants(BaseBTotalPower) then
			BaseBCamera = Actor.Create("camera.base", true, { Owner = player, Location = Flare.Location })
		end
		
		if Map.LobbyOption("difficulty") ~= hard and CheckPowerPlants(BaseCTotalPower) then
			BaseCCamera = Actor.Create("camera.base", true, { Owner = player, Location = BaseCCam.Location })
		end
		
		if Map.LobbyOption("difficulty") ~= hard and CheckPowerPlants(BaseMTotalPower) then
			MainBaseCamera = Actor.Create("camera.base", true, { Owner = player, Location = MainBaseCam.Location })
		end
		
		Spy = Actor.Create(SpyTypeAfterObj, true, { Owner = player, Location = Radardome.Location })
		Spy.DisguiseAsType("e1", enemy)
		Spy.Move(SpyReturn.Location)
		
		Trigger.OnKilled(Spy, function() 
			enemy.MarkCompletedObjective(StopAllies) 
		end) 
	end)     
end)

Trigger.OnKilled(Radardome, function()
	if DomeCam then 
		DomeCam.Destroy() 
	end
	
	if CheckPowerPlants(BaseATotalPower) then 
		if BaseACamera then
			BaseACamera.Destroy()
		end
	end
	
	if CheckPowerPlants(BaseBTotalPower) then 
		if BaseBCamera then
			BaseBCamera.Destroy()
		end
	end
	
	if CheckPowerPlants(BaseCTotalPower) then 
		if BaseCCamera then
			BaseCCamera.Destroy()
		end
	end
	
	if CheckPowerPlants(BaseMTotalPower) then 
		if BaseMCamera then
			BaseMCamera.Destroy()
		end
	end
	
	if AfterInfDome == false then 
		enemy.MarkCompletedObjective(StopAllies)
	else
		alliedPower.Destroy()
		alliedDome.Destroy()
		if Map.LobbyOption("difficulty") == "easy" then
			vision_tesla1.Destroy()
			vision_tesla2.Destroy()
			vision_tesla3.Destroy()
			vision_tesla4.Destroy()
			vision_tesla5.Destroy()
		end
	end
end)

AddTime = function()
    addtime = true
    remainingTime = remainingTime + AddedTime
    Trigger.AfterDelay(DateTime.Seconds(1), function() addtime = false end)
end

FinishTimer = function()
	for i = 0, 5, 1 do
		local c = TimerColor
		if i % 2 == 0 then
			c = HSLColor.White
		end
		Trigger.AfterDelay(DateTime.Seconds(i), function()
            enemy.MarkCompletedObjective(StopAllies) 
            UserInterface.SetMissionText("")
        end)
	end
end

Tick = function()
	
    if remainingTime == DateTime.Minutes(10) then
        Media.PlaySpeechNotification(player, "TenMinutesRemaining")
    elseif remainingTime == DateTime.Minutes(5) then
        Media.PlaySpeechNotification(player, "WarningFiveMinutesRemaining")
    elseif remainingTime == DateTime.Minutes(4) then
        Media.PlaySpeechNotification(player, "WarningFourMinutesRemaining")
    elseif remainingTime == DateTime.Minutes(3) then
        Media.PlaySpeechNotification(player, "WarningThreeMinutesRemaining")
    elseif remainingTime == DateTime.Minutes(2) then
        Media.PlaySpeechNotification(player, "WarningTwoMinutesRemaining")
    elseif remainingTime == DateTime.Minutes(1) then
        Media.PlaySpeechNotification(player, "WarningOneMinuteRemaining")
    end

    if remainingTime > 0 and timerStarted then
        UserInterface.SetMissionText("Soviet communications re-stablished in " .. Utils.FormatTime(remainingTime), player.Color)
        if not addtime then
            remainingTime = remainingTime - 1
        end
    elseif remainingTime == 0 then
        UserInterface.SetMissionText("")
        enemy.MarkCompletedObjective(StopAllies)
    end
        
    if initialized and player.HasNoRequiredUnits() then
        enemy.MarkCompletedObjective(StopAllies)
    end
end
    
InitTriggers = function()
    
    Actor.Create("camera.map", true, { Owner = enemy, Location = MapCamera1.Location })
    Actor.Create("camera.map", true, { Owner = enemy, Location = MapCamera2.Location })
end

InitObjectives = function()
	
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

    
	destroyPowerplants = player.AddPrimaryObjective("Destroy all power plants.")
	spySurvive = player.AddPrimaryObjective("Spy must survive.")
	
	infRadardome = player.AddSecondaryObjective("Infiltrate radar dome.")
	destroySams = player.AddSecondaryObjective("Clear nearby sam sites to ensure reinforcements.")
	
	StopAllies = enemy.AddPrimaryObjective("Stop Allied infiltration.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("ENGLAND")
	enemy = Player.GetPlayer("USSR")
	ussrradar = Player.GetPlayer("USSRRADAR")
    neutral = Player.GetPlayer("Neutral")
	
	InitObjectives()
	InitTriggers()
	
    SetupAlliedUnits()
	SendConvoy()
	
	ActivatePatrols()
    
	Trigger.AfterDelay(DateTime.Seconds(1), function() 
		Media.PlaySpeechNotification(player, "MissionTimerInitialised") 
	end)
	
    TimerColor = player.Color
    
end