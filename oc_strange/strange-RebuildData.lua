--Naval Data
Gunboat1	= { type = "pt"	,	cost = 500	,	path = {NavalWaypointA, NavalWaypointA1}, exists = false	}
Destroyer1	= { type = "dd"	,	cost = 1000	,	path = {NavalWaypointA, NavalWaypointA2}, exists = false	}

Gunboat2	= { type = "pt"	,	cost = 500	,	path = {NavalWaypointA, NavalWaypointB, NavalWaypointB1}, exists = false	}
Destroyer2	= { type = "dd"	,	cost = 1000	,	path = {NavalWaypointA, NavalWaypointB, NavalWaypointB2}, exists = false	}

Gunboat3	= { type = "pt"	,	cost = 500	,	path = {NavalWaypointA, NavalWaypointC, NavalWaypointC1}, exists = false	}
Destroyer3	= { type = "dd"	,	cost = 1000	, 	path = {NavalWaypointA, NavalWaypointC, NavalWaypointC2}, exists = false	}

Gunboat4	= { type = "pt"	,	cost = 500	,	path = {NavalWaypointA, NavalWaypointB, NavalWaypointD1}, exists = false	}
Destroyer4	= { type = "dd"	,	cost = 1000	, 	path = {NavalWaypointA, NavalWaypointB, NavalWaypointD2}, exists = false	}

Gunboat5	= { type = "pt"	,	cost = 500	,	path = {NavalWaypointA, NavalWaypointC, NavalWaypointD3}, exists = false	}
Destroyer5	= { type = "dd"	,	cost = 1000	, 	path = {NavalWaypointA, NavalWaypointC, NavalWaypointD4}, exists = false	}

VesselsToBuild =
{
	Gunboat1,
	Destroyer1,
	Gunboat2,
	Destroyer2,
	Gunboat3,
	Destroyer3,
	Gunboat4,
	Destroyer4,
	Gunboat5,
	Destroyer5
}

--Buildings Data
Power1	= { type = "powr", pos = CVec.New(1,3)	, cost = 300	, exists = true }
Power2	= { type = "apwr", pos = CVec.New(-2,4)	, cost = 500	, exists = true }
Power3	= { type = "apwr", pos = CVec.New(6,-1)	, cost = 500	, exists = true }

Tent	= { type = "tent", pos = CVec.New(10,3)	, cost = 500	, exists = true }
Weap	= { type = "weap", pos = CVec.New(9,-1)	, cost = 2000	, exists = true }
Dome	= { type = "dome", pos = CVec.New(7,3)	, cost = 1000	, exists = true }

Syrd	= { type = "syrd", pos = CVec.New(19,1)	, cost = 1000	, exists = true }

Gun1	= { type = "gun", pos = CVec.New(1,10)	, cost = 500	, exists = true }
Gun2	= { type = "gun", pos = CVec.New(3,10)	, cost = 500	, exists = true }
Gun3	= { type = "gun", pos = CVec.New(15,8)	, cost = 500	, exists = true }

Pbox1	= { type = "pbox", pos = CVec.New(2,10)	, cost = 500	, exists = true }
Pbox2	= { type = "pbox", pos = CVec.New(14,8)	, cost = 500	, exists = true }

Agun1	= { type = "agun", pos = CVec.New(2,0)	, cost = 500	, exists = true }
Agun2	= { type = "agun", pos = CVec.New(11,7)	, cost = 500	, exists = true }

BaseBuildings =
{
Power1,
Power2,
Power3,
Tent,
Weap,
Dome,
Syrd,
Gun1,
Gun2,
Gun3,
Pbox1,
Pbox2,
Agun1,
Agun2
}

Trigger.OnKilled(BasePower1, function()
	Power1.exists = false
end)

Trigger.OnKilled(BasePower2, function()
	Power2.exists = false
end)

Trigger.OnKilled(BasePower3, function()
	Power3.exists = false
end)

Trigger.OnKilled(BaseTent, function()
	Tent.exists = false
end)

Trigger.OnKilled(BaseWeap, function()
	Weap.exists = false
end)

Trigger.OnKilled(BaseDome, function()
	Dome.exists = false
end)

Trigger.OnKilled(BaseNaval, function()
	Syrd.exists = false
end)

Trigger.OnKilled(BaseGun1, function()
	Gun1.exists = false
end)

Trigger.OnKilled(BaseGun2, function()
	Gun2.exists = false
end)

Trigger.OnKilled(BaseGun3, function()
	Gun3.exists = false
end)

Trigger.OnKilled(BasePbox1, function()
	Pbox1.exists = false
end)

Trigger.OnKilled(BasePbox2, function()
	Pbox2.exists = false
end)

Trigger.OnKilled(BaseAgun1, function()
	Agun1.exists = false
end)

Trigger.OnKilled(BaseAgun2, function()
	Agun2.exists = false
end)
