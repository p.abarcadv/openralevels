
if Map.LobbyOption("difficulty") == "easy" then
	infAmount = 8
	TankProductionDelay = DateTime.Seconds(120)
elseif Map.LobbyOption("difficulty") == "normal" then
	infAmount = 8
	TankProductionDelay = DateTime.Seconds(90)
else
	infAmount = 12
	TankProductionDelay = DateTime.Seconds(60)
end

AttackPaths = 
	{
	{AtkWay1,AtkWay2,AtkWay3,AtkWay3A},
	{AtkWay1,AtkWay2,AtkWay3,AtkWay3B,AtkWay3B1,AtkWay3B2},
	}

AlliedInfantryUnits = {"e1", "e1", "e1", "e3","e3"}
AlliedTankUnits = { "1tnk", "1tnk", "jeep", "arty", "arty"}

TankAttack = {}
InfantryAttack = {}

tankAmount = 5
infAmount = 8


IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if EFact.IsDead or EFact.Owner ~= enemy then 
			return
		end
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EFactLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function()
	if not Tent.exists --[[or not Tent.Owner == enemy]] then
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(AlliedInfantryUnits) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		InfantryAttack[#InfantryAttack + 1] = unit[1]
		if #InfantryAttack >= infAmount then
			SendUnits(InfantryAttack, Path)
			InfantryAttack = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceTanks = function()
	if not Weap.exists or not Weap.Owner == enemy then
		Trigger.AfterDelay(DateTime.Minutes(1), function()
			Media.Debug("Test")
			ProduceTanks()
		end)
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(30), DateTime.Seconds(35))
	local toBuild = { Utils.Random(AlliedTankUnits) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		TankAttack[#TankAttack + 1] = unit[1]

		if #TankAttack >= tankAmount then
			SendUnits(TankAttack, Path)
			TankAttack = { }
			Trigger.AfterDelay(TankProductionDelay, ProduceTanks)
		else
			Trigger.AfterDelay(delay, ProduceTanks)
		end
	end)
end


BuildVessels = function()
	for c,n in ipairs(VesselsToBuild) do
		if not n.exists then
			ProduceNaval(n)
			return
		end
	end
end

ProduceNaval = function(n)
	if not Syrd.exists or not Syrd.Owner == enemy then
		return
	end

	enemy.Build({n.type}, function(units)
		n.exists = true
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			if not units[1].IsDead then
				SendStationaryUnits(units[1], n.path)
				
				Trigger.AfterDelay(DateTime.Seconds(1), BuildVessels)
			end
		end)
		
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

SendStationaryUnits = function(unit, waypoints)
	if not unit.IsDead then
		Utils.Do(waypoints, function(waypoint)
			unit.AttackMove(waypoint.Location)
		end)
	end
end