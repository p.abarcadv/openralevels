
if Map.LobbyOption("difficulty") == "easy" then
	ConvoyDelay = DateTime.Minutes(25)
	StartingCash = 7500
elseif Map.LobbyOption("difficulty") == "normal" then
	ConvoyDelay = DateTime.Minutes(20)
	StartingCash = 5000
else
	ConvoyDelay = DateTime.Minutes(15)
	StartingCash = 3000
end

tankRes = {"3tnk"}
infRes = {"e1", "e1", "e2"}

ConvoyArrived = false

HelpingCamerasCoords =
	{
	{CPos.New(69,43), CPos.New(69,44), CPos.New(69,45)},
	{CPos.New(46,50)},
	{
	CPos.New(37,51), CPos.New(37,50), CPos.New(36,50), CPos.New(36,49), CPos.New(35,49), CPos.New(35,48), CPos.New(34,48),
	CPos.New(33,48), CPos.New(32,48), CPos.New(31,48), CPos.New(30,48), CPos.New(29,48)
	},
	{
	CPos.New(34,39), CPos.New(35,39), CPos.New(35,38), CPos.New(36,38), CPos.New(37,38), CPos.New(37,37), CPos.New(38,37), CPos.New(39,37)
	},
	{CPos.New(51,30), CPos.New(51,31)}
	}

ConvoyPath = 
{ 
	--entry
	{ConvoyWay1.Location, ConvoyWay2.Location, ConvoyWay3.Location, ConvoyWay4.Location, 
	ConvoyWay5.Location, ConvoyWay6.Location, ConvoyWay7.Location, ConvoyWay8.Location},
	--exit
	{ConvoyWay7.Location, ConvoyWay6.Location, ConvoyWay5.Location, ConvoyWay4.Location, 
	ConvoyWay3.Location, ConvoyWay2.Location, ConvoyWay1.Location},
	--entry
	{ConvoyWay1.Location, ConvoyWay2.Location, ConvoyWay3.Location, ConvoyWay4.Location, 
	ConvoyWay5.Location, ConvoyWay6.Location, TankPoint1.Location},
	--entry
	{ConvoyWay1.Location, ConvoyWay2.Location, ConvoyWay3.Location, ConvoyWay4.Location, 
	ConvoyWay5.Location, ConvoyWay6.Location, TankPoint2.Location},
	--exit
	{ConvoyWay6.Location, ConvoyWay5.Location, ConvoyWay4.Location, 
	ConvoyWay3.Location, ConvoyWay2.Location, ConvoyWay1.Location}
}

ResPath1 = { ResEntry1.Location, ResEntry1.Location + CVec.New(-7,0) }
ResPath2 = { ResEntry2.Location, ResEntry2.Location + CVec.New(-5,0) }
ResPath3 = { ResEntry3.Location, ResEntry3.Location + CVec.New(-7,0) }

LSTPath = { LSTCreation.Location, LSTDst.Location }
LSTType = "lst.in"

afterInfiltration = false

moveToVillage = true

VillageCoords = 
{
	CPos.New(88,30), CPos.New(89,30), CPos.New(90,30), 
	CPos.New(91,30), CPos.New(92,30), CPos.New(93,30), CPos.New(94,30)
}

CrateAcquisition = { CPos.New(74,27) }

ATekCameraPos = { CPos.New(64,31), CPos.New(64,32), CPos.New(64,33), CPos.New(64,34), CPos.New(64,35), CPos.New(64,36), CPos.New(64,37) }

AlertedGuards = {GuardJeep1, GuardJeep2, GuardTank1, GuardTank2, GuardTank3, GuardTank4, GuardTank5}

AlertedGuardsPos = 
	{ 
	CPos.New(51,38), CPos.New(84,37), CPos.New(34,41), CPos.New(39,41), CPos.New(65,33), 
	CPos.New(53,35), CPos.New(56,38)
	}

PatrolTime = DateTime.Seconds(5)

PatrolAPath = 
	{
	PatrolAWay2.Location, PatrolAWay1.Location, PatrolAWay2.Location, PatrolAWay3.Location, 
	PatrolAWay4.Location, PatrolAWay5.Location, PatrolAWay4.Location, PatrolAWay3.Location
	}
	
PatrolBPath = {PatrolBWay1.Location, PatrolBWay2.Location}
PatrolCPath = {PatrolCWay1.Location, PatrolCWay2.Location}
PatrolDPath = {PatrolDWay1.Location, PatrolDWay2.Location}

PatrolEAPath = {PatrolEAWay1.Location, PatrolEAWay2.Location}
PatrolEBPath = {PatrolEBWay1.Location, PatrolEBWay2.Location}

PatrolAUnits = { PatrolAInf1, PatrolAInf2, PatrolAInf3, PatrolAInf4, PatrolAInf5 }
PatrolBUnits = { PatrolBInf1, PatrolBInf2, PatrolBInf3}
PatrolCUnits = { PatrolCInf1, PatrolCInf2, PatrolCInf3, PatrolCInf4 }
PatrolDUnits = { PatrolDInf1, PatrolDInf2 }

PatrolEAUnits = { PatrolEAInf1, PatrolEAInf2, PatrolEAInf3 }
PatrolEBUnits = { PatrolEBInf1, PatrolEBInf2, PatrolEBInf3 }

NavalWaypoints = { NavalWaypointA.Location, NavalWaypointB.Location, NavalWaypointC.Location }

NavalPoints =
{
	NavalWaypointA1.Location, NavalWaypointA2.Location, NavalWaypointB1.Location,
	NavalWaypointB2.Location, NavalWaypointC1.Location, NavalWaypointC2.Location
}

ActivateInitPatrols = function()
	GroupPatrol(PatrolBUnits, PatrolBPath, PatrolTime)
	
	GroupPatrol(PatrolCUnits, PatrolCPath, PatrolTime)
	GroupPatrol(PatrolDUnits, PatrolDPath, PatrolTime)
	
	GroupPatrol(PatrolEAUnits, PatrolEAPath, PatrolTime)
	GroupPatrol(PatrolEBUnits, PatrolEBPath, PatrolTime)
end

GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end
			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)
				if bool then
					stop = true
					i = i + 1
					if i > #waypoints then
						i = 1
					end
					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end

InitializeCameras = function()
	SetupCamera(HelpingCamerasCoords[1], CameraSpawn1.Location)
	SetupCamera(HelpingCamerasCoords[2], CameraSpawn2.Location)
	SetupCamera(HelpingCamerasCoords[3], CameraSpawn3.Location)
	SetupCamera(HelpingCamerasCoords[4], CameraSpawn4.Location)
	SetupCamera(HelpingCamerasCoords[5], CameraSpawn5.Location)
end

SetupCamera = function(triggerArea, camPos)
	Trigger.OnEnteredFootprint(triggerArea, function(a, id)
		if a.Owner == player then
			Trigger.RemoveFootprintTrigger(id)
			local cam = Actor.Create( "camera.medium", true,{ Owner = player, Location = camPos })
			
			Trigger.AfterDelay(DateTime.Seconds(10), function()
				if not cam.IsDead then
					cam.Destroy()
				end
			end)
		end
	end)
end

Trigger.OnEnteredFootprint({CPos.New(46,50)}, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		GroupPatrol(PatrolAUnits, PatrolAPath, PatrolTime)
	end
end)

SetupEnemyAI = function()
	if not Jeep1.IsDead then
		Jeep1.Move(JeepPoint1.Location)
	end
	
	if not Jeep2.IsDead then
		Jeep2.Move(JeepPoint2.Location)
	end
	
	for i=1, #AlertedGuards,1 do
		AlertedGuards[i].Move(AlertedGuardsPos[i])
	end
	
	RunAI()
	
	Trigger.AfterDelay(ConvoyDelay, function()
		SendConvoy()
	end)
end

RunAI = function()
	ProduceInfantry()
	ProduceTanks()
	
	BuildVessels()
	BuildBase()
end

SendInitialSovietForces = function()
	Trigger.AfterDelay(DateTime.Seconds(4), function()
		StartingCamera.Destroy()
	end)
	
	scientist = Reinforcements.ReinforceWithTransport(player, LSTType, {"e1","e1","e1","e1","chan"}, LSTPath, {LSTExit.Location})[2][5]

	Trigger.OnKilled(scientist, function()
		player.MarkFailedObjective(ScientistSurvive)
	end)
end

CheckScientistLife = function()
	if not scientist.IsDead then
		player.MarkCompletedObjective(ScientistSurvive)
	end
end

SendSovietBaseForces = function()
	Camera.Position = CameraMarker.CenterPosition
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	
	player.Cash = StartingCash
	
	Reinforcements.Reinforce( player, tankRes, ResPath1, 1 )
	Reinforcements.Reinforce( player, tankRes, ResPath3, 1 )
	
	Trigger.AfterDelay(DateTime.Seconds(3), function()
		AlertingJeep.Move(AlertingJeepDestiny.Location)
		
		Reinforcements.Reinforce( player, infRes, ResPath1, 1 )
		Reinforcements.Reinforce( player, infRes, ResPath3, 1 )
	end)
	DisableTek = player.AddPrimaryObjective("Infiltrate the Allied Tech Center.")
	Reinforcements.Reinforce( player, {"mcv"}, ResPath2, 1 )
end

Trigger.OnEnteredFootprint({AlertingJeepDestiny.Location}, function(a,id)
	if a == AlertingJeep then
		Trigger.RemoveFootprintTrigger(id)
		SetupAlert()
	end
end)

SetupAlert = function()
	Trigger.AfterDelay(DateTime.Seconds(1), function()Media.PlaySoundNotification(player, "AlertBuzzer")end)
	Trigger.AfterDelay(DateTime.Seconds(3), function()Media.PlaySoundNotification(player, "AlertBuzzer")end)
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		Media.PlaySoundNotification(player, "AlertBuzzer")
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			TruckEscape = player.AddSecondaryObjective("Prevent the extraction of research material.")
			SetupEnemyAI()
		end)
	end)
end

Trigger.OnEnteredFootprint(VillageCoords, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		
		if moveToVillage == true then
			moveToVillage = false
			player.MarkCompletedObjective(HideInVillage)
			
			Trigger.AfterDelay(DateTime.Seconds(3), function()
				if not VillageCam.IsDead then
					VillageCam.Destroy()
				end

				if not VillageFlare.IsDead then
					VillageFlare.Destroy()
				end
				Trigger.AfterDelay(DateTime.Seconds(3), function()
					SendSovietBaseForces()
				end)
			end)
		end
	end
end)

SendConvoy = function()
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		Media.Debug("1")
		tank1 = Reinforcements.Reinforce( enemy, {"1tnk.convoy"}, ConvoyPath[3], 1 )[1]
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.Debug("2")
			truk = Reinforcements.Reinforce( enemy, {"truk.mission"}, ConvoyPath[1], 1 )[1]
			
			Trigger.OnKilled(truk, function()
				player.MarkCompletedObjective(TruckEscape)
			end)
			
			Trigger.AfterDelay(DateTime.Seconds(1), function()
				Media.Debug("3")
				tank2 = Reinforcements.Reinforce( enemy, {"1tnk.convoy"}, ConvoyPath[4], 1 )[1]
			end)
		end)
	end)
	
	Trigger.OnEnteredFootprint({ConvoyWay1.Location}, function(a, id)
		if ConvoyArrived == true then
			Trigger.AfterDelay(DateTime.Seconds(0.5), function()
				if a.Type == "truk.mission" or a.Type == "1tnk.convoy" then
					a.Destroy()
				end
			end)
			if a.Type == "truk.mission" then
				player.MarkFailedObjective(TruckEscape)
			end
		end
	end)
	
	Trigger.OnEnteredFootprint({ConvoyWay7.Location}, function(a, id)
		if ConvoyArrived == true then
			if a.Type == "truk.mission" then
				Trigger.RemoveFootprintTrigger(id)
				if not tank1.IsDead then
					MoveAcrossPath(tank1, ConvoyPath[5])
				end
				Trigger.AfterDelay(DateTime.Seconds(1), function()
					if not tank2.IsDead then
						MoveAcrossPath(tank2, ConvoyPath[5])
					end
				end)
			end
		end
	end)
	
	Trigger.OnEnteredFootprint({ConvoyWay8.Location}, function(a, id)
		if a.Type == "truk.mission" then
			Trigger.RemoveFootprintTrigger(id)
			ConvoyArrived = true
			
			Trigger.AfterDelay(DateTime.Seconds(5), function()
				if not a.IsDead then
					MoveAcrossPath(truk, ConvoyPath[2])
				end
			end)
		end
	end)
end

MoveAcrossPath = function(unit, waypoints)
	Utils.Do(waypoints, function(waypoint)
		if not unit.IsDead then
			unit.Move(waypoint)
			
		end
	end)
end

Trigger.OnEnteredFootprint(ATekCameraPos, function(a, id)
	if (a.Owner == player and a.Type == "chan") then
		Trigger.RemoveFootprintTrigger(id)
		player.MarkCompletedObjective(FindOut)
		
		local techCamera = Actor.Create( "camera.medium", true,{ Owner = player, Location = TechCamera.Location })
		VillageCam = Actor.Create( "camera.medium", true,{ Owner = player, Location = VillageLoc.Location })
		VillageFlare = Actor.Create( "flare", true,{ Owner = player, Location = VillageLoc.Location })
	
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			HideInVillage = player.AddPrimaryObjective("Hide in the nearby village.")
			techCamera.Destroy()
		end)

	end
end)

Trigger.OnKilled(WeatherControlCore, function()
	if afterInfiltration == false then
		player.MarkFailedObjective(DisableTek)
	end
end)

Trigger.OnInfiltrated(WeatherControlCore, function()

	Actor.Create( "crate", true,{ Owner = neutral, Location = CreateCrateLoc.Location })
	
	flare = Actor.Create( "flare", true,{ Owner = player, Location = CreateCrateLoc.Location + CVec.New(-1,0) })
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		player.MarkCompletedObjective(ScientistSurvive)
		player.MarkCompletedObjective(TruckEscape)
		player.MarkCompletedObjective(DisableTek)
	end)
end)

--Add extraction process
--[[
ExtractionProcedure = function()
	DisableTek = player.AddPrimaryObjective("Infiltrate the Allied Tech Center.")
	
	
	
	Trigger.OnRemovedFromWorld(a, function)
		player.MarkCompletedObjective(ScientistSurvive)
	end)
end
]]
Tick = function()

end

InitTriggers = function()
	Camera.Position = StartingCamera.CenterPosition
	
	SendInitialSovietForces()
	
	if Map.LobbyOption("difficulty") ~= "hard" then
		InitializeCameras()
	end
	
	ActivateInitPatrols()
	
	enemy.Cash = 100000
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
	
	--
	
	FindOut = player.AddPrimaryObjective("Find out about the strange phenomena.")
	ScientistSurvive = player.AddPrimaryObjective("The scientist must survive.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	neutral = Player.GetPlayer("Neutral")

	InitTriggers()
	InitObjectives()
end