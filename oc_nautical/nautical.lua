if Map.LobbyOption("difficulty") == "easy" then
	SovietReinforcements = {"msub", "sub", "sub"}
	GapAvailable = false
	HardConditions = false
	NuclearSub = true
	HeliPatrolDelay = DateTime.Minutes(1)

elseif Map.LobbyOption("difficulty") == "normal" then
	SovietReinforcements = {"msub", "sub", "sub"}
	GapAvailable = true
	HardConditions = false
	NuclearSub = true
	HeliPatrolDelay = DateTime.Seconds(30)

else
	SovietReinforcements = {"msub", "sub"}
	GapAvailable = true
	HardConditions = true
	NuclearSub = false
	HeliPatrolDelay = DateTime.Seconds(15)

end

ShipsEscaped = false

ChokepointGuns = {CPGun1, CPGun2, CPGun3, CPGun4, CPGun5, CPGun6}

InitialUnitsArrived = false

MainNavalYards = {MainSYard1, MainSYard2, MainSYard3}
NavalYards = { Naval1, Naval2, Naval3, Naval4, Naval5, Naval6, Naval7, Naval8, MainSYard1, MainSYard2, MainSYard3 }

NBase1Ships = {NBase1Ship1, NBase1Ship2, NBase1Ship3}
NBase2Ships = {NBase2Ship1, NBase2Ship2, NBase2Ship3}
NBase3Ships = {NBase3Ship1, NBase3Ship2, NBase3Ship3, NBase3Ship4, NBase3Ship5}

Alerted = false

AlertableUnits = 
{ 
AlertableUnit1, AlertableUnit2, AlertableUnit3, AlertableUnit4, AlertableUnit5, AlertableUnit6, AlertableUnit7
}

Base1 = { Base1Build1, Base1Build2, Base1Build3, Base1Build4, Base1Build5, Base1Build6, Base1Build7 }

BlockadingVessels = { Blockading1, Blockading2, Blockading3, Blockading4, Blockading5, Blockading6 }

LSTPath1 = {LSTEntry1.Location, LSTDst1.Location}
LSTPath2 = {LSTEntry3.Location, LSTDst3.Location}
LSTPath3 = {LSTEntry2.Location, LSTDst2.Location}
LSTPath4 = {SubEntry3.Location, SubDst3.Location+CVec.New(3,0)}
LSTPath5 = {SubEntry4.Location, SubDst4.Location+CVec.New(3,0)}

--SubPath1 = { SubEntry3.Location, SubDst3.Location}
--SubPath2 = { SubEntry4.Location, SubDst4.Location}

SovUnitsBatch1 = {"e1","e1","e2","3tnk","ftrk"}
SovUnitsBatch2 = {"e4","e4","4tnk","v2rl","v2rl"}
SovUnitsBatch3 = {"e1","e1","e2","3tnk","ttnk"}
--SovNavalBatch = {"ss","msub"}

Helipads = { Hpad1, Hpad2, Hpad3, Hpad4, Hpad5, Hpad6 }
Helis = { AttackHeli1, AttackHeli2, AttackHeli3, AttackHeli4, AttackHeli5, AttackHeli6 }
--Turrets killed in water chokepoint

EscapeesSafepoint = { CPos.New(70,50), CPos.New(73,47), CPos.New(78,46) }

EscapingShips = { EscapingCruiser1, EscapingCruiser2, EscapingCruiser3 }

NukeCameras = { NukeCamera1, NukeCamera2, NukeCamera3 }
NukeCameraActivation = { CPos.New(89,40), CPos.New(89,41), CPos.New(88,40), CPos.New(88,41) }

PatrollingHeli = ScoutingHeli

LeftPatrolPath = 
{
HelicopterWayL1, HelicopterWayL2, HelicopterWayL3, HelicopterWayMid, HelicopterWayR3, HelicopterWayR2, HelicopterWayR1
}
RightPatrolPath = 
{
HelicopterWayR1, HelicopterWayR2, HelicopterWayR3, HelicopterWayMid, HelicopterWayL3, HelicopterWayL2, HelicopterWayL1
}

SendInitialReinforcements = function()
	Reinforcements.ReinforceWithTransport(player, "lst", SovUnitsBatch1, LSTPath1 )
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		Reinforcements.ReinforceWithTransport(player, "lst", SovUnitsBatch1, LSTPath2 )
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Reinforcements.ReinforceWithTransport(player, "lst", SovUnitsBatch2, LSTPath3 )
		end)
	end)
	InitialUnitsArrived = true
end

SendLastReinforcements = function()
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		
		Reinforcements.ReinforceWithTransport(player, "lst", SovUnitsBatch3, LSTPath1 )
		Reinforcements.ReinforceWithTransport(player, "lst", SovUnitsBatch3, LSTPath2 )
	end)
end

SendAdditionalReinforcements = function()
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		
		Reinforcements.Reinforce(player, {"ss"}, { SubEntry3.Location, SubDst3.Location }, 25 )
		Reinforcements.Reinforce(player, {"ss"}, { SubEntry4.Location, SubDst4.Location }, 25 )
		Trigger.AfterDelay(DateTime.Seconds(3), function()
			Reinforcements.Reinforce(player, {"msub"}, { SubEntry3.Location, SubDst3.Location + CVec.New(2,0)}, 25 )
			Reinforcements.Reinforce(player, {"msub"}, { SubEntry4.Location, SubDst4.Location + CVec.New(2,0)}, 25 )
		end)
		if NuclearSub then
			Trigger.AfterDelay(DateTime.Seconds(5), function()
				GrantNuclearSubmarine()
				
				Trigger.OnEnteredFootprint(NukeCameraActivation, function(a, id)
					if a.Type == "ss.nuclear" then
						Trigger.RemoveFootprintTrigger(id)
						Utils.Do(NukeCameras, function(c)
							local cam = Actor.Create("camera", true, { Owner = player, Location = c.Location} )
							
							Trigger.OnAllKilled(MainNavalYards, function()
								if not cam.IsDead then
									cam.Destroy()
								end
							end)
						end)
					end
				end)
			end)
		end
	end)
end

PatrolBoats = function()
	PatrolBoatA.Patrol({VesselPatrolA1.Location, VesselPatrolA2.Location}, true, 100)
	PatrolBoatB.Patrol({VesselPatrolB1.Location, VesselPatrolB2.Location}, true, 100)
end

Trigger.OnAllKilled(BlockadingVessels, function()
	SendAdditionalReinforcements()
	player.MarkCompletedObjective(EliminateBlockade)
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		BlockadeCamera.Destroy()
	end)
end)

Trigger.OnAllKilled(ChokepointGuns, function()
	player.MarkCompletedObjective(DestroyGuns)
	SendLastReinforcements()
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		if GunCameras then
			GunCameras.Destroy()
		end
	end)
end)

Trigger.OnAllKilled(NavalYards, function()
	player.MarkCompletedObjective(DestroyNavalYards)
end)

CreateAdditionalUnits = function()
	Actor.Create("gun", true, { Owner = enemy, Location = CPGun7Spawn.Location, Facing = 96} )
	Actor.Create("gun", true, { Owner = enemy, Location = CPGun8Spawn.Location, Facing = 96} )
	Actor.Create("gun", true, { Owner = enemy, Location = CPGun9Spawn.Location, Facing = 96} )
	Actor.Create("gun", true, { Owner = enemy, Location = CPGun10Spawn.Location, Facing = 96} )
end

DefendBase = function()
	if Alerted == false then
		Alerted = true
		Utils.Do(AlertableUnits, function(unit)
			IdleHunt(unit)
		end)
	end
end

EscapingPath1 = { EscapingWaypoint.Location, EscapingCruiserDst1.Location }
EscapingPath2 = { EscapingWaypoint.Location, EscapingCruiserDst2.Location }
EscapingPath3 = { EscapingWaypoint.Location, EscapingCruiserDst3.Location }

MoveEscapingBoats = function()
	EscapingCruiser1.Move(EscapingWaypoint.Location)
	EscapingCruiser2.Move(EscapingWaypoint.Location)
	EscapingCruiser3.Move(EscapingWaypoint.Location)
	
	EscapingCruiser1.Move(EscapingCruiserDst1.Location)
	EscapingCruiser2.Move(EscapingCruiserDst2.Location)
	EscapingCruiser3.Move(EscapingCruiserDst3.Location)
	--MoveGuardBoats()
end

MoveGuardBoats = function()
	if not BlockingDD1.IsDead or not BlockingDD2.IsDead then
		BlockingDD1.Move(DD1Dst.Location)
		BlockingDD2.Move(DD2Dst.Location)
	end
	Trigger.AfterDelay(DateTime.Seconds(75), function()
		if not BlockingDD1.IsDead or not BlockingDD2.IsDead then
			BlockingDD1.Move(DD1Post.Location)
			BlockingDD2.Move(DD2Post.Location)
		end
	end)
end

Trigger.OnEnteredFootprint(EscapeesSafepoint, function(a, id)
	if a.Type == "ca.damaged" then
		Trigger.RemoveFootprintTrigger(id)
		ShipsEscaped = true
		
		player.MarkFailedObjective(DestroyEscapees)
		
		Utils.Do(EscapingShips, function(u)
			if not u.IsDead then
				u.Stance = "Defend"
			end
		end)
	end
end)

Trigger.OnEnteredFootprint({EscapingWaypoint.Location}, function(a, id)
	if a.Type == "ca.damaged" then
		Trigger.RemoveFootprintTrigger(id)
		MoveGuardBoats()
	end
end)

Trigger.OnAllKilled(EscapingShips, function()
	if ShipsEscaped == false then
		player.MarkCompletedObjective(DestroyEscapees)
	end
end)

ProduceHeli = function()
	if ScoutingHPad.IsDead then
		return
	else
		local toBuild = {heli.autotarget}

		enemy.Build(toBuild, function(u)
			local heli = u[1]
			Trigger.OnAddedToWorld(heli, function(h)
				CheckPatrollingHeli(heli)
			end)
			
			Trigger.OnKilled(heli, function()
				CheckPatrollingHeli()
			end)
		end)
	end
end

Trigger.OnKilled(ScoutingHeli, function()
	CheckPatrollingHeli()
end)

CheckPatrollingHeli = function(heli)
	if not heli then
		ProduceHeli()
	else
		HeliPatrol(heli)
		--Trigger.AfterDelay(DateTime.Seconds(10), function()
		--	CheckPatrollingHeli(heli)
		--end)
	end
end

--[[]]
--SetDefendingShips(NBase1Ships)
--SetDefendingShips(NBase2Ships)
--SetDefendingShips(NBase3Ships)
--[[]]

SetDefendingShips = function(units)
	Utils.Do(units, function(u)
		Trigger.OnDamaged(u, function()
			Utils.Do(units, function(x)
				IdleHunt(x)
			end)
		end)
	end)
end

HeliPatrol = function(heli)
	local path = Utils.Random({LeftPatrolPath, RightPatrolPath})
	local lastPoint = path[7]
	
	Utils.Do(path, function(w)
		if not heli.IsDead then
			heli.AttackMove(w.Location)
		end
	end)
	
	Trigger.OnEnteredProximityTrigger(lastPoint.CenterPosition, WDist.FromCells(1), function(a, id)
		if a.Type == "heli" then
			Trigger.RemoveProximityTrigger(id)
			if not ScoutingHPad.IsDead or not heli.IsDead then
				heli.ReturnToBase(ScoutingHPad)
				Trigger.AfterDelay(HeliPatrolDelay, function()
					if not ScoutingHPad.IsDead or not heli.IsDead then
						HeliPatrol(heli)
					end
				end)
			end
		end	
	end)
	--Add check for reloading
	--Add delay for patrolling
end

GrantNuclearSubmarine = function()
	Reinforcements.Reinforce(player, {"ss.nuclear"}, { SubEntry3.Location + CVec.New(0,3), SubDst3.Location + CVec.New(1,3) }, 25 )
	
	Media.DisplayMessage("Use our tactical nuke submarine to plunder the naval base.","Command")
	Trigger.AfterDelay(DateTime.Seconds(3), function()
		Media.DisplayMessage("With its increased range and destruction capability the allies don't stand a chance.","Command")
		Trigger.AfterDelay(DateTime.Seconds(3), function()
			Media.DisplayMessage("Remember though. You only have one shot. Use it well.","Command")
		end)
	end)
end

Flavor = function()
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		local landings = Helipads
		local aircrafts = Helis
		
		for i = 1, #aircrafts, 1 do
			aircrafts[i].ReturnToBase(landings[i])
		end
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then 
		Trigger.OnIdle(unit, unit.Hunt)
	end 
end

Tick = function()
	if player.HasNoRequiredUnits() and InitialUnitsArrived == true then
		enemy.MarkCompletedObjective(DenySoviets)
	end
end

InitTriggers = function()
	Camera.Position = StartingCameraPos.CenterPosition
	
	SendInitialReinforcements()
	
	PatrolBoats()
	
	CheckPatrollingHeli(ScoutingHeli)
	
	if GapAvailable == true then
		MobGap1.Destroy()
		MobGap2.Destroy()
		MobGap3.Destroy()
		MobGap4.Destroy()
		MobGap5.Destroy()
	end
	
	if HardConditions == true then
		CreateAdditionalUnits()
	else
		HardCruiser1.Destroy()
		HardCruiser2.Destroy()
	end
	
	SetDefendingShips(NBase1Ships)
	SetDefendingShips(NBase2Ships)
	SetDefendingShips(NBase3Ships)
	
	Utils.Do(Base1, function(build)
		Trigger.OnDamaged(build, function()
			if build.Health > build.MaxHealth*0.95 then
				Trigger.ClearAll(build)
				DefendBase()
			end
		end)
	end)
	
	MoveEscapingBoats()
	
	Flavor()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DestroyNavalYards = player.AddPrimaryObjective("Destroy all naval yards.")
	DestroyGuns = player.AddPrimaryObjective("Destroy the turrets.")
	
	EliminateBlockade = player.AddSecondaryObjective("Eliminate blockade for additional support.")
	DestroyEscapees = player.AddSecondaryObjective("Destroy escaping ships.")
	
	DenySoviets =  enemy.AddPrimaryObjective("Deny the soviets.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	
	InitTriggers()
	InitObjectives()
end