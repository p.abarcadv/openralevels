if Map.LobbyOption("difficulty") == "easy" then
	PlayerCash = 4000
	IntervalBetweenTruks = 180
	DelayOfAttacks = DateTime.Minutes(2)
elseif Map.LobbyOption("difficulty") == "normal" then
	PlayerCash = 4000
	IntervalBetweenTruks = 160
	DelayOfAttacks = DateTime.Minutes(2)
else
	PlayerCash = 2000
	IntervalBetweenTruks = 120
	DelayOfAttacks = DateTime.Minutes(1)
end

SendAttacks = function()
	

end

Tick = function()
	
end

InitTriggers = function()
	Player.Cash = PlayerCash
	
	Camera.Position = StatingCamera.CenterPosition
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
    
    DestroyRemains = player.AddPrimaryObjective("Destroy the ruined nuclear facility.")
	VolkovSurvive = player.AddPrimaryObjective("Volkov must survive.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the allies.")
    
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "MissionFailed")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "MissionAccomplished")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	england = Player.GetPlayer("England")

	InitTriggers()
	InitObjectives()
end