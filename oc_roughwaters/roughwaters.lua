if Map.LobbyOption("difficulty") == "easy" then
	MSubMax = 0
	AtkIncrementMax = 2
	SupplyCash = 6000
	ObjText = "cruisers"
	OptionalDome = true
elseif Map.LobbyOption("difficulty") == "normal" then
	MSubMax = 1
	AtkIncrementMax = 2
	SupplyCash = 5000
	ObjText = "cruiser"
	OptionalDome = true
else
	MSubMax = 2
	AtkIncrementMax = 3
	SupplyCash = 4000
	ObjText = "cruiser"
	OptionalDome = false 
end

CruiserList = {Cruiser1}
InfantryReinforcements = {"e1","e1","e1", "e1", "e3","e3", "e3","e3"}

BotBaseBuildings = { BotBase1,BotBase2, BotBase3, BotBase4, BotBase5, BotBase6, BotBase7, BotBase8, ATekObj }
Subs = {Sub1, Sub2, Sub3, Sub4, Sub5, Sub6, Sub7, Sub8, Sub9, Sub10, Sub11, Sub12, Sub13, Sub14, Sub15, Sub16, Sub17}
cameras = {Cam1, Cam2, Cam3, Cam4, Cam5, Cam6, Cam7, Cam8, Cam9, Cam10, Cam11, Cam12}

AddCruiserObjective = function()
	if Map.LobbyOption("difficulty") == "easy" then
		table.insert(CruiserList, Cruiser2)
	end
	Trigger.OnAnyKilled(CruiserList, function()
		player.MarkFailedObjective(ProtectCruiser)
	end)
end

StartTranEntry = function()
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	PlayerTran = Reinforcements.ReinforceWithTransport(player, "tran",InfantryReinforcements,{TranEntry.Location, TranDst.Location})[1]
end

FlavorReinforce = function()
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	if Map.LobbyOption("difficulty") == "easy" then
		Cruiser2 = Reinforcements.Reinforce(player, {"ca"}, {NavalEntry2.Location, NavalDst2.Location})[1]
	end
	if Map.LobbyOption("difficulty") == "hard" then
		Cruiser2 = Reinforcements.Reinforce(player, {"dd"}, {NavalEntry2.Location, NavalDst2.Location})[1]
	end
	Reinforcements.Reinforce(player, {"pt"}, {NavalEntry1.Location, NavalDst1.Location})
	Reinforcements.Reinforce(player, {"pt"}, {NavalEntry3.Location, NavalDst3.Location})
	if Map.LobbyOption("difficulty") ~= "hard" then
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Reinforcements.Reinforce(player, {"dd"}, {NavalEntry1.Location, NavalDst1.Location+CVec.New(0,-2)})
			Reinforcements.Reinforce(player, {"dd"}, {NavalEntry3.Location, NavalDst3.Location+CVec.New(0,-2)})
		end)
	end
	AddCruiserObjective()
end

Trigger.OnAllKilled(Subs, function()
	if not Cruiser1.IsDead then
		player.MarkCompletedObjective(ProtectCruiser)
	end
	player.MarkCompletedObjective(ClearSubs)
	player.MarkCompletedObjective(DefendBase)
end)

Trigger.OnKilled(ATekObj, function()
	player.MarkFailedObjective(DefendBase)
end)

Tick = function()
	local hind = Utils.Where(Map.ActorsInWorld, function(actor) return actor.Type == "hind.outofmap" end)
	
	Utils.Do(hind, function(unit)
		if unit.AmmoCount() == 0 then
			unit.Stance = "HoldFire"
			unit.Reload("primary",1)
			unit.Stop()
		end
	end)
end

InitTriggers = function()
	Camera.Position = StartingCameraPos.CenterPosition
	player.Cash = SupplyCash
	
	AssignRandomStance(Subs)
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		StartTranEntry()
	end)
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		FlavorReinforce()
	end)
	Actor.Create("healcrate", true, { Owner = neutral, Location = HealCrateLoc.Location  })
	
	ConstantAttack()
	
	if OptionalDome then
		Actor.Create("dome", true, { Owner = player, Location = OptionalDomeWp.Location  })
	end
	
	Trigger.AfterDelay(DateTime.Minutes(5), function()
		ConstantAirAttacks()
	end)
	Trigger.AfterDelay(DateTime.Minutes(11), function()
		MissileSubAttack()
	end)
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DefendBase = player.AddPrimaryObjective("Protect the Allied Tech Center.") --x
	ClearSubs = player.AddPrimaryObjective("Clear all submarines in the area.") --x
	ProtectCruiser = player.AddSecondaryObjective("The ".. ObjText .. " must survive.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	neutral = Player.GetPlayer("Neutral")

	InitTriggers()
	InitObjectives()
end
