if Map.LobbyOption("difficulty") == "easy" then
	MSubMax = 0
	AtkIncrementMax = 2
elseif Map.LobbyOption("difficulty") == "normal" then
	MSubMax = 1
	AtkIncrementMax = 2
else
	MSubMax = 2
	AtkIncrementMax = 3
end

AtkIncrement = 0
MSubCount = 0

HindAtkEntries = { HindEntry1.Location, HindEntry2.Location, HindEntry3.Location, HindEntry4.Location, HindEntry5.Location, HindEntry6.Location }
LandingsOffBase = {LandingOffBase1.Location, LandingOffBase2.Location}

AtksComp = {}

--[[AI CONTROL (UNIT)]]--
IdleHunt = function(unit) if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end end

HindAtk = function(unit, target, way) 
	if not unit.IsDead then
		Trigger.OnIdle(unit, function()
			if unit.AmmoCount() > 10 then
				unit.Attack(ATekObj,false,true)
			else
				unit.Move(way)
				unit.Destroy()
			end
		end)
	end
end

AssignRandomStance = function(coll)
	Utils.Do(coll, function(unit)
		local stance = Utils.Random({"AttackAnything", "Defend"})
		unit.Stance = stance
	end)	
end
--[[AI CONTROL (UNIT)]]--


--[[ATTACKS]]--
WaveIncrements = function()
	local delay = 5
	Trigger.AfterDelay(DateTime.Minutes(delay), function()
		if AtkIncrement < AtkIncrementMax then
			AtkIncrement = AtkIncrement + 1
			WaveIncrements()
		end
	end)
end

--[[REFACTOR]]--
ChangeEnemyAtkComposition = function()
	if AtkIncrement == 0 then
		AtksComp =
		{
		{"e1", "e1", "ftrk"},
		{"e1", "e2", "ftrk"},
		{"e1", "e1", "3tnk"}
		}
	end
	if AtkIncrement == 1 then
		AtksComp =
		{
		{"e1", "e1", "e2", "3tnk"},
		{"e1", "e1", "e1", "3tnk"},
		{"e1", "e1", "e1", "ftrk"}
		}
	end
	if AtkIncrement == 2 then
		AtksComp =
		{
		{"e1", "e1", "e2", "e2", "3tnk"},
		{"e1", "e1", "e1", "e1", "ftrk"},
		{"e1", "e1", "e1", "e1", "v2rl"}
		}
	end
	if AtkIncrement == 3 then
		AtksComp =
		{
		{"e1", "e1", "e2", "e2", "3tnk"},
		{"e1", "e1", "e2", "e2", "ftrk"},
		{"e1", "e1", "e1", "e1", "ttnk"},
		{"e1", "e1", "e1", "e1", "v2rl"}
		}
	end
end

ConstantAttack = function()
	WaveIncrements()
	ChangeEnemyAtkComposition()
	Trigger.AfterDelay(DateTime.Minutes(1), function()
		BotAttacks()
	end)
	Trigger.AfterDelay(DateTime.Minutes(5), function()
		TopAttacks()
	end)
end

TopAttacks = function()
	local dst = Utils.Random(LandingsOffBase)
	local units = Reinforcements.ReinforceWithTransport(enemy, "lst", {"e1", "e1", "e2", "e2", "3tnk"},{EnemyLstEntrySouth.Location, dst}, {EnemyLstEntrySouth.Location})[2]
	
	Utils.Do(units, function(unit)
		Trigger.OnAddedToWorld(unit, function()
			unit.AttackMove(TopTarget.Location)
			IdleHunt(unit)
		end)
	end)
	Trigger.AfterDelay(DateTime.Minutes(5.5), function()
		TopAttacks()
	end)
end

BotAttacks = function()
	local TopOrBot = Utils.Random({"Top", "Bot"})
	local delay = Utils.Random({2.5, 2.6, 2.7})
	local Comp = Utils.Random(AtksComp)
	
	if TopOrBot == "Top" then
		entry = EnemyLstEntryNorth.Location
		dst = LandingDefBase1.Location
	else
		entry = EnemyLstEntrySouth.Location
		dst = LandingDefBase2.Location
	end
		
	local units = Reinforcements.ReinforceWithTransport(enemy, "lst", Comp, {entry, dst}, {entry})[2]
	
	Utils.Do(units, function(unit)
		Trigger.OnAddedToWorld(unit, function()
			unit.AttackMove(BotTarget.Location)
			IdleHunt(unit)
		end)
	end)
	Trigger.AfterDelay(DateTime.Minutes(delay), function()
		BotAttacks()
	end)
end

ConstantAirAttacks = function()
	local entry = Utils.Random(HindAtkEntries)
	local delay = Utils.Random({3.25, 3.5})
	local unit = {"hind.outofmap"}
	
	local hind = Reinforcements.Reinforce(enemy, unit, {entry, entry})[1]
	
	HindAtk(hind,ATekObj,entry)

	Trigger.AfterDelay(DateTime.Minutes(delay), function()
		ConstantAirAttacks()
	end)
end

MissileSubAttack = function()
	local entry = Utils.Random({EnemyLstEntryNorth.Location, EnemyLstEntrySouth.Location})
	local dst = Utils.Random({MSubDst1.Location, MSubDst2.Location})
	local Comp = {"msub.attack"}
	
	if MSubCount < MSubMax then
		MSubCount = MSubCount + 1
		Reinforcements.Reinforce(enemy, Comp, {entry, dst})
		Trigger.AfterDelay(DateTime.Minutes(7), function()
			MissileSubAttack()
		end)
	end
end

