Power1	={type = "powr"	, pos = CVec.New(0,3)	, cost = 300, exists = true }
Power2	={type = "apwr"	, pos = CVec.New(16,-1)	, cost = 500, exists = true }
Power3	={type = "apwr"	, pos = CVec.New(13,1)	, cost = 500, exists = true }
Power4	={type = "apwr"	, pos = CVec.New(16,2)	, cost = 500, exists = true }
Power5	={type = "apwr"	, pos = CVec.New(13,4)	, cost = 500, exists = true }
Power6	={type = "apwr"	, pos = CVec.New(16,5)	, cost = 500, exists = true }
Power7	={type = "powr"	, pos = CVec.New(-1,-16), cost = 300, exists = true }
Power8	={type = "apwr"	, pos = CVec.New(1,-17)	, cost = 500, exists = true }
Power9	={type = "apwr"	, pos = CVec.New(4,-16)	, cost = 500, exists = true }
Power10	={type = "powr"	, pos = CVec.New(6,-16)	, cost = 300, exists = true }

Tent	={type = "tent"	, pos = CVec.New(5,6)	, cost = 300, exists = true }
Weap	={type = "weap"	, pos = CVec.New(7,-1)	, cost = 2000, exists = true }
Ref1	={type = "proc"	, pos = CVec.New(-6,-3)	, cost = 1500, exists = true }
Ref2	={type = "proc"	, pos = CVec.New(-14,-7), cost = 1500, exists = true }

Dome	={type = "dome"	, pos = CVec.New(-4,-11), cost = 1000, exists = true }
Fix		={type = "fix"	, pos = CVec.New(7,3)	, cost = 1000, exists = true }

Silo1	={type = "silo"	, pos = CVec.New(11,1)	, cost = 150, exists = true }
Silo2	={type = "silo"	, pos = CVec.New(11,2)	, cost = 150, exists = true }
Silo3	={type = "silo"	, pos = CVec.New(11,4)	, cost = 150, exists = true }
Silo4	={type = "silo"	, pos = CVec.New(11,5)	, cost = 150, exists = true }

PBox1	={type = "pbox"	, pos = CVec.New(1,-3)	, cost = 150, exists = true}
PBox2	={type = "pbox"	, pos = CVec.New(1,7)	, cost = 150, exists = true}
PBox3	={type = "pbox"	, pos = CVec.New(14,-2)	, cost = 150, exists = true}

AGun1	={type = "agun"	, pos = CVec.New(-1,5)	, cost = 150, exists = true}
AGun2	={type = "agun"	, pos = CVec.New(6,-1)	, cost = 150, exists = true}
AGun3	={type = "agun"	, pos = CVec.New(16,-2)	, cost = 150, exists = true}
AGun4	={type = "agun"	, pos = CVec.New(16,8)	, cost = 150, exists = true}
AGun5	={type = "agun"	, pos = CVec.New(-6,-9), cost = 150, exists = true}

BaseBuildings =
{
Power1,
Power2,
Power3,
Power4,
Power5,
Power6,
Power7,
Power8,
Power9,
Power10,
Tent,
Weap,
Ref1,
Ref2,
Dome,
Fix,
Silo1,
Silo2,
Silo3,
Silo4,
PBox1,
PBox2,
PBox3,
AGun1,
AGun2,
AGun3,
AGun4,
AGun5
}

Trigger.OnKilled(EPower1, function()
	Power1.exists = false
end)

Trigger.OnKilled(EPower2, function()
	Power2.exists = false
end)

Trigger.OnKilled(EPower3, function()
	Power3.exists = false
end)

Trigger.OnKilled(EPower4, function()
	Power4.exists = false
end)

Trigger.OnKilled(EPower5, function()
	Power5.exists = false
end)

Trigger.OnKilled(EPower6, function()
	Power6.exists = false
end)

Trigger.OnKilled(EPower7, function()
	Power7.exists = false
end)

Trigger.OnKilled(EPower8, function()
	Power8.exists = false
end)

Trigger.OnKilled(ETent, function()
	Tent.exists = false
end)

Trigger.OnKilled(EWeap, function()
	Weap.exists = false
end)

Trigger.OnKilled(ERef1, function()
	Ref1.exists = false
end)

Trigger.OnKilled(ERef2, function()
	Ref2.exists = false
end)

Trigger.OnKilled(ESilo1, function()
	Silo1.exists = false
end)

Trigger.OnKilled(ESilo1, function()
	Silo2.exists = false
end)

Trigger.OnKilled(ESilo3, function()
	Silo3.exists = false
end)

Trigger.OnKilled(ESilo4, function()
	Silo4.exists = false
end)

Trigger.OnKilled(EDome, function()
	Dome.exists = false
end)

Trigger.OnKilled(EFix, function()
	Fix.exists = false
end)

Trigger.OnKilled(EPbox1, function()
	PBox1.exists = false
end)

Trigger.OnKilled(EPbox2, function()
	PBox2.exists = false
end)

Trigger.OnKilled(EPbox3, function()
	PBox3.exists = false
end)

Trigger.OnKilled(EAGun1, function()
	AGun1.exists = false
end)

Trigger.OnKilled(EAGun2, function()
	AGun2.exists = false
end)

Trigger.OnKilled(EAGun3, function()
	AGun3.exists = false
end)

Trigger.OnKilled(EAGun4, function()
	AGun4.exists = false
end)

Trigger.OnKilled(EAGun5, function()
	AGun5.exists = false
end)
