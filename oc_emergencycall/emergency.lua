if Map.LobbyOption("difficulty") == "easy" then
	PlayerCash = 5000
	
	TranAttackInitDelay = DateTime.Seconds(60)
	
	DelayOfControlTruks = DateTime.Minutes(2)
	
	SendSecondaryTrucksDelay = DateTime.Minutes(8)
	IntervalBetweenTruks = 120
	
	NotifySiteRetrieval = true
	
	SendAxuliaryHinds = true
	
	RemoveExtraUnits = 0
elseif Map.LobbyOption("difficulty") == "normal" then
	PlayerCash = 5000
	
	TranAttackInitDelay = DateTime.Seconds(30)
	
	DelayOfControlTruks = DateTime.Minutes(2)
	
	SendSecondaryTrucksDelay = DateTime.Minutes(7)
	IntervalBetweenTruks = 120
	
	NotifySiteRetrieval = true
	
	SendAxuliaryHinds = true
	
	RemoveExtraUnits = 1
else
	PlayerCash = 4000

	TranAttackInitDelay = DateTime.Seconds(5)

	DelayOfControlTruks = DateTime.Minutes(1.5)

	SendSecondaryTrucksDelay = DateTime.Minutes(7)
	IntervalBetweenTruks = 120
	
	NotifySiteRetrieval = false
	
	SendAxuliaryHinds = false
	
	RemoveExtraUnits = 2
end

NotifyDelay = DateTime.Seconds(2)

GuidanceTruckSwitch = false

FlavorHinds = {}

RemoveOnDifficultyTanks =
{
ExMedTank1, ExMedTank2
}

RemoveOnDifficultyGuns =
{
ExtGun1, ExtGun2, ExtGun3, ExtGun4
}

tranInsertionPath = {tranInsertEntry.Location, tranInsertDst.Location}
tranExtractionPath = {tranExtractEntry.Location, tranExtractDst.Location}

tranI="tran.insertion"
tranE="tran.extraction"

NukeCameraCoords =
{
CPos.New(47,43), CPos.New(47,44), CPos.New(47,45), CPos.New(47,46), CPos.New(47,47), CPos.New(47,48),
CPos.New(47,36), CPos.New(47,37), CPos.New(47,38), CPos.New(47,39), CPos.New(47,40)
}
RadiationMessage = "High levels of radiation detected. Move fast!"
RadiationHeadsUpCooldown = 0

NuclearRemainsDestroyed = false

EscapePath = {EscapeTrukPath1, EscapeTrukPath2, EscapeTrukPath3, EscapeRoute}
EscapingTruks = 
{
InitTruk1, InitTruk2, InitTruk3, InitTruk4, InitTruk5, InitTruk6, InitTruk7, InitTruk8, InitTruk9
}

EscapedTruks = 0

DestroyedTrucks = 0

mainPhaseStarted = false

SovPath1 = { PUnitsEntry1.Location, PUnitsDst1.Location }
SovPath2 = { PUnitsEntry2.Location, PUnitsDst2.Location }
SovPath3 = { PUnitsEntry3.Location, PUnitsDst3.Location }
SovPath4 = { PUnitsEntry2.Location, PUnitsDst2.Location + CVec.New(-2,2)}

AlertPlayerArea = {CPos.New(100,45), CPos.New(101,45), CPos.New(102,45), CPos.New(103,45), CPos.New(104,45)}

InitTrukActivationArea = {CPos.New(110,41), CPos.New(110,40), CPos.New(110,39), CPos.New(110,38)}

InitTrukPath = {MovingTrukPath1,MovingTrukPath2,MovingTrukPath3,MovingTrukPath4,MovingTrukPath5,MovingTrukDst}

ControlTruks = {trukControl1, trukControl2, MovingTruk}

ControlTruksDestroyed = false

RetrievalPath = {AttackTranEntry.Location, RetrievalTranPoint.Location}

ControlTrukTarget = { CPos.New(61,37) }

RetrievalTranReady = false
FirstRetrievalTrukReady = false

EnableSecondaryObj = false

SecondVolkInsertionPath = {SecondTranEntry.Location, VolkovReturns.Location}

TentInfAlerted = false
TentInfantry = { TentInf1, TentInf2, TentInf3, TentInf4, TentInf5 }

SendControlTruks = function()
	if ControlTruksDestroyed == false and NuclearRemainsDestroyed == false then
		local aliveTrucks = Utils.Where(ControlTruks, function(self) return not self.IsDead end)
		firstTruk = aliveTrucks[1]
		
		firstTruk.Move(TrukNuclearRetrieval.Location)
			
		Trigger.OnEnteredFootprint({TrukNuclearRetrieval.Location}, function(a,id)
			if a.Owner == goodguy and a.Type == "truk.mission" then
				Trigger.RemoveFootprintTrigger(id)
				a.Move(ControlTrukTarget[1])
				retriever = Reinforcements.ReinforceWithTransport(goodguy, tranI, {}, RetrievalPath, nil)[1]
			end
		end)
	else
		return
	end
end

Trigger.OnEnteredFootprint({RetrievalTranPoint.Location}, function(a, id)
	if a.Owner == goodguy and a.Type == "tran.insertion" and not firstTruk.IsDead then
		Trigger.RemoveFootprintTrigger(id)
		RetrievalTranReady = true
	end
end)

Trigger.OnEnteredFootprint(ControlTrukTarget, function(a, id)
	if a == firstTruk and not retriever.IsDead then
		Trigger.RemoveFootprintTrigger(id)
		FirstRetrievalTrukReady = true
	end
end)

RetrievalTranExit = function()
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		if not retriever.IsDead then
			retriever.Move(AttackTranEntry.Location)
			retriever.Destroy()
			Media.DisplayMessage("The allies have sacked the Nuclear Facility remains.","Command")
			Trigger.AfterDelay(NotifyDelay, function()
				Media.PlaySpeechNotification(player, "ObjectiveNotMet")
				Trigger.AfterDelay(NotifyDelay, function()
					player.MarkFailedObjective(DestroyRemains)
				end)
			end)
		end
	end)
end

Trigger.OnAllKilled(ControlTruks, function()
	ControlTruksDestroyed = true
end)

Trigger.OnEnteredFootprint(AlertPlayerArea, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		if mainPhaseStarted == false then
			Media.DisplayMessage("The enemy base is that way. Forget about it! Destroy the nuclear facility to the west before the allies sack it.","Command")
		end
	end
end)

Trigger.OnEnteredFootprint(InitTrukActivationArea, function(a,id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		if GuidanceTruckSwitch == false then
			GuidanceTruckSwitch = true
			SendGuidanceTruck()
		end
	end
end)

Trigger.AfterDelay(DateTime.Seconds(20), function()
	if GuidanceTruckSwitch == false then
		GuidanceTruckSwitch = true
		SendGuidanceTruck()
	end
end)

SendGuidanceTruck = function()
	Utils.Do(InitTrukPath, function(way)
		MovingTruk.Move(way.Location)
	end)
end

Trigger.OnEnteredFootprint(NukeCameraCoords, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		CamRemains = Actor.Create("camera", true, { Owner = player, Location = ReactorRemains.Location } )
		
		if SendAxuliaryHinds == true then
			HindFlavorEntry()
		end
		
		Trigger.OnKilled(ReactorRemains, function()
			if CamRemains then
				Trigger.AfterDelay(DateTime.Seconds(18), function()
					CamRemains.Destroy()
				end)
			end
		end)
	end
end)

VolkovInsertion = function()
	Volkov = Reinforcements.ReinforceWithTransport(player, tranI, {"volk"}, tranInsertionPath, {tranInsertionPath[1]})[2][1]

	Trigger.OnKilled(Volkov, function()
		Trigger.AfterDelay(NotifyDelay, function()
			enemy.MarkCompletedObjective(DenySoviets)
		end)
	end)

	Trigger.AfterDelay(DateTime.Seconds(15), function()
		if InitialCam then
			InitialCam.Destroy()
		end
	end)
end

Trigger.OnKilled(ReactorRemains, function()
	NuclearRemainsDestroyed = true
	ExtractVolkov = player.AddPrimaryObjective("Extract Volkov from the area.")
	player.MarkCompletedObjective(DestroyRemains)	
	VolkovExtraction()
	Trigger.AfterDelay(DateTime.Seconds(15), function()
		HindFlavorExit()
	end)
end)

HindFlavorExit = function()
	Utils.Do(FlavorHinds, function(h)
		h.Move(HindExitPoint.Location)
		h.Destroy()
	end)
end

VolkovExtraction = function()
	ExtractCam = Actor.Create("camera", true, { Owner = player, Location = tranExtractDst.Location } )
	local flare = Actor.Create("flare", true, { Owner = player, Location = tranExtractDst.Location+CVec.New(0,1) })
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		flare.Destroy()
	end)
	
	tranExtract = Reinforcements.ReinforceWithTransport(player, tranE, nil, tranExtractionPath)[1]
	
	if not Volkov.IsDead then
		Trigger.OnRemovedFromWorld(Volkov, EvacuateHelicopter)
	end

	Trigger.OnKilled(tranExtract, ExtractionFailed)
	Trigger.OnRemovedFromWorld(tranExtract, ExtractionSuccessful)
end

EvacuateHelicopter = function()
	if tranExtract.HasPassengers then
		tranExtract.Move(tranExtractEntry.Location)
		tranExtract.Destroy()
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			ExtractCam.Destroy()
			SendSovReinforcements()
		end)
	end
end

ExtractionSuccessful = function()
	if not tranExtract.IsDead then
		Trigger.AfterDelay(DateTime.Seconds(1), function()	
			player.MarkCompletedObjective(ExtractVolkov)
			--player.MarkCompletedObjective(VolkovSurvive)
			RunAIActivities()
		end)
	end
end

ExtractionFailed = function()
	Media.PlaySpeechNotification(player, "ObjectiveNotMet")
	Trigger.AfterDelay(NotifyDelay, function()
		player.MarkFailedObjective(VolkovSurvive)
		player.MarkFailedObjective(ExtractVolkov)
	end)
end

StartTruksProcedure = function()
	for i=1, #EscapingTruks, 1 do
		Trigger.AfterDelay(DateTime.Seconds(IntervalBetweenTruks*i), function()
			SendRetrievalTruks(EscapingTruks[i])
		end)
	end
end

SendRetrievalTruks = function(truk)
	if not truk.IsDead then
		if NotifySiteRetrieval == true then
			Media.DisplayMessage("An allied special alloy truck has started moving towards the site.","Command")
		end
		truk.Move(TrukNuclearRetrieval.Location)
		Trigger.OnEnteredFootprint({TrukNuclearRetrieval.Location}, function(a,id)
			if a == truk and not a.IsDead then
				Trigger.RemoveFootprintTrigger(id)
				Utils.Do(EscapePath, function(path)
					a.Move(path.Location)
				end)
			end
		end)
	end
end

Trigger.OnEnteredFootprint({EscapeRoute.Location}, function(a)
	if a.Type == "truk.mission" and not a.IsDead then
		Trigger.AfterDelay(5, function()
			if not a.IsDead then
				a.Destroy()
				EscapedTruks = EscapedTruks+1
			end
		end)
	end
end)

SendSovReinforcements = function()
	mainPhaseStarted = true
	RadiationMessage = "High levels of radiation detected. Careful!"
	
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	player.Cash = PlayerCash
	
	Reinforcements.Reinforce(player, {"3tnk"}, SovPath1, 1)
	Reinforcements.Reinforce(player, {"3tnk"}, SovPath2, 1)
	Reinforcements.Reinforce(player, {"3tnk"}, SovPath3, 1)
	Trigger.AfterDelay(DateTime.Seconds(3), function()
		Reinforcements.Reinforce(player, {"mcv"}, SovPath4, 1)
		Camera.Position = PUnitsDst2.CenterPosition + WVec.New(-2*1024,2*1024,0)
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			MinelayerEscape()
		end)
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			DestroyAllies = player.AddPrimaryObjective("Destroy all allied units in the area.")
			PreventTruksEscape = player.AddSecondaryObjective("Prevent escape of allied retrieval trucks.")
			EnableSecondaryObj = true
		end)
	end)	
end

MinelayerEscape = function()
	if not EscapingMinelayer.IsDead then
		EscapingMinelayer.Move(MinelayerEscapeDst.Location)
		EscapingMinelayer.Destroy()
	end
end

RunAIActivities = function()
	enemy.Cash = 30000

	GiveCashToAI()

	ProduceInfantry()
	ProduceArmor()
	
	Trigger.AfterDelay(DateTime.Minutes(8), OoMAtk)
	
	SetBuildingsRepairs()
	
	Trigger.AfterDelay(TranAttackInitDelay, function()
		SendTranAttack()
	end)
	
	Trigger.AfterDelay(SendSecondaryTrucksDelay, function()
		StartTruksProcedure()
	end)

	BuildBase()
	MineFieldCleared()
end

AlertTentInf = function()
	Utils.Do(TentInfantry, function(u)
		if not u.IsDead then
			Trigger.OnDamaged(u, function()
				if TentInfAlerted == false then 
					TentInfAlerted = true
					SendAlertInf()
				end
			end)
		end
	end)
end

SendAlertInf = function()
	Utils.Do(TentInfantry, function(u)
		IdleHunt(u)
	end)
end

SecondVolkovInsertion = function()
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	
	Volkov = Reinforcements.ReinforceWithTransport(player, tranI, {"volk"}, SecondVolkInsertionPath, {SecondVolkInsertionPath[1]})[2][1]
	
	Media.DisplayMessage("Great job commander. Volkov has returned to help you get rid of the allies.","Command")
	
	Trigger.OnKilled(Volkov, function()
		Trigger.AfterDelay(NotifyDelay, function()
			player.MarkFailedObjective(VolkovSurvive)
		end)
	end)
end

--Remove this when harv works properly
GiveCashToAI = function()
	enemy.Cash = 30000
	
	Trigger.AfterDelay(DateTime.Minutes(1), function()
		GiveCashToAI()
	end)
end

HindFlavorEntry = function()
	
	HindFlavor1 = Actor.Create("hind.protector", true, { Owner = badguy, Location = HindExitPoint.Location + CVec.New(-2,0) })
	HindFlavor2 = Actor.Create("hind.protector", true, { Owner = badguy, Location = HindExitPoint.Location })
	HindFlavor3 = Actor.Create("hind.protector", true, { Owner = badguy, Location = HindExitPoint.Location + CVec.New(1,0)})
	
	table.insert(FlavorHinds,HindFlavor1)
	table.insert(FlavorHinds,HindFlavor2)
	table.insert(FlavorHinds,HindFlavor3)
	
	for i=1, 3, 1 do
		FlavorHinds[i].Move(AircraftPos[i])
	end
end

Tick = function()
	enemy.Resources = enemy.Resources - (0.01 * enemy.ResourceCapacity / 25)
	
	UnitsToDamage = Map.ActorsInCircle(Emission.CenterPosition, WDist.FromCells(15), function(a) 
		return a.HasProperty("Health") 
		and a.Type ~= "yak" 
		and a.Type ~= "mig"
		and a.Type ~= "hind"
		and a.Type ~= "hind.protector"
		and a.Type ~= "heli.protector" 
		and a.Type ~= "tran.insertion" 
		and a.Type ~= "bio" 
		and a.Type ~= "brik" 
		and a.Type ~= "truk.mission"
		and a.Type ~= "badr"
		and a.Type ~= "u2"
		and a.Type ~= "boxes07" 
		and a.Type ~= "boxes03" 
		and not a.HasProperty("StartBuildingRepairs")
	end)
	
	Utils.Do(UnitsToDamage, function(unit)
		if not unit.IsDead then
			if unit.Owner == player and RadiationHeadsUpCooldown == 0 then
				RadiationHeadsUpCooldown = 1
				Media.DisplayMessage(RadiationMessage,"Command")
				Media.PlaySound("geiger.wav")
			end
			unit.Health = unit.Health-160
		end
	end)
		
	if RadiationHeadsUpCooldown > 0 then
		RadiationHeadsUpCooldown = RadiationHeadsUpCooldown+1
		if RadiationHeadsUpCooldown > 1000 then
			RadiationHeadsUpCooldown = 0
		end
	end
	
	if RetrievalTranReady == true and FirstRetrievalTrukReady == true then
		RetrievalTranReady = false
		FirstRetrievalTrukReady = false
		RetrievalTranExit()
	end
	
	if player.HasNoRequiredUnits() and mainPhaseStarted == true then
		mainPhaseStarted = false
		Trigger.AfterDelay(NotifyDelay, function()
			enemy.MarkCompletedObjective(DenySoviets)
		end)
	end
	
	if goodguy.HasNoRequiredUnits() and enemy.HasNoRequiredUnits() and mainPhaseStarted == true then
		mainPhaseStarted = false
		Trigger.AfterDelay(NotifyDelay, function()
			player.MarkCompletedObjective(DestroyAllies)
			player.MarkCompletedObjective(VolkovSurvive)
		end)
	end
	
	if EnableSecondaryObj == true then
		if DestroyedTrucks == 9 then
			DestroyedTrucks = -1
			EnableSecondaryObj = false
			player.MarkCompletedObjective(PreventTruksEscape)
			Media.PlaySpeechNotification(player, "ObjectiveMet")
			Trigger.AfterDelay(NotifyDelay, function()
				SecondVolkovInsertion()
			end)
		end
	end
	
	if EscapedTruks > 0 and mainPhaseStarted == true then
		EscapedTruks = -1
		player.MarkFailedObjective(PreventTruksEscape)
	end
end

InitTriggers = function()
	Camera.Position = StartingCameraLoc.CenterPosition
	EscortControl()
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		VolkovInsertion()
	end)

	Trigger.AfterDelay(DelayOfControlTruks, function()
		SendControlTruks()
	end)
	
	AlertTentInf()
	
	Utils.Do(EscapingTruks, function(t)
		Trigger.OnKilled(t, function()
			DestroyedTrucks = DestroyedTrucks+1
		end)
	end)
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
    
    DestroyRemains = player.AddPrimaryObjective("Destroy the ruined nuclear facility before it's sacked.")
	VolkovSurvive = player.AddPrimaryObjective("Volkov must survive.")
	
	DenySoviets = enemy.AddPrimaryObjective("Deny the soviets.")
    
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "MissionFailed")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "MissionAccomplished")
	end)
end

SetupDifficulty = function()
	if RemoveExtraUnits < 1 then
		Utils.Do(RemoveOnDifficultyTanks, function(x)
			x.Destroy()
		end)
	end
	if RemoveExtraUnits < 2 then
		Utils.Do(RemoveOnDifficultyGuns, function(x)
			x.Destroy()
		end)
	end
end

DebugDifficulty = function()
	Media.Debug(Map.LobbyOption("difficulty"))
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	badguy = Player.GetPlayer("BadGuy")
	enemy = Player.GetPlayer("Greece")
	goodguy = Player.GetPlayer("GoodGuy")

	InitTriggers()
	InitObjectives()
	SetupDifficulty()
	--DebugDifficulty()
end