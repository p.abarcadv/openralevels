if Map.LobbyOption("difficulty") == "easy" then
	InfAtkCount = 7
	ArmorAtkCount = 5
	AlliedVehicleTypes = {"2tnk", "1tnk", "1tnk", "jeep", "arty"}
	OoMAtkSwitch = true
elseif Map.LobbyOption("difficulty") == "normal" then
	InfAtkCount = 8
	ArmorAtkCount = 6
	AlliedVehicleTypes = {"2tnk", "2tnk", "1tnk", "1tnk", "jeep", "arty"}
	OoMAtkSwitch = true
else
	InfAtkCount = 12
	ArmorAtkCount = 8
	AlliedVehicleTypes = {"2tnk", "2tnk", "2tnk", "1tnk", "jeep", "arty", "arty"}
	OoMAtkSwitch = true
end

AttackTranDsts = 
{ 
AttackTranDst1.Location, AttackTranDst2.Location, AttackTranDst3.Location, AttackTranDst4.Location
}

MFBMFDPathCleared = false
MFCMFDPathCleared = false
MFFPathCleared = false

heliAdded = false
heliCount = 0

AircraftPos = {HeliPost1.Location,HeliPost2.Location,HeliPost3.Location}

AttackPaths =
{
	{MainPath1, Path3, Path1}
}

AlliedInfTypes = {"e1", "e1", "e3"}

ArmorAttack = { }
InfAttack = { }

--New paths for AI attacks open if mine clumps are cleared
MineFieldCleared = function()
	MFB = Map.ActorsInCircle(MinefieldB.CenterPosition, WDist.FromCells(6), function(m) 
		return m.Type == "minv"
	end)
	
	MFC = Map.ActorsInCircle(MinefieldC.CenterPosition, WDist.FromCells(6), function(m) 
		return m.Type == "minv"
	end)
	
	MFD = Map.ActorsInCircle(MinefieldD.CenterPosition, WDist.FromCells(6), function(m) 
		return m.Type == "minv"
	end)
	
	MFE = Map.ActorsInCircle(MinefieldE.CenterPosition, WDist.FromCells(6), function(m) 
		return m.Type == "minv"
	end)

	MFF = Map.ActorsInCircle(MinefieldF.CenterPosition, WDist.FromCells(6), function(m) 
		return m.Type == "minv"
	end)
	
	if #MFB == 0 and #MFD == 0 and MFBMFDPathCleared == false then
		MFBMFDPathCleared = true
		table.insert(AttackPaths,{MainPath1, MainPath2, MainPath3, Path2})
	end

	if #MFC == 0 and #MFD == 0 and MFCMFDPathCleared == false then
		MFCMFDPathCleared = true
		table.insert(AttackPaths,{MainPath1, MainPath2, MainPath3, BotTarget})
	end

	if #MFF == 0 and MFFPathCleared == false then
		MFFPathCleared = true
		table.insert(AttackPaths,{MainPath1, MainPath2, Path1})
	end
		
	if #MFB == 0 and #MFC == 0 and #MFD == 0 and #MFE == 0 and #MFF == 0 then
		return
	else
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			MineFieldCleared()
		end)
	end
end

SendTranAttack = function()
	local delay = Utils.RandomInteger(10, 60)
	local dst = Utils.Random(AttackTranDsts)

	local tranInf = Reinforcements.ReinforceWithTransport(enemy, tranI, {"e3", "e3", "e3", "e3", "e3"}, {AttackTranEntry.Location, dst}, {AttackTranEntry.Location})[2]
	
	Utils.Do(tranInf, function(u)
		if not u.IsDead then
			Trigger.OnAddedToWorld(u, function()
				IdleHunt(u)
			end)
		end
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(300+delay), function()
		SendTranAttack()
	end)
end

EscortControl = function()
	--[[
	heliCount = 3
	CheckHelis()
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		SendHeliEscort(HeliEntry1.Location, HeliPost1.Location)
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			SendHeliEscort(HeliEntry2.Location, HeliPost2.Location)
			Trigger.AfterDelay(DateTime.Seconds(1), function()
				SendHeliEscort(HeliEntry3.Location, HeliPost3.Location)
			end)
		end)
	end)
	]]
end

OoMAtk = function()
	if OoMAtkSwitch == false then
		return
	else
		local entry = EscapeRoute.Location
		local Path = Utils.Random(AttackPaths)
		local AtkType = {"2tnk", "2tnk", "2tnk", "arty"}
		
		local unitsGrp1 = Reinforcements.Reinforce(enemy, AtkType, {entry+CVec.New(0, -1), entry+CVec.New(-5, -1)}, 5)
		local unitsGrp2 = Reinforcements.Reinforce(enemy, AtkType, {entry+CVec.New(0, 1), entry+CVec.New(-5, 1)}, 5)
		
		Trigger.AfterDelay(DateTime.Seconds(10), function()
			SendUnits(unitsGrp1, Path)
			SendUnits(unitsGrp2, Path)
		end)
		
		Trigger.AfterDelay(DateTime.Minutes(12),OoMAtk)
	end
end

SendHeliEscort = function(entry, dst)
	local heli = Reinforcements.Reinforce(enemy, {"heli.protector"}, {entry, entry}, 60)[1]
	Trigger.AfterDelay(1, function()
		heli.AttackMove(dst)
	end)
	
	Trigger.OnKilled(heli, function()
		Trigger.ClearAll(heli)
		heliCount = heliCount-1
	end)
	
	Trigger.OnRemovedFromWorld(heli, function()
		heliCount = heliCount-1
		Trigger.ClearAll(heli)
	end)
end

CheckHelis = function()
	--Media.Debug(tostring(heliCount) .. " Heli count ")
	
	local helis = Utils.Where(Map.ActorsInWorld, function(self) return self.Type == "heli.protector" end)
	Utils.Do(helis, function(h)
		if h.AmmoCount() == 0 then
			--h.Reload("primary",1)
			h.Move(HeliEntry2.Location)
			h.Destroy()
		end
	end)

	if heliCount == 0 then
		Trigger.AfterDelay(DateTime.Seconds(5), EscortControl)
	else
		Trigger.AfterDelay(DateTime.Seconds(5), CheckHelis)
	end
end

SetBuildingsRepairs = function()
	local buildings = Utils.Where(Map.ActorsInWorld, function(self) return self.Owner == enemy and self.HasProperty("StartBuildingRepairs") end)
	Utils.Do(buildings, function(actor)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if EFact.IsDead or EFact.Owner ~= enemy then 
			return
		end
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EFactLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost
		building.exists = true
		--Media.Debug("Building ready - ".. building.type)
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function()
	if ETent.IsDead or ETent.Owner ~= enemy then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(AlliedInfTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		
		InfAttack[#InfAttack + 1] = unit[1]
		if #InfAttack >= InfAtkCount then
			SendUnits(InfAttack, Path)
			InfAttack = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceArmor = function()
	if EWeap.IsDead or EWeap.Owner ~= enemy then
		return
	--elseif baseharv.IsDead and enemy.Resources <= 599 then
	--	return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(AlliedVehicleTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		ArmorAttack[#ArmorAttack + 1] = unit[1]

		if #ArmorAttack >= ArmorAtkCount then
			SendUnits(ArmorAttack, Path)
			ArmorAttack = { }
			Trigger.AfterDelay(DateTime.Minutes(2), ProduceArmor)
		else
			Trigger.AfterDelay(delay, ProduceArmor)
		end
	end)
end

GiveCashToAI = function()
	enemy.Cash = 30000
	
	Trigger.AfterDelay(DateTime.Minutes(1), function()
		GiveCashToAI()
	end)
end