Power1	= { type = "apwr", pos = CVec.New(-17,3), cost = 500, exists = true }
Power2	= { type = "apwr", pos = CVec.New(-14,3), cost = 500, exists = true }
Power3	= { type = "apwr", pos = CVec.New(-11,3), cost = 500, exists = true }
Power4	= { type = "powr", pos = CVec.New(-8,2), cost = 300, exists = true }
Power5	= { type = "apwr", pos = CVec.New(-7,-1), cost = 500, exists = true }

Power6	= { type = "apwr", pos = CVec.New(-19,-7), cost = 500, exists = true }
Power7	= { type = "apwr", pos = CVec.New(-19,-4), cost = 500, exists = true }
Power8	= { type = "apwr", pos = CVec.New(-16,-6), cost = 500, exists = true }
Power9	= { type = "powr", pos = CVec.New(-15,-3), cost = 300, exists = true }
Power10	= { type = "apwr", pos = CVec.New(-13,-5), cost = 500, exists = true }

Power11	= { type = "apwr", pos = CVec.New(11,2), cost = 500, exists = true }
Power12	= { type = "apwr", pos = CVec.New(9,6), cost = 500, exists = true }
Power13	= { type = "powr", pos = CVec.New(13,5), cost = 300, exists = true }
Power14	= { type = "apwr", pos = CVec.New(8,11), cost = 500, exists = true }

--Correct
Tent1	= { type = "tent", pos = CVec.New(-18,7), cost = 500, exists = true }
Tent2	= { type = "tent", pos = CVec.New(15,9), cost = 500, exists = true }
Weap	= { type = "weap", pos = CVec.New(-14,9), cost = 2000, exists = true }

Ref		= { type = "proc", pos = CVec.New(0,-8), cost = 1500, exists = true } 
--Corrected Gotta Recheck
Silo1	= { type = "silo", pos = CVec.New(2,-8), cost = 150, exists = true }
Silo2	= { type = "silo", pos = CVec.New(3,-8), cost = 150, exists = true }
Silo3	= { type = "silo", pos = CVec.New(4,-8), cost = 150, exists = true }

--Correct
Dome	= { type = "dome", pos = CVec.New(-5,3), cost = 1000, exists = true }
--Correct
Fix		= { type = "fix", pos = CVec.New(-6,-5), cost = 1000, exists = true }
--Correct
ATek	= { type = "atek", pos = CVec.New(-27,-1), cost = 2000, exists = true }

--Corrected Gotta Recheck
Hpad1	= { type = "hpad", pos = CVec.New(-29,3), cost = 1000, exists = true }
Hpad2	= { type = "hpad", pos = CVec.New(-26,3), cost = 1000, exists = true }
Hpad3	= { type = "hpad", pos = CVec.New(-23,-1), cost = 1000, exists = true }

BaseBuildings =
{
	Power1,
	Power2,
	Power3,
	Power4,
	Power5,
	Power6,
	Power7,
	Power8,
	Power9,
	Power10,
	Power11,
	Power12,
	Power13,
	Power14,
	Tent1,
	Tent2,
	Weap,
	Ref,
	Hpad1,
	Hpad2,
	Hpad3,
	ATek,
	Fix,
	Dome,
	Silo1,
	Silo2,
	Silo3
}

Trigger.OnKilled(BasePower1, function()
	Power1.exists = false
end)

Trigger.OnKilled(BasePower2, function()
	Power2.exists = false
end)

Trigger.OnKilled(BasePower3, function()
	Power3.exists = false
end)

Trigger.OnKilled(BasePower4, function()
	Power4.exists = false
end)

Trigger.OnKilled(BasePower5, function()
	Power5.exists = false
end)

Trigger.OnKilled(BasePower6, function()
	Power6.exists = false
end)

Trigger.OnKilled(BasePower7, function()
	Power7.exists = false
end)

Trigger.OnKilled(BasePower8, function()
	Power8.exists = false
end)

Trigger.OnKilled(BasePower9, function()
	Power9.exists = false
end)

Trigger.OnKilled(BasePower10, function()
	Power10.exists = false
end)

Trigger.OnKilled(BasePower11, function()
	Power11.exists = false
end)

Trigger.OnKilled(BasePower12, function()
	Power12.exists = false
end)

Trigger.OnKilled(BasePower13, function()
	Power13.exists = false
end)

Trigger.OnKilled(BasePower14, function()
	Power14.exists = false
end)

Trigger.OnKilled(BaseTent1, function()
	Tent1.exists = false
end)

Trigger.OnKilled(BaseTent2, function()
	Tent2.exists = false
end)

Trigger.OnKilled(BaseWeap, function()
	Weap.exists = false
end)

Trigger.OnKilled(BaseRef, function()
	Ref.exists = false
end)

Trigger.OnKilled(BaseSilo1, function()
	Silo1.exists = false
end)

Trigger.OnKilled(BaseSilo2, function()
	Silo2.exists = false
end)

Trigger.OnKilled(BaseSilo3, function()
	Silo3.exists = false
end)

Trigger.OnKilled(BaseHpad1, function()
	Hpad1.exists = false
end)

Trigger.OnKilled(BaseHpad2, function()
	Hpad2.exists = false
end)

Trigger.OnKilled(BaseHpad3, function()
	Hpad3.exists = false
end)

Trigger.OnKilled(BaseATek, function()
	ATek.exists = false
end)

Trigger.OnKilled(BaseFix, function()
	Fix.exists = false
end)

Trigger.OnKilled(BaseDome, function()
	Dome.exists = false
end)



