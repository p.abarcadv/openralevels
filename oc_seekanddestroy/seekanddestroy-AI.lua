AlliedHeliTypes = {"mh60.autotarget", "heli.autotarget"}
HeliCount = 3
--HeliAtk = {}

AlliedVehlTypes = {"jeep","2tnk","1tnk","arty"}
VehlCount = 5
VehlAtk = {}

AlliedInfantryTypes = {"e1", "e1", "e1", "e3", "e3"}
InfCount = 10
InfAtk = {}

AlliedNavalTypes = {"pt", "dd", "ca"}
NavalCount = 5
NavalAtk = {}

BaseHelis =
{
DefenceHeli1,
DefenceHeli2,
DefenceHeli3
}

BaseHpads =
{
BaseHpad1,
BaseHpad2,
BaseHpad3
}
--

HeliAtkPath =
{
HeliAtkEntry1.Location,
HeliAtkEntry2.Location,
HeliAtkEntry3.Location
}

VesselAtks = { "pt", "pt","dd", "dd", "ca" }
WaterAtkPaths =
{
{ NavalAtk0, NavalAtk1A, NavalAtk2A, NavalAtk3A },
{ NavalAtk0, NavalAtk1B, NavalAtk2B },
{ NavalAtk0, NavalAtk1C, NavalAtk2C, NavalAtk3C }
}

DefendingHelix = { DefenceHeli1, DefenceHeli2, DefenceHeli3 }
HPads = { BaseHpad1, BaseHpad2, BaseHpad3 }

HeliFlavor = function()
	local landings = HPads
	local aircrafts = DefendingHelix
	
	Utils.Do(DefendingHelix, function(h)
		for i = 1, #aircrafts, 1 do
			aircrafts[i].ReturnToBase(landings[i])
		end
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end

IdleHeliHunt = function(unit, way) 
	if not unit.IsDead then
		--Trigger.OnIdle(unit, function()
			unit.Hunt()
		--end
		--Trigger.OnIdle(unit, function()
		--	if unit.AmmoCount() == 0 then
		--		unit.Move(way)
		--		unit.Destroy()
		--	else
		--		unit.Hunt() 
		--	end
		--end)
	end
end

SendHelis = function()
	local origin = Utils.Random(HeliAtkPath)
	local atkHelis = Reinforcements.Reinforce( enemy, { "heli.outofmap", "heli.outofmap" }, { origin, origin + CVec.New(0,5) }, 1 )

	Utils.Do(atkHelis, function(h)
		--h.AttackMove(
		IdleHeliHunt(h,origin)
		
	end)
end

HeliFlavor = function()
	Trigger.AfterDelay(DateTime.Seconds(4), function()
		local landings = BaseHpads
		local aircrafts = BaseHelis
		
		for i = 1, #aircrafts, 1 do
			aircrafts[i].ReturnToBase(landings[i])
		end
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if CYard.IsDead or CYard.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EFactLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
				
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceTanks = function()
	
	
end

ProduceInfantry = function()
	
	
end

ProduceNaval = function()
	if Weap.exists == false --[[or Weap.Owner ~= enemy]] then
		Media.Debug("Production disabled (Weap) - " .. "Weap.exists = " .. tostring(Weap.exists))
		Trigger.AfterDelay(DateTime.Minutes(1), function()
			ProduceNaval()
		end)
		return
		--elseif baseharv.IsDead and enemy.Resources <= 599 then
		--	return
	end
	Media.Debug("Production initiated (Naval) - " .. "Weap.exists = " .. tostring(Tent.exists))
	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(AlliedVehicleTypes) }
	local Path = Utils.Random(LandAtkPaths)
	enemy.Build(toBuild, function(unit)
		VehlAtk[#VehlAtk + 1] = unit[1]
		
		if #VehlAtk >= VehlAtkCount then
			SendUnits(VehlAtk, Path)
			Media.Debug("Tank Atk Send - Path Chosen - ".. tostring(Path))
			VehlAtk = { }
			Trigger.AfterDelay(DateTime.Minutes(1), ProduceTanks)
		else
			Trigger.AfterDelay(delay, ProduceTanks)
		end
	end)
	
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

ConstantLandings = function()
	charge = charge + 1
	Media.Debug("Charge value is: " ..tostring(charge))
	if charge == chargeMax then
		charge = 0
		for x = 1, chargeMax, 1 do
			PrepareLSTAttack()
		end
	else
		PrepareLSTAttack()
	end
	
	Trigger.AfterDelay(landingsDelay, function()
		ConstantLandings()
	end)
end

NavalForceAtk = function()
	local units = Reinforcements.Reinforce(enemy, {"pt", "pt","dd", "dd", "ca"}, { AlliedLstEntry.Location, AlliedLstEntry.Location + CVec.New(0,2) }, 1)
	local waypoints = Utils.Random(WaterAtkPaths)
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		Utils.Do(units, function(unit)
			if not unit.IsDead then
				Utils.Do(waypoints, function(waypoint)
					unit.AttackMove(waypoint.Location)
				end)
				IdleHunt(unit)
			end
		end)
	end)
end