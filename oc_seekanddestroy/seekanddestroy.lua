if Map.LobbyOption("difficulty") == "easy" then
	charge = 0
	chargeMax = 5
	landingsDelay = DateTime.Minutes(5)
	StartingCash = 7000
	AIDelay = DateTime.Minutes(3)
elseif Map.LobbyOption("difficulty") == "normal" then
	charge = 0
	chargeMax = 4
	landingsDelay = DateTime.Minutes(4)
	StartingCash = 5000
	AIDelay = DateTime.Minutes(2)
else
	charge = 3
	chargeMax = 4
	landingsDelay = DateTime.Minutes(3)
	StartingCash = 5000
	AIDelay = DateTime.Minutes(1)
end

seaPatrol1 = {}
seaPatrol2 = {}
seaPatrol3 = {}
seaPatrol1Path = { }
seaPatrol2Path = { }
seaPatrol3Path = { }

AlliedLSTPaths =
{
{AlliedLstEntry.Location, BeachDestiny1.Location},
{AlliedLstEntry.Location, BeachDestiny2.Location},
{AlliedLstEntry.Location, BeachDestiny3.Location},
{AlliedLstEntry.Location, BeachDestiny4.Location}
}

SovNavalEntry = 
{
{ PlayerLstEntry1.Location, PlayerLstDst1.Location },
{ PlayerLstEntry1.Location, PlayerLstDst2.Location },
{ SubEntry1.Location, SubDst1.Location },
{ SubEntry2.Location, SubDst2.Location }
}

AlliedLSTOccupants =
{
{"e1","e1","1tnk","2tnk","2tnk"},
{"e3","e3","e3","arty","arty"},
{"e1","e1","e3","e3","1tnk"},
{"e3","e3","jeep","jeep","2tnk"}
}

MainInitForce = {"3tnk", "3tnk", "3tnk", "e2", "e2"}
MCVForce = {"e1", "e1", "e1", "mcv", "v2rl"}

NavalYards = { Naval1, Naval2, Naval3 }

gameStarted = false

Trigger.OnAllKilled(NavalYards, function()
	player.MarkCompletedObjective(DestroNavalYards)
end)

SendInitUnits = function()
	Reinforcements.Reinforce(player, {"ss"}, SovNavalEntry[3], 1)
	Reinforcements.Reinforce(player, {"ss"}, SovNavalEntry[4], 1)
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		gameStarted = true
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		Reinforcements.ReinforceWithTransport(player, "lst.in", MainInitForce, SovNavalEntry[1],{SovNavalEntry[1][1]})
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			Reinforcements.ReinforceWithTransport(player, "lst.in", MCVForce, SovNavalEntry[1],{SovNavalEntry[2][1]})
		end)
	end)
end

PrepareLSTAttack = function()
	local unitComp = Utils.Random(AlliedLSTOccupants)
	local path = Utils.Random(AlliedLSTPaths)
	
	SendLSTAttack(unitComp, path)
end

SendLSTAttack = function(units, route)
	local lstAttack = Reinforcements.ReinforceWithTransport(enemy, "lst", units, route, {route[1]})[2]
	Utils.Do(lstAttack, function(unit)
		IdleHunt(unit)
	end)
end

RunAI = function()
	BuildBase()
	
	SendHelis()
	NavalForceAtk()
	
	HeliFlavor()
	
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		ConstantLandings()
	end)
end

Tick = function()
	if gameStarted == true then
		if player.HasNoRequiredUnits() then
			enemy.MarkCompletedObjective(AlliedObj)
		end
	end
	if enemy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(DestroyAllies)
	end
	
end

InitTriggers = function()
	Camera.Position = StartingPosition.CenterPosition
	
	player.Cash = StartingCash
	
	SendInitUnits()
	
	Trigger.AfterDelay(AIDelay, function()
		RunAI()
	end)
end

InitObjectives = function()

	DestroyAllies = player.AddPrimaryObjective("Destroy the allied Naval Base.") --x
	DestroNavalYards = player.AddSecondaryObjective("Destroy allied naval yards to reduce their naval offensive.")

	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	--BribeTheCivs = player.AddSecondaryObjective("Avoid attacking inhabitants of the area.")
	
	AlliedObj =  enemy.AddPrimaryObjective("Eliminate all soviet presence in the area.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	
	InitObjectives()
	InitTriggers()
end