if Map.LobbyOption("difficulty") == "easy" then
	extraUnits = true
elseif Map.LobbyOption("difficulty") == "normal" then
	extraUnits = true
else	
	extraUnits = false
end

UnitsDeployed = false

UnDamaged1 = true
UnDamaged2 = true
UnDamaged3 = true

RescuableBuildings = { RescuableHPad, RescuablePwr, RescuableHind, barb1, barb2, barb3, barb4, barb5, barb6, barb7, barb8, barb9, barb10, barb11, barb12, barb13, barb14 }
RescuableUnits = { Scientist, HelperMedic }

AllCivTargets = { Civ1, Civ2, Civ3, Civ4, Civ5, Civ6, Civ7, Civ8, Civ9, Civ10, Civ11, Civ12, Civ13, Civ14, Civ15 }
AllCivBuildingTargets = { CivBuilding1, CivBuilding2, CivBuilding3, CivBuilding4, CivBuilding5, CivBuilding6, CivBuilding7, CivBuilding8, CivBuilding9, CivBuilding10, CivBuilding11, CivBuilding12, CivBuilding13, CivBuilding14, CivBuilding15, CivBuilding16, CivBuilding17 }

FindBaseTriggerArea = { CPos.New(49,80), CPos.New(27,75), CPos.New(27,76), CPos.New(34,76) }

ReserveGroupA = {ReserveInfA1, ReserveInfA2, ReserveInfA3, ReserveInfA4, ReserveUnit1}
ReserveGroupB = {ReserveInfB1, ReserveInfB2, ReserveInfB3, ReserveInfB4, ReserveUnit2, ReserveUnit4}
ReserveGroupC = {ReserveInfC1, ReserveInfC2, ReserveInfC3, ReserveInfC4, ReserveUnit3, ReserveUnit5}

FindScientistTriggerArea = 
{ 
CPos.New(98,55), CPos.New(98,56), CPos.New(99,56), CPos.New(100,56), CPos.New(102,56), CPos.New(102,57), CPos.New(98,46), CPos.New(100,48), CPos.New(100,49), CPos.New(100,51), CPos.New(101,51), CPos.New(102,51), CPos.New(103,51), CPos.New(104,51), CPos.New(105,51), CPos.New(106,51), CPos.New(107,51), CPos.New(108,51)
}

CivGroup1 = { Civ2, Civ3, Civ4, Civ5, Civ6 }
CivGroup2 = { Civ7, Civ8, Civ9, Civ10 }
CivGroup3 = { Civ11, Civ12, Civ13, Civ14, Civ15 }

Village1 = { CivBuilding3, CivBuilding4, CivBuilding5, CivBuilding6, CivBuilding7, CivBuilding8, Civ2, Civ3, Civ4, Civ5, Civ6 }
Village2 = { CivBuilding9, CivBuilding10, CivBuilding11, CivBuilding12, Civ7, Civ8, Civ9, Civ10 }
Village3 = { CivBuilding13, CivBuilding14, CivBuilding15, CivBuilding16, CivBuilding17,Civ11, Civ12, Civ13, Civ14, Civ15 }

DefineDamage = function()
	Utils.Do(Village1, function(building)
		Trigger.OnDamaged(building, function(self,a)
			if a.Owner == player and UnDamaged1 == true then
				CivsEscape(CivGroup1)
				UnDamaged1 = false
			end
		end)
	end)
	
	Utils.Do(Village2, function(building)
		Trigger.OnDamaged(building, function(self,a)
			if a.Owner == player and UnDamaged2 == true then
				CivsEscape(CivGroup2)
				UnDamaged2 = false
			end
		end)
	end)

	Utils.Do(Village3, function(building)
		Trigger.OnDamaged(building, function(self,a)
			if a.Owner == player and UnDamaged3 == true then
				CivsEscape(CivGroup3)
				UnDamaged3 = false
			end
		end)
	end)
end

CivsEscape = function(civs)
	Utils.Do(civs, function(actor)
		if not actor.IsDead then
			actor.Move(civsDestiny.Location)
		end
	end)
end

Trigger.OnExitedProximityTrigger(civsDestiny.CenterPosition, WDist.FromCells(2),  function(a, id)
	if a.Type == "c1" or a.Type == "c2" or a.Type == "c3" or a.Type == "c4" or a.Type == "c5" or a.Type == "c6" then 
		if not a.IsDead then
			a.Move(civsDestiny.Location)
		end
	end
end)

Trigger.OnEnteredProximityTrigger(civsDestiny.CenterPosition, WDist.FromCells(2),  function(a, id)
	if a == Civ2 or a == Civ3 or a == Civ4 or a == Civ5 or a == Civ6 then 
		Trigger.RemoveProximityTrigger(id)
		SendReserve(ReserveGroupA, RescueGroupDstA)
	end
end)

Trigger.OnEnteredProximityTrigger(civsDestiny.CenterPosition, WDist.FromCells(2),  function(a, id)
	if a == Civ7 or a == Civ8 or a == Civ9 or a == Civ10 then 
		Trigger.RemoveProximityTrigger(id)
		SendReserve(ReserveGroupB, RescueGroupDstB)
	end
end)

Trigger.OnEnteredProximityTrigger(civsDestiny.CenterPosition, WDist.FromCells(2),  function(a, id)
	if a == Civ11 or a == Civ12 or a == Civ13 or a == Civ14 or a == Civ15 then 
		Trigger.RemoveProximityTrigger(id)
		SendReserve(ReserveGroupC, RescueGroupDstC)
	end
end)

Trigger.OnEnteredProximityTrigger(OutpostCam.CenterPosition, WDist.FromCells(12),  function(a, id)
	if a.Owner == player and a.Type ~= "waypoint" then 
		Trigger.RemoveProximityTrigger(id)
		if Map.LobbyOption("difficulty") ~= "hard" then
			local outCam = Actor.Create("camera.medium", true, { Owner = player, Location = OutpostCam.Location })
			
			Trigger.AfterDelay(DateTime.Seconds(12), function()
				outCam.Destroy()
			end)
		end
	end
end)

SendReserve = function(units, loc)
	Utils.Do(units, function(actor)
		if not actor.IsDead then
			actor.AttackMove(loc.Location)
		end
	end)
end

--GENERAL PURPOSE
TransferOwnership = function(actors)
	Utils.Do(actors, function(actor)
		if not actor.IsDead then
			actor.Owner = player
		end
	end)
end

IntroSequence = function()
	Reinforcements.Reinforce(player, {"apc.flamegroup"}, {SovAPCEntry1.Location, SovAPCDst1.Location},0)	
	Reinforcements.Reinforce(player, {"apc.flamegroup"}, {SovAPCEntry3.Location, SovAPCDst3.Location},0)
	if extraUnits == true then
		Reinforcements.Reinforce(player, {"apc.standard"}, {SovAPCEntry2.Location, SovAPCDst2.Location},0)
	end

	Trigger.AfterDelay(1, function()
		UnitsDeployed = true
	end)
end

Trigger.OnEnteredProximityTrigger(AbandonedHosp.CenterPosition, WDist.FromCells(10), function(a, id)
	if Map.LobbyOption("difficulty") ~= "hard" then
		if a.Owner == player then
			Trigger.RemoveProximityTrigger(id)
			local facilityCam = Actor.Create("camera.medium", true, { Owner = player, Location = AbandonedHosp.Location + CVec.New(0,3) })
			Trigger.AfterDelay(DateTime.Seconds(10), function()
				facilityCam.Destroy()
			end)
		end
	end
end)

--HELIPORT
RescueBuildings = function()
	TransferOwnership(RescuableBuildings)
	player.MarkCompletedObjective(FindHeliport)
end


Trigger.OnEnteredFootprint(FindBaseTriggerArea, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		RescueBuildings()
	end
end)

--SCIENTIST
Trigger.OnEnteredFootprint(FindScientistTriggerArea, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		TransferOwnership(RescuableUnits)
		player.MarkCompletedObjective(FindSurvivors)
		ScientistEvacuation()
	end
end)

ScientistEvacuation = function()
	EvacuateScientist = player.AddSecondaryObjective("Bring the scientist to the heliport for evacuation.")
	
	Trigger.OnKilled(Scientist, function()
		ExtractionFailed()
		if tran then
			tran.Move(TranEntryExit.Location)
			tran.Destroy()
		end
	end)
	
	Trigger.OnEnteredProximityTrigger(RescuableHPad.CenterPosition, WDist.FromCells(5), function(a, id)
		if a.Type == "scientist" then
			Trigger.RemoveProximityTrigger(id)
			SendExtractionTran()
			local flare = Actor.Create("flare", true, { Owner = player, Location = ExtractionPoint.Location })
			Trigger.AfterDelay(DateTime.Seconds(12), function()
				flare.Destroy()
			end)
		end
	end)
end

SendExtractionTran = function()
	tran = Reinforcements.ReinforceWithTransport(player, "tran.extraction", nil, {TranEntryExit.Location, ExtractionPoint.Location})[1]
	
	if not Scientist.IsDead then
		Trigger.OnRemovedFromWorld(Scientist, EvacuateHelicopter)
	end

	Trigger.OnKilled(tran, ExtractionFailed)
	Trigger.OnRemovedFromWorld(tran, ExtractionSuccessful)
end

EvacuateHelicopter = function()
	if tran.HasPassengers then
		tran.Move(TranEntryExit.Location)
		tran.Destroy()
	end
end

ExtractionFailed = function()
	Media.PlaySpeechNotification(player, "ObjectiveNotMet")
	player.MarkFailedObjective(EvacuateScientist)
end

ExtractionSuccessful = function()
	if not tran.IsDead then
		Media.PlaySpeechNotification(player, "TargetRescued")
		Trigger.AfterDelay(DateTime.Seconds(1), function()	
			player.MarkCompletedObjective(EvacuateScientist)
		end)
	end
end
--SCIENTIST
Trigger.OnAllKilled(AllCivTargets, function()
	player.MarkCompletedObjective(KillCivs)
end)

Trigger.OnAllKilled(AllCivBuildingTargets, function()
	player.MarkCompletedObjective(DestroyCivs)
end)

Trigger.OnKilled(AbandonedHosp, function()
	player.MarkCompletedObjective(DestroyHosp)
end)

Tick = function()
	if player.HasNoRequiredUnits() and UnitsDeployed == true then
		enemy.MarkCompletedObjective(DenySoviets)
		UnitsDeployed = false
	end
end

DifficultyCheck = function()
	--Maybe use for and ipairs??
	if Map.LobbyOption("difficulty") == "hard" then
		Actor.Create("2tnk", true, { Owner = enemy, Location = AdditionalUnit1.Location, Facing = 64 })

		Actor.Create("1tnk", true, { Owner = enemy, Location = AdditionalUnit2.Location })
		
		Actor.Create("e1.autotarget", true, { Owner = enemy, Location = AdditionalUnit3.Location , SubCell = 1, Facing = 0})
		Actor.Create("e3.autotarget", true, { Owner = enemy, Location = AdditionalUnit3.Location , SubCell = 2, Facing = 0})
		Actor.Create("medi", true, { Owner = enemy, Location = AdditionalUnit3.Location , SubCell = 5, Facing = 0})
		Actor.Create("e1.autotarget", true, { Owner = enemy, Location = AdditionalUnit3.Location , SubCell = 4, Facing = 0})
		
		Actor.Create("2tnk", true, { Owner = enemy, Location = AdditionalUnit4.Location, Facing = 192 })
		
		Actor.Create("e1.autotarget", true, { Owner = enemy, Location = AdditionalUnit5.Location , SubCell = 1, Facing = 0})
		Actor.Create("e3.autotarget", true, { Owner = enemy, Location = AdditionalUnit5.Location , SubCell = 2, Facing = 0})
		Actor.Create("e1.autotarget", true, { Owner = enemy, Location = AdditionalUnit5.Location , SubCell = 5, Facing = 0})
		Actor.Create("medi", true, { Owner = enemy, Location = AdditionalUnit5.Location , SubCell = 4, Facing = 0})
		
		Actor.Create("e1.autotarget", true, { Owner = enemy, Location = AdditionalUnit6.Location , SubCell = 1, Facing = 128})
		Actor.Create("e3.autotarget", true, { Owner = enemy, Location = AdditionalUnit6.Location , SubCell = 2, Facing = 128})
		Actor.Create("e1.autotarget", true, { Owner = enemy, Location = AdditionalUnit6.Location , SubCell = 3, Facing = 128})
	end
	
	if Map.LobbyOption("difficulty") == "easy" then
		OutPBox.Destroy()
		Out2Tnk1.Destroy()
		Out2Tnk2.Destroy()
	end
end


InitTriggers = function()
	Camera.Position = StartingCameraPos.CenterPosition
	
	DifficultyCheck()
	
	RescuableHind.ReturnToBase(RescuableHPad)
	
	IntroSequence()

	DefineDamage()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DestroyCivs = player.AddPrimaryObjective("Raze all civilian buildings.")
	KillCivs = player.AddPrimaryObjective("Eliminate every civilian you find.")
	DestroyHosp = player.AddPrimaryObjective("Locate the medical facility and destroy it.")
	
	FindHeliport = player.AddSecondaryObjective("Find the abandoned heliport.")
	FindSurvivors = player.AddSecondaryObjective("Find any survivor of the accident.")
	
	DenySoviets = enemy.AddPrimaryObjective("Eliminate all soviet presence in the area.")

	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")

	InitTriggers()
	InitObjectives()
end