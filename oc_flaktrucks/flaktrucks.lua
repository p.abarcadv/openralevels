
--NOTES: PROBLEM WITH OBJECTIVES


if Map.LobbyOption("difficulty") == "easy" then
	--AttackIntervals = {2.5, 3}
	InitStartingCash = 2000
	
	SovietReinforcements = {"3tnk", "ftrk", "e1", "e1", "e2"}

	InitTransDelay = DateTime.Minutes(1)
	DelayBetweenTrans = 140
	totalTrans = 9
	DestroyedChoppersRequired = 6
	transPossibleToMiss = 3
	
	HelpingCameras = true
elseif Map.LobbyOption("difficulty") == "normal" then
	--AttackIntervals = {2, 2.5}
	InitStartingCash = 2000
	
	SovietReinforcements = {"ftrk", "e1", "e1", "e2"}

	InitTransDelay = DateTime.Minutes(1)
	DelayBetweenTrans = 120
	totalTrans = 10
	DestroyedChoppersRequired = 8
	transPossibleToMiss = 2
	
	HelpingCameras = true
else
	--AttackIntervals = {1, 1.5, 2}
	InitStartingCash = 1000
	
	SovietReinforcements = {"e1", "e1", "e2"}

	InitTransDelay = DateTime.Minutes(1)
	DelayBetweenTrans = 90
	totalTrans = 11
	DestroyedChoppersRequired = 11
	transPossibleToMiss = 0
	
	HelpingCameras = false
end

HiddenPillboxes = 
{
HardHbox1.Location, HardHbox2.Location, HardHbox3.Location, HardHbox4.Location, HardHbox5.Location, HardHbox6.Location, HardHbox7.Location
}

AlliedBase = { EBasePower1, EBasePower2, EBaseTent, EBaseHPad1, EBaseHPad2}

AlliedGroup1 = { Point1EUnit1, Point1EUnit2, Point1EUnit3, Point1EUnit4, Point1EUnit5, Point1EUnit6, Point1EUnit7 }
AlliedGroup2 = { Point2EUnit1, Point2EUnit2, Point2EUnit3, Point2EUnit4, Point2EUnit5 }
AlliedGroup3 = { Point3EUnit1, Point3EUnit2, Point3EUnit3, Point3EUnit4, Point3EUnit5, Point3EUnit6 }
AlliedGroup4 = { Point4EUnit1, Point4EUnit2, Point4EUnit3, Point4EUnit4, Point4EUnit5, Point4EUnit6, Point4EUnit7, Point4EUnit8, Point4EUnit9 }

BriefingText = ""

arrivedTrans = 0
escapedTrans = 0

InstertionHelicopterType = "tran.insertion"

TownBuildings =
{
	Town1House1, Town1House2, Town1House3, Town1House4, Town2House1, Town2House2, Town2House3,
	Town2House4, Town2House5, Town3House1, Town3House2, Town3House3, Town3House4, Town4House1,
	Town4House2, Town4House4, Town4House5,
}

StartingHpads = {EBaseHPad1, EBaseHPad2, EBaseHPad3, EBaseHPad4}
StartingHelis = {Heli1, Heli2, Heli3, Heli4}

ChopperEntryPs = 
{ 
	ChopperEntry1.Location, ChopperEntry2.Location, ChopperEntry3.Location,
	ChopperEntry4.Location, ChopperEntry5.Location, ChopperEntry6.Location
}
ChopperLandingPs = 
{ 
	SupplyPos1.Location, SupplyPos2.Location, SupplyPos3.Location,
	SupplyPos4.Location
}

ChopperRescuers = 
	{
	{"medi", "e1", "e6", "e1", "medi" },
	{"e3", "e1", "e6", "e1", "medi" },
	{"e1", "e1", "e3", "e3", "medi" },
	{"medi", "e1", "e6", "e1", "medi" }
	}

DownedCount = 0

DeploySpot = {CPos.New(40,36)}

EBase1 = {EBaseTent, EBasePower1, EBasePower2, EBaseHPad1, EBaseHPad2, EnemyMcv}
EBase1Defs = {EBaseHBox1, EBaseHBox2, EBaseHBox3, EBaseHBox4}

CivilianUprising = function()
	Utils.Do(TownBuildings, function(actor)
		if not actor.IsDead and actor.HasPassengers then
			actor.UnloadPassengers()
		end
	end)
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		CivStrike()
	end)
end

CivStrike = function()
	
	civUprising = Utils.Where(Map.ActorsInWorld, function(actor) return actor.Owner == neutral and actor.HasProperty("AttackMove") end)
	Media.Debug(tostring(#civUprising))
	
	Utils.Do(civUprising, function(c)
		c.Owner = hostileCivs
		c.AttackMove(CivAtkTarget1.Location)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		TellEUnitsToEscape()
	end)
end

RunInitAIActivities = function()
	enemy.Cash = 100000
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		ProduceInfantry()
	end)
end

StartTransEntries = function()
	CallTrans({ ChopperEntryPs[4], ChopperLandingPs[2] })

	Trigger.AfterDelay(DateTime.Seconds(30), function()
		ConstantTrans()
	end)
end

ConstantTrans = function()
	local entry = Utils.Random(ChopperEntryPs)
	local dst = Utils.Random(ChopperLandingPs)
	local delay = Utils.RandomInteger(-10,11)
	
	if arrivedTrans < totalTrans then
		CallTrans({entry,dst})
		DelayBetweenTrans = DelayBetweenTrans + delay
		Trigger.AfterDelay(DateTime.Seconds(DelayBetweenTrans), function()
			ConstantTrans()
		end)
	else
		return
	end
end

CallTrans = function(path)
	if Map.LobbyOption("difficulty") ~= "hard" then
		SignalTranEntrance(path[1])
	end
	
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		arrivedTrans = arrivedTrans + 1
	
		local units = Utils.Random(ChopperRescuers)
		local tran = Reinforcements.ReinforceWithTransport(enemy, InstertionHelicopterType, units, path, {path[1]})[1]
		local landed = false
		local tranN = arrivedTrans
		
		Trigger.OnEnteredFootprint({path[2]}, function(a,id)
			if a.Type == InstertionHelicopterType and tranN == arrivedTrans then
				Trigger.RemoveFootprintTrigger(id)
				escapedTrans = escapedTrans + 1
				landed = true
				if Map.LobbyOption("difficulty") ~= "easy" then
					player.MarkFailedObjective(DestroyAllChoppers)
				end

				if escapedTrans > transPossibleToMiss then
					player.MarkFailedObjective(DowningChoppers)
				end
			end
		end)

		Trigger.OnKilled(tran, function()
			if not landed then
				DownedCount = DownedCount + 1
			else
				return
			end
		end)
	end)
end

SignalTranEntrance = function(pos)
	Media.DisplayMessage("A supply chopper will arrive soon. Be prepared!","Command")
	local warningCam = Actor.Create("camera.medium", true, { Owner = player, Location = pos })
	
	Trigger.AfterDelay(DateTime.Seconds(15), function()
		warningCam.Destroy()
	end)
end

InitHeliSetup = function()
	for i=1,4, 1 do
		StartingHelis[i].ReturnToBase(StartingHpads[i])
	end
end

SetupHeliAtks = function()
	
end

HeliAtk = function()
	if not Heli1.IsDead then
		Heli1.AttackMove(CameraStartingPos.Location)
	end
end

TellEUnitsToEscape = function()
	AlliedUnitsEscape(AlliedGroup1, EscapePoint1.Location)
	AlliedUnitsEscape(AlliedGroup2, EscapePoint2.Location)
	AlliedUnitsEscape(AlliedGroup3, EscapePoint3.Location)
	AlliedUnitsEscape(AlliedGroup4, EscapePoint3.Location)
end

AlliedUnitsEscape = function(units, way)
	Utils.Do(units, function(u)
		if not u.IsDead then
			u.Move(way)
		end
	end)
end

Trigger.OnEnteredFootprint({EscapePoint1.Location, EscapePoint1.Location+CVec.New(0,1), EscapePoint1.Location+CVec.New(0,-1)}, function(a,id)
	if a.Owner == enemy and not a.IsDead then
		Trigger.AfterDelay(10, function()
			a.Destroy()
		end)
	end
end)

Trigger.OnEnteredFootprint({EscapePoint2.Location, EscapePoint2.Location+CVec.New(0,1), EscapePoint2.Location+CVec.New(0,-1)}, function(a,id)
	if a.Owner == enemy and not a.IsDead then
		Trigger.AfterDelay(10, function()
			a.Destroy()
		end)
	end
end)

Trigger.OnEnteredFootprint({EscapePoint3.Location, EscapePoint3.Location+CVec.New(1,0), EscapePoint3.Location+CVec.New(-1,0)}, function(a,id)
	if a.Owner == enemy and not a.IsDead then
		Trigger.AfterDelay(10, function()
			a.Destroy()
		end)
	end
end)

SpawnHBoxes = function()
	Utils.Do(HiddenPillboxes, function(spawn)
		Actor.Create("hbox.e3", true, { Owner = enemy, Location = spawn })
	end)
end

SetupSecondPhase = function()
	if Map.LobbyOption("difficulty") == "easy" then
		player.MarkCompletedObjective(DowningChoppers)
		else
			DestroyAllies = player.AddPrimaryObjective("Destroy the allied base at the Northwest.") --x
			player.MarkCompletedObjective(DowningChoppers)
			
			Trigger.OnAllKilled(AlliedBase, function()
				player.MarkCompletedObjective(DestroyAllies)
			end)
			
		if escapedTrans == 0 then
			player.MarkCompletedObjective(DestroyAllChoppers)
		end
				
		Trigger.AfterDelay(DateTime.Seconds(10), function()
			CivilianUprising()
		end)
	end

	Trigger.OnAnyKilled(TownBuildings, function()
		enemy.MarkCompletedObjective(AlliesObj)
	end)
end

Tick = function()
	if arrivedTrans < totalTrans then
		if DownedCount >= DestroyedChoppersRequired then
			DownedCount = -1
			SetupSecondPhase()
		end
	end
	
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(AlliesObj)
	end
	
	UserInterface.SetMissionText(
	"Supply arrival prevented: " .. tostring(DownedCount) .. "\nSupplies arrived to villages: " .. tostring(escapedTrans), TextColor)
end

InitTriggers = function()
	Camera.Position = CameraStartingPos.CenterPosition
	
	player.Cash = InitStartingCash
	
	InitHeliSetup()
	
	Trigger.AfterDelay(InitTransDelay, function()
		StartTransEntries()
	end)
	
	if HelpingCameras == true then
		Utils.Do(ChopperLandingPs, function(c)
			Actor.Create("camera.medium", true, { Owner = player, Location = c })
		end)
	end
	
	Trigger.AfterDelay(DateTime.Minutes(1), function()
		RunInitAIActivities()
	end)
	
	if Map.LobbyOption("difficulty") == "hard" then
		SpawnHBoxes()
		PlayerFtur2.Destroy()
		PlayerDome.Destroy()
	end
	
	if Map.LobbyOption("difficulty") ~= "hard" then
		PlayerDome.Owner = player
		PlayerFtur2.Owner = player
	end
end

InitObjectives = function()
	
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	if Map.LobbyOption("difficulty") == "hard" then
		BriefingText = "Destroy any chopper that tries to supply the villages."
	else
		BriefingText = 
		"Destroy any chopper that tries to supply the villages. \nDon't miss " .. transPossibleToMiss .. " choppers."
	end
	
	DowningChoppers = player.AddPrimaryObjective(BriefingText)
	ProtectCivilians = player.AddPrimaryObjective("Do not destroy any civilian building.")
	
	if Map.LobbyOption("difficulty") ~= "hard" then
		DestroyAllChoppers = player.AddSecondaryObjective("Prevent any supply of the villages.")
	end
	
	AlliesObj =  enemy.AddPrimaryObjective("Eliminate all soviet presence in the area.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
	
end

WorldLoaded = function()
	enemy = Player.GetPlayer("Greece")
	player = Player.GetPlayer("USSR")
	neutral = Player.GetPlayer("Neutral")
	hostileCivs = Player.GetPlayer("Hostiles")

	TextColor = HSLColor.White

	InitTriggers()
	InitObjectives()
end 
