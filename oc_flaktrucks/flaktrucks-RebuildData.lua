--POWER
Pwr1 = { type = "powr", pos = CVec.New(2, -1), cost = 300, exists = false}
Pwr2 = { type = "apwr", pos = CVec.New(7, -1), cost = 300, exists = false}
Pwr3 =	{ type = "apwr", pos = CVec.New(-1, 3), cost = 500, exists = false}
Pwr4 = { type = "apwr", pos = CVec.New(5, -5), cost = 500, exists = false}

Proc =	{ type = "proc", pos = CVec.New(-4, 2), cost = 1500, exists = false }

Tent =	{ type = "tent", pos = CVec.New(5,2), cost = 500, exists = false }
Weap =	{ type = "weap", pos = CVec.New(4, -2), cost = 2000, exists = false }
Dome = { type = "dome", pos = CVec.New(-6, 3), cost = 1400, exists = false }
Fix = { type = "fix", pos = CVec.New(2, 5), cost = 1000, exists = false }

Silo1 = { type = "silo", pos = CVec.New(2, 3), cost = 150, exists = false }
Silo2 = { type = "silo", pos = CVec.New(2, 4), cost = 150, exists = false }

Gun1 = { type = "gun", pos = CVec.New(11, 0), cost = 800, exists = false }
Gun2 = { type = "gun", pos = CVec.New(11, 4), cost = 800, exists = false }
Gun3 = { type = "gun", pos = CVec.New(2, 9), cost = 800, exists = false }
Pbox1 = { type = "pbox", pos = CVec.New(11, 2), cost = 600, exists = false }
Pbox2 = { type = "pbox", pos = CVec.New(0, 9), cost = 600, exists = false }

Agun1 = { type = "agun", pos = CVec.New(7, -2), cost = 800, exists = false }
Agun2 = { type = "agun", pos = CVec.New(-2, 6), cost = 800, exists = false }

Atek = { type = "atek", pos = CVec.New(-8, 3), cost = 1500, exists = false }

BaseBuildings = 
{
Pwr1,
Proc,
Tent,
Silo1,
Weap,
Pwr2,
Dome,
Fix,
Pwr3,
Pwr4,
Silo2,
Gun1,
Gun2,
Gun3,
Pbox1,
Pbox2,
Agun1,
Agun2,
Atek
}

