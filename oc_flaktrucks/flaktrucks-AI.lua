if Map.LobbyOption("difficulty") == "easy" then
	--AttackIntervals = {2.5, 3}
	InfAtkCount = 5
elseif Map.LobbyOption("difficulty") == "normal" then
	--AttackIntervals = {2, 2.5}
	InfAtkCount = 6
else
	--AttackIntervals = {1, 1.5, 2}
	InfAtkCount = 8
end

InfAtkTypes = {"e1", "e3"}
InfAtk = {}

LandAtkPaths =
{
{AtkPathA1, AtkPathA2, AtkPathA3 },
{AtkPathA1, AtkPathB1 },
{AtkPathA1, AtkPathB1 }
}

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end


ProduceInfantry = function()
	if EBaseTent.IsDead or EBaseTent.Owner ~= enemy then
		return
	end
	
	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(InfAtkTypes) }
	local Path = Utils.Random(LandAtkPaths)
	enemy.Build(toBuild, function(unit)
		InfAtk[#InfAtk + 1] = unit[1]
		if #InfAtk >= InfAtkCount then
			SendUnits(InfAtk, Path)
			InfAtk = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

HeliAttack = function()
	--Heli1.AttackMove(CameraStartingPos.Location)
	--Heli2.AttackMove(CameraStartingPos.Location)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end