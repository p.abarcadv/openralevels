if Map.LobbyOption("difficulty") == "easy" then
	playerFunds = 3000
	TimeValue = 180 --Time before attack.
	activateCameras = true
	airAtksDelay = 120
elseif Map.LobbyOption("difficulty") == "normal" then
	playerFunds = 2000
	TimeValue = 150 --Time before attack.
	activateCameras = true
	airAtksDelay = 90
else
	playerFunds = 2000
	TimeValue = 120 --Time before attack.
	activateCameras = false
	airAtksDelay = 60
end

planeSent = "TBD"

modtime = TimeValue - 60
defeatCond = true
extraAttack = false

--Changed Extension recently
Extension = { WVec.New(-1024*7, -1024*7, 0), WVec.New (1024*7,1024*7, 0), WVec.New(-1024*12, -1024*12, 0), WVec.New (1024*12,1024*12, 0)}

PlayerBase = {baseref, basepower1, basepower2, basepower3, basetent, baseweap, basepbox}

MainEnemyTarget = CapturedMADTank

EvacPositions = { CPos.New(60,124), CPos.New(61,124), CPos.New(62,124), CPos.New(63,124), CPos.New(64,124), CPos.New(65,124), CPos.New(66,124), CPos.New(67,124), CPos.New(68,124), CPos.New(69,124), CPos.New(70,124), CPos.New(71,124), CPos.New(72,124), CPos.New(73,124), CPos.New(74,124), CPos.New(75,124), CPos.New(76,124) }

PathCamera1Coords = {CPos.New(40,79), CPos.New(40,78), CPos.New(41,78), CPos.New(41,77), CPos.New(42,77), CPos.New(42,76), CPos.New(43,76), CPos.New(43,75), CPos.New(44,75), CPos.New(44,74), CPos.New(45,74), CPos.New(45,73), CPos.New(46,73)}
PathCamera2Coords = {CPos.New(70,69), CPos.New(70,68), CPos.New(70,67), CPos.New(70,66), CPos.New(70,65), CPos.New(70,64), CPos.New(70,63)}
PathCamera3Coords = {CPos.New(95,91), CPos.New(95,90), CPos.New(95,89), CPos.New(96,89)}

BarrelList = { Barrel1, Barrel2, Barrel3, Barrel4, Barrel5, Barrel6 }

StartAtk = {AtkMmtnk1, AtkMmtnk2, AtkMmtnk3, AtkInf1, AtkInf2, AtkInf3, AtkInf4, AtkInf5, AtkInf6, AtkInf7, AtkInf8, AtkInf9, AtkInf10, AtkInf11, AtkInf12, AtkV21, AtkV22}

AlertUnits1 = { Alert1Unit1, Alert1Unit2, Alert1Unit3, Alert1Unit4, Alert1Unit5, Alert1Unit6, Alert1Unit7, Alert1Unit8, Alert1Unit9 }
AlertUnits2 = { Alert2Unit1, Alert2Unit2, Alert2Unit3, Alert2Unit4, Alert2Unit5, Alert2Unit6 }

SovReinforcements = {"4tnk","4tnk", "e1","e1","e1","e4","e4"}

SovReinforcementPath = 
{
{EnemyReinforce1Entry.Location, EnemyReinforce1Dst.Location, EnemyReinforce1Atk1.Location, baseatktarget2.Location, baseatktarget3.Location},
{EnemyReinforce2Entry.Location, EnemyReinforce2Dst.Location, EnemyReinforce2Atk1.Location, baseatktarget2.Location, baseatktarget3.Location}
}

--[[
GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end

			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)

				if bool then
					stop = true

					i = i + 1
					if i > #waypoints then
						i = 1
					end

					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end]]

RunPatrols = function()
	
end

RunAIActivities = function()
	--StartMmthAttack()
	--SendSpyPlane()
	--CheckPlanes()
	--SendAtkPlanes()
end

--[[VICTORY CONDITION]]
Trigger.OnEnteredFootprint(EvacPositions, function(a,id)
	--if a.Type == "mad" then
	--	Trigger.RemoveFootprintTrigger(id)
	--	player.MarkCompletedObjective(MADSurvive)
	--	player.MarkCompletedObjective(TenUnitsSurvive)
	--end
end)

SendSpyPlane = function()
	--if not Afld1.IsDead and not MainEnemyTarget.IsDead then
	local facing = Utils.Random({16,-16})
		
	--local spyplane = Actor.Create("U2", true, { Owner = player, Location = FirstSpyPlaneEntry.Location })
		
	
		--Afld1.SendAirstrike(MainEnemyTarget.CenterPosition, false, 0+facing)
		--Set attack action
		--Trigger.AfterDelay(DateTime.Seconds(10), function() --time should be determined by airAtksDelay 
			--AttackMCUArea() --remove this when debug is ready
		--end)
		--repeat
	
	
	Trigger.AfterDelay(DateTime.Minutes(1), function() --time should be determined by "airAtksDelay"
		SendSpyPlane()
	end)
end

--Warning cameras
Trigger.OnEnteredFootprint(PathCamera1Coords, function(a,id)
	if a.Owner == player and Map.LobbyOption("difficulty") ~= "hard" then
		Trigger.RemoveFootprintTrigger(id)
		if activateCameras then
			local camera = Actor.Create("camera", true, { Owner = player, Location = PathCamera1.Location })
			Trigger.AfterDelay(DateTime.Seconds(10), function()
				camera.Destroy()
			end)
		end
	end
end)

Trigger.OnEnteredFootprint(PathCamera2Coords, function(a,id)
	if a.Owner == player and Map.LobbyOption("difficulty") ~= "hard" then
		Trigger.RemoveFootprintTrigger(id)
		if activateCameras then
			local camera = Actor.Create("camera", true, { Owner = player, Location = PathCamera2.Location })
			Trigger.AfterDelay(DateTime.Seconds(10), function()
				camera.Destroy()
			end)
		end
	end
end)

Trigger.OnEnteredFootprint(PathCamera3Coords, function(a,id)
	if a.Owner == player and Map.LobbyOption("difficulty") ~= "hard" then
		Trigger.RemoveFootprintTrigger(id)
		if activateCameras then
			local camera = Actor.Create("camera", true, { Owner = player, Location = PathCamera3.Location })
			Trigger.AfterDelay(DateTime.Seconds(10), function()
				camera.Destroy()
			end)
		end
	end
end)
--Warning cameras

SendAtkPlanes = function()
	--TargetForPlanes(MigBase1)
	--TargetForPlanes(YakBase1)
end

StartMmthAttack = function()
	Utils.Do(StartAtk, function(unit)
		unit.AttackMove(baseatktarget1.Location)
		unit.AttackMove(baseatktarget2.Location)
		unit.AttackMove(baseatktarget3.Location)
		--IdleHunt(unit)
	end)
end

Trigger.AfterDelay(DateTime.Seconds(TimeValue+600), function()
	DeployExtraAtk()
end)

DeployExtraAtk = function()
	--ReinforceAndAttack(SovReinforcements, SovReinforcementPath[1])
	--ReinforceAndAttack(SovReinforcements, SovReinforcementPath[2])
end

ReinforceAndAttack = function(units, path)
	local units = Reinforcements.Reinforce(enemy, units, {path[1], path[2]}, 5)
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		Utils.Do(units, function(unit)
			unit.AttackMove(path[3])
			unit.AttackMove(path[4])
			IdleHunt(unit)
		end)
	end)
end

Trigger.OnAnyKilled(BarrelList,  function()
	if not DestroyableTesla.IsDead then
		DestroyableTesla.Kill()
	end
	if not DestroyableBrik.IsDead then
		DestroyableBrik.Kill()
	end
end)

--[[
Trigger.OnAnyKilled(AlertUnits1, function()
	Utils.Do(AlertUnits1, function(actor)
		if actor.IsDead then
			IdleHunt(actor)
		end
	end)
end)

Trigger.OnAnyKilled(AlertUnits2, function()
	Utils.Do(AlertUnits2, function(actor)
		if actor.IsDead then
			IdleHunt(actor)
		end
	end)
end)
]]

--flavor
Flavor = function()
	--PlaneLand()
	Trigger.AfterDelay(DateTime.Seconds(modtime), function()
		FlavorSpyPlane()
	end)
end

PlaneLand = function()
	local planes = StartingSovPlanes
	local aflds = Airfields
	
	for d = 1, #planes, 1 do
		planes[d].ReturnToBase(aflds[d])
	end
end

FlavorSpyPlane = function()
	Media.Debug("HERE!")
	--if not Afld1.IsDead and not MainEnemyTarget.IsDead then
	local facing = Utils.RandomInteger(-16,16)
		
	local spyplane = Actor.Create("yak", true, { Owner = enemy, Location = FirstSpyPlaneEntry.Location --[[, Angle.SouthEast]]})
	
		
	
		--Afld1.SendAirstrike(MainEnemyTarget.CenterPosition, false, 0+facing)
		--Set attack action
		--Trigger.AfterDelay(DateTime.Seconds(10), function() --time should be determined by airAtksDelay 
			--AttackMCUArea() --remove this when debug is ready
		--end)
		--repeat
	
	--Afld1.SendAirstrike(FirstSpyPlaneDst.CenterPosition, false, 112)
	--Trigger.AfterDelay(DateTime.Seconds(3), function()
	--	Afld1.SendAirstrike(FirstSpyPlaneDst.CenterPosition, false, 96)
	--end)
end

--difficulty
DifficultySetup = function()
	if Map.LobbyOption("difficulty") == "easy" then
		
	elseif Map.LobbyOption("difficulty") == "normal" then
		--remove aguns
		baseagun1.Destroy()
		baseagun2.Destroy()
		--remove fix
		basefix.Destroy()
	else
		--remove fix
		basefix.Destroy()
		--remove 2tnks
		MedTank1.Destroy()
		MedTank2.Destroy()
		--remove aguns
		baseagun1.Destroy()
		baseagun2.Destroy()
		--remove barrels
		Utils.Do(BarrelList, function(actor)
			if not actor.IsDead then
				actor.Destroy()
			end
		end)
		BarrelList = { }
		--add tesla and brik walls
		Actor.Create("tsla", true, { Owner = enemy, SkipMakeAnims, Location = TeslaSpawn.Location })
		Trigger.AfterDelay(DateTime.Seconds(0), function()
			local x = -1
			local y = -1
			for i = 1, 9, 1 do
				if x > 1 then 
					x = -1
					y = y+1
				end
				if y == 0 and x == 0 then
					-- (y ~= 0 and x ~= 0) doesn't work?
				else
					Actor.Create( "brik", true, { Owner = enemy, Location = TeslaSpawn.Location + CVec.New(x, y) } )
				end
				x = x+1
			end
		end)
	end
end

Trigger.OnKilled(MainEnemyTarget, function()
	enemy.MarkCompletedObjective(DenyAllies)
end)

Tick = function()
	
	--[[
	if defeatCond == true then
		local mcv = Utils.Where(player.GetActors(), function(actor)
			return actor.Type == "mcv" or actor.Type == "fact"
		end)
		
		if mcv[1] == nil  then
			enemy.MarkCompletedObjective(DenyAllies)
			defeatCond = false
		end
	end
	]]
	--Remove this
	--MessageColor = HSLColor.White
	--UserInterface.SetMissionText("AtkUnits[] " .. #AtkUnits .. " AtkBase[] " .. #AtkBase .. " StartingSovPlanes[] " .. #StartingSovPlanes .. " AtkU-Plane " .. planeSent, MessageColor)
end

InitTriggers = function()
	Camera.Position = StartingCamera.CenterPosition
	player.Cash = playerFunds
	
	Flavor()
	--CheckCYardPos()
	
	Trigger.AfterDelay(DateTime.Seconds(TimeValue), function()
		--RunAIActivities()
	end)

	--DifficultySetup()
	--[[
	Utils.Do(AlertUnits1, function(actor)
		Trigger.OnDamaged(actor, function()
			Utils.Do(AlertUnits1, function(unit)
				if not unit.IsDead then
					IdleHunt(unit)
				end
			end)
		end)
	end)
	
	Utils.Do(AlertUnits2, function(actor)
		Trigger.OnDamaged(actor, function()
			Utils.Do(AlertUnits2, function(unit)
				if not unit.IsDead then
					IdleHunt(unit)
				end
			end)
		end)
	end)]]
	
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	MADSurvive = player.AddPrimaryObjective("Evactuate the captured M.A.D. Tank.")
	TenUnitsSurvive = player.AddSecondaryObjective("Evacuate extra units.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the Allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	
	--TimerColor = player.Color
	InitObjectives()
	InitTriggers()
end