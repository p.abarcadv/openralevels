--right
mig1 = {type = "mig", afld = Afld1, behavior = "atkBase", exists = true, Index = 1}
yak1 = {type = "yak", afld = Afld2, behavior = "atkUnits", exists = true, Index = 2}

--left
mig2 = {type = "mig", afld = Afld3, behavior = "atkUnits", exists = true, Index = 1}
yak2 = {type = "yak", afld = Afld4, behavior = "atkBase", exists = true, Index = 2}

AtkUnits = {MigUnits1, YakUnits1}
AtkBase = {MigBase1, YakBase1}

SovPlanes = 
{ 
mig1, 
yak1, 
mig2, 
yak2
}
--[[
StartingSovPlanes = {MigBase1, YakUnits1, MigUnits1, YakBase1}

Airfields = {Afld1, Afld2, Afld3, Afld4}

CheckPlanes = function()
	for i,v in ipairs(SovPlanes) do
		if not v.exists then
			CreatePlanes(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		CheckPlanes()
	end)
end

CreatePlanes = function(plane)
	Trigger.AfterDelay((Actor.BuildTime(plane.type))/2, function()
		local actor = Actor.Create(plane.type, true, { Owner = enemy, Location = (plane.afld).Location }) 
		plane.exists = true
		actor.ReturnToBase(plane.afld)
		if plane.behavior == "atkUnits" then
			table.insert(AtkUnits, actor)
		else
			table.insert(AtkBase, actor)
		end
			
		Trigger.OnKilled(actor, function() 
			plane.exists = false 
			if plane.behavior == "atkUnits" then
				CheckTable(AtkUnits)
			else
			--	table.remove(AtkBase, plane.index)
			end
		end)
	
		Trigger.AfterDelay(DateTime.Seconds(5), CheckPlanes)
	end)
end
--Media.PlaySpeechNotification(player, "ReinforcementsArrived")
CheckTable = function(coll)
	local number = #coll
	
	if number > 0 then
		for i = 1, number do
			if AtkUnits[i] ~= nil and AtkUnits[i].IsDead then
				table.remove(AtkUnits, i)
				--Media.PlaySpeechNotification(player, "ReinforcementsArrived")
			end
		end
	end
	--Trigger.AfterDelay(DateTime.Seconds(2.5), CheckTable)
end

--Gets targets around MCU
AttackMCUArea = function()
	local areaPivot = Actor.Create("waypoint", true, { Owner = player, Location = TargetMCU.Location })
	Trigger.AfterDelay(DateTime.Seconds(12), function()
		local possTargets = Utils.Where(Map.ActorsInBox(areaPivot.CenterPosition + Extension[1], areaPivot.CenterPosition + Extension[2]), function(actor)
			return actor.Owner == player and actor.HasProperty("Health") and actor.Type ~= "sbag" and actor.Type ~= "brik"
		end)
		
		if #possTargets > 0 then
			local target = Utils.Random(possTargets)
			local plane = Utils.Random(AtkUnits)
			
			--Media.PlaySoundNotification(player, "AlertBuzzer")
			--DEBUG
			if plane.Type == "mig" then
				planeSent = "mig"
			else
				planeSent = "yak"
			end
			TargetForPlanes(plane, target)

		end
		areaPivot.Destroy()
	end)
end

--Gets targets around base
AttackBaseArea = function()
	local possTargets = Utils.Where(Map.ActorsInBox(FirstSpyPlaneDst.CenterPosition + Extension[3], FirstSpyPlaneDst.CenterPosition + Extension[4]), function(actor)
		return actor.Owner == player and actor.HasProperty("Health") and actor.Type ~= "sbag" and actor.Type ~= "brik"
	end)
	
	if #possTargets > 0 then
		local target = Utils.Random(possTargets)
		local plane = Utils.Random(AtkBase)
		
		TargetForPlanes(plane, target)
	end
end

--TAKEN AND REPURPOSED FROM allies08b-AI
TargetForPlanes = function(plane, target)
	if plane.IsDead then
		return
	end
	if not target or target.IsDead or (not target.IsInWorld) then
		local enemies = Utils.Where(Map.ActorsInWorld, function(u) 
			return u.Owner == player and u.HasProperty("Health") and plane.CanTarget(u) and u.Type ~= "sbag" and u.Type ~= "brik"
		end)
		if #enemies > 0 then
			target = Utils.Random(enemies)
		end
	end
	if target and plane.AmmoCount() > 0 and plane.CanTarget(target) then
		plane.Attack(target)
	else
		plane.ReturnToBase()
	end
	plane.CallFunc(function()
		Trigger.AfterDelay(1, function()
			TargetForPlanes(plane, target)
		end)
	end)
end

CheckCYardPos = function()
	local mcv = Utils.Where(player.GetActors(), function(actor)
		return actor.Type == "mcv" or actor.Type == "fact" 
	end)
	local point = mcv[1]
	
	if point ~= nil then
		if not point.IsDead then
			TargetMCU = point
		end
	else
		
	end
	Trigger.AfterDelay(DateTime.Seconds(1), function() 
		CheckCYardPos()
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

--RIGHT
Trigger.OnKilled(MigBase1, function()
	mig1.exists = false
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		CheckTable(AtkUnits)
	end)
end)
Trigger.OnKilled(YakUnits1, function()
	yak1.exists = false
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		CheckTable(AtkUnits)
	end)
end)

--LEFT
Trigger.OnKilled(MigUnits1, function()
	mig2.exists = false
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		CheckTable(AtkUnits)
	end)
end)
Trigger.OnKilled(YakBase1, function()
	yak2.exists = false
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		CheckTable(AtkUnits)
	end)
end)

REFACTOR TO THIS
Trigger.OnAnyKilled(AtkUnits, function()
end)

Trigger.OnAnyKilled(AtkBase, function()
end)

]]