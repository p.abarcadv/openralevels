if Map.LobbyOption("difficulty") == "easy" then
	tankAmount = 6
	HardBoats = false
	InitialAtkDelay = DateTime.Minutes(3)
	ArtyDelay = DateTime.Minutes(4)
elseif Map.LobbyOption("difficulty") == "normal" then
	tankAmount = 8
	HardBoats = false
	InitialAtkDelay = DateTime.Minutes(2)
	ArtyDelay = DateTime.Minutes(4)
else
	tankAmount = 12
	HardBoats = true
	InitialAtkDelay = DateTime.Minutes(1)
	ArtyDelay = DateTime.Minutes(3)
end

--[[ATTACKS]]
--[[IN MAP ATTACK]]

ChronoDeployZones = {ChronoDeploy1.Location, ChronoDeploy2.Location, ChronoDeploy3.Location}

--To change owner of bricks after cosntruction yard has been captured
BrikCollection = {}

AttackPaths = 
	{
	{PathA_1, PathA_2},
	{PathA_1, PathA_3, PathA_4}
	}

AlliedInfantryUnits = {"e1", "e1","e1", "e3","e3"}
AlliedTankUnits = {"2tnk", "2tnk", "2tnk", "1tnk", "jeep", "arty", "arty"}

TankAttack = {}
InfantryAttack = {}

infAmount = 12
tankAmount = 6

--[[OUT OF MAP GROUND ATTACK]]
Lst = "lst"
LstLandingPaths = { LstLanding1.Location, LstLanding2.Location, LstLanding3.Location}
LstUnits = 
	{
	{ "2tnk", "e1", "e1", "e3", "e3"},
	{ "arty", "e1", "e1", "e3", "arty" },
	{ "2tnk", "1tnk", "e1", "e1", "arty"},
	{ "1tnk", "1tnk", "1tnk", "jeep", "jeep" },
	{ "2tnk", "2tnk", "arty", "e3", "e3"}
	}

--[[OUT OF MAP GROUND ATTACK]]
AttackHelis = {"heli.outofmap", "heli.outofmap", "heli.outofmap"}
AttackHelisEntry = {MountainHelis.Location, SeaHelis.Location, CornerHelis.Location}

--[[DEFENCE]]
GuardingHelis = {}	
heliAmount = 2
GuardedHeliArea = 
	{ 
	HeliGuarded1, HeliGuarded2, HeliGuarded3, HeliGuarded4, 
	HeliGuarded5, HeliGuarded6, HeliGuarded7
	}

artyAmount = 2
ArtyAttack = {  }
ArtyAttackPath = {ArtyAttackWay1, ArtyAttackWay2, ArtyAttackWay3}

AddBoats = function()
	Actor.Create("ca", true, { Owner = enemy, Location = SpawnCruiserHard.Location })
	Actor.Create("dd", true, { Owner = enemy, Location = SpawnDestroyerHard.Location })
end

ChronoAttack = function()
	Media.PlaySound("chrono2.aud")
	local delay = Utils.RandomInteger(0,61)
	local Loc = Utils.Random(ChronoDeployZones)
	
	for i = 1,3,1 do
		local posModX = Utils.RandomInteger(-2,3)
		local posModY = Utils.RandomInteger(-2,3)
		
		local c = Actor.Create("ctnk", true, { Owner = enemy, Location = Loc + CVec.New(posModX,posModY) })
		
		IdleHunt(c)
	end	
	Trigger.AfterDelay(DateTime.Seconds(1 + delay), ChronoAttack)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end

IdleHeliHunt = function(unit, way) 
	if not unit.IsDead then
		Trigger.OnIdle(unit, function()
			if unit.AmmoCount() == 0 then
				unit.Move(way)
			else
				unit.Hunt() 
			end
		end)
	end
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if EFact.IsDead or EFact.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EFactLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function()
	if not Tent1.exists and not Tent1.Owner ~= enemy and not Tent2.exists and not Tent2.Owner ~= enemy then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(AlliedInfantryUnits) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		
	
		InfantryAttack[#InfantryAttack + 1] = unit[1]
		if #InfantryAttack >= infAmount then
			SendUnits(InfantryAttack, Path)
			InfantryAttack = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end


ProduceArty = function()
	if Outpost2_Weap.IsDead or Outpost2_Weap.Owner ~= goodguy then
		return 
	end
	
	local delay = DateTime.Seconds(1)
	local toBuild = { "arty" }
	local Path = ArtyAttackPath
	
	--artyAmount
	goodguy.Build(toBuild, function(unit)
		ArtyAttack[#ArtyAttack + 1] = unit[1]

		if #ArtyAttack >= artyAmount then
			SendUnits(ArtyAttack, Path)
			ArtyAttack = { }
			Trigger.AfterDelay(DateTime.Minutes(3), ProduceArty)
		else
			Trigger.AfterDelay(delay, ProduceArty)
		end
	end)
end

ProduceTanks = function()
	if not Weap.exists --[[or Weap.Owner ~= enemy]] then
		return
	--elseif baseharv.IsDead and enemy.Resources <= 599 then
	--	return
	end
	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(AlliedTankUnits) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		TankAttack[#TankAttack + 1] = unit[1]

		if #TankAttack >= tankAmount then
			SendUnits(TankAttack, Path)
			TankAttack = { }
			Trigger.AfterDelay(DateTime.Minutes(1), ProduceTanks)
		else
			Trigger.AfterDelay(delay, ProduceTanks)
		end
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

--[[SEA TRANSPORTED ATTACK]]
SeaAttack = function()
	local way = Utils.Random(LstLandingPaths)
	local units = Utils.Random(LstUnits)
	local delay = Utils.RandomInteger(200,220)
	local attackersGround = Reinforcements.ReinforceWithTransport(enemy, Lst, units, {LstEntry.Location, way}, {LstEntry.Location})[2]
	
	Utils.Do(attackersGround, function(a)
		Trigger.OnAddedToWorld(a, function()
			a.AttackMove(PBaseCam.Location)
			IdleHunt(a)
		end)
	end)

	Trigger.AfterDelay(DateTime.Seconds(delay), function()
		SeaAttack()
	end)
end

--[[HELI ATTACK (WEST, MOUNTAINS, SOUTH, SEA, CORNER, BASE)]]
HeliAttack = function()
	
	local way = Utils.Random(AttackHelisEntry)
	local delay = Utils.RandomInteger(300,350)
	local attackersAir = Reinforcements.Reinforce(enemy, AttackHelis, {way, PBaseCam.Location})
	local attackArea = Utils.Random(BaseAttackableArea)
	
	Utils.Do(attackersAir, function(a)
		a.AttackMove(PBaseCam.Location + attackArea)
		IdleHeliHunt(a, way)
	end)

	Trigger.AfterDelay(DateTime.Seconds(delay), function()
		HeliAttack()
	end)
end

--[[REMOVE EMPTIED HELIS (WEST, MOUNTAINS)]]
Trigger.OnEnteredProximityTrigger(MountainHelis.CenterPosition, WDist.FromCells(5), function(a, id)
	if a.Type == "heli.outofmap" and a.AmmoCount() == 0 then
		a.Destroy()
	end
end)

--[[REMOVE EMPTIED HELIS (SOUTH, SEA)]]
Trigger.OnEnteredProximityTrigger(SeaHelis.CenterPosition, WDist.FromCells(5), function(a, id)
	if a.Type == "heli.outofmap" and a.AmmoCount() == 0	 then
		a.Destroy()
	end
end)

--[[REMOVE EMPTIED HELIS (SW-CORNER, BASE)]]
Trigger.OnEnteredProximityTrigger(CornerHelis.CenterPosition, WDist.FromCells(5), function(a, id)
	if a.Type == "heli.outofmap" and a.AmmoCount() == 0	 then
		a.Destroy()
	end
end)

--[[BASE DEFENDING HELIS]]
HeliDefence = function()
	Utils.Do(GuardedHeliArea, function(actor)
		Trigger.OnEnteredProximityTrigger(actor.CenterPosition, WDist.FromCells(10), function(a, id)
			if a.Owner == player then
				if Heli1.exists then
					HeliTargetting(Heli1, a)
				end
				if Heli2.exists then
					HeliTargetting(Heli2, a)
				end
			end
		end)
	end)
end

HeliTargetting = function(heli, target)
	if not heli.IsDead and heli.IsIdle then
		heli.Attack(target)
	else
		if heli.Type == "heli" then --crashes if not stated
			heli.Reload()
		end
	end
end

--[[
CheckHelis = function()
	if Heli1.exists and Heli2.exists then
		return
	else
		RebuildHelis(BaseHelis[1].type)
		--if Heli1.exists then
			--RebuildHelis(BaseHelis[1].type)
		--else
			--RebuildHelis(BaseHelis[2].type)
		--end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), CheckHelis)
end

RebuildHelis = function(heli)
	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { "heli" }
	enemy.Build(toBuild, function(unit)
		--TankAttack[#TankAttack + 1] = unit[1]
		local actor = unit[1]
		--Trigger.OnKilled(actor, HeliDefence
	end)
end
]]