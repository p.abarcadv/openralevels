--[[DONE]]
BasePower1 = {type = "apwr", pos = CVec.New(-10,-2), cost = 300, exists = true}
BasePower2 = {type = "apwr", pos = CVec.New(-10,-5), cost = 500, exists = true}
BasePower3 = {type = "powr", pos = CVec.New(-7,-3), cost = 300, exists = true}
BasePower4 = {type = "apwr", pos = CVec.New(-7,-7), cost = 500, exists = true}
BasePower5 = {type = "apwr", pos = CVec.New(4,-7), cost = 500, exists = true}
BasePower6 = {type = "apwr", pos = CVec.New(6,-4), cost = 500, exists = true}
BasePower7 = {type = "apwr", pos = CVec.New(7,-7), cost = 500, exists = true}
BasePower8 = {type = "powr", pos = CVec.New(1,-15), cost = 300, exists = true}
BasePower9 = {type = "powr", pos = CVec.New(3,-17), cost = 300, exists = true}
BasePower10 = {type = "apwr", pos = CVec.New(8,-15), cost = 500, exists = true}
BasePower11 = {type = "apwr", pos = CVec.New(11,-14), cost = 500, exists = true}
BasePower12 = {type = "powr", pos = CVec.New(16,-16), cost = 300, exists = true}
BasePower13 = {type = "apwr", pos = CVec.New(18,-18), cost = 500, exists = true}
--[[DONE]]
Ref1 = {type = "proc", pos = CVec.New(16,-24), cost = 1400, exists = true}
Ref2 = {type = "proc", pos = CVec.New(12,-22), cost = 1400, exists = true}
--[[DONE]]
Silo1 = {type = "silo", pos = CVec.New(18,-24), cost = 150, exists = true}
Silo2 = {type = "silo", pos = CVec.New(16,-24), cost = 150, exists = true}
Silo3 = {type = "silo", pos = CVec.New(14,-22), cost = 150, exists = true}
Silo4 = {type = "silo", pos = CVec.New(12,-22), cost = 150, exists = true}
--[[DONE]]
Tent1 = {type = "tent", pos = CVec.New(25,-26), cost = 500, exists = true}
Tent2 = {type = "tent", pos = CVec.New(-3,-7), cost = 500, exists = true}
Weap = {type = "weap", pos = CVec.New(21,-26), cost = 2000, exists = true}

--Guns
--[[DONE]]
Gun1 = {type = "gun", pos = CVec.New(37,-25), cost = 800, exists = true}
Gun2 = {type = "gun", pos = CVec.New(37,-21), cost = 800, exists = true}
Gun3 = {type = "gun", pos = CVec.New(28,-19), cost = 800, exists = true}
Gun4 = {type = "gun", pos = CVec.New(19,-14), cost = 800, exists = true}
Gun5 = {type = "gun", pos = CVec.New(11,-2), cost = 800, exists = true}
Gun6 = {type = "gun", pos = CVec.New(5,2), cost = 800, exists = true}
Gun7 = {type = "gun", pos = CVec.New(-7,2), cost = 800, exists = true}

--PBoxes
--[[DONE]]
Pbox1 = {type = "pbox", pos = CVec.New(37,-26), cost = 650, exists = true}
Pbox2 = {type = "pbox", pos = CVec.New(37,-20), cost = 650, exists = true}
Pbox3 = {type = "pbox", pos = CVec.New(3,-30), cost = 650, exists = true}
Pbox4 = {type = "pbox", pos = CVec.New(-2,-30), cost = 650, exists = true}
Pbox5 = {type = "pbox", pos = CVec.New(2,-9), cost = 650, exists = true}

--Hboxes
--[[DONE]]
Hbox1 = {type = "hbox", pos = CVec.New(-1,-22), cost = 750, exists = true}
Hbox2 = {type = "hbox", pos = CVec.New(7,-22), cost = 750, exists = true}	

--Gap
--[[DONE]]
Gap1 = {type = "gap", pos = CVec.New(33,-24), cost = 750, exists = true}
Gap2 = {type = "gap", pos = CVec.New(0,-27), cost = 750, exists = true}

--Others
--[[DONE]]
Dome = {type = "dome", pos = CVec.New(0,-20), cost = 2000, exists = true }
ATech = {type = "atek", pos = CVec.New(23,-33), cost = 2000, exists = true }
Helipad1 = {type = "hpad", pos = CVec.New(31,-34), cost = 1000, exists = true }
Helipad2 = {type = "hpad", pos = CVec.New(34,-34), cost = 1000, exists = true }
Fix = {type = "fix", pos = CVec.New(24,-21), cost = 100, exists = true }
--[[DONE]]
AAGun1 = {type = "agun", pos = CVec.New(33,-22), cost = 750, exists = true }
AAGun2 = {type = "agun", pos = CVec.New(35,-31), cost = 750, exists = true }
AAGun3 = {type = "agun", pos = CVec.New(22,-18), cost = 750, exists = true }
AAGun4 = {type = "agun", pos = CVec.New(19,-31), cost = 750, exists = true }
AAGun5 = {type = "agun", pos = CVec.New(27,-36), cost = 750, exists = true }
AAGun6 = {type = "agun", pos = CVec.New(6,-13), cost = 750, exists = true }
AAGun7 = {type = "agun", pos = CVec.New(-2,2), cost = 750, exists = true }
AAGun8 = {type = "agun", pos = CVec.New(2,2), cost = 750, exists = true }
AAGun9 = {type = "agun", pos = CVec.New(1,-26), cost = 750, exists = true }
--[[DONE]]
Naval = {type = "syrd", pos = CVec.New(10,2), cost = 750, exists = true }
--[[DONE]]
Heli1 = {type = "heli", exists = true}
Heli2 = {type = "heli", exists = true}

BaseBuildings = 
	{
	Gun1,
	Gun2,
	Gun3,
	Gun4,
	Gun5,
	Gun6,
	Gun7,
	
	--Fourth Batch
	Pbox1,
	Pbox2,
	Pbox3,
	Pbox4,
	Pbox5,
	
	
	--Fifth Batch
	Hbox1,
	Hbox2,
	Gap1,
	Gap2,
	AAGun1,
	AAGun2,
	AAGun3,
	AAGun4,
	AAGun5,
	AAGun6,
	AAGun7,
	AAGun8,
	AAGun9,
	
	Silo1,
	Silo2,
	Silo3,
	Silo4,
	
	BasePower1,
	BasePower2,
	BasePower3,
	BasePower4,
	BasePower5,
	BasePower6,
	BasePower7,
	BasePower8,
	BasePower9,
	BasePower10,
	BasePower11,
	BasePower12,
	BasePower13,
	--Second Batch
	Tent1,
	Tent2,
	Weap,
	Ref1,
	Ref2,
	Dome,
	ATech,
	Naval,
	Fix,
	Helipad1,
	Helipad2
	--Third Batch
	}

BaseHelis = { Heli1, Heli2 }

--POWER
Trigger.OnKilled(EBasePower1, function()
	BasePower1.exists = false
end)
Trigger.OnKilled(EBasePower2, function()
	BasePower2.exists = false
end)
Trigger.OnKilled(EBasePower3, function()
	BasePower3.exists = false
end)
Trigger.OnKilled(EBasePower4, function()
	BasePower4.exists = false
end)
Trigger.OnKilled(EBasePower5, function()
	BasePower5.exists = false
end)
Trigger.OnKilled(EBasePower6, function()
	BasePower6.exists = false
end)
Trigger.OnKilled(EBasePower7, function()
	BasePower7.exists = false
end)
Trigger.OnKilled(EBasePower8, function()
	BasePower8.exists = false
end)
Trigger.OnKilled(EBasePower9, function()
	BasePower9.exists = false
end)
Trigger.OnKilled(EBasePower10, function()
	BasePower10.exists = false
end)
Trigger.OnKilled(EBasePower11, function()
	BasePower11.exists = false
end)
Trigger.OnKilled(EBasePower12, function()
	BasePower12.exists = false
end)
Trigger.OnKilled(EBasePower13, function()
	BasePower13.exists = false
end)
--GUNS
Trigger.OnKilled(EGun1, function()
	Gun1.exists = false
end)
Trigger.OnKilled(EGun2, function()
	Gun2.exists = false
end)
Trigger.OnKilled(EGun3, function()
	Gun3.exists = false
end)
Trigger.OnKilled(EGun4, function()
	Gun4.exists = false
end)
Trigger.OnKilled(EGun5, function()
	Gun5.exists = false
end)
Trigger.OnKilled(EGun6, function()
	Gun6.exists = false
end)
Trigger.OnKilled(EGun7, function()
	Gun7.exists = false
end)
--GAPS
Trigger.OnKilled(EGap1, function()
	Gap1.exists = false
end)
Trigger.OnKilled(EGap2, function()
	Gap2.exists = false
end)
--PBOXES
Trigger.OnKilled(EPbox1, function()
	Pbox1.exists = false
end)
Trigger.OnKilled(EPbox2, function()
	Pbox2.exists = false
end)
Trigger.OnKilled(EPbox3, function()
	Pbox3.exists = false
end)
Trigger.OnKilled(EPbox4, function()
	Pbox4.exists = false
end)
Trigger.OnKilled(EPbox5, function()
	Pbox5.exists = false
end)
--HBOXES
Trigger.OnKilled(EHbox1, function()
	Hbox1.exists = false
end)
Trigger.OnKilled(EHbox2, function()
	Hbox2.exists = false
end)
--AAGUNS
Trigger.OnKilled(EAAGun1, function()
	AAGun1.exists = false
end)
Trigger.OnKilled(EAAGun2, function()
	AAGun2.exists = false
end)
Trigger.OnKilled(EAAGun3, function()
	AAGun3.exists = false
end)
Trigger.OnKilled(EAAGun4, function()
	AAGun4.exists = false
end)
Trigger.OnKilled(EAAGun5, function()
	AAGun5.exists = false
end)
Trigger.OnKilled(EAAGun6, function()
	AAGun6.exists = false
end)
Trigger.OnKilled(EAAGun7, function()
	AAGun7.exists = false
end)
Trigger.OnKilled(EAAGun8, function()
	AAGun8.exists = false
end)
Trigger.OnKilled(EAAGun9, function()
	AAGun9.exists = false
end)
--HPADS
Trigger.OnKilled(EHelipad1, function()
	Helipad1.exists = false
end)
Trigger.OnKilled(EHelipad2, function()
	Helipad2.exists = false
end)
--PRODUCTION
Trigger.OnKilled(ETent1, function()
	Tent1.exists = false
end)
Trigger.OnKilled(ETent2, function()
	Tent2.exists = false
end)
Trigger.OnKilled(EWeap, function()
	Weap.exists = false
end)
Trigger.OnKilled(ENaval, function()
	Naval.exists = false
end)
--REFS
Trigger.OnKilled(ERef1, function()
	Ref1.exists = false
end)
Trigger.OnKilled(ERef2, function()
	Ref2.exists = false
end)
--OTHER
Trigger.OnKilled(EDome, function()
	Dome.exists = false
end)
Trigger.OnKilled(EATech, function()
	ATech.exists = false
end)
Trigger.OnKilled(EFix, function()
	Fix.exists = false
end)
--SILOS
Trigger.OnKilled(ESilo1, function()
	Silo1.exists = false
end)
Trigger.OnKilled(ESilo2, function()
	Silo2.exists = false
end)
Trigger.OnKilled(ESilo3, function()
	Silo3.exists = false
end)
Trigger.OnKilled(ESilo4, function()
	Silo4.exists = false
end)
--HELIS
Trigger.OnKilled(EHeli1, function()
	Heli1.exists = false
end)
Trigger.OnKilled(EHeli2, function()
	Heli2.exists = false
end)