if Map.LobbyOption("difficulty") == "easy" then
	SniperType = "sniper"
	PlayerCash = 6000
elseif Map.LobbyOption("difficulty") == "normal" then
	SniperType = "sniper.noautotarget"
	PlayerCash = 6000
else
	SniperType = "sniper.noautotarget"
	PlayerCash = 5000
end

StartObjectives = false

FlareType = "flare"
HiddenPboxes = 
	{
	}

ReocuppiedBunkers = false
alerted = false
tranEntry = false
RunningAI = false

ChopperUnits = {"e6", "e6", "e6", "e6", "e6", "e1", "e1", "e1"}
Chopper = "tran"
ChopperPath = { ChopperEntry.Location, ChopperDst.Location }

HeliRemoveArea = --Innecessary
	{
	CPos.New(11,74), CPos.New(11,75), CPos.New(11,76), CPos.New(60,119), CPos.New(61,119), 
	CPos.New(62,119), CPos.New(12,74), CPos.New(12,75), CPos.New(12,76), CPos.New(60,118), 
	CPos.New(61,118), CPos.New(62,118)
	}

PBaseGuards = { PBaseGuard1, PBaseGuard2, PBaseGuard3, PBaseGuard4 }

AreaSpottedOutpost =
	{
	CPos.New(11,37), CPos.New(12,37), CPos.New(13,37), CPos.New(14,37),
	CPos.New(15,37), CPos.New(16,37), CPos.New(17,37), CPos.New(18,37)
	}

AlertingArea = 
	{ 
	CPos.New(65,38), CPos.New(66,38), CPos.New(67,38), CPos.New(68,38), CPos.New(69,38),
	CPos.New(70,38), CPos.New(71,38), CPos.New(72,38), CPos.New(73,38), CPos.New(74,38)
	}

BaseFound = { CPos.New(52,26), CPos.New(53,26), CPos.New(54,28), CPos.New(54,29), CPos.New(54,30), CPos.New(54,31)}

BaseEntered = 
	{
	CPos.New(78,16), CPos.New(78,17), CPos.New(78,18), CPos.New(64,30), 
	CPos.New(65,30), CPos.New(66,30), CPos.New(67,30), CPos.New(68,30),
	CPos.New(69,30), CPos.New(70,30), CPos.New(71,30), CPos.New(74,30)
	}

BaseAttackableArea =
	{
	CVec.New(0,1), CVec.New(0,2), CVec.New(0,3), CVec.New(0,-1), CVec.New(0,-2), CVec.New(0,-3),
	CVec.New(1,0), CVec.New(2,0), CVec.New(3,0), CVec.New(-1,0), CVec.New(-2,0), CVec.New(-3,0),
	CVec.New(1,1), CVec.New(2,2), CVec.New(3,3), CVec.New(-1,-1), CVec.New(-2,-2), CVec.New(-3,-3),
	CVec.New(-1,1), CVec.New(-2,2), CVec.New(-3,3), CVec.New(1,-1), CVec.New(2,-2), CVec.New(3,-3)
	}

WallsToGive =
{
brik1,brik2,brik3,brik4,brik5,brik6,brik7,brik8,brik9,brik10,brik11,brik12,brik13,brik14,brik15,brik16,brik17,brik18,brik19,brik20,brik21,brik22,brik23,brik23,brik24,brik25,brik26,brik27,brik28,brik29,brik30,brik31,brik32,brik33,brik34,brik35,brik36,brik37,brik38,brik39,brik40,brik41,brik42,brik43,brik44,brik45,brik46,brik47,brik48,brik49,
}

DisarmableHboxes = { pbase_hbox_1, pbase_hbox_2, pbase_hbox_3, pbase_hbox_4, pbase_hbox_5, pbase_hbox_6 }
HboxesGunners = 
	{ 
	pbase_hbox1_gunner, pbase_hbox2_gunner, pbase_hbox3_gunner, pbase_hbox4_gunner, pbase_hbox5_gunner, pbase_hbox6_gunner
	}

PBaseEngineers = { PBaseEngi1, PBaseEngi2 }

PBaseDefendingUnits =
	{
	PBaseDefVehicle1, PBaseDefVehicle2, PBaseDefInf1, PBaseDefInf2, PBaseDefInf3, PBaseDefInf4,
	PBaseDefInf5, PBaseDefInf6, PBaseDefInf7, PBaseDefInf8, PBaseDefInf9  
	}

SpawnSovietTeam = function()
	Reinforcements.Reinforce(player, {SniperType,SniperType,SniperType}, {SovietTroopsEntry.Location, SovietDst3.Location},0)
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Reinforcements.Reinforce(player, {"e2","e2"}, {SovietTroopsEntry.Location, SovietDst2.Location},0)
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Reinforcements.Reinforce(player, {"e1","e1","e1"}, {SovietTroopsEntry.Location, SovietDst1.Location},0)
			StartObjectives = true
		end)
	end)

end

GiveWalls = function()
	Utils.Do(WallsToGive, function(w)
		if not w.IsDead then
			w.Owner = player
		end
	end)
end

Trigger.OnEnteredFootprint(AreaSpottedOutpost, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		if not outpostCam then
			OutCam1 = Actor.Create("camera.basemini", true, { Owner = player, Location = OutpostCam1.Location })
			OutCam2 = Actor.Create("camera.basemini", true, { Owner = player, Location = OutpostCam2.Location })
			Trigger.AfterDelay(DateTime.Seconds(20), function()
				OutCam1.Destroy()
				OutCam2.Destroy()
			end)
		end
	end
end)

Trigger.OnCapture(PFact, function()
	ElminateAllies = player.AddPrimaryObjective("Remove allied presence in the area.")
	if RunningAI == false then
		RunningAI = true
		ActivateAI()
	end
	player.MarkCompletedObjective(RecaptureBase)
	
	GiveWalls()
	
	Trigger.AfterDelay(DateTime.Seconds(60), function()
		if alerted == false then
			AlertedUnitsAttack()
		end
	end)
end)

Trigger.OnEnteredFootprint(BaseEntered, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		if tranEntry == false then
			tranEntry = true
			Trigger.AfterDelay(DateTime.Seconds(120), function() 
				if RunningAI == false then
					RunningAI = true
					ActivateAI()
				end
			end)
			SetAlarmGuards() --[[On Entering Base]]
			HarassCapturedBase()
			TranEntry() --[[TranEntry]]
		end
	end
end)

SetAlarmGuards = function()
	Trigger.AfterDelay(DateTime.Seconds(0.1), function() end) 
	Utils.Do(PBaseGuards, function(unit) 
		if not unit.IsDead then
			unit.Move(AlertingLoc.Location)
		end
	end)
end

TranEntry = function()
	local flare = Actor.Create(FlareType, true, { Owner = player, Location = ChopperDst.Location })
	Reinforcements.ReinforceWithTransport(player, Chopper, ChopperUnits, ChopperPath )
	Trigger.AfterDelay(DateTime.Seconds(60), function()
		flare.Destroy()
		if BaseCamA then
			BaseCamA.Destroy()
		end
		if BaseCamB then
			BaseCamB.Destroy()
		end
		if BaseCamC then
			BaseCamC.Destroy()
		end
		if BaseCamD then
			BaseCamD.Destroy()
		end
	end)
end

Trigger.OnEnteredFootprint(BaseFound, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		BaseCamA = Actor.Create("camera.base", true, { Owner = player, Location = PBaseCam.Location })
		BaseCamB = Actor.Create("camera.basemini", true, { Owner = player, Location = PBaseExtraCam1.Location })
		BaseCamC = Actor.Create("camera.basemini", true, { Owner = player, Location = PBaseExtraCam2.Location })
		BaseCamD = Actor.Create("camera.basemini", true, { Owner = player, Location = PBaseExtraCam3.Location })
	end
end)

Trigger.OnAllKilled(HboxesGunners, function()
	if ReocuppiedBunkers == false then
		player.MarkCompletedObjective(ClearBunkers)
	end
end)

HarassCapturedBase = function()
	
	if not pbase_hbox1_gunner.IsDead and not pbase_hbox_1.IsDead then
		pbase_hbox1_gunner.EnterTransport(pbase_hbox_1)
	end
	if not pbase_hbox2_gunner.IsDead and not pbase_hbox_2.IsDead then
		pbase_hbox2_gunner.EnterTransport(pbase_hbox_2)
	end
	if not pbase_hbox3_gunner.IsDead and not pbase_hbox_3.IsDead then 
		pbase_hbox3_gunner.EnterTransport(pbase_hbox_3)
	end
	if not pbase_hbox4_gunner.IsDead and not pbase_hbox_4.IsDead then
		pbase_hbox4_gunner.EnterTransport(pbase_hbox_4)
	end
	if not pbase_hbox5_gunner.IsDead and not pbase_hbox_5.IsDead then
		pbase_hbox5_gunner.EnterTransport(pbase_hbox_5)
	end
	if not pbase_hbox6_gunner.IsDead and not pbase_hbox_6.IsDead then
		pbase_hbox6_gunner.EnterTransport(pbase_hbox_6)
	end
	
	Utils.Do(HboxesGunners, function(unit)
		if not unit.IsDead then
			player.MarkFailedObjective(ClearBunkers)
		end
	end)
	
	ReocuppiedBunkers = true
end

GiveCredits = function()
	enemy.Cash = 10000
	goodguy.Cash = 10000
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		GiveCredits()
	end)
end

Trigger.OnEnteredFootprint(AlertingArea, function(a, id)
	if a.Owner == enemy and alerted == false then
		Trigger.RemoveFootprintTrigger(id)
		AlertedUnitsAttack()
	end
end)

AlertedUnitsAttack = function()
	alerted = true
	Utils.Do(PBaseDefendingUnits, function(unit)
		if not unit.IsDead then
			unit.AttackMove(PBaseCam.Location)
			IdleHunt(unit)
		end
	end)
	Utils.Do(PBaseEngineers, function(unit)
		if not unit.IsDead then
			unit.Move(PBaseCam.Location)
		end
	end)
end

ActivateAI = function()
	BuildBase()
	--CheckHelis()
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		ProduceTanks()
		ProduceInfantry()
	end)
	Trigger.AfterDelay(DateTime.Minutes(3), function()
		SeaAttack()
		ProduceArty()
	end)
	Trigger.AfterDelay(DateTime.Minutes(4), function()
		HeliAttack()
	end)
	Trigger.AfterDelay(DateTime.Minutes(20), ChronoAttack)

end

Tick = function()
	if player.HasNoRequiredUnits() and StartObjectives == true then
		enemy.MarkCompletedObjective(alliesObj)
	end
	
	if enemy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(ElminateAllies)
	end
end

InitTriggers = function()
	player.Cash = PlayerCash
	
	Camera.Position = StartingCamPos.CenterPosition
	
	SpawnSovietTeam()
	
	GiveCredits()
	
	if HardBoats == true then
		AddBoats()
	end
	
	ETent1.IsPrimaryBuilding = true
	Outpost2_Weap.IsPrimaryBuilding = true
	
	if ETent1.IsDead or ETent1.Owner ~= enemy then
		if Tent1.exist == true or Tent1.Owner ~= enemy then
			Tent1.IsPrimaryBuilding = true
		end
	end
	
	--HeliDefence()
	
	Utils.Do(PBaseDefendingUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			if alerted == false then
				AlertedUnitsAttack()
			end
		end)
	end)
	
	Utils.Do(PBaseGuards, function(unit)
		Trigger.OnDamaged(unit, function()
			if not unit.IsDead then
				unit.Move(AlertingLoc.Location)
			end
		end)
	end)
	
	--Trigger.AfterDelay(DateTime.Seconds(5), CheckHelis)
end
	
InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	RecaptureBase = player.AddPrimaryObjective("Recapture your base at north-east.")
	ClearBunkers = player.AddSecondaryObjective("Prevent recrewing of camo-pillboxes before recapturing your base.")
	alliesObj =  enemy.AddPrimaryObjective("Eliminate all soviet presence in the area.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)


end
	
WorldLoaded = function()
	enemy = Player.GetPlayer("Greece")
	goodguy = Player.GetPlayer("GoodGuy")
	player = Player.GetPlayer("USSR")

	InitTriggers()
	InitObjectives()
end