if Map.LobbyOption("difficulty") == "easy" then
	atkGroup = 
	{
	{"arty.reach"},
	{"2tnk","2tnk","1tnk","arty"}
	}
	
	waterAtkGroup = {"e1", "e1", "1tnk", "1tnk", "1tnk"}
	waterDelay = DateTime.Minutes(4)
	AttackNorthDelay = 5
elseif Map.LobbyOption("difficulty") == "normal" then
	atkGroup = 
	{
	{"arty.reach","arty.reach"},
	{"2tnk","2tnk","2tnk","1tnk","1tnk","arty","arty"}
	}
	waterAtkGroup = {"e1", "e3", "1tnk", "2tnk", "2tnk"}
	waterDelay = DateTime.Minutes(3)
	AttackNorthDelay = 4
else
	atkGroup = 
	{
	{"arty.reach","arty.reach"},
	{"2tnk","2tnk","2tnk","1tnk","1tnk","arty","arty"}
	}
	waterAtkGroup = {"e1", "e1", "arty", "2tnk", "2tnk"}
	waterDelay = DateTime.Minutes(2)
	AttackNorthDelay = 3
end

outpostDisabled = false

WaterReinforcements =
{
{AlliedNavalReinforce1.Location, AlliedNavalDst1.Location},
{AlliedNavalReinforce2.Location, AlliedNavalDst2.Location}
}

WaterArtyPath = { AlliedNavalReinforce1.Location, AlliedNavalArtyDst.Location }

ArtyAtkSouthTarget =
{
ArtySpotSouth1.Location, ArtySpotSouth2.Location, ArtySpotSouth3.Location
}

AtkNorthTarget =
{
{OoMAtk2Entry.Location, ArtySpotNorth1.Location},
{OoMAtk2Entry.Location, OoMAtk2Dst.Location}
}

AlliedInfantryTypes = {"e1", "e1", "e3"}
InfAtkCount = 12
InfAtk = {}

AlliedVehicleTypes = {"1tnk", "1tnk", "2tnk", "2tnk", "jeep", "arty"}
VehlAtkCount = 7
VehlAtk = {}

LandAtkPaths =
{
{AtkPathA1, AtkPathA2, AtkTarget},
{AtkPathB1, AtkPathC3, AtkPathC4, AtkTarget},
{AtkPathC1, AtkPathC2, AtkPathC3, AtkPathC4, AtkTarget},
{AtkTarget}
}

SendLandAttack = function(rng)
	if not rng then
		rng = Utils.RandomInteger(1,3)
	end
	local units = Reinforcements.Reinforce(goodguy, atkGroup[rng], AtkNorthTarget[rng], 1)
	
	Utils.Do(units, function(u)
		--u.AttackMove(AtkNorthTarget[rng][2])
		IdleHunt(u)
	end)
	Trigger.AfterDelay(DateTime.Minutes(AttackNorthDelay), function()
		AttackNorthDelay = AttackNorthDelay + 1
		
		SendLandAttack()
	end)
end

SendWaterArties = function()
	local artyCam = Actor.Create("camera", true, { Owner = enemy, Location = spawnSouthArtyCam.Location })
	
	local endpoint = Utils.Random(ArtyAtkSouthTarget)
	local arties = 
	Reinforcements.ReinforceWithTransport(goodguy, "lst", {"arty.reach","arty.reach"}, WaterArtyPath, {WaterArtyPath[1]})[2]
	
	Utils.Do(arties, function(u)
		Trigger.OnAddedToWorld(u, function()
			u.AttackMove(endpoint)
		end)
	end)
	
	if artyCam then
		artyCam.Destroy()
	end
end

WaterAttacks = function()
	if outpostDisabled == true then
		return
	end
 
	local atkType = Utils.Random( {"1", "2", "3"} )
	
	if atkType ~= "1" then
		local dst = Utils.Random(WaterReinforcements)
		local target = AtkTarget.Location
		local units = Reinforcements.ReinforceWithTransport(goodguy, "lst", waterAtkGroup, dst, {dst[1]})[2]
		
		Utils.Do(units, function(u)
			Trigger.OnAddedToWorld(u, function()
				u.AttackMove(target)
				IdleHunt(u)
			end)
		end)
	else
		SendWaterArties()
	end
	
	Trigger.AfterDelay(waterDelay, function()
		WaterAttacks()
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if EFact.IsDead or EFact.Owner ~= enemy then 
			return
		end
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EFactLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost
		building.exists = true
		Media.Debug("Building ready - ".. building.type)
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function()
	if Tent.exists == false --[[or Tent.Owner ~= enemy]] then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		Media.Debug("Production disabled (Tent) - " .. "Tent.exists = " .. tostring(Tent.exists))
		Trigger.AfterDelay(DateTime.Minutes(1), function()
			ProduceInfantry()
		end)
		return
	end
	Media.Debug("Production initiated (Tent) - " .. "Tent.exists = " .. tostring(Tent.exists))
	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(AlliedInfantryTypes) }
	local Path = Utils.Random(LandAtkPaths)
	enemy.Build(toBuild, function(unit)
		InfAtk[#InfAtk + 1] = unit[1]
		if #InfAtk >= InfAtkCount then
			SendUnits(InfAtk, Path)
			Media.Debug("Inf Atk Send - Path Chosen - ".. tostring(Path))
			InfAtk = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceTanks = function()
	if Weap.exists == false --[[or Weap.Owner ~= enemy]] then
		Media.Debug("Production disabled (Weap) - " .. "Weap.exists = " .. tostring(Weap.exists))
		Trigger.AfterDelay(DateTime.Minutes(1), function()
			ProduceTanks()
		end)
		return
	--elseif baseharv.IsDead and enemy.Resources <= 599 then
	--	return
	end
	Media.Debug("Production initiated (Weap) - " .. "Weap.exists = " .. tostring(Tent.exists))
	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(AlliedVehicleTypes) }
	local Path = Utils.Random(LandAtkPaths)
	enemy.Build(toBuild, function(unit)
		VehlAtk[#VehlAtk + 1] = unit[1]
		
		if #VehlAtk >= VehlAtkCount then
			SendUnits(VehlAtk, Path)
			Media.Debug("Tank Atk Send - Path Chosen - ".. tostring(Path))
			VehlAtk = { }
			Trigger.AfterDelay(DateTime.Minutes(1), ProduceTanks)
		else
			Trigger.AfterDelay(delay, ProduceTanks)
		end
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

heliTracker = 0

SendHeliAttack = function()
	local helis = Reinforcements.Reinforce(enemy, {"heli.outofmap","heli.outofmap","heli.outofmap"}, {HeliStrikeSpawn.Location, HeliStrikeSpawn.Location + CVec.New(2,2)}, 1)
	heliTracker = 3
	
	Trigger.AfterDelay(DateTime.Seconds(3), function()
		Utils.Do(helis, function(h)
			Trigger.OnRemovedFromWorld(h, function()
				heliTracker = heliTracker - 1
			end)
			if not h.IsDead then
				h.AttackMove(AtkTarget.Location)
			end
		end)
	end)
	CheckHeliAtk()
	
	Trigger.AfterDelay(DateTime.Minutes(4), SendHeliAttack)
end

CheckHeliAtk = function()
	local helisLeaving = Utils.Where(Map.ActorsInWorld, function(self) return self.Type == "heli.outofmap" and self.AmmoCount() == 0 end)

	if #helisLeaving > 0 then
		Utils.Do(helisLeaving, function(h)
			if not h.IsDead then
				h.Stop()
				h.Move(HeliStrikeSpawn.Location)
				h.Destroy()
			end
		end)
	end
	
	if heliTracker > 0 then
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			CheckHeliAtk()
		end)
	else
		--Media.Debug("checking..." .. " All heli removed " .. tostring(heliTracker))
		return
	end
end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end