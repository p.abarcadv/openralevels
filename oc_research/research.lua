if Map.LobbyOption("difficulty") == "easy" then
	StartingFunds = 3000
	teslaBuildings = 1
elseif Map.LobbyOption("difficulty") == "normal" then
	StartingFunds = 2000
	teslaBuildings = 2
else
	StartingFunds = 2000
	teslaBuildings = 3
end

DefensiveCopters = {DefensiveCopter1, DefensiveCopter2, DefensiveCopter3, OutpostCopter}
BaseHpads = {EHpad1, EHpad2, EHpad3, OutHpad}

Trigger.OnKilled(SovTek, function()
	player.MarkFailedObjective(ProtectSTek)
end)

SetupDifficulty = function()
	
end

GiveCashToAI = function()
	enemy.Cash = enemy.Cash + 20000
	
	Trigger.AfterDelay(DateTime.Minutes(3), GiveCashToAI)
end

RunAI = function()
	enemy.Cash = 200000
	
	GiveCashToAI()
	
	BuildBase()

	ProduceInfantry()
	ProduceTanks()
	
	WaterAttacks()
	SendLandAttack(1)
	
	Trigger.AfterDelay(DateTime.Minutes(3), SendHeliAttack)
end

Trigger.OnKilledOrCaptured(SecondaryObj, function()
	player.MarkCompletedObjective(DisableDome)
	outpostDisabbled = false
end)

TeslasAvailable = function()
	if teslaBuildings == 2 then
		Tesla2.Destroy()
		Tesla3.Destroy()
	end
	if teslaBuildings == 3 then
		Tesla1.Destroy()
		Tesla2.Destroy()
		Tesla3.Destroy()
	end
end

Tick = function()
	if enemy.HasNoRequiredUnits() and goodguy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(ProtectSTek)
		player.MarkCompletedObjective(DenyAllies)
	end
end

SetupMood = function()

end

Flavor = function()
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		local landings = BaseHpads
		local aircrafts = DefensiveCopters
		
		for i = 1, #aircrafts, 1 do
			aircrafts[i].ReturnToBase(landings[i])
		end
	end)
end

InitTriggers = function()
	Camera.Position = StartingCamLoc.CenterPosition
	player.Cash = StartingFunds
	
	TeslasAvailable()
	
	RunAI()
	
	Flavor()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	ProtectSTek = player.AddPrimaryObjective("Protect the Technology Center")
	DenyAllies = player.AddPrimaryObjective("Destroy Allied presence in the area.")
	DisableDome = player.AddSecondaryObjective("Capture or detroy the Radar Dome to negate \nfurther enemy reinforcements.")
	
	DenySoviets = enemy.AddPrimaryObjective("Deny the Allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)

end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	goodguy = Player.GetPlayer("GoodGuy")
	turkey = Player.GetPlayer("Turkey")
	
	SetupMood()
	InitTriggers()
	InitObjectives()
end