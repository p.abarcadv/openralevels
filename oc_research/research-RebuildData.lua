Power1	={type = "apwr", pos = CVec.New(-4,-9), cost = 500, exists = true }
Power2	={type = "apwr", pos = CVec.New(-4,-6), cost = 500, exists = true }
Power3	={type = "powr", pos = CVec.New(-1,-8), cost = 300, exists = true }
Power4	={type = "apwr", pos = CVec.New(1,-9), cost = 500, exists = true }
Power5	={type = "apwr", pos = CVec.New(0,-5), cost = 500, exists = true }
Power6	={type = "apwr", pos = CVec.New(3,-6), cost = 500, exists = true }
Power7	={type = "powr", pos = CVec.New(4,-9), cost = 300, exists = true }
Power8	={type = "apwr", pos = CVec.New(-4,3), cost = 500, exists = true }
Power9	={type = "apwr", pos = CVec.New(6,-6), cost = 500, exists = true }
Power10	={type = "powr", pos = CVec.New(2,0), cost = 300, exists = true }

Tent	={type = "tent", pos = CVec.New(12,1), cost = 300, exists = true }
Weap	={type = "weap", pos = CVec.New(10,-6), cost = 300, exists = true }
Ref1	={type = "proc", pos = CVec.New(0,4), cost = 300, exists = true }

Dome	={type = "dome", pos = CVec.New(7,3), cost = 1000, exists = true }
Fix		={type = "fix", pos = CVec.New(7,-2), cost = 1000, exists = true }

Hpad1	={type = "hpad", pos = CVec.New(7,-9), cost = 1000, exists = true }
Hpad2	={type = "hpad", pos = CVec.New(9,-9), cost = 1000, exists = true }
Hpad3	={type = "hpad", pos = CVec.New(11,-9), cost = 1000, exists = true }

Atek	={type = "atek", pos = CVec.New(-4,-1), cost = 1000, exists = true }

Silo1	={type = "silo", pos = CVec.New(-3,6), cost = 150, exists = true }
Silo2	={type = "silo", pos = CVec.New(-3,7), cost = 150, exists = true }
Silo3	={type = "silo", pos = CVec.New(-2,6), cost = 150, exists = true }
Silo4	={type = "silo", pos = CVec.New(-2,7), cost = 150, exists = true }

PBox1	={type = "pbox", pos = CVec.New(0,11), cost = 500, exists = true}
PBox2	={type = "pbox", pos = CVec.New(6,11), cost = 500, exists = true}
PBox3	={type = "pbox", pos = CVec.New(16,7), cost = 500, exists = true}
PBox4	={type = "pbox", pos = CVec.New(5,-3), cost = 500, exists = true}

Gun1	={type = "gun", pos = CVec.New(-2,11), cost = 500, exists = true}
Gun2	={type = "gun", pos = CVec.New(8,11), cost = 500, exists = true}
Gun3	={type = "gun", pos = CVec.New(10,11), cost = 500, exists = true}
Gun4	={type = "gun", pos = CVec.New(12,11), cost = 500, exists = true}
Gun5	={type = "gun", pos = CVec.New(16,9), cost = 500, exists = true}
Gun6	={type = "gun", pos = CVec.New(16,-2), cost = 500, exists = true}
Gun7	={type = "gun", pos = CVec.New(16,-4), cost = 500, exists = true}
Gun7	={type = "gun", pos = CVec.New(16,-4), cost = 500, exists = true}
Gun8	={type = "gun", pos = CVec.New(16,-6), cost = 500, exists = true}

AGun1	={type = "agun", pos = CVec.New(-2,-3), cost = 500, exists = true}
AGun2	={type = "agun", pos = CVec.New(-2,9), cost = 500, exists = true}
AGun3	={type = "agun", pos = CVec.New(13,9), cost = 500, exists = true}
AGun4	={type = "agun", pos = CVec.New(13,-4), cost = 500, exists = true}

Hbox	={type = "hbox",pos = CVec.New(1,-6), cost = 500, exists = true}

Gap1	={type = "gap", pos = CVec.New(14,-4), cost = 1000, exists = true}
Gap2	={type = "gap", pos = CVec.New(11,9), cost = 1000, exists = true}
Gap3	={type = "gap", pos = CVec.New(1,-2), cost = 1000, exists = true}

BaseBuildings =
{
Power3,
Tent,
Ref1,
Power1,
Power2,
Power4,
Power5,
Power6,
Power7,
Power8,
Power9,
Weap,
Dome,
Fix,
Hpad1,
Hpad2,
Hpad3,
Atek,
Silo1,
Silo2,
Silo3,
Silo4,
PBox1,
PBox2,
PBox3,
PBox4,
Gun1,
Gun2,
Gun3,
Gun4,
Gun5,
Gun6,
Gun7,
Gun8,
AGun1,
AGun2,
AGun3,
AGun4,
Gap1,
Gap2,
Gap3,
Hbox
}

Trigger.OnKilled(EPower1, function()
	Power1.exists = false
end)

Trigger.OnKilled(EPower2, function()
	Power2.exists = false
end)

Trigger.OnKilled(EPower3, function()
	Power3.exists = false
end)

Trigger.OnKilled(EPower4, function()
	Power4.exists = false
end)

Trigger.OnKilled(EPower5, function()
	Power5.exists = false
end)

Trigger.OnKilled(EPower6, function()
	Power6.exists = false
end)

Trigger.OnKilled(EPower7, function()
	Power7.exists = false
end)

Trigger.OnKilled(EPower8, function()
	Power8.exists = false
end)

Trigger.OnKilled(EPower9, function()
	Power9.exists = false
end)

Trigger.OnKilled(EPower10, function()
	Power10.exists = false
end)

Trigger.OnKilled(ETent, function()
	Tent.exists = false
end)

Trigger.OnKilled(EWeap, function()
	Weap.exists = false
end)

Trigger.OnKilled(ERef1, function()
	Ref1.exists = false
end)

Trigger.OnKilled(ESilo1, function()
	Silo1.exists = false
end)

Trigger.OnKilled(ESilo1, function()
	Silo2.exists = false
end)

Trigger.OnKilled(ESilo3, function()
	Silo3.exists = false
end)

Trigger.OnKilled(ESilo4, function()
	Silo4.exists = false
end)

Trigger.OnKilled(EDome, function()
	Dome.exists = false
end)

Trigger.OnKilled(EFix, function()
	Fix.exists = false
end)

Trigger.OnKilled(EPbox1, function()
	PBox1.exists = false
end)

Trigger.OnKilled(EPbox2, function()
	PBox2.exists = false
end)

Trigger.OnKilled(EPbox3, function()
	PBox3.exists = false
end)

Trigger.OnKilled(EPbox4, function()
	PBox4.exists = false
end)

Trigger.OnKilled(EGun1, function()
	Gun1.exists = false
end)

Trigger.OnKilled(EGun2, function()
	Gun2.exists = false
end)

Trigger.OnKilled(EGun3, function()
	Gun3.exists = false
end)

Trigger.OnKilled(EGun4, function()
	Gun4.exists = false
end)

Trigger.OnKilled(EGun5, function()
	Gun5.exists = false
end)

Trigger.OnKilled(EGun6, function()
	Gun6.exists = false
end)

Trigger.OnKilled(EGun7, function()
	Gun7.exists = false
end)

Trigger.OnKilled(EGun8, function()
	Gun8.exists = false
end)

Trigger.OnKilled(EAGun1, function()
	AGun1.exists = false
end)

Trigger.OnKilled(EAGun2, function()
	AGun2.exists = false
end)

Trigger.OnKilled(EAGun3, function()
	AGun3.exists = false
end)

Trigger.OnKilled(EAGun4, function()
	AGun4.exists = false
end)

Trigger.OnKilled(EGap1, function()
	Gap1.exists = false
end)

Trigger.OnKilled(EGap2, function()
	Gap2.exists = false
end)

Trigger.OnKilled(EGap3, function()
	Gap3.exists = false
end)
	
Trigger.OnKilled(EHpad1, function()
	Hpad1.exists = false
end)

Trigger.OnKilled(EHpad2, function()
	Hpad2.exists = false
end)

Trigger.OnKilled(EHpad3, function()
	Hpad3.exists = false
end)

Trigger.OnKilled(EAtek, function()
	Atek.exists = false
end)

Trigger.OnKilled(EHbox, function()
	Hbox.exists = false
end)