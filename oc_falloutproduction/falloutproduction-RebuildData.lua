
Barr1 = {type = "barr", pos = CVec.New(4,3), cost = 500, exists = true}
Weap1 = {type = "weap", pos = CVec.New(13,3), cost = 2000, exists = true}
Hpad1 = {type = "hpad", pos = CVec.New(5,-7), cost = 1000, exists = true}

Power1 = {type = "apwr", pos = CVec.New(9,1), cost = 500, exists = true}
Power2 = {type = "apwr", pos = CVec.New(6,-1), cost = 500, exists = true}
Power3 = {type = "apwr", pos = CVec.New(9,-2), cost = 500, exists = true}
Power4 = {type = "apwr", pos = CVec.New(12,-1), cost = 500, exists = true}
Power5 = {type = "apwr", pos = CVec.New(9,-5), cost = 500, exists = true}
Power6 = {type = "apwr", pos = CVec.New(12,-4), cost = 500, exists = true}
Power7 = {type = "powr", pos = CVec.New(9,-10), cost = 300, exists = true}
Power8 = {type = "apwr", pos = CVec.New(9,-13), cost = 500, exists = true}
Power9 = {type = "powr", pos = CVec.New(-3,0), cost = 300, exists = true}
Power10 = {type = "apwr", pos = CVec.New(-4,-16), cost = 500, exists = true}
Power11 = {type = "apwr", pos = CVec.New(-1,-17), cost = 500, exists = true}
Power12 = {type = "powr", pos = CVec.New(18,3), cost = 300, exists = true}

Dome = {type = "powr", pos = CVec.New(-16,3), cost = 1000, exists = true}
Fix = {type = "fix", pos = CVec.New(28,-13), cost = 1000, exists = true}
STek = {type = "stek", pos = CVec.New(3,-1), cost = 1500, exists = true}

Afld1 = {type = "afld", pos = CVec.New(-1,3), cost = 500, exists = true}
Afld2 = {type = "afld", pos = CVec.New(-1,5), cost = 500, exists = true}

Tesla1 = {type = "tsla", pos = CVec.New(19,1), cost = 1500, exists = true}
Tesla2 = {type = "tsla", pos = CVec.New(-3,5), cost = 1500, exists = true}
Tesla3 = {type = "tsla", pos = CVec.New(15,7), cost = 1500, exists = true}
Tesla4 = {type = "tsla", pos = CVec.New(5,7), cost = 1500, exists = true}
Tesla5 = {type = "tsla", pos = CVec.New(1,-6), cost = 1500, exists = true}
Tesla6 = {type = "tsla", pos = CVec.New(32,-10), cost = 1500, exists = true}

Ftur1 = {type = "ftur", pos = CVec.New(13,9), cost = 600, exists = true}
Ftur2 = {type = "ftur", pos = CVec.New(7,9), cost = 600, exists = true}
Ftur3 = {type = "ftur", pos = CVec.New(20,-5), cost = 600, exists = true}

Sam1 = {type = "sam", pos = CVec.New(18,2), cost = 1000, exists = true} 
Sam2 = {type = "sam", pos = CVec.New(16,7), cost = 1000, exists = true}
Sam3 = {type = "sam", pos = CVec.New(3,7), cost = 1000, exists = true}
Sam4 = {type = "sam", pos = CVec.New(-3,7), cost = 1000, exists = true}
Sam5 = {type = "sam", pos = CVec.New(-2,-2), cost = 1000, exists = true}
Sam6 = {type = "sam", pos = CVec.New(11,-8), cost = 1000, exists = true}
Sam7 = {type = "sam", pos = CVec.New(-1,-12), cost = 1000, exists = true}

--Silo1 = {type = "silo", pos = CVec.New(20,-5), cost = 300, exists = true}
--Silo2 = {type = "silo", pos = CVec.New(20,-5), cost = 300, exists = true}
--Silo3 = {type = "silo", pos = CVec.New(20,-5), cost = 300, exists = true}

Kenn = {type = "kenn", pos = CVec.New(20,-5), cost = 300, exists = true}

BaseBuildings =
	{
		Barr1,
		Weap1,
		Hpad1,
		Power1,
		Power2,
		Power3,
		Power4,
		Power5,
		Power6,
		Power7,
		Power8,
		Power9,
		Power10,
		Power11,
		Power12,
		Afld1,
		Afld2,
		Tesla1,
		Tesla2,
		Tesla3,
		Tesla4,
		Tesla5,
		Tesla6,
		Ftur1,
		Ftur2,
		Ftur3,
		Sam1,
		Sam2,
		Sam3,
		Sam4,
		Sam5,
		Sam6,
		Sam7,
		Kenn,
		Dome,
		Fix,
		STek
	}

--Hind1 = {type = "hind", cost = 1000, exists = true}
--Hind2 = {type = "hind", cost = 1000, exists = true}

--BaseHinds =
--	{
--		Hind1,
--		Hind2
--	}
--[[
Trigger.OnKilled(BaseHind1, function()
	Hind1.exists = false
end)

Trigger.OnKilled(BaseHind2, function()
	Hind2.exists = false
end)
]]

Trigger.OnKilled(BaseWeap1, function()
	Weap1.exists = false
end)

Trigger.OnKilled(BaseWeap2, function()
	if not BaseWeap1.IsDead then
		BaseWeap1.IsPrimaryBuilding = true
	end
end)

Trigger.OnKilled(BaseBarr1, function()
	Barr1.exists = false
end)

Trigger.OnKilled(MainbaseHpad1, function()
	Hpad1.exists = false
end)

Trigger.OnKilled(MainbasePwr1, function()
	Power1.exists = false
end)

Trigger.OnKilled(MainbasePwr2, function()
	Power2.exists = false
end)

Trigger.OnKilled(MainbasePwr3, function()
	Power3.exists = false
end)

Trigger.OnKilled(MainbasePwr4, function()
	Power4.exists = false
end)

Trigger.OnKilled(MainbasePwr5, function()
	Power5.exists = false
end)

Trigger.OnKilled(MainbasePwr6, function()
	Power6.exists = false
end)

Trigger.OnKilled(MainbasePwr7, function()
	Power7.exists = false
end)

Trigger.OnKilled(MainbasePwr8, function()
	Power8.exists = false
end)

Trigger.OnKilled(MainbasePwr9, function()
	Power9.exists = false
end)

Trigger.OnKilled(MainbasePwr10, function()
	Power10.exists = false
end)

Trigger.OnKilled(MainbasePwr11, function()
	Power11.exists = false
end)

Trigger.OnKilled(MainbasePwr12, function()
	Power12.exists = false
end)

Trigger.OnKilled(MainbaseAfld1, function()
	Afld1.exists = false
end)

Trigger.OnKilled(MainbaseAfld2, function()
	Afld2.exists = false
end)

Trigger.OnKilled(MainbaseTesla1, function()
	Tesla1.exists = false
end)

Trigger.OnKilled(MainbaseTesla2, function()
	Tesla2.exists = false
end)

Trigger.OnKilled(MainbaseTesla3, function()
	Tesla3.exists = false
end)

Trigger.OnKilled(MainbaseTesla4, function()
	Tesla4.exists = false
end)

Trigger.OnKilled(MainbaseTesla5, function()
	Tesla5.exists = false
end)

Trigger.OnKilled(MainbaseTesla6, function()
	Tesla6.exists = false
end)

Trigger.OnKilled(MainbaseSam1, function()
	Sam1.exists = false
end)

Trigger.OnKilled(MainbaseSam2, function()
	Sam2.exists = false
end)

Trigger.OnKilled(MainbaseSam3, function()
	Sam3.exists = false
end)

Trigger.OnKilled(MainbaseSam4, function()
	Sam4.exists = false
end)

Trigger.OnKilled(MainbaseSam5, function()
	Sam5.exists = false
end)

Trigger.OnKilled(MainbaseSam6, function()
	Sam6.exists = false
end)

Trigger.OnKilled(MainbaseSam7, function()
	Sam7.exists = false
end)

Trigger.OnKilled(MainbaseFtur1, function()
	Ftur1.exists = false
end)

Trigger.OnKilled(MainbaseFtur2, function()
	Ftur2.exists = false
end)

Trigger.OnKilled(MainbaseFtur3, function()
	Ftur3.exists = false
end)

Trigger.OnKilled(MainbaseFix, function()
	Fix.exists = false
end)

Trigger.OnKilled(MainbaseKenn, function()
	Kenn.exists = false
end)

Trigger.OnKilled(MainbaseStek, function()
	STek.exists = false
end)