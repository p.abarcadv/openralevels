
SovietInfantryTypes = {"e1", "e1", "e2", "e4"}
SovietVehicleTypes = {"3tnk", "3tnk", "ftrk", "v2rl"}
SovietAircraftTypes = {"yak", "mig"}

DifficultyIncrement = 0

InfAttack = {}
ArmorAttack = {}
AircraftAtk = {}

InfAtkCount = 8
ArmorAtkCount = 3

paratroopDelay = 4
ParatroopersLZs = {ParadropLZ1.CenterPosition, ParadropLZ2.CenterPosition, ParadropLZ3.CenterPosition}

DemoWeaps = {BadGuyWeap1, BadGuyWeap2, BadGuyWeap3}
InitialTrucks = { DemoTruck1, DemoTruck2 }

AttackPaths =
{
	{Path1, Path2},
	{Path1, Path2},
	{Path3, Path4},
	{Path3, Path4},
	{Path5, Path6},
	{Path5, Path6},
	{Path1, Path2, Path7}
}

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end

IncreaseDifficulty = function()
	ArmorAtkCount = 4
	SovietVehicleTypes = {"3tnk", "3tnk", "4tnk", "4tnk", "v2rl"}
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if EBaseFact.IsDead or EBaseFact.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EBaseFactLocation.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
				
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

--[[
AddCash = function()
	if enemy.Cash < 10000 then
		enemy.Cash = enemy.Cash + 10000
	end
	Trigger.AfterDealy(DateTime.Seconds(15), function()
		AddCash()
	end)
end
]]

PrimaryStructureRandomizer = function()
	local PrimaryWeap = Utils.Random(DemoWeaps)

	if not PrimaryWeap.IsDead and PrimaryWeap.Owner == badguy then

		PrimaryWeap.IsPrimaryBuilding = true
		Trigger.AfterDelay(DateTime.Seconds(20), PrimaryStructureRandomizer)
	else
		if (not DemoWeaps[1].IsDead and DemoWeaps[1].Owner == badguy) or (not DemoWeaps[2].IsDead and DemoWeaps[2].Owner == badguy) or (not DemoWeaps[3].IsDead and DemoWeaps[3].Owner == badguy) then
			PrimaryStructureRandomizer()
		end
	end
end

IncreaseEnemyAtkCount = function()
	Trigger.AfterDelay(DateTime.Minutes(10), function()
		InfAtkCount = 10
		ArmorAtkCount = 4
	end)
	Trigger.AfterDelay(DateTime.Minutes(15), function()
		InfAtkCount = 12
		ArmorAtkCount = 5
	end)
	Trigger.AfterDelay(DateTime.Minutes(20), function()
		InfAtkCount = 14
		ArmorAtkCount = 6
	end)
end

PrimaryStructureSetup = function()
	if not BaseBarr2.IsDead then
		BaseBarr2.IsPrimaryBuilding = true
	end
	
	if not BaseWeap2.IsDead then
		BaseWeap2.IsPrimaryBuilding = true
	end
	--OnAllKilledOrCaptured({BaseBarr2}, function)
	--OnAllKilledOrCaptured({BaseWeap2}, function)
end

SendParatroopers = function()
	local destiny = Utils.Random(ParatroopersLZs)
	if paratroopDelay >= 8 then
		paratroopDelay = 8
	else
		paratroopDelay = paratroopDelay + 1.5
	end
	
	local plane = PowerProxy.TargetParatroopers(destiny, Angle.SouthWest)

	Utils.Do(plane, function(unit)
		Trigger.OnPassengerExited(unit, function(t, p)
			IdleHunt(p)
		end)
	end)
	
	Trigger.AfterDelay(DateTime.Minutes(paratroopDelay), SendParatroopers)
end

ProduceInfantry = function()
	if not ( Barr1.exists or Barr1.Owner ~= enemy ) then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(SovietInfantryTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		
		InfAttack[#InfAttack + 1] = unit[1]
		if #InfAttack >= InfAtkCount then
			SendUnits(InfAttack, Path)
			InfAttack = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceArmor = function()
	if not (Weap1.exists or Weap1.Owner ~= enemy) then
		return
	--elseif baseharv.IsDead and enemy.Resources <= 599 then
	--	return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(SovietVehicleTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		ArmorAttack[#ArmorAttack + 1] = unit[1]

		if #ArmorAttack >= ArmorAtkCount then
			SendUnits(ArmorAttack, Path)
			ArmorAttack = { }
			Trigger.AfterDelay(DateTime.Minutes(1), ProduceArmor)
		else
			Trigger.AfterDelay(delay, ProduceArmor)
		end
	end)
end

ProduceAircraft = function()
	if (MainbaseAfld1.IsDead or MainbaseAfld1.Owner ~= enemy) or (MainbaseAfld2.IsDead or MainbaseAfld2.Owner ~= enemy) then
		return
	end

	enemy.Build(SovietAircraftTypes, function(units)
		local planes = units[1]
		AircraftAtk[#AircraftAtk + 1] = planes
		Trigger.OnKilled(planes, function(units)
			if #AircraftAtk == 1 then
				Trigger.AfterDelay(DateTime.Seconds(10), function()
					ProduceAircraft()
				end)
			end
		end)
		InitializeAttackAircraft(planes, player)
	end)
end

SendInitialTrucks = function()
	local number = Utils.RandomInteger(1, #InitialTrucks+1)
	
	if #InitialTrucks > 0 then
		if not InitialTrucks[number].IsDead then
			TargetDemo(InitialTrucks[number])
			table.remove(InitialTrucks, number)
			Trigger.AfterDelay(DemoDelay, SendInitialTrucks)
		end
	else
		if not DemoTruck3.IsDead then
			TargetDemo(DemoTruck3)
		end
		Trigger.AfterDelay(DemoDelay, ProduceDemos)
	end
end

ProduceDemos = function()
	if (BadGuyWeap1.IsDead or BadGuyWeap1.Owner ~= badguy) and (BadGuyWeap2.IsDead or BadGuyWeap2.Owner ~= badguy) and (BadGuyWeap3.IsDead or BadGuyWeap3.Owner ~= badguy)
	then
		return
	else 
		Media.DisplayMessage("A demoliton truck is in production. Be prepared","Command")
	end
	
	local delay = DateTime.Seconds(1)--DemoDelay-DateTime.Minutes(1)
	local toBuild = {"dtrk"}
	badguy.Build(toBuild, function(units)
		
		TargetDemo(units[1])
		Trigger.AfterDelay(delay, ProduceDemos)
	end)
end

TargetDemo = function(unit)
	Media.DisplayMessage("A demoliton truck is on it's way. Be prepared","Command")

	demotrucks = demotrucks + 1 --For debugging
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		local targets = Utils.Where(player.GetActors(), function(actor)
			return actor.HasProperty("Health") and actor.Type ~= "brik" and actor.Type ~= "sbag" and actor.Type ~= "fenc"
		end)
		
		if #targets > 0 then
			target = Utils.Random(targets)
		end

		if not unit.IsDead then
			unit.Move(target.Location)
			Trigger.OnIdle(unit, function()
				unit.Kill()
			end)
		end
	end)
end
--[[
CheckAircraft = function()
  local planes = Utils.Where(Map.ActorsInWorld, function(actor)
    return planes.Owner and planes.Type == "yak" and planes.Type == "mig" 
  end)
  
  if #planes <= 1 then
    ProduceAircraft()
  end
  Trigger.AfterDelay(DateTime.Seconds(30), CheckAircraft)
end

AircraftTargeting = function(plane, target)
	if not target or target.IsDead or (not target.IsInWorld) then
    local possTargets = Utils.Where(Map.ActorsInWorld, function(actor) return actor.Owner == player and actor.HasProperty("Health") and plane.CanTarget(actor) end)
    
    if #enemies > 0 then
			target = Utils.Random(enemies)
		end
  end

  if not target or target.IsDead or (not target.IsInWorld) then
		local enemies = Utils.Where(Map.ActorsInWorld, function(self) return self.Owner == player and self.HasProperty("Health") and yak.CanTarget(self) end)

		if #enemies > 0 then
			target = Utils.Random(enemies)
		end
	end


  --local target = Utils.Random(possTargets)
  
   -- if target.IsDead and plane.AmmoCount > 0 then
      --plane.Attack(PlayerMcv)
      plane.Attack(PlayerMcv)
   -- else
   --   if (plane.Type == "yak" or plane.Type == "mig") and plane.AmmoCount <= 0 then
   --     plane.Reload()
   --   end
   -- end
  end
 ]]

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end