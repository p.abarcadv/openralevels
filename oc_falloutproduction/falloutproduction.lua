if Map.LobbyOption("difficulty") == "easy" then
	AttackIntervals = {2.5, 3}
	
	InitDemoDelay = DateTime.Minutes(6)
	DemoDelay = DateTime.Minutes(6)
	
	StartingCash = 10000
	
	ExtraTrucks = 0
	
	SelfDestructTime = DateTime.Minutes(2)
	
	MsloType = "mslo.normal"
	MsloAtk = false
	NukeAtkDelay = DateTime.Minutes(40)
elseif Map.LobbyOption("difficulty") == "normal" then
	AttackIntervals = {2, 2.5}
	
	InitDemoDelay = DateTime.Minutes(6)
	DemoDelay = DateTime.Minutes(6)
	
	StartingCash = 7500
	
	ExtraTrucks = 2
	
	SelfDestructTime = DateTime.Minutes(2)
	
	MsloType = "mslo.normal"
	MsloAtk = false
	NukeAtkDelay = DateTime.Minutes(40)
else
	AttackIntervals = {1, 1.5, 2}
	
	InitDemoDelay = DateTime.Minutes(6)
	DemoDelay = DateTime.Minutes(5)
	
	StartingCash = 5000
	
	ExtraTrucks = 5
	
	SelfDestructTime = DateTime.Minutes(2)
	
	MsloType = "mslo.hard"
	MsloAtk = true
	NukeAtkDelay = DateTime.Minutes(40)
end

NotifyDelay = DateTime.Seconds(2)

NukeFacts = {Facility0, Facility1, Facility2, Facility3}
DemoWeaps = {BadGuyWeap1, BadGuyWeap2, BadGuyWeap3}

Timer0 = SelfDestructTime
Timer1 = SelfDestructTime
Timer2 = SelfDestructTime
Timer3 = SelfDestructTime

Fac0CountdownActive = false
Fac1CountdownActive = false
Fac2CountdownActive = false
Fac3CountdownActive = false

Facility0Message = ""
Facility1Message = ""
Facility2Message = ""
Facility3Message = ""

CountdownMessage = "Nuclear Facility self-destruct countdown: "
StoppedMessage = "Self-destruct sequence aborted. "
DeleteMessage = ""

PatrolAPath = {PatrolAPath1.Location, PatrolAPath2.Location}
PatrolAUnits = {PatrolAUnit1, PatrolAUnit2, PatrolAUnit3, PatrolAUnit4}
PatrolBPath = {PatrolBPath1.Location, PatrolBPath2.Location}
PatrolBUnits = {PatrolBUnit1, PatrolBUnit2, PatrolBUnit3, PatrolBUnit4}
PatrolTime = DateTime.Seconds(5)

StartingVehicles = {PlayerMcv, StartingJeep, Starting1Tnk1, Starting1Tnk2, Starting2Tnk1, Starting2Tnk2}

facilitiesCaptured = 0

GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end

			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)

				if bool then
					stop = true

					i = i + 1
					if i > #waypoints then
						i = 1
					end

					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end

ActivatePatrols = function()
	GroupPatrol(PatrolAUnits,PatrolAPath,PatrolTime)
	GroupPatrol(PatrolBUnits,PatrolBPath,PatrolTime)
end

CreateExtraTrucks = function()
	DemoTruck4 = Actor.Create("dtrk", true, { Owner = badguy, Location = HardTruck1.Location, Facing = Angle.SouthWest } )
	DemoTruck5 = Actor.Create("dtrk", true, { Owner = badguy, Location = HardTruck2.Location, Facing = Angle.West } )
	table.insert(InitialTrucks, DemoTruck4)
	table.insert(InitialTrucks, DemoTruck5)
	
	if ExtraTrucks == 5 then
		DemoTruck6 = Actor.Create("dtrk", true, { Owner = badguy, Location = HardTruck3.Location, Facing = Angle.West } )
		DemoTruck7 = Actor.Create("dtrk", true, { Owner = badguy, Location = HardTruck4.Location, Facing = Angle.East } )
		DemoTruck8 = Actor.Create("dtrk", true, { Owner = badguy, Location = HardTruck5.Location, Facing = Angle.East } )
		table.insert(InitialTrucks, DemoTruck6)
		table.insert(InitialTrucks, DemoTruck7)
		table.insert(InitialTrucks, DemoTruck8)
	end
end

Trigger.OnEnteredProximityTrigger(Facility0Camera.CenterPosition, WDist.New(14 * 1024), function(a,id)
	if a.Owner == player and a.Type ~= "waypoint" then
		Trigger.RemoveProximityTrigger(id)
		
		local InitCam = Actor.Create("camera", true, { Owner = player, Location = Facility0Camera.Location })
		
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Fac0CountdownActive = true
			InitSelfDestruct(Facility0)
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(10), function()
			InitCam.Destroy()
		end)
	end
end)

--TO REMOVE
CreateNuclearSilos = function()
	Mslo1 = Actor.Create(MsloType, true, { Owner = badguy, Location = MsloBadguy1Spawn.Location })
	Mslo2 = Actor.Create(MsloType, true, { Owner = badguy, Location = MsloBadguy2Spawn.Location })
	Mslo3 = Actor.Create(MsloType, true, { Owner = badguy, Location = MsloBadguy3Spawn.Location })

	if MsloAtk == true then
		Media.DisplayMessage("They have warheads ready!? Commander! You must destroy the nuclear silos!","Command")
		Trigger.AfterDelay(NukeAtkDelay, function()	
			UseNuclearPower()
		end)
	end
end
--

UseNuclearPower = function()
	if not Mslo1.IsDead or not Mslo2.IsDead or not Mslo3.IsDead then
		local mslos = Utils.Where(Map.ActorsInWorld, function(m) return m.Type == MsloType and m.Owner == badguy end)
		local mslo = mslos[Utils.RandomInteger(1, #mslos+1)]
		local targetBuilding = Utils.Where(Map.ActorsInWorld, function(b) return (b.HasProperty("StartBuildingRepairs") and b.Owner == player) end)
		if #targetBuilding == 0 then
			local targetAny = Utils.Where(Map.ActorsInWorld, function(u) return u.Owner == player and u.HasProperty("Health") end)
			local target = Utils.Random(targetAny)
			 
			mslo.ActivateNukePower(target.Location)
		else
			local target = Utils.Random(targetBuilding)
			mslo.ActivateNukePower(target.Location)
		end
		Media.PlaySpeechNotification(player, "AbombLaunchDetected")
		Trigger.AfterDelay(DateTime.Seconds(10), UseNuclearPower)
	end	
end

SetupNukeFacts = function()
	Trigger.OnAnyKilled(NukeFacts, function()
		player.MarkFailedObjective(CaptureFacilities)
	end)
	
	Utils.Do(NukeFacts, function(n)
		Trigger.OnCapture(n, function()
			facilitiesCaptured = facilitiesCaptured + 1
		end)
	end)
end

Trigger.OnKilledOrCaptured(BadGuyWeap1, function()
	if Facility1.Owner ~= player then
		Fac1CountdownActive = true
		InitSelfDestruct(Facility1)
	end
end)

Trigger.OnKilledOrCaptured(BadGuyWeap2, function()
	if Facility2.Owner ~= player then
		Fac2CountdownActive = true
		InitSelfDestruct(Facility2)
	end
end)

Trigger.OnKilledOrCaptured(BadGuyWeap3, function()
	if Facility3.Owner ~= player then
		Fac3CountdownActive = true
		InitSelfDestruct(Facility3)
	end
end)

InitSelfDestruct = function(building)
	Media.DisplayMessage("The soviets set the nuclear facility to self-destruct. Capture it to stop the sequence!","Command")

	Trigger.AfterDelay(NotifyDelay, function()
		Media.PlaySpeechNotification(player, "TimerStarted")
	end)
	
	building.Flash(20)
end

RunAIActivities = function()
	SetBuildingsRepair()
	
	BuildBase()
	ProduceArmor()
	ProduceInfantry()
	--ProduceAircraft()
	
	IncreaseEnemyAtkCount()
	Trigger.AfterDelay(InitDemoDelay, SendInitialTrucks)
	
	--PrimaryStructureRandomizer()
	PrimaryStructureSetup()
	
	Trigger.AfterDelay(DateTime.Minutes(5), function()
		SendParatroopers()
	end)
end

SetBuildingsRepair = function()
	Utils.Do(Map.NamedActors, function(actor)
		if (actor.Owner == enemy or actor.Owner == badguy) and actor.HasProperty("StartBuildingRepairs") then
			Trigger.OnDamaged(actor, function(building)
				if building.Owner == enemy and building.Health < 3/4 * building.MaxHealth then
					building.StartBuildingRepairs()
				end
			end)
		end
	end)
end

FlavorMove = function()
	Utils.Do(StartingVehicles, function(unit)
		unit.Move(unit.Location+CVec.New(-3,0))
	end)
	
	BaseHind1.ReturnToBase(MainbaseHpad1)
	BaseHind2.ReturnToBase(MainbaseHpad2)
end

Tick = function()
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(DenyAllies)
	end
	if facilitiesCaptured >= 4 then
		if enemy.HasNoRequiredUnits() and badguy.HasNoRequiredUnits() then
			player.MarkCompletedObjective(DestroySoviets)
			player.MarkCompletedObjective(CaptureFacilities)
		end
	end
	
	if Fac0CountdownActive == true then
		if Facility0.Owner ~= player then
			Timer0 = Timer0 - 1
			Facility0Message = CountdownMessage .. Utils.FormatTime(Timer0)
		else
			Fac0CountdownActive = false
			Timer0 = -1
			Facility0Message = StoppedMessage
			Media.DisplayMessage("Self-destruction sequence aborted.","Command")
			Trigger.AfterDelay(NotifyDelay, function()	
				Facility0Message = ""
				Media.PlaySpeechNotification(player, "TimerStopped")
			end)
		end
	end
	
	if Fac1CountdownActive == true then
		if Facility1.Owner ~= player then
			Timer1 = Timer1 - 1
			Facility1Message = CountdownMessage .. Utils.FormatTime(Timer1)
		else
			Fac1CountdownActive = false
			Timer1 = -1
			Facility1Message = StoppedMessage
			Media.DisplayMessage("Self-destruction sequence aborted.","Command")
			Trigger.AfterDelay(NotifyDelay, function()	
				Facility1Message = ""
				Media.PlaySpeechNotification(player, "TimerStopped")
			end)
		end
	end
	
	if Fac2CountdownActive == true then
		if Facility2.Owner ~= player then
			Timer2 = Timer2 - 1
			Facility2Message = CountdownMessage .. Utils.FormatTime(Timer2)
		else
			Fac2CountdownActive = false
			Timer2 = -1
			Facility2Message = StoppedMessage
			Trigger.AfterDelay(NotifyDelay, function()	
				Facility2Message = ""
				Media.PlaySpeechNotification(player, "TimerStopped")
			end)
			
		end
	end
	
	if Fac3CountdownActive == true then
		if Facility3.Owner ~= player then
			Timer3 = Timer3 - 1
			Facility3Message = CountdownMessage .. Utils.FormatTime(Timer3)
		else
			Fac3CountdownActive = false
			Timer3 = -1
			Facility3Message = StoppedMessage
			Trigger.AfterDelay(NotifyDelay, function()	
				Facility3Message = ""
				Media.PlaySpeechNotification(player, "TimerStopped")
			end)
		end
	end
	
	if Timer0 == 0 then
		Fac1CountdownActive = false
		Facility0.Kill()
		Timer0 = -1
	end
	
	if Timer1 == 0 then
		Fac1CountdownActive = false
		Facility1.Kill()
		Timer1 = -1
	end
	
	if Timer2 == 0 then
		Fac2CountdownActive = false
		Facility2.Kill()
		Timer2 = -1
	end
	
	if Timer3 == 0 then
		Fac3CountdownActive = false
		Facility3.Kill()
		Timer3 = -1
	end

	UserInterface.SetMissionText(
	Facility0Message .. "\n" .. 
	Facility1Message .. "\n" .. 
	Facility2Message .. "\n" .. 
	Facility3Message, MessageColor)
	
	badguy.Cash = 10000
	enemy.Cash = 500000
end

InitTriggers = function()
	Camera.Position = StartingCamera.CenterPosition
	player.Cash = StartingCash
	
	PrimaryStructureRandomizer()
	
	ActivatePatrols()
	
	FlavorMove()

	--ProduceAircraft()

	PowerProxy = Actor.Create("powerproxy.paratroopers", false, { Owner = enemy })
	
	Trigger.AfterDelay(DateTime.Minutes(3), SendParatroopers)
	
	--Trigger.AfterDelay(DateTime.Minutes(5), CreateNuclearSilos)
	
	SetupNukeFacts()

	Trigger.AfterDelay(DateTime.Minutes(4), function()
		RunAIActivities()
	end)
	
	if ExtraTrucks > 0 then
		CreateExtraTrucks()
	end
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
    
    CaptureFacilities = player.AddPrimaryObjective("Capture and protect the nuclear facilities.")
	DestroySoviets = player.AddPrimaryObjective("Eliminate all soviet presence in the area.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the allies!")
    
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "MissionFailed")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "MissionAccomplished")
	end)
end
	
WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	badguy = Player.GetPlayer("BadGuy")
	
	InitTriggers()
	InitObjectives()
	
	MessageColor = HSLColor.White
end