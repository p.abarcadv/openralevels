--Issues of tanks not leaving

if Map.LobbyOption("difficulty") == "easy" then
	NumberToDestroy = 15
	LeavingArmorLimit = 10
	MissionText = "Prevent at least " .. NumberToDestroy .. " tanks to leave the area."
	startingCameras = true
	supplyDelay = DateTime.Seconds(160)
	sniperType = "sniper"
elseif Map.LobbyOption("difficulty") == "normal" then
	NumberToDestroy = 20
	LeavingArmorLimit = 5
	MissionText = "Prevent at least " .. NumberToDestroy .. " tanks to leave the area."
	startingCameras = true
	supplyDelay = DateTime.Seconds(140)
	sniperType = "sniper.noautotarget"
else
	NumberToDestroy = 26 --check this
	LeavingArmorLimit = 0
	MissionText = "Prevent all Allied tanks to leave the area."
	startingCameras = false
	supplyDelay = DateTime.Seconds(100)
	sniperType = "sniper.noautotarget"
end

--WreckList = { , , , , , , }

WarningText = "A tank has left the area!"
WarningMessage = ""

setupReady = false

OilDerricks = {OilDerr1, OilDerr2, OilDerr3}

ArmorLeft = 0
KillCount = 0

TanksAttackedByMigs = { AttackableTank1, AttackableTank2, AttackableTank3 }

TankNorthClump1 = { ObjTankNorth8, ObjTankNorth11, ObjTankNorth15, ObjTankNorth16 }
TankNorthClump2 = { ObjTankNorth4, ObjTankNorth5, ObjTankNorth6, ObjTankNorth7 }
TankNorthClump3 = { ObjTankNorth9, ObjTankNorth10, ObjTankNorth17 }
TankNorthClump4 = { ObjTankNorth12, ObjTankNorth13, ObjTankNorth14 }
TankNorthClump5 = { ObjTankNorth1, ObjTankNorth2, ObjTankNorth3 }

TankSouthClump1 = { ObjTankSouth1, ObjTankSouth2, ObjTankSouth3 }
TankSouthClump2 = { ObjTankSouth4, ObjTankSouth5, ObjTankSouth6 }
TankSouthClump3 = { ObjTankSouth7, ObjTankSouth8, ObjTankSouth9 }

NorthTanks = 
{
	ObjTankNorth1, ObjTankNorth2, ObjTankNorth3, ObjTankNorth4, ObjTankNorth5, ObjTankNorth6,
	ObjTankNorth7, ObjTankNorth8, ObjTankNorth9, ObjTankNorth10, ObjTankNorth11, ObjTankNorth12, 
	ObjTankNorth13, ObjTankNorth14, ObjTankNorth15, ObjTankNorth16, ObjTankNorth17
}

SouthTanks =
{
	ObjTankSouth1, ObjTankSouth2, ObjTankSouth3, ObjTankSouth4, ObjTankSouth5, 
	ObjTankSouth6, ObjTankSouth7, ObjTankSouth8, ObjTankSouth9
}

WestWaterSuppliesPath = {WestLstEntry1.Location, WestLstDst1.Location}
EastWaterSuppliesPath = {EastLstEntry2.Location, EastLstDst2.Location}

WaterUnits = {"truk.mission", "truk.mission", "mech", "e1", "e1"}
mechSquad = {"mech", "e1", "e1" }

BotRoadPath = 
{
	BotRoad1.Location, BotRoad2.Location, BotRoad3.Location, BotRoad4.Location, 
	BotRoad5.Location, BotRoad6.Location, BotRoad7.Location
}

TopRoadPath =
{
	Road0.Location, Road1.Location, Road2.Location, Road3.Location, Road4.Location, 
	Road5.Location, Road6.Location, Road7.Location, Road8.Location, Road9.Location, 
	Road10.Location, Road11.Location, Road12.Location, Road13.Location
}

AuxNorthPath =
{
	{WestLstEntry1.Location, WestLstDst1.Location},
	{WestLstEntry2.Location, WestLstDst2.Location}
}

AuxSouthPath =
{
	BotRoad3.Location
}

NorthResupplyPoints = 
{ 
	ResupplyPointNorth1, ResupplyPointNorth2, 
	ResupplyPointNorth3, ResupplyPointNorth4
}

SouthResupplyPoints = 
{ 
	ResupplyPointSouth1, ResupplyPointSouth2, 
	ResupplyPointSouth3
}

SovReinforcements1A = {"e1","e1","e2"}
SovReinforcements1B = {"e1","e2"}
SovReinforcements2 = {sniperType, sniperType}
SovReinforcements3A = {"shok", "shok", "shok"}
SovReinforcements3B = {"shok", "shok"}

ReinforceNorthPath1 = { ReinforceAEntry1.Location, ReinforceADst1.Location }
ReinforceNorthPath2 = { ReinforceAEntry2.Location, ReinforceADst2.Location }
ReinforceNorthPath3 = { ReinforceAEntry3.Location, ReinforceADst3.Location }

ReinforceSouthPath1 = { ReinforceBEntry1.Location, ReinforceBDst1.Location }
ReinforceSouthPath2 = { ReinforceBEntry2.Location, ReinforceBDst2.Location }

CameraSpawnPoints = 
{ 
	Spy0Loc.Location, Spy1Loc.Location, Spy2Loc.Location,
	Spy3Loc.Location, Spy4Loc.Location, Spy5Loc.Location
}

--NORTH
Trigger.OnAllKilled(TankNorthClump1, function()
	NorthResupplyPoints[1] = nil
end)

Trigger.OnAllKilled(TankNorthClump2, function()
	NorthResupplyPoints[2] = nil
end)

Trigger.OnAllKilled(TankNorthClump3, function()
	NorthResupplyPoints[3] = nil
end)

Trigger.OnAllKilled(TankNorthClump4, function()
	NorthResupplyPoints[4] = nil
end)

Trigger.OnAllKilled(TankNorthClump5, function()
	NorthResupplyPoints[5] = nil
end)

--SOUTH
Trigger.OnAllKilled(TankSouthClump1, function()
	SouthResupplyPoints[1] = nil
end)

Trigger.OnAllKilled(TankSouthClump2, function()
	SouthResupplyPoints[2] = nil
end)

Trigger.OnAllKilled(TankSouthClump3, function()
	SouthResupplyPoints[3] = nil
end)

---------------------------
--------- UNUSED ----------
---------------------------
--[[
MechanicsReinforce = function()
	local units = Reinforce.Reinforcements(enemy, {"mech","mech","e1","e1","e3"}, {MechsEntry.Location, MechsDst.Location}, 1)
end

RecoverWrecks = function(units)
	local wrecks = Map.ActorsInBox(Map.TopLeft, Map.BottomRight, function(w)
		return w.Type == "husk.1tnk" and w.Type == "husk.2tnk"
	end)
	
	Utils.Do(units, function(a)
		if not a.IsDead and a.Type == "mech" and not wrecks.IsDead then
			a.Infiltrate(wreck)
		end
	end)
end
]]

--[[
SendFuelConvoy = function(origin, dst)
	Reinforcements.Reinforce(enemy, {"truk.loaded"}, {BotRoad1.Location, OilDerrApc3.Location}, 1)
	Reinforcements.Reinforce(enemy, {"truk.mission"}, {BotRoad1.Location, OilDerrTruk3.Location}, 1)

	Media.PlaySpeechNotification(player, "ConvoyApproaching")
end
]]

--[[ NOT USED FOR NOW
Trigger.OnCapture(OilDerr3, function()
	local truk = Map.ActorsInCircle(OilDerr3.CenterPosition, WDist.FromCells(3), function(actor)
		return actor.Type == "truk.mission" and actor.Owner == enemy
	end)
	
	if truk ~= nil then
		truk[1].Move(OilDerr2.Location)
	end
end)
]]


--[[
Trigger.OnEnteredFootprint({OilDerrApc1.Location}, function(a,id)
	Trigger.RemoveFootprintTrigger(id)
	if a.Type == "truk.loaded" then
		a.UnloadPassengers()
	end
end)

Trigger.OnEnteredFootprint({OilDerrApc2.Location}, function(a,id)
	Trigger.RemoveFootprintTrigger(id)
	if a.Type == "truk.loaded" then
		a.UnloadPassengers()
	end
end)

Trigger.OnEnteredFootprint({OilDerrApc3.Location}, function(a,id)
	Trigger.RemoveFootprintTrigger(id)
	if a.Type == "truk.loaded" then
		a.UnloadPassengers()
	end
end)

Trigger.OnEnteredProximityTrigger(OilDerrApc3.CenterPosition, WDist.FromCells(3), function(a, id)
	if a.Type == "e6" then
		Trigger.RemoveProximityTrigger(id)
		a.Capture(OilDerr3)
	end
end)
]]

SendPlayerUnits = function()
	SendTopPlayerUnits()

	Trigger.AfterDelay(DateTime.Seconds(5), function()
		SendBottomPlayerUnits()
	end)
end

SendTopPlayerUnits = function()
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")

	Reinforcements.Reinforce(player, SovReinforcements1A, {ReinforceNorthPath1[1], ReinforceNorthPath1[2]+CVec.New(0,0)}, 1)
	Reinforcements.Reinforce(player, SovReinforcements3A, {ReinforceNorthPath2[1], ReinforceNorthPath2[2]+CVec.New(-3,0)}, 1)
	Reinforcements.Reinforce(player, SovReinforcements1A, {ReinforceNorthPath3[1], ReinforceNorthPath3[2]+CVec.New(0,0)}, 1)
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Reinforcements.Reinforce(player, SovReinforcements2, {ReinforceNorthPath1[1], ReinforceNorthPath1[2]+CVec.New(-2,0)}, 1)
		Reinforcements.Reinforce(player, SovReinforcements2, {ReinforceNorthPath3[1], ReinforceNorthPath3[2]+CVec.New(-2,0)}, 1)
	end)

	Radar.Ping(player, ReinforceADst2.CenterPosition, HSLColor.Red, 10)
end

SendBottomPlayerUnits = function()
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		
	Reinforcements.Reinforce(player, SovReinforcements1B, {ReinforceSouthPath1[1], ReinforceSouthPath1[2]+CVec.New(1,0)}, 1)
	Reinforcements.Reinforce(player, SovReinforcements1B, {ReinforceSouthPath2[1], ReinforceSouthPath2[2]+CVec.New(1,0)}, 1)
		
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Reinforcements.Reinforce(player, SovReinforcements2, {ReinforceSouthPath1[1], ReinforceSouthPath1[2]+CVec.New(-1,0)}, 1)
		Reinforcements.Reinforce(player, SovReinforcements3B, {ReinforceSouthPath2[1], ReinforceSouthPath2[2]+CVec.New(-1,0)}, 1)
	end)
		
	Radar.Ping(player, ReinforceBDst2.CenterPosition, HSLColor.Red, 1)
end

--Tanks leave the area
Trigger.OnEnteredFootprint({Road13.Location}, function(a, id)
	if a.Owner == enemy then
		a.Destroy()
		if a.Type == "1tnk" or a.Type == "2tnk" then
			ArmorLeft = ArmorLeft + 1
		end
	end
end)

Trigger.OnEnteredFootprint({BotRoad1.Location}, function(a, id)
	if a.Owner == enemy then
		a.Destroy()
		if a.Type == "1tnk" or a.Type == "2tnk" then
			ArmorLeft = ArmorLeft + 1
		end
	end
end)

IntroSequence = function()
	Media.Debug("Intro Sequence - Started")
	
	local initCam = Actor.Create( "camera", true,{ Owner = player, Location = StartingCameraLoc.Location })
	local proxy = Actor.Create("powerproxy.parabombs", false, { Owner = badguy })
	proxy.SendAirstrikeFrom(ParabombSpawn.Location, StartingCameraLoc.Location)
	proxy.Destroy()
	
	Plane1.Attack(AttackableTank1)
	Plane2.Attack(AttackableTank2)
	Plane3.Attack(AttackableTank3)
	Plane1.Move(PlaneExit.Location)
	Plane2.Move(PlaneExit.Location)
	Plane3.Move(PlaneExit.Location)
	AttackableTank1.Move(Road0.Location)
	AttackableTank2.Move(Road0.Location)
	AttackableTank3.Move(Road0.Location)
	
	Utils.Do(TanksAttackedByMigs, function(u)
		Trigger.OnDamaged(u, function()
			Trigger.ClearAll(u)
			u.Kill()
		end)
	end)
	
	--DAMAGE BRIDGE
	local bridges = Utils.Where(Map.ActorsInWorld, function(actor) return actor.Type == "br1" or actor.Type == "br2" or actor.Type == "br3" end)
	
	Utils.Do(bridges, function(b)
		b.Health = b.Health/2
		Trigger.AfterDelay(DateTime.Seconds(9.0), function()
			b.Health = b.Health/10
		end)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(12), function()
		initCam.Destroy()
		SetupGame()
		--WaterAlliedSupplies(WestWaterSuppliesPath, WaterUnits)
		--WaterAlliedSupplies(EastWaterSuppliesPath, WaterUnits)
		--SendFuelConvoy()
	end)
	
	Trigger.OnEnteredProximityTrigger(PlaneExit.CenterPosition, WDist.FromCells(2), function(a, id)
		if a.Type == "mig" then
			a.Destroy()
		end
	end)
end

SendAuxConvoyNorth = function()
	local target = nil
	if #NorthResupplyPoints > 0 then
		while target == nil do
			target = Utils.Random(NorthResupplyPoints)
		end
	else
		return
	end
	
	Media.PlaySpeechNotification(player, "AlliedReinforcementsNorth")
	local waterPath = Utils.Random(AuxNorthPath)
	local supplyUnits = Reinforcements.ReinforceWithTransport(enemy, "lst", mechSquad, waterPath, { waterPath[1] })[2]
	
	Utils.Do(supplyUnits, function(u)
		Trigger.OnAddedToWorld(u, function()
			if u.Type == "mech" then
				u.Move(target.Location)
			else	
				u.AttackMove(target.Location)
			end
		end)
	end)
	
	Trigger.AfterDelay(supplyDelay, function()
		SendAuxConvoyNorth()
	end)
end

SendAuxConvoySouth = function()
	local target = nil
	if #SouthResupplyPoints > 0 then
		while target == nil do
			target = Utils.Random(SouthResupplyPoints)
		end
	else
		return
	end

	Media.PlaySpeechNotification(player, "AlliedReinforcementsSouth")
	Reinforcements.ReinforceWithTransport(enemy, "apc", mechSquad, {BotRoad1.Location, target.Location}, {BotRoad1.Location})
	
	Trigger.AfterDelay(supplyDelay, function()
		SendAuxConvoySouth()
	end)
end


WaterAlliedSupplies = function(path, units)
	local supplyUnits = Reinforcements.ReinforceWithTransport(enemy, "lst", units, path, { path[1] })[2]
		
	Utils.Do(supplyUnits, function(a)
		Trigger.OnAddedToWorld(a, function()
			a.Move(Road12.Location)
			a.Move(Road13.Location)
		end)
	end)
end

--Game Starts
SetupGame = function()
	Actor.Create("powerproxy.spyplane", true, { Owner = player })
	
	SendPlayerUnits()
	
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		InitiateAuxConvoys()
	end)
	
	Trigger.AfterDelay(1, function()
		setupReady = true
	end)
end

InitiateAuxConvoys = function()
	local rng = Utils.RandomInteger(1,3)

	if rng == 1 then
		SendAuxConvoyNorth()
		Trigger.AfterDelay(DateTime.Seconds(210), function()
			SendAuxConvoySouth()
		end)
	else
		SendAuxConvoySouth()
		Trigger.AfterDelay(DateTime.Seconds(210), function()
			SendAuxConvoyNorth()
		end)
	end
end

Trigger.OnAllKilled(OilDerricks, function()
	player.MarkCompletedObjective(DestroyOils)
end)

Tick = function()
	if setupReady then
		UserInterface.SetMissionText("Tanks that have left the area: " .. tostring(ArmorLeft) .. "\n" .. WarningMessage, TextColor)
	end
	
	if KillCount == NumberToDestroy then
		KillCount = -1
		player.MarkCompletedObjective(DestroyTanks)
	end
	
	if LeavingArmorLimit < ArmorLeft then
		enemy.MarkCompletedObjective(DenySoviets)
	end
	
	if player.HasNoRequiredUnits() and setupReady then
        enemy.MarkCompletedObjective(DenySoviets)
    end
		
	Utils.Do(NorthTanks, function(u)
		if not u.IsDead then
			if u.Type == "1tnk" and u.Health == u.MaxHealth then
				u.Health = u.MaxHealth-1
				u.Move(Road13.Location)
			elseif u.Type == "2tnk" and u.Health == u.MaxHealth then
				u.Health = u.MaxHealth-1
				u.Move(Road13.Location)
			end
		end
	end)
	
	Utils.Do(SouthTanks, function(u)
		if not u.IsDead then
			if u.Type == "1tnk" and u.Health == u.MaxHealth then
				u.Health = u.MaxHealth-1
				u.Move(BotRoad1.Location)
			elseif u.Type == "2tnk" and u.Health == u.MaxHealth then
				u.Health = u.MaxHealth-1
				u.Move(BotRoad1.Location)
			end
		end
	end)
end

InitTriggers = function()
	Camera.Position = StartingCameraLoc.CenterPosition
	IntroSequence()
	
	Utils.Do(NorthTanks, function(t)
		Trigger.OnKilled(t, function()
			KillCount = KillCount + 1
		end)
	end)
	
	Utils.Do(SouthTanks, function(t)
		Trigger.OnKilled(t, function()
			KillCount = KillCount + 1
		end)
	end)
end

InitObjectives = function()
	DestroyTanks = player.AddPrimaryObjective(MissionText)
	
	DenySoviets = enemy.AddPrimaryObjective("Deny the Allies.")
	DestroyOils = player.AddSecondaryObjective("Destroy oil derricks to prevent refueling.")
	
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionFailed")
		end)
	end)
	Trigger.OnPlayerWon(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionAccomplished")
		end)
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	badguy  = Player.GetPlayer("BadGuy")
	
	if startingCameras == true then
		Utils.Do(CameraSpawnPoints, function(cam)
			local camera = Actor.Create( "camera.medium", true,{ Owner = player, Location = cam })
			if Map.LobbyOption("difficulty") ~= "easy" then
				Trigger.AfterDelay(DateTime.Seconds(1), function()
					camera.Destroy()
				end)
			end
		end)
	end
	
	TextColor = HSLColor.White
	
	InitObjectives()
	InitTriggers()
end