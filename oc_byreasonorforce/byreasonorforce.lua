if Map.LobbyOption("difficulty") == "easy" then
	maxReinforce = 9
elseif Map.LobbyOption("difficulty") == "normal" then
	maxReinforce = 7
else
	maxReinforce = 5
end

ReinforcingCount = 0

FreeMilitiaLeader = false
SovMilitiaCooperation = 0

CooperatingWithVills = false

EBase = 
{
    EBasePower1, EBasePower2, EBasePower3, EBasePower4,
    EBasePower5, EBasePower6, EBasePower7, EBasePower8,
	EBasePower9, EBasePower10, EBasePower11, EBaseFact,
	EBaseWeap, EBaseRef1, EBaseDome, EBaseFix, EBaseTent1,
	EBaseTent2, EBaseHpad1, EBaseHpad2
}
Detaining = {OutpostTent1 ,sbag1, sbag2, sbag3, sbag4, sbag5, sbag8, sbag9 }


--Patrol1 = { , }
Patrol2 = { Patrol2Inf1, Patrol2Inf2, Patrol2Inf3, Patrol2Inf4 }
Patrol3 = { Patrol3Inf1, Patrol3Inf2, Patrol3Inf3, Patrol3Inf4 }
--Patrol1Path = { .Location, .Location}
Patrol2Path = { Patrol2Way1.Location, Patrol2Way2.Location }
Patrol3Path = { Patrol3Way1.Location, Patrol3Way2.Location, Patrol3Way3.Location }

PatrolDelay = DateTime.Seconds(5)

MinesPatrol = {MinesInf1,MinesInf2,MinesInf3,MinesInf4,MinesInf5}
MinesPath = 
{
MinesPatrolWay1.Location,MinesPatrolWay2.Location,MinesPatrolWay3.Location,
MinesPatrolWay4.Location,MinesPatrolWay3.Location,MinesPatrolWay2.Location
}

MinesPatrolDelay = DateTime.Seconds(2.5)

Mines =
    {
    Mine4, Mine5, Mine6,
	Mine7, Mine8, Mine9, Mine10, Mine11, Mine12,
	Mine13, Mine15, Mine16
    }

NorthTankTraps =	--Only remove [2]
	{
		NorthTTrap1,NorthTTrap2,NorthTTrap3,NorthTTrap4,NorthTTrap5
	}

SouthTankTraps =	--Only remove [3]
	{
		SouthTTrap1,SouthTTrap2,SouthTTrap3,SouthTTrap4
	}

DetentionCoords = 
    {
    CPos.New(19,41), CPos.New(19,40), CPos.New(19,39), CPos.New(21,39), CPos.New(20,39), CPos.New(22,39), CPos.New(22,38),
    CPos.New(23,38), CPos.New(24,38), CPos.New(25,38) 
    }

DetaineeUnitsToSpawn =
{
	"e1","e1","e1","e2","e1"
}

spawnedSoldier = false

--[[]]
--[[
DetentionGuards = {Guard1, Guard2, Guard3, Guard4, Guard5, GuardTank}
]]

DetentionTent = { OutpostTent1, sbag1, sbag2, sbag3, sbag4, sbag5, sbag6, sbag7, sbag8, sbag9 }

VillageDiscoverCoords = 
    {
    CPos.New(19,41), CPos.New(19,40), CPos.New(19,39), CPos.New(21,39), CPos.New(20,39), CPos.New(22,39), CPos.New(22,38),
    CPos.New(23,38), CPos.New(24,38), CPos.New(25,38) 
    }

InitialReinforcements = function()
	Reinforcements.Reinforce(player, {"e2", "e2", "e2"}, { StartReinforce1Entry.Location, StartReinforce1Dst.Location }, 10)
	Reinforcements.Reinforce(player, {"e2", "e2", "e2"}, { StartReinforce2Entry.Location, StartReinforce2Dst.Location }, 10)
end

SovPath1 = { Reinforce1Entry.Location, Reinforce1Dst.Location }
SovPath2 = { Reinforce2Entry.Location, ReinforceCenterDst.Location }
SovPath3 = { Reinforce3Entry.Location, ReinforceCenterDst.Location }
SovGroup1 = {"3tnk", "v2rl", "e1", "e1", "e2"}
SovGroup2 = {"3tnk", "ftrk", "e1", "e1", "e2"}

AlliedBackupTanks =
{
	backupTank1, backupTank2, backupTank3,
	backupTank4, backupTank5, backupTank6
}

BackupTanksBarrels = 
{
	tankBarrel, tankBarrel2, tankBarrel3, tankBarrel4,
	tankBarrel5, tankBarrel6, tankBarrel7, tankBarrel8
}

ActivatePatrols = function()
    --GroupPatrol(Patrol1, Patrol1Path, PatrolDelay)
	
    GroupPatrol(Patrol2, Patrol2Path, PatrolDelay)
    GroupPatrol(Patrol3, Patrol3Path, PatrolDelay)
	
	GroupPatrol(MinesPatrol, MinesPath, MinesPatrolDelay)

end

RemoveBarrier = function(ttrap)
	if not ttrap.IsDead then
		ttrap.Kill()
	end
end

GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false
	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end
			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)

				if bool then
					stop = true

					i = i + 1
					if i > #waypoints then
						i = 1
					end

					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end

DetaineeToSpawn = function()
	Utils.Do(DetaineeUnitsToSpawn, function(u)
		Actor.Create(u, true, { Owner = player, Location = SpawnExtraSoldiers.Location })
	end)
	
	
end

ConstantReinforcements = function()
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	ReinforcingCount = ReinforcingCount + 1

	if ReinforcingCount % 2 == 0 then
		group = SovGroup1
	else
		group = SovGroup2
	end
	local path = Utils.Random({SovPath1, SovPath2, SovPath3}, 30)	
	
	Reinforcements.Reinforce(player, group, path, 10)
	
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		if ReinforcingCount >= maxReinforce then
			ReinforcingCount = -1
			Media.DisplayMessage("Our forces are ready Commander.","Soldier")
		else
			ConstantReinforcements()
		end
	end)
end

Trigger.OnAnyKilled(Detaining, function()
	FreeMilitiaLeader = true
	SovMilitiaCooperation = SovMilitiaCooperation + 1
	
	Media.DisplayMessage("Thanks for freeing me. I will not forget this.","Militia Leader")
	MilitiaLeader.Move(LeaderWaypoint1.Location)
	Trigger.OnEnteredFootprint({LeaderWaypoint1.Location}, function(a, id)
		if a.Owner == militia then
			Trigger.RemoveFootprintTrigger(id)
			local militia = Reinforcements.Reinforce(militia, {"e1","e1","e1","e1"}, {MilitiaReinforce1Entry.Location, MilitiaReinforce1Dst.Location},1)
			Trigger.AfterDelay(DateTime.Seconds(5), function()
				Utils.Do(militia, function(u)
					u.AttackMove(ReinforceTank2.Location)
				end)
				MilitiaLeader.AttackMove(ReinforceTank2.Location)
			end)
		end
	end)
	
end)

Trigger.OnKilled(MilitiaLeader, function()
	SovMilitiaCooperation = SovMilitiaCooperation - 1
end)

Trigger.OnEnteredFootprint(VillageDiscoverCoords, function(a, id)
	local switch = true
	
	if a.Owner == player and switch == true then
		switch = false
		Trigger.RemoveFootprintTrigger(id)

		DetaineeToSpawn()
		
		local detCamera = Actor.Create("camera", true, { Owner = player, Location = DetentionCamera.Location })
		local villCamera = Actor.Create("camera", true, { Owner = player, Location = VillageCamera.Location })
		--Actor.Create("flare", true, { Owner = player, Location = startingcamera.Location })
	--ttrap2.Destroy()
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Media.DisplayMessage("If you want our help then bring to us supplies.","Militia")
		end)

	end
end)

KillSovietDetainee = function()
	if not detaineeBarrel.IsDead then
		detaineeBarrel.Kill()
	end
end


Trigger.OnKilled(detaineeBarrel, function()
	if spawnedSoldier == true then
		Utils.Do(a, function(s)
			
		end)
	end
end)

ChangeOwnership = function(newOwner, units)
	Utils.Do(units, function(u)
		if not u.IsDead then
			u.Owner = newOwner
		end
	end)
end

Trigger.OnAllKilled(EBase, function()
    player.MarkCompletedObjective(DestroyBase)
end)

Trigger.OnAnyKilled(BackupTanksBarrels, function()
	Utils.Do(BackupTanksBarrels, function(b)
		if not b.IsDead then
			b.Kill()
		end
	end)
	Utils.Do(AlliedBackupTanks, function(t)
		if not t.IsDead then
			t.Kill()
		end
	end)
end)

Trigger.OnAllKilled(AlliedBackupTanks, function()
	
end)

Tick = function()
    --[[if player.HasNoRequiredUnits() then
        enemy.MarkCompletedObjective(DenySoviets)
    end]]

end

InitTriggers = function()
    Camera.Position = StartingCameraLoc.CenterPosition
	
	InitialReinforcements()
	
	ActivatePatrols() 
	
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		ConstantReinforcements()
	end)
	
end

InitObjectives = function()
    
    Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
    
	ReachVillage = player.AddPrimaryObjective("Reach militia village.")
    DestroyBase = player.AddPrimaryObjective("Destroy the enemy base.")

	DestroyBackUp = player.AddSecondaryObjective("Destroy reserve allied tanks.")

    --ASDF = player.AddSecondaryObjective("Destroy the enemy base.")
	--DASD = player.AddSecondaryObjective("Destroy the enemy base.")

	DenySoviets = enemy.AddPrimaryObjective("Deny the soviets.")

	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)

end

SetupMood = function()
	Media.PlayMusic("search")
end

WorldLoaded = function()
    player = Player.GetPlayer("USSR")
    enemy = Player.GetPlayer("Greece")
    militia = Player.GetPlayer("Militia")
	militiaAGGRESIVE = Player.GetPlayer("MilitiaAggresive")
	
	SetupMood()
    InitTriggers()
    InitObjectives()
end