
Power1	= 	{type = "powr"	, pos = CVec.New(-6,-1)		, cost = 300	, 	exists = true}
Power2	= 	{type = "apwr"	, pos = CVec.New(-9,0)		, cost = 500	, 	exists = true}

Power3	= 	{type = "apwr"	, pos = CVec.New(-22,-3)	, cost = 500	, 	exists = true}
Power4	= 	{type = "apwr"	, pos = CVec.New(-19,-3)	, cost = 500	, 	exists = true}
Power5	= 	{type = "apwr"	, pos = CVec.New(-22,1)		, cost = 500	, 	exists = true}
Power6	= 	{type = "apwr"	, pos = CVec.New(-19,1)		, cost = 500	, 	exists = true}

Power7	= 	{type = "powr"	, pos = CVec.New(-17,-17)	, cost = 300	, 	exists = true}
Power8	= 	{type = "powr"	, pos = CVec.New(-17,-14)	, cost = 300	, 	exists = true}

Power9	= 	{type = "apwr"	, pos = CVec.New(-32,-23)	, cost = 500	, 	exists = true}
Power10	= 	{type = "apwr"	, pos = CVec.New(-29,-23)	, cost = 500	, 	exists = true}
Power11	= 	{type = "apwr"	, pos = CVec.New(-32,-17)	, cost = 500	, 	exists = true}
Power12	= 	{type = "apwr"	, pos = CVec.New(-29,-17)	, cost = 500	, 	exists = true}

Power13	= 	{type = "apwr"	, pos = CVec.New(5,0)		, cost = 500	, 	exists = true}

--Power14	= 	{type = "apwr"	, pos = CVec.New(-29,-17)	, cost = 500	, 	exists = true}
--Power15	= 	{type = "apwr"	, pos = CVec.New(-29,-17)	, cost = 500	, 	exists = true}

Tent	= 	{type = "tent"	, pos = CVec.New(-23,7)		, cost = 500	, 	exists = true}
Weap	= 	{type = "weap"	, pos = CVec.New(-20,7)		, cost = 2000	, 	exists = true}
Ref		= 	{type = "proc"	, pos = CVec.New(-8,7)		, cost = 1400	, 	exists = true}

Naval	= 	{type = "syrd"	, pos = CVec.New(-8,-26)	, cost = 1000	, 	exists = true}

Dome	=	{type = "dome"	, pos = CVec.New(-20,-9)	, cost = 1400	, 	exists = true}
Fix		=	{type = "fix"	, pos = CVec.New(-14,-4)	, cost = 1000	, 	exists = true}

HPad1	= 	{type = "hpad"	, pos = CVec.New(-1,7)		, cost = 500	, 	exists = true}
HPad2	= 	{type = "hpad"	, pos = CVec.New(-4,7)		, cost = 500	, 	exists = true}
HPad3	= 	{type = "hpad"	, pos = CVec.New(-1,11)		, cost = 500	, 	exists = true}
HPad4	= 	{type = "hpad"	, pos = CVec.New(-4,11)		, cost = 500	, 	exists = true}

Silo1	= 	{type = "silo"	, pos = CVec.New(-9,5)		, cost = 150	, 	exists = true}
Silo2	= 	{type = "silo"	, pos = CVec.New(-8,5)		, cost = 150	, 	exists = true}
Silo3	= 	{type = "silo"	, pos = CVec.New(-6,5)		, cost = 150	, 	exists = true}
Silo4	= 	{type = "silo"	, pos = CVec.New(-5,5)		, cost = 150	, 	exists = true}

Gun1	= 	{type = "gun"	, pos = CVec.New(-16,16)	, cost = 500	, 	exists = true}
Gun2	=	{type = "gun"	, pos = CVec.New(-10,16)	, cost = 500	, 	exists = true}
Gun3	=	{type = "gun"	, pos = CVec.New(-3,15)		, cost = 500	, 	exists = true}

Gun4	=	{type = "gun"	, pos = CVec.New(-29,5)		, cost = 500	, 	exists = true}
Gun5	=	{type = "gun"	, pos = CVec.New(-26,8)		, cost = 500	, 	exists = true}
Gun6	=	{type = "gun"	, pos = CVec.New(-26,13)	, cost = 500	, 	exists = true}

PBox1	= 	{type = "pbox"	, pos = CVec.New(2,1)		, cost = 500	, 	exists = true}
PBox2	= 	{type = "pbox"	, pos = CVec.New(-20,0)		, cost = 500	, 	exists = true}

HBox1	=	{type = "hbox"	, pos = CVec.New(-17,18)	, cost = 500	, 	exists = true}
HBox2	=	{type = "hbox"	, pos = CVec.New(-2,17)		, cost = 500	, 	exists = true}

Agun1	= 	{type = "agun"	, pos = CVec.New(-3,-1)		, cost = 1000	, 	exists = true}
Agun2	= 	{type = "agun"	, pos = CVec.New(3,-1)		, cost = 1000	, 	exists = true}
Agun3	= 	{type = "agun"	, pos = CVec.New(-21,-8)	, cost = 1000	, 	exists = true}
Agun4	= 	{type = "agun"	, pos = CVec.New(-23,3)		, cost = 1000	, 	exists = true}
Agun5	= 	{type = "agun"	, pos = CVec.New(-16,3)		, cost = 1000	, 	exists = true}

Agun6	= 	{type = "agun"	, pos = CVec.New(-23,13)	, cost = 1000	, 	exists = true}
Agun7	= 	{type = "agun"	, pos = CVec.New(-18,13)	, cost = 1000	, 	exists = true}
Agun8	= 	{type = "agun"	, pos = CVec.New(-7,13)		, cost = 1000	, 	exists = true}

--Incomplete
Agun9	= 	{type = "agun"	, pos = CVec.New(-7,13)		, cost = 1000	, 	exists = true}
Agun10	= 	{type = "agun"	, pos = CVec.New(-7,13)		, cost = 1000	, 	exists = true}


Gap1	= 	{type = "gap"	, pos = CVec.New(-2,1)		, cost = 1000	, 	exists = true}
Gap2	= 	{type = "gap"	, pos = CVec.New(-19,0)	, cost = 1000	, 	exists = true}
Gap3	= 	{type = "gap"	, pos = CVec.New(-12,10)	, cost = 1000	, 	exists = true}

BaseBuildings = 
{
Power1,
Power2,
Power3,
Power4,
Power5,
Power6,
Power7,
Power8,
Power9,
Power10,
Power11,
Power12,
Power13,
--Power14,
--Power15,

Tent,
Weap,
Ref,

Naval,
Dome,
Fix,

Silo1,
Silo2,
Silo3,
Silo4,

Gun1,
Gun2,
Gun3,
Gun4,
Gun5,
Gun6,

PBox1,
PBox2,

HBox1,
HBox2,

Agun1,
Agun2,
Agun3,
Agun4,
Agun5,
Agun6,
Agun7,
Agun8,

Gap1,
Gap2,
Gap3
}

Trigger.OnKilled(BasePower1, function()
	Power1.exists = false
end)

Trigger.OnKilled(BasePower2, function()
	Power2.exists = false
end)

Trigger.OnKilled(BasePower3, function()
	Power3.exists = false
end)

Trigger.OnKilled(BasePower4, function()
	Power4.exists = false
end)

Trigger.OnKilled(BasePower5, function()
	Power5.exists = false
end)

Trigger.OnKilled(BasePower6, function()
	Power6.exists = false
end)

Trigger.OnKilled(BasePower7, function()
	Power7.exists = false
end)

Trigger.OnKilled(BasePower8, function()
	Power8.exists = false
end)

Trigger.OnKilled(BasePower9, function()
	Power9.exists = false
end)

Trigger.OnKilled(BasePower10, function()
	Power10.exists = false
end)

Trigger.OnKilled(BasePower11, function()
	Power11.exists = false
end)

Trigger.OnKilled(BasePower12, function()
	Power12.exists = false
end)

Trigger.OnKilled(BasePower13, function()
	Power13.exists = false
end)

Trigger.OnKilled(BasePower14, function()
	Power14.exists = false
end)

Trigger.OnKilled(BasePower15, function()
	Power15.exists = false
end)

Trigger.OnKilled(BaseTent, function()
	Tent.exists = false
end)

Trigger.OnKilled(BaseWeap, function()
	Weap.exists = false
end)

Trigger.OnKilled(BaseRef, function()
	Ref.exists = false
end)

Trigger.OnKilled(BaseDome, function()
	Dome.exists = false
end)

Trigger.OnKilled(BaseNaval, function()
	Naval.exists = false
end)

Trigger.OnKilled(BaseFix, function()
	Fix.exists = false
end)

Trigger.OnKilled(BaseHPad1, function()
	HPad1.exists = false
end)

Trigger.OnKilled(BaseHPad2, function()
	HPad2.exists = false
end)

Trigger.OnKilled(BaseHPad3, function()
	HPad3.exists = false
end)

Trigger.OnKilled(BaseHPad4, function()
	HPad4.exists = false
end)

Trigger.OnKilled(BaseSilo1, function()
	Silo1.exists = false
end)

Trigger.OnKilled(BaseSilo2, function()
	Silo2.exists = false
end)

Trigger.OnKilled(BaseSilo3, function()
	Silo3.exists = false
end)

Trigger.OnKilled(BaseSilo4, function()
	Silo4.exists = false
end)

Trigger.OnKilled(BaseGun1, function()
	Gun1.exists = false
end)

Trigger.OnKilled(BaseGun2, function()
	Gun2.exists = false
end)

Trigger.OnKilled(BaseGun3, function()
	Gun3.exists = false
end)

Trigger.OnKilled(BaseGun4, function()
	Gun4.exists = false
end)

Trigger.OnKilled(BaseGun5, function()
	Gun5.exists = false
end)

Trigger.OnKilled(BaseGun6, function()
	Gun6.exists = false
end)

Trigger.OnKilled(BasePBox1, function()
	PBox1.exists = false
end)

Trigger.OnKilled(BasePBox2, function()
	PBox2.exists = false
end)

Trigger.OnKilled(BaseHBox1, function()
	HBox1.exists = false
end)

Trigger.OnKilled(BaseHBox2, function()
	HBox2.exists = false
end)

Trigger.OnKilled(BaseAgun1, function()
	Agun1.exists = false
end)

Trigger.OnKilled(BaseAgun2, function()
	Agun2.exists = false
end)

Trigger.OnKilled(BaseAgun3, function()
	Agun3.exists = false
end)

Trigger.OnKilled(BaseAgun4, function()
	Agun4.exists = false
end)

Trigger.OnKilled(BaseAgun5, function()
	Agun5.exists = false
end)

Trigger.OnKilled(BaseAgun6, function()
	Agun6.exists = false
end)

Trigger.OnKilled(BaseAgun7, function()
	Agun7.exists = false
end)

Trigger.OnKilled(BaseAgun8, function()
	Agun8.exists = false
end)
--[[
Trigger.OnKilled(BaseAgun9, function()
	Agun9.exists = false
end)

Trigger.OnKilled(BaseAgun10, function()
	Agun10.exists = false
end)
]]
Trigger.OnKilled(BaseGap1, function()
	Gap1.exists = false
end)

Trigger.OnKilled(BaseGap2, function()
	Gap2.exists = false
end)

Trigger.OnKilled(BaseGap3, function()
	Gap3.exists = false
end)