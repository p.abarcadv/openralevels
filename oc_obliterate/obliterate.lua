if Map.LobbyOption("difficulty") == "easy" then
	PlayerCash = 4000
elseif Map.LobbyOption("difficulty") == "normal" then
	PlayerCash = 2000
else
	PlayerCash = 2000
end

Tick = function()
	if enemy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(DenyAllies)
	end

	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(DenySoviets)
	end
end

RunAI = function()
	ProduceInfantry()
	ProduceArmor()
end

InitTriggers = function()
	Camera.Position = StartingCameraLoc.CenterPosition
	
	player.Cash = PlayerCash
	enemy.Cash = 1000000
	
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		CruiserChronoAtk()
	end)
	
	PlayerMCV.Move(PlayerMCVDst.Location)
	PlayerMCV.Deploy()
	
	BaseTent.IsPrimaryBuilding = true
	
	StartPlayerBaseLocationCheck()
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		RunAI()
	end)

	BuildBase()
end

InitObjectives = function()

	DenyAllies = player.AddPrimaryObjective("Deny the Allies.")	
	DenySoviets = enemy.AddPrimaryObjective("Deny the Soviets.")
	
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionFailed")
		end)
	end)
	Trigger.OnPlayerWon(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionAccomplished")
		end)
	end)
end


WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	
	InitObjectives()
	InitTriggers()
end