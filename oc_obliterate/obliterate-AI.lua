if Map.LobbyOption("difficulty") == "easy" then
	InfAtkCount = 6
	ArmorAtkCount = 6
elseif Map.LobbyOption("difficulty") == "normal" then
	InfAtkCount = 9
	ArmorAtkCount = 7
else
	InfAtkCount = 13
	ArmorAtkCount = 8
end

ProbablePlayerBase =
{
	{FirstBasePos1, FirstBasePos2},
	{SecondBasePos1, SecondBasePos2},
	{ThirdBasePos1, ThirdBasePos2},
	{FourthBasePos1, FourthBasePos2}
}

SavedAttackPaths =
{
	{AtkBotPathPivot, AtkBotPath1},
	{AtkBotPathPivot, AtkBotPath1, AtkBotPath2},
	{AtkLeftPathPivot, AtkLeftPath1},
	{AtkTopPathPivot}
}

AttackPaths = 
{ 
{AtkBotPathPivot, AtkBotPath1}
}

AlliedInfTypes = {"e1", "e1", "e3"}
AlliedVehicleTypes = {"2tnk", "1tnk", "2tnk", "1tnk", "jeep", "arty"}

ArmorAttack = { }
InfAttack = { }

InfAtkCount = 8
ArmorAtkCount = 8

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end


IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end

StartPlayerBaseLocationCheck = function()
	--Media.Debug("Start check")
	for i=1, 4, 1 do
		local fact = Map.ActorsInBox(ProbablePlayerBase[i][1].CenterPosition, ProbablePlayerBase[i][2].CenterPosition,
		function(self) return self.Owner == player and self.Type == "mcv" end)
		if #fact > 0 then
			ChangeAtkPaths(i)
			--Media.Debug("Found fact or mcv at point " .. tostring(i)) 
			return
		end
	end
	
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		StartPlayerBaseLocationCheck()
	end)
end

ChangeAtkPaths = function(index)
	AttackPaths = {SavedAttackPaths[index]}
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		StartPlayerBaseLocationCheck()
	end)
end

CruiserChronoAtk = function()
	--Change this to Chronoshift
	Media.PlaySound(Media.PlaySound("chrono2.aud"))
	ChronoCruiser.Teleport(CruiserTeleportPos1.Location)
	
	Trigger.AfterDelay(DateTime.Seconds(60), function()
		if not ChronoCruiser.IsDead then
			ChronoCruiser.Teleport(ChronoCruiserInitPos.Location)
		end
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if BaseFact.IsDead or BaseFact.Owner ~= enemy then 
			return
		end
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EFactLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost
		building.exists = true
		Media.Debug("Building ready - ".. building.type)
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function()
	if not Tent.exists or BaseTent.Owner ~= enemy then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		return
	end
	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(AlliedInfTypes) }
	local Path = Utils.Random(AttackPaths)
	
	enemy.Build(toBuild, function(unit)
		InfAttack[#InfAttack + 1] = unit[1]
		if #InfAttack >= InfAtkCount then
			SendUnits(InfAttack, Path)
			InfAttack = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceArmor = function()
	if not Weap.exists or BaseWeap.Owner ~= enemy then
		return
	--elseif baseharv.IsDead and enemy.Resources <= 599 then
	--	return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(AlliedVehicleTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		ArmorAttack[#ArmorAttack + 1] = unit[1]

		if #ArmorAttack >= ArmorAtkCount then
			SendUnits(ArmorAttack, Path)
			ArmorAttack = { }
			Trigger.AfterDelay(DateTime.Minutes(2), ProduceArmor)
		else
			Trigger.AfterDelay(delay, ProduceArmor)
		end
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end