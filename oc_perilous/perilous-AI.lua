if Map.LobbyOption("difficulty") == "easy" then

elseif Map.LobbyOption("difficulty") == "normal" then

else

end

VolkovsSent = false

InfAtk = {}
InfTypes = {"e1","e3"}
InfAmount = 4

ArmorAtk = {}
ArmorTypes = {"arty","2tnk"}

InfProductionStarted = 0
ArmorProductionStarted = 0

--[[
Trigger.OnKilled(, function()

end)

Trigger.OnKilled(, function()

end)
]]

SetOffAlarm = function()
	Media.Debug("1")
	if AlarmOff == true then
		--Send Unis
		Media.Debug("2")
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			Media.Debug("3")
			Utils.Do(GarrisonInfs, function(a)
				IdleHunt(a)
			end)
			Utils.Do(GarrisonTanks, function(b)
				IdleHunt(b)
			end)
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(30), function()
			if InfProductionStarted == 0 then
				Media.Debug("Start to produce infantry")
				ProduceInfantry()
				InfProductionStarted = 1
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(20), function()
			if ArmorProductionStarted == 0 then
				Media.Debug("Start to produce armor")
				ProduceArmor()
				ArmorProductionStarted = 1
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			if not MedicalTent.IsDead then
				if VolkovsSent == false then
					ProduceVolkovClones()
					VolkovsSent = true
				end
			end
		end)
		--Trigger.AfterDelay(DateTime.Seconds(20), function()
			
		--end)
	end
end

--[[CHECK THIS CODE LAST]]
--[[
SendDefensiveHelis = function()
	local defensiveCopters = Utils.Where(Map.ActorsInWorld, function(self) return self.Type == "heli" end)

	if #defensiveCopters > 0 then
		CheckHeliAmmo(defensiveCopters[1])
	end
end

CheckHeliAmmo = function(unit)
	local targets = Utils.Where(Map.ActorsInWorld, function(self) return self.Owner == player and self.HasProperty("Health") end)
	if #targets > 0 then
		if not unit.IsDead then
			IdleHunt(unit)
		end
	end
end
]]
--[[CHECK THIS CODE LAST]]

ProduceVolkovClones = function()
	local superSoldiers = Reinforcements.Reinforce(enemy, {"asdf","asdf","asdf"}, {EnemyVolkovEntry.Location, EnemyVolkovDst.Location}, 10)
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		Utils.Do(superSoldiers, function(s)
			IdleHunt(s)
		end)
	end)
end

ProduceInfantry = function()
	if Tent1.IsDead and Tent2.IsDead and Tent3.IsDead then
		return
	end
	Media.Debug("Inf Production in progress")
	local delay = DateTime.Seconds(1)
	local toBuild = { Utils.Random(InfTypes) }
	enemy.Build(toBuild, function(unit)
		InfAtk[#InfAtk + 1] = unit[1]
		if #InfAtk >= InfAmount then
			Utils.Do(InfAtk, function(i)
				IdleHunt(i)
			end)
			InfAtk = { }
			Trigger.AfterDelay(DateTime.Seconds(20), function()
				ProduceInfantry()
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceInfantry()
			end)
		end
	end)
end

ProduceArmor = function()
	if Weap1.IsDead and Weap2.IsDead and Weap3.IsDead then
		return
	end
	local toBuild = { Utils.Random(ArmorTypes) }
	Media.Debug("HERE!!!")
	enemy.Build(toBuild, function(unit)
		Media.Debug("Send tanks")
		Utils.Do(unit, function(u)
			IdleHunt(u)
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(20), function()
			ProduceArmor()
		end)
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end
