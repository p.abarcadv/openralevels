if Map.LobbyOption("difficulty") == "easy" then
	ExpandedObjective = false
	TimerTicks = DateTime.Seconds(10)
elseif Map.LobbyOption("difficulty") == "normal" then
	ExpandedObjective = true
	TimerTicks = DateTime.Seconds(10)
else
	ExpandedObjective = true
	TimerTicks = DateTime.Seconds(10)
end

NotifyDelay = DateTime.Seconds(2)

ticked = TimerTicks

VolkovLiberated = 0
AntiAirOff = 0
AlarmOff = false

VolkovDied = false

timerStarted = false
timerEnded = false

tranExtractionPath = {ExtractTranDst.Location, ExtractTranEntry.Location}

--Flavor
BaseHpads = {Hpad1, Hpad2, Hpad3, Hpad4}
DefensiveCopters = {Helicopter1, Helicopter2, Helicopter3, Helicopter4}
--End Flavor

PatrolGuardsHospA = {PatrolGuardAHosp1, PatrolGuardAHosp2}
PatrolGuardsHospB = {PatrolGuardBHosp1, PatrolGuardBHosp2}
PatrolGuardsHpadA = {PatrolGuardAHpad1, PatrolGuardAHpad2}
PatrolGuardsHpadB = {PatrolGuardBHpad1, PatrolGuardBHpad2}
PatrolGuardsPrisonTop = {PatrolGuardTop1, PatrolGuardTop2, PatrolGuardTop3, PatrolGuardTop4, PatrolGuardTop5}
PatrolGuardsPrisonBot = {PatrolGuardBot1, PatrolGuardBot2, PatrolGuardBot3, PatrolGuardBot4, PatrolGuardBot5}
PatrolGuardsEntrance = 
{
PatrolGuardEntrance1, PatrolGuardEntrance2, PatrolGuardEntrance3, PatrolGuardEntrance4, PatrolGuardEntrance5
}

PatrolEntranceWays = {EntryPatrolWay1.Location, EntryPatrolWay2.Location}

PatrolHospWaysA = {HospPatrolWay1.Location, HospPatrolWay2.Location}
PatrolHospWaysB = {HospPatrolWay2.Location, HospPatrolWay1.Location}
PatrolHpadWaysA = {HpadPatrolWay1.Location, HpadPatrolWay2.Location}
PatrolHpadWaysB = {HpadPatrolWay2.Location, HpadPatrolWay1.Location}

PatrolTopWays = {PatrolTopWay1.Location, PatrolTopWay2.Location}
PatrolBotWays = {PatrolBotWay1.Location, PatrolBotWay2.Location}

PatrolTime = DateTime.Seconds(3)

PBoxBuildLocations = {BuildPBox1.Location, BuildPBox2.Location, BuildPBox3.Location, BuildPBox4.Location}

EntranceCameraSpawn = { CPos.New(87,67), CPos.New(87,68), CPos.New(87,69), CPos.New(87,70), CPos.New(87,71) }

EntranceUnits = 
{
TopEntryTurret, BotEntryTurret, PatrolGuardEntrance1, PatrolGuardEntrance2, PatrolGuardEntrance3, PatrolGuardEntrance4, PatrolGuardEntrance5
}

GarrisonTanks = {AggroTank1, AggroTank2, AggroTank3, AggroTank4, AggroTank5, AggroTank6}

GarrisonInfs = {AggroInf1, AggroInf2, AggroInf3, AggroInf4, AggroInf5, AggroInf6, AggroInf7, AggroInf8, AggroInf9, AggroInf10}

GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end
			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)
				if bool then
					stop = true
					i = i + 1
					if i > #waypoints then
						i = 1
					end
					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end

ActivatePatrols = function()
	GroupPatrol(PatrolGuardsEntrance,PatrolEntranceWays,PatrolTime)
	GroupPatrol(PatrolGuardsPrisonTop,PatrolTopWays,PatrolTime)
	GroupPatrol(PatrolGuardsPrisonBot,PatrolBotWays,PatrolTime)
	GroupPatrol(PatrolGuardsHospA,PatrolHospWaysA,PatrolTime)
	GroupPatrol(PatrolGuardsHospB,PatrolHospWaysB,PatrolTime)
	GroupPatrol(PatrolGuardsHpadA,PatrolHpadWaysA,PatrolTime)
	GroupPatrol(PatrolGuardsHpadB,PatrolHpadWaysB,PatrolTime)
end

--EntranceUnits
Trigger.OnEnteredFootprint(EntranceCameraSpawn, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		Actor.Create("camera", true, { Owner = player, Location = CameraSpawnEntry.Location } )
	end
end)

SetUpAlarmingUnits = function()
	Utils.Do(EntranceUnits, function(a)
		Trigger.OnDamaged(a, function()
			if not a.IsDead then
				AlarmOff = true
				SetOffAlarm()
				Trigger.ClearAll(a)
				
				Trigger.AfterDelay(DateTime.Seconds(2), function()
					timerStarted = true
				end)
			end
		end)
	end)
end

CreateExtraDefences = function()
	local pos = 1
	
	Utils.Do(PBoxBuildLocations, function(h)
		Actor.Create("pbox", true, { Owner = enemy, Location = PBoxBuildLocations[pos] } )
		pos = pos + 1
	end)
end

CreateFact = function()
	local factpos = Utils.Random({FactPos1.Location, FactPos2.Location, FactPos3.Location})

	Fact = Actor.Create("fact", true, { Owner = enemy, Location = factpos } )
end

Laboratory = {MedicalTent, TechHosp, TechCenter}

--Discover Laboratory
OnDiscoveredX = function()
	DiscoverLab = player.AddSecondaryObjective("Look for additional intel.")
end

Flavor = function()
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		local landings = BaseHpads
		local aircrafts = DefensiveCopters
		
		for i = 1, #aircrafts, 1 do
			aircrafts[i].ReturnToBase(landings[i])
		end
	end)
end

Trigger.OnKilled(JailTechCenter, function()
	VolkovLiberated = 1
	
	Volkov = Actor.Create("volkov", true, { Owner = player, Location = SpawnPlayerVolkov.Location } )
	Volkov.Health = 10000
	
	VolkovSurvive = player.AddPrimaryObjective("Volkov must survive.")
	ExtractVolkov = player.AddPrimaryObjective("Extract Volkov from the area.")
	
	CheckAntiAir()
	
	Trigger.OnKilled(Volkov, function()
		player.MarkFailedObjective(VolkovSurvive)
	end)
end)

CheckAntiAir = function()

end

SendExtractionTran = function()
	--ExtractCam = Actor.Create("camera", true, { Owner = player, Location = tranExtractDst.Location } )
	local flare = Actor.Create("flare", true, { Owner = player, Location = ExtractTranDst.Location+CVec.New(0,-1) })
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		flare.Destroy()
	end)
	
	tranExtract = Reinforcements.ReinforceWithTransport(player, tranE, nil, tranExtractionPath)[1]
	
	if not Volkov.IsDead then
		Trigger.OnRemovedFromWorld(Volkov, EvacuateHelicopter)
	end

	--Trigger.OnKilled(tranExtract, ExtractionFailed)
	--Trigger.OnRemovedFromWorld(tranExtract, ExtractionSuccessful)
end

EvacuateHelicopter = function()
	if tranExtract.HasPassengers then
		tranExtract.Move(tranExtractEntry.Location)
		tranExtract.Destroy()
	end
end

ExtractionSuccessful = function()
	if not tranExtract.IsDead then
		Trigger.AfterDelay(DateTime.Seconds(1), function()	
			player.MarkCompletedObjective(ExtractVolkov)
			--player.MarkCompletedObjective(VolkovSurvive)
		end)
	end
end

ExtractionFailed = function()
	Media.PlaySpeechNotification(player, "ObjectiveNotMet")
	Trigger.AfterDelay(NotifyDelay, function()
		player.MarkFailedObjective(VolkovSurvive)
		player.MarkFailedObjective(ExtractVolkov)
	end)
end

Trigger.OnAllKilled(Laboratory, function()
	
	
	--RescueVolkov = player.AddPrimaryObjective("Free the VIP from the Research Complex.")
	if VolkovDied then
	--player.MarkCompletedObjective(DestroyLaboratory)
	end
end)

Tick = function()
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(DenySoviets)
	end
	
	if VolkovLiberated == 1 then
		--CheckAntiAir()
		if AntiAirOff == true then
			AntiAirOff = false
		end
	end

	if timerStarted == true then
		if ticked > 0 then
			UserInterface.SetMissionText("Arrival of extraction unit in: " .. Utils.FormatTime(ticked), TimerColor)
			ticked = ticked - 1
		elseif ticked == 0 then
			FinishTimer()
			ticked = ticked - 1
			Trigger.AfterDelay(DateTime.Seconds(1), function() SendExtractionTran() end)
		end
	end
end

FinishTimer = function()
	timerEnded = true
	for i = 0, 5, 1 do
		local c = TimerColor
		if i % 2 == 0 then
			c = HSLColor.White
		end
		
		Trigger.AfterDelay(DateTime.Seconds(i), function() UserInterface.SetMissionText("Transport incoming.", c) end)
	end
	Trigger.AfterDelay(DateTime.Seconds(6), function() UserInterface.SetMissionText("") end)
end

InitTriggers = function()
	Camera.Position = StartingCameraLoc.CenterPosition
	
	enemy.Cash = 100000
	
	CreateFact()
	
	Flavor()
	
	ActivatePatrols()
	
	OnDiscoveredX()
	
	CreateExtraDefences()
	
	SetUpAlarmingUnits()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
    
    RescueVolkov = player.AddPrimaryObjective("Free the VIP from the Jail Complex.")
	
	DenySoviets = enemy.AddPrimaryObjective("Deny the soviets.")
    
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "MissionFailed")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "MissionAccomplished")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	goodguy = Player.GetPlayer("GoodGuy")

	TimerColor = player.Color

	InitTriggers()
	InitObjectives()
end