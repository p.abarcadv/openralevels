if Map.LobbyOption("difficulty") == "easy" then
	TanyaType = "e7"
	VasiliType= "vasily.autotarget"
elseif Map.LobbyOption("difficulty") == "normal" then
	TanyaType = "e7.noautotarget"
	VasiliType= "vasily"
else
	TanyaType = "e7.noautotarget"
	VasiliType= "vasily.injured"
end

SnipersCreated = false
SniperCount = 0

GuardsAlerted = 0

SendCivToMinePos = { CPos.New(75,37), CPos.New(76,37), CPos.New(77,37), CPos.New(77,36), CPos.New(78,36) }

Hostages = {Hostage1, Hostage2, Hostage3, Hostage4, Hostage5, Hostage6, Hostage7}

SniperSpawn = 
{
	SniperLoc1, SniperLoc2, SniperLoc3, SniperLoc4, SniperLoc5, SniperLoc6, SniperLoc7,
	SniperLoc8, SniperLoc9, SniperLoc10, SniperLoc11, SniperLoc12
}

AlertZone = 
{
	CPos.New(58,56), CPos.New(59,56), CPos.New(62,54), CPos.New(63,54), CPos.New(63,53), CPos.New(64,53), 
	CPos.New(65,53), CPos.New(66,53), CPos.New(67,53), CPos.New(68,53)
}

Guards = {Guard1, Guard2, Guard3, Guard4, Guard5, Guard6, Guard7}
SniperTeam = { HostageTargetingSniper1, HostageTargetingSniper2, HostageTargetingSniper3 }

Executioners = { Guard1, Guard2, Guard3, Guard4, Guard5, Guard6, Guard7, HostageTargetingSniper1, HostageTargetingSniper2, HostageTargetingSniper3 }

SendTanya = function()
	Tanya = Reinforcements.Reinforce(player, {TanyaType}, {InitUnitsEntry.Location, InitUnitsDst.Location}, 1)[1]
	
	Trigger.AfterDelay(1, function()
		Trigger.OnKilled(Tanya, function()
			player.MarkFailedObjective(TanyaSurvive)
		end)
	end)
end

Trigger.OnEnteredFootprint(AlertZone, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		DestroyBarrels()
	end
end)

DestroyBarrels = function()
	if not BarrelToAttack.IsDead then
		if not Guard2.IsDead then
			Guard2.Attack(BarrelToAttack, true, true)
		end
	end
end

Trigger.OnKilled(BarrelToAttack, function()
	local bridge = Utils.Where(Map.ActorsInWorld, function(actor) return actor.Type == "bridge1" end)[1]

	if not bridge.IsDead then
		bridge.Kill()
	end
end)

Trigger.OnAnyKilled(Guards,function()
	if GuardsAlerted == 1 then
		GuardsAlerted = -1
		ExecuteCivs()
	end
end)

ExecuteCivs = function(unit)
	if unit == nil then
		unit = Utils.Random(Executioners)
	end
	
	if not unit.IsDead then
		Media.Debug("Unit not dead")
		Utils.Do(Hostages, function(c)
			if not c.IsDead then
				unit.Attack(c,true,true)
			end
		end)
	else
		Media.Debug("Look for another executioner")
		ExecuteCivs()
	end
end

Trigger.OnAnyKilled(Hostages, function()
	--player.MarkFailedObjective(ProtectCivs)
end)

Tick = function()
	if Map.LobbyOption("difficulty") ~= "hard" then
		if SnipersCreated == true then
			SnipersCreated = false
			Media.DisplayMessage("There are " .. tostring(SniperCount) .. " snipers in the area.","Vasily")
		end
	end
end

VasiliReinforcePath = { SniperResEntry.Location, SniperResDst.Location }

SendVasily = function()

	Vasili = Reinforcements.Reinforce(player, {VasiliType}, VasiliReinforcePath, 1)[1]
	
	if Map.LobbyOption("difficulty") == "hard" then
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Media.DisplayMessage("He has been gravely injured. Imparing his vision.","Command")
		end)
	end
	
	VasiliSurvive = player.AddSecondaryObjective("Vasili must survive.")
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		Media.DisplayMessage("Vasili will be helping in this mission.","Command")
	end)
	
	Trigger.AfterDelay(1, function()
		Trigger.OnKilled(Vasili, function()
			player.MarkFailedObjective(VasiliSurvive)
		end)
	end)
end

SendCivToMine = function()
	HelpingCiv.Move(MineToStepOn.Location)
end

CreateSnipers = function()
	Utils.Do(SniperSpawn, function(p)
		Actor.Create("sniper", true, { Owner = enemy, Location = p.Location })
		SniperCount = SniperCount+1
	end)
	
	Trigger.AfterDelay(10, function()
		SnipersCreated = true
	end)
end

-- player.AddSecondaryObjective(".")

InitTriggers = function()
	Camera.Position = StartingCamera.CenterPosition
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		ExecuteCivs(HostageTargetingSniper3)
		SendCivToMine()
	end)

	CreateSnipers()
	SendVasily()
	SendTanya()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	SaveHostages = player.AddPrimaryObjective("Free the hostages.")
	TanyaSurvive = player.AddPrimaryObjective("Tanya must survive.")
	
	--DenyAllies = enemy.AddPrimaryObjective("Deny the Allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)

end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	neutral = Player.GetPlayer("Neutral")
	--goodguy = Player.GetPlayer("GoodGuy")
	
	InitTriggers()
	InitObjectives()
end