
if Map.LobbyOption("difficulty") == "easy" then
	ZombieParty = { "zombie","zombie" }
elseif Map.LobbyOption("difficulty") == "normal" then
	ZombieParty = { "zombie","zombie","zombie" }
else
	ZombieParty = { "zombie","zombie","zombie","zombie" }
end

PlayerInf = {"e1","e1","e1","e1","e1","e6","e6","e6","e6"}

GenerateFence = 
{
CPos.New(66,63),CPos.New(66,64),CPos.New(67,64),
CPos.New(66,67),CPos.New(66,68),CPos.New(65,68),CPos.New(65,69),
CPos.New(70,64),CPos.New(70,63),
CPos.New(69,59),CPos.New(70,59),CPos.New(69,60),
CPos.New(72,64),CPos.New(72,65),CPos.New(72,66),
CPos.New(71,68),CPos.New(72,68),CPos.New(72,67),CPos.New(73,67),
CPos.New(75,62),CPos.New(75,63),CPos.New(75,64),CPos.New(76,64),
CPos.New(77,68),CPos.New(78,68),CPos.New(79,68),
CPos.New(78,65),CPos.New(79,65),CPos.New(78,66)
}

AlliedUnitsA = { AlldUnitA1, AlldUnitA2, AlldUnitA3, AlldUnitA4, AlldUnitA5, AlldUnitA6, AlldUnitA7 } 

AlliedUnitsB = {  }

DiscoverAlliesA = { CPos.New(39,80), CPos.New(40,80), CPos.New(41,80), CPos.New(42,80), CPos.New(43,80), CPos.New(45,79), CPos.New(46,75), CPos.New(47,78), CPos.New(48,78), CPos.New(49,78), CPos.New(50,78) }

DiscoverAlliesB = { CPos.New(42,55), CPos.New(43,55), CPos.New(44,55), CPos.New(45,55) } 

RemovablePower = { RemovablePower1, RemovablePower2, RemovablePower3, RemovablePower4 }

Village1Footprints = { CPos.New(97,46), CPos.New(98,46) }
Village2Footprints = { CPos.New(84,84), CPos.New(84,85), CPos.New(85,84), CPos.New(86,84) }
Village3Footprints = { CPos.New(49,86), CPos.New(50,86), CPos.New(50,85), CPos.New(51,85), CPos.New(52,85), CPos.New(53,85) }

TriggerArea1 = { CPos.New(62,50), CPos.New(63,50), CPos.New(64,50), CPos.New(65,50), CPos.New(66,50) }
TriggerArea2 = { CPos.New(68,64) }

cameraSpawnPos = { CreateCam1, CreateCam2, CreateCam3 }

switchProgressA = false
switchProgressB = false

AbandonedStuff =
{
AbandonedPower1, AbandonedPower2, AbandonedPower3, 
AbandonedPower4, MedicalFacility, AbandonedPower5,
AbandonedBarr, AbandonedDome 
}
baseZombieGen = true

SpawnZombies = function()
	
end

Trigger.OnEnteredFootprint(TriggerArea1, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		switchProgressA=true
	end
	
end)

Trigger.OnEnteredFootprint(TriggerArea2, function(a, id)
	if switchProgressA == true and a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		Media.StopMusic()
		Media.PlayMusic("run1226m")
		Trigger.AfterDelay(DateTime.Seconds(10), function()
			if switchProgressB == false then
				switchProgressB = true
				CreateZombies()
			end
		end)
	end
end)

Trigger.OnEnteredFootprint(DiscoverAlliesA, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		ChangeOwners(goodguy, AlliedUnitsA)
	end
end)

Trigger.OnEnteredFootprint(DiscoverAlliesB, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		--ChangeOwners(goodguy, AlliedUnitsA)
	end
end)

Trigger.OnEnteredFootprint(Village1Footprints, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		local vill1Cam = Actor.Create("camera.medium", true, { Owner = player, Location = Village1Cam.Location })
		PassingByGuy.Move(RemovePassingByGuy.Location)
		PassingByGuy.Destroy()
	end
end)

Trigger.OnEnteredFootprint(Village2Footprints, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		local vill2Cam = Actor.Create("camera.medium", true, { Owner = player, Location = Village2Cam.Location })
		SpawnZombies()
	end
end)

Trigger.OnEnteredFootprint(Village3Footprints, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		local vill3Cam = Actor.Create("camera.medium", true, { Owner = player, Location = Village3Cam.Location })
		SpawnZombies()
	end
end)

ChangeOwners = function(giveToOwner, units)
	Utils.Do(units, function(u)
		u.Owner = giveToOwner
	end)
end

SendTran = function()
	local tran = Reinforcements.ReinforceWithTransport(player, "tran.insertion", PlayerInf, {TranEntry.Location, TranDst.Location}, {TranEntry.Location} )
end

Zombificator = function()
	
end

CreateZombies = function()
	if baseZombieGen == true then
		local randomBuilding = Utils.Random(AbandonedStuff)
		local zomboids = Reinforcements.Reinforce(zombie, ZombieParty, {randomBuilding.Location+CVec.New(0,1), randomBuilding.Location+CVec.New(0,2)}, 1 )
		--Reinforcements.Reinforce(enemy, Convoy, ConvoyPathA, 20 )
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			SendZombies(zomboids)
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(15), function()
			CreateZombies()
		end)
	else
		return
	end
	
end

SendZombies = function(undead)
	local wannabeZombies = Utils.Where(player.GetActors(), function(w)
		return w.Owner == player and (w.Type == "e1" or w.Type == "e6" or w.Type == "e2")
	end)
	Media.Debug(tostring(#wannabeZombies))
	if #wannabeZombies > 0 then
		local targetPos = (Utils.Random(wannabeZombies))
		Utils.Do(undead, function(u)
			if not u.IsDead --[[ <--Irony ]] then
				u.AttackMove(targetPos.Location)
			end
		end)
	end
end

SetupTriggers = function()
	b_count = 0
	Media.Debug("Total objects: " .. #AbandonedStuff)
	Utils.Do(AbandonedStuff, function(building)
		b_count = b_count + 1
		Media.Debug(tostring(b_count))
		Media.Debug(tostring(AbandonedStuff[b_count]))
		Trigger.OnKilledOrCaptured(building, function()
			table.remove(AbandonedStuff, GetTablePos(building))
			if #AbandonedStuff <= 0 then
				baseZombieGen = false
			end
			DebugClause()
		end)
	end)
end



GetTablePos = function(building)
	for x,y in ipairs(AbandonedStuff) do
		if y == building then
			return x
		end
	end
end

DebugClause = function()
		--Trigger.AfterDelay(DateTime.Seconds(25), function()
		--Utils.Do(AbandonedStuff, function(x)
		Media.Debug("Total objects: " .. #AbandonedStuff)
		Media.Debug(tostring(AbandonedStuff[1]))
		Media.Debug(tostring(AbandonedStuff[2]))
		Media.Debug(tostring(AbandonedStuff[3]))
		Media.Debug(tostring(AbandonedStuff[4]))
		Media.Debug(tostring(AbandonedStuff[5]))
		Media.Debug(tostring(AbandonedStuff[6]))
		Media.Debug(tostring(AbandonedStuff[7]))
		Media.Debug(tostring(AbandonedStuff[8]))
		--end)
	--end)

end

Tick = function()
	if player.HasNoRequiredUnits() then
		zombie.MarkCompletedObjective(ZombieWin)
	end
	
	if (Utils.RandomInteger(1, 1000) == 10) then
		local delay = Utils.RandomInteger(1, 10)
		Lighting.Flash("LightningStrike", delay)
		Trigger.AfterDelay(delay, function()
			Media.PlaySound("thunder" .. Utils.RandomInteger(1,6) .. ".aud")
		end)
	end
	if (Utils.RandomInteger(1, 200) == 10) then
		Media.PlaySound("thunder-ambient.aud")
	end
end

InitTriggers = function()
	Camera.Position = TranDst.CenterPosition
	InitCamera = Actor.Create("camera.medium", true, {Owner = player, Location = TranDst.Location})
	Trigger.AfterDelay(DateTime.Seconds(13), function()
		InitCamera.Destroy()
		Utils.Do(RemovablePower, function(building)
			building.Destroy()
		end)
		Utils.Do(GenerateFence, function(fenceLoc)
			Actor.Create("fenc", true, {Owner = badguy, Location = fenceLoc})
		end)
	end)
	
	Utils.Do(cameraSpawnPos, function(cam)
		local camToDestroy = Actor.Create("camera.medium", true, {Owner = player, Location = cam.Location})
		camToDestroy.Destroy()
	end)
	
	
	--local initCam = Actor.Create(player, "camera", {Location = TranDst})
	
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		Media.PlayMusic("search")
	end)
	SendTran()
	Media.SetBackgroundMusic("rain")
	
	SetupTriggers()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	AcquireMoney = player.AddPrimaryObjective("Locate the Medical Research Facility.")
	TakeOver = player.AddSecondaryObjective("")
	ZombieWin =  zombie.AddPrimaryObjective("Deny the soviets.")
	

	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

SetupMood = function()
	--Media.PlayMusic("await")
	Media.StopMusic()
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	goodguy = Player.GetPlayer("GoodGuy")
	badguy = Player.GetPlayer("BadGuy")
	enemy = Player.GetPlayer("Greece")
	zombie = Player.GetPlayer("Zombies")
	
	InitTriggers()
	InitObjectives()
	SetupMood()
end