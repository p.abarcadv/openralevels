
if Map.LobbyOption("difficulty") == "easy" then
	FixedTime = DateTime.Minutes(10)
	RelativeTime = Utils.Random({-1,0,1,2})
elseif Map.LobbyOption("difficulty") == "normal" then
	FixedTime = DateTime.Minutes(15)
	RelativeTime = Utils.Random({-1,0,1,2})
elseif Map.LobbyOption("difficulty") == "hard" then
	FixedTime = DateTime.Minutes(18)
	RelativeTime = Utils.Random({1,2,3,4,5})
end

DefenseLineApproach1 = { CPos.New(57,93), CPos.New(58,93), CPos.New(58,92), CPos.New(59,92) }
DefenseLineApproach2 = { CPos.New(57,93), CPos.New(58,93), CPos.New(58,92), CPos.New(59,92) }

AttackingUnitTypes = {"1tnk","1tnk","2tnk","2tnk","arty","apc.loaded","apc.loaded","jeep","jeep"}

DefendingUnitTypes = {"3tnk","ftrk","v2rl"}

TimeForReinforcements = Utils.Random({0.5,1}, function()end)

TimerTicks = 10
ticked = TimerTicks

AmbushedUnits = { }




EnemyEntryLocs = 
	{
	EnemyEntry1.Location, EnemyEntry2.Location, EnemyEntry3.Location, 
	EnemyEntry4.Location, EnemyEntry5.Location, EnemyEntry6.Location
	}

ReinforcingEntryLocs =
{
ReinforcingEntry1, ReinforcingEntry2,ReinforcingEntry3
}

Enemy = { EnemyEntry1.Location }
	
	
WreckedBaseGuards = 
{
guard1, guard2, guard3, guard4, guard5, 
guard6, guard7, guard8, guard9, guard10, 
guard11, guard12
}
	
WreckedBase =
{
WreckedPower1, WreckedPower2, WreckedWeap, WreckedBarr, WreckedProc
}
	
--[[
CratersDestroyedBase = 
	{ 
	CPos.New(42, 72), CPos.New(46, 72), CPos.New(43, 70), CPos.New(46, 70), CPos.New(47, 69), CPos.New(42, 65), 
	CPos.New(35, 64), CPos.New(39, 61), CPos.New(45, 62), CPos.New(49, 62), CPos.New(45, 67)
	}
]]	--[[
CratersDestroyedBase = 
	{ 
	CPos.New(63, 98), CPos.New(65, 97), CPos.New(63, 97), CPos.New(65, 100), CPos.New(47, 69), CPos.New(42, 65), 
	CPos.New(69, 99), CPos.New(75, 102), CPos.New(73, 103), CPos.New(75, 106), CPos.New(35, 64), CPos.New(39, 61),
	CPos.New(45, 62), CPos.New(49, 62), CPos.New(45, 67)
	}

CratersOnRoad =
	{
	CPos.New(65, 90), CPos.New(70, 87), CPos.New(67, 83), CPos.New(63, 63) : 68, 71 : 63, 105 : 64, 104 : 60, 106 : 60, 105 : 61, 105 : 74, 97 : 76, 96
	}
]]

Ambushers =
	{
	ambusher1, ambusher2, ambusher3, ambusher4, ambusher5, ambusher6, ambusher7, ambusher8,
	ambusherdog1, ambusherdog2, ambusherdog3, ambusherdog4, ambusherdog5
	}
	
CheckpointActors = 
	{ 
	rifle1, rifle2, rifle3, rifle4, rifle5, rifle6, rifle7, rifle8, rifle9, rifle10, rifle11, rifle12,
	hvytnk1, hvytnk2, hvytnk3, flak1, flak2, power1, power2, power3, barr1, ftur1, ftur2, v2rl1, grendade1,
	grenade2, grenade3, grenade4, tsla1, sams1
	}

--First 20 seconds or so will be used for showing the battle

Trigger.OnAllKilled(WreckedBaseGuards, function()

	player.MarkCompletedObjective(ClearBase)

	Actor.Create("moneycrate.lowcash", true, { Owner = neutral, Location = CashSpawn1.Location })
	Actor.Create("moneycrate.lowcash", true, { Owner = neutral, Location = CashSpawn2.Location })

	Utils.Do(WreckedBase, function(actor)
		if not actor.IsDead then
			actor.Owner = player
		end
	end)
end)

IntroSequence = function()

	Trigger.AfterDelay(DateTime.Seconds(1), function()
		Ambushed1 = Reinforcements.Reinforce(enemy, {"2tnk"}, { EnemyEntry1.Location, FirstEnemyDst1.Location })[1]
		IdleHunt(Ambushed1)
	end)

	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Ambushed2 = Reinforcements.Reinforce(enemy, {"2tnk"}, { EnemyEntry3.Location, FirstEnemyDst2.Location })[1]
		IdleHunt(Ambushed2)
	end)

	Trigger.AfterDelay(DateTime.Seconds(3), function()
		Ambushed3 = Reinforcements.Reinforce(enemy, {"2tnk"}, { EnemyEntry5.Location, FirstEnemyDst3.Location })[1]
		IdleHunt(Ambushed3)
		
		AmbushedUnits = { Ambushed1, Ambushed2, Ambushed3}
		Trigger.OnAllKilled(AmbushedUnits, function()
			
			Utils.Do(Ambushers, function(unit)
				if not unit.IsDead then
					unit.Move(GuerillaEntry1.Location)
					unit.Move(GuerillaDst1.Location)
				end
			end)
		
			Trigger.AfterDelay(DateTime.Seconds(4), function()
				ChangeOwners(CheckpointActors, player)
				
				Camera.Position = CheckpointCamera.CenterPosition
			end)
		end)
	end)


	Trigger.AfterDelay(DateTime.Seconds(4), function()
		Utils.Do(Ambushers, function(unit)
			if not unit.IsDead then
				unit.AttackMove(StartingCamera.Location)
			end
		end)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		CreateAttacks()
	end)
end

ChangeOwners = function(actors, p)
	Utils.Do(actors, function(unit)
		if not unit.IsDead then
			unit.Owner = p
		end
	end)
end

--If apc is attacked, this will unload all soldiers inside
DeployWhenHurt = function(unit)
	Trigger.OnDamaged(unit, function()
		if unit.Type == "apc.loaded" and unit.PassengerCount ~= 0 and unit.Owner == enemy then
			unit.Stop()
			unit.UnloadPassengers()
			unit.Hunt()
		end
	end)
end

Trigger.OnEnteredFootprint(DefenseLineApproach1, function(a, id)
	if a.Owner == badguy then
	--Trigger.RemoveFootprintTrigger(id)
		a.Owner = player
	end
end)

delayBetweenAtks = 30

enemyCol = {}
playerCol = {}

PrepareWaves = function(amount, p)
	--Media.PlaySoundNotification(player, "AlertBuzzer")
	--for i = 0, amount, 1 do
	--	local entry = Utils.Random(EnemyEntryLocs)
	--	local unitsE = Utils.Random(AttackingUnitTypes)
		--local unitsP = Utils.Random(DefendingUnitTypes)
		
	--	table.insert(enemyCol, unitsE[0])
		--if p == player then
		--	table.insert(playerCol, units[1])
		--elseif p == enemy then
		--	
		--end

		
			--Trigger.AfterDelay(DateTime.Seconds(4), function()
				
			--end)
		--end)
	--end
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		local units = Reinforcements.Reinforce(player, {"2tnk","2tnk"}, { entry, FirstEnemyDst2.Location })
		--local xxxx = Reinforcements.Reinforce(player, Prison3Escapees, Prison3Path, 10) end)
		return units
	end)
end

CreateAttacks = function()
	
	local units =  Reinforcements.Reinforce(enemy, {"apc.loaded","apc.loaded"}, { entry, FirstEnemyDst2.Location })
	--{"apc.loaded", "apc.loaded"}--PrepareWaves(3, enemy)
	Media.PlaySoundNotification(player, "AlertBuzzer")
	
	Utils.Do(units, function(actor)
		actor.Hunt()
		if actor.Owner == enemy then
			actor.Hunt()	
			if actor.Type == "apc.loaded" then
				DeployWhenHurt(actor)
			end
		end
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(delayBetweenAtks), function()
		CreateAttacks()
	end)
end


IdleHunt = function(unit)
	if not unit.IsDead and unit.IsIdle then
		unit.Hunt()
		IdleHunt(unit)
	end
end

Trigger.AfterDelay((FixedTime+DateTime.Minutes(RelativeTime)), function()
	player.MarkCompletedObjective(DefendArea)
end)

Tick = function()
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(AlliedObj)
	end
end

InitTriggers = function()
	Camera.Position = StartingCamera.CenterPosition
		--Actor404
		--Actor171
	player.Cash = 2000
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DefendArea = player.AddPrimaryObjective("Defend your position.")
	DiscoverVillage = player.AddSecondaryObjective("Discover the village.") --Adds Guards
	ClearBase = player.AddSecondaryObjective("Salvage the ruined base.") --Adds Money
	AlliedObj = enemy.AddPrimaryObjective("Deny the soviets.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	enemy = Player.GetPlayer("Greece")
	player = Player.GetPlayer("USSR")
	neutral = Player.GetPlayer("Neutral")
	badguy = Player.GetPlayer("BadGuy")


	InitTriggers()
	InitObjectives()
	IntroSequence()
	
end