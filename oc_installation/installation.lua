if Map.LobbyOption("difficulty") == "easy" then

	InfAtkGroup = {"e1","e1","e1","e1","e3","e3","e3","e6"}
	
elseif Map.LobbyOption("difficulty") == "normal" then

	InfAtkGroup = {"e1","e1","e1","e1","e3","e3","e3","e6"}

else

	InfAtkGroup = {"e1","e1","e1","e1","e3","e3","e3","e6"}

end

introEnded = false
outroStarted = false

MainFrameControl = 0

ActiveMainFrame1 = false
ActiveMainFrame2 = false
ActiveMainFrame3 = false

ReinforcePath1 = { PUnitsEntry1.Location, PUnitsEntry1.Location+CVec.New(2,0) }
ReinforcePath2 = { PUnitsEntry2.Location, PUnitsEntry2.Location+CVec.New(2,0) }

ReinforcePath3 = { PUnitsEntry3.Location, PUnitsEntry3.Location+CVec.New(2,0) }
ReinforcePath4 = { PUnitsEntry4.Location, PUnitsEntry4.Location+CVec.New(2,0) }

ReinforcePath5 = { PUnitsEntry5.Location, PUnitsDst5.Location }
ReinforcePath6 = { PUnitsEntry6.Location, PUnitsDst6.Location }

ReinforcePath7 = { PUnitsEntry7.Location, PUnitsDst7.Location }
ReinforcePath8 = { PUnitsEntry8.Location, PUnitsDst8.Location }

ReinforcePath9 = { PUnitsEntry9.Location, PUnitsDst9.Location }
ReinforcePath10 = { PUnitsEntry10.Location, PUnitsDst10.Location }

GuardsA = { GuardingA1, GuardingA2 }

GuardsB = { GuardingB1, GuardingB2, GuardingB3, GuardingB4, GuardingB5 }

GuardsC = { GuardingC1, GuardingC2 }

MineCreationCoords = 
{
CPos.New(65,50), CPos.New(65,52), CPos.New(66,51), CPos.New(66,53), CPos.New(67,50), CPos.New(67,52), CPos.New(68,51), CPos.New(68,53), CPos.New(69,51), CPos.New(69,53), CPos.New(70,50), CPos.New(70,52), CPos.New(71,51), CPos.New(71,53), CPos.New(72,50), CPos.New(72,52)
}

PilboxCamCoords = { CPos.New(32,60), CPos.New(32,61), CPos.New(32,62), CPos.New(32,63) }

KillableMedics = { KillableMed1, KillableMed2 }

TriggerBazookaCoords = { CPos.New(64,73), CPos.New(64,74), CPos.New(64,75), CPos.New(64,76) }

AtkEntries = 
{
	AtkEntry1.Location,
	AtkEntry2.Location,
	AtkEntry2.Location,
	AtkEntry3.Location
}

AtkPaths =
{
	AtkWay1.Location,
	AtkWay2.Location,
	AtkWay3.Location,
	AtkWay4.Location
}



AttackingIsEnabled = true

ForcesAcquired = 0

SendAtkGroup = function()
	Media.Debug("Initiate Atk")

	if AttackingIsEnabled == true then
		local n = Utils.RandomInteger(1,4)
		local rngPos = Utils.RandomInteger(-3,3)
	
		local units = Reinforcements.Reinforce( enemy, InfAtkGroup, {AtkEntries[n], AtkEntries[n] +CVec.New(-2,0) }, 1 )
		
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Utils.Do(units, function(u)
				if not u.IsDead then
					if u.Type ~= "e6" then
						u.AttackMove(AtkPaths[n])
						u.AttackMove(AtkTarget.Location + CVec.New(rngPos,0))
					else
						u.Move(AtkPaths[n])
						u.Move(AtkTarget.Location + CVec.New(rngPos,0))
					end
				end
			end)
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(60), SendAtkGroup)
	else
		return
	end
end

Trigger.OnAllKilled(GuardsA, function()
	ExtraUnits(ReinforcePath5, ReinforcePath6, {"e1", "e4", "e4"})

end)

Trigger.OnAllKilled(GuardsB, function()
	ExtraUnits(ReinforcePath3, ReinforcePath4, {"e1", "e4", "e4"})

end)

Trigger.OnAllKilled(GuardsC, function()
	ExtraUnits(ReinforcePath9, ReinforcePath10, {"e1", "e4", "e4"})

end)

Trigger.OnEnteredProximityTrigger(AtkTarget.CenterPosition, WDist.New(3 * 1024), function(a, id)
	if a.Type == "e6" and a.Owner == enemy then
		Trigger.RemoveProximityTrigger(id)
		
		enemy.MarkCompletedObjective(DenySoviet)
	end
end)

----
----
----
Trigger.OnEnteredFootprint({MainDefControl1.Location}, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		if ActiveMainFrame1 == false then
			ActiveMainFrame1 = true
			MainFrameControl = MainFrameControl + 1
		end
	end
end)

Trigger.OnEnteredFootprint({MainDefControl2.Location}, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		if ActiveMainFrame2 == false then
			ActiveMainFrame2 = true
			MainFrameControl = MainFrameControl + 1
		end
	end
end)

Trigger.OnEnteredFootprint({MainDefControl3.Location}, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		if ActiveMainFrame3 == false then
			ActiveMainFrame3 = true
			MainFrameControl = MainFrameControl + 1
		end
	end
end)
----
----
----

ExtraUnits = function(path1, path2, units)
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	
	Reinforcements.Reinforce( player, units, path1, 5 )
	Reinforcements.Reinforce( player, units, path2, 5 )
	
	ForcesAcquired = ForcesAcquired + 1 
end

CreateDefenses = function()
	player.MarkCompletedObjective(RestoreDefences)
	
	Actor.Create( "ftur", true,{ Owner = player, Location = DefFturSpawn1.Location })
	Actor.Create( "ftur", true,{ Owner = player, Location = DefFturSpawn2.Location })
	
	Trigger.AfterDelay(DateTime.Seconds(3), function()
		Actor.Create( "tsla", true,{ Owner = player, Location = DefTslaSpawn1.Location })
		Actor.Create( "tsla", true,{ Owner = player, Location = DefTslaSpawn2.Location })
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		player.MarkCompletedObjective(PreventInfiltration)
	end)
end

Trigger.OnEnteredFootprint(PilboxCamCoords, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		local pilboxCam1 = Actor.Create( "camera", true,{ Owner = player, Location = PillboxWhereCamIs.Location })
	
		Trigger.OnKilled(PillboxWhereCamIs, function()
			pilboxCam1.Destroy()
		end)
	end
end)

Trigger.OnEnteredFootprint(TriggerBazookaCoords, function(a, id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		Cam1 = Actor.Create( "camera", true,{ Owner = player, Location = QuickCamera.Location })
		local barrel = Utils.Random({TargetBarrel1, TargetBarrel2})
		if not TriggeringBazooka.IsDead then
			TriggeringBazooka.Move(FiringPosition.Location)
			TriggeringBazooka.Attack(barrel, true, true)
		end
		
		ExtraUnits(ReinforcePath7, ReinforcePath8, { "e1","e4","e4" } )
		
		Trigger.AfterDelay(DateTime.Seconds(30), function()
			Cam1.Destroy()
		end)
	end
end)

Trigger.OnKilled(KillMeds, function()
	Utils.Do(KillableMedics, function(u) 
		if not u.IsDead then
			u.Kill()
		end
	end)
end)

Tick = function()
	if introEnded == true and outroStarted == false then
		if player.HasNoRequiredUnits() then
			enemy.MarkCompletedObjective(DenySoviet)
		end
	end
	
	if ForcesAcquired >= 3 then
		ForcesAcquired = -1
		player.MarkCompletedObjective(RegroupForces)
	end
	
	if MainFrameControl >= 3 then
		MainFrameControl = -1
		CreateDefenses()
	end
	
	UserInterface.SetMissionText(tostring(MainFrameControl), player.Color)
end

InitTriggers = function()
	Camera.Position = CameraStartingLoc.CenterPosition
	
	--it may be better to have a proper intro sequence.
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		ExtraUnits( ReinforcePath1, ReinforcePath2, {"e1","e1","e1","e2","e2","e2"} )
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			introEnded = true
		end)
	end)
	
	SendAtkGroup()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	PreventInfiltration = player.AddPrimaryObjective("Prevent infiltration of main computer.")
	RestoreDefences = player.AddPrimaryObjective("Reboot defence system.")
	RegroupForces = player.AddSecondaryObjective("Clear entry ways for additional units.")
	
	DenySoviet = enemy.AddPrimaryObjective("Deny the soviets.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	
	InitTriggers()
	InitObjectives()
end