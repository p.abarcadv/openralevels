if Map.LobbyOption("difficulty") == "easy" then
	--AttackIntervals = {2.5, 3}
elseif Map.LobbyOption("difficulty") == "normal" then
	--AttackIntervals = {2, 2.5}
else
	--AttackIntervals = {1, 1.5, 2}
end

AddToDefcon = function()
	Defcon = Defcon+1
end

Tick = function()
	UserInterface.SetMissionText("Reinforcements arrive in " .. Utils.FormatTime(ticked), HUDColor)
end

InitTriggers = function()

end

InitObjectives = function()

end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	goodguy = Player.GetPlayer("GoodGuy")
	badguy = Player.GetPlayer("BadGuy")
	turkey = Player.GetPlayer("Turkey")

	InitTriggers()
	InitObjectives()
	
	HUDColor = player.Color
end