if Map.LobbyOption("difficulty") == "easy" then
	StartingCashP2 = 4000
	AllowCameras = true
elseif Map.LobbyOption("difficulty") == "normal" then
	StartingCashP2 = 3000
	AllowCameras = true
else
	StartingCashP2 = 2000
	AllowCameras = false
end

--Add cameras to spawn on gates.
TeslasGate =
{
CPos.New(94,70), CPos.New(94,71), CPos.New(94,72), CPos.New(94,73), CPos.New(94,74), CPos.New(94,75),
CPos.New(94,76), CPos.New(94,77)
}

SnipersGate =
{
CPos.New(81,69), CPos.New(80,69), CPos.New(80,70), CPos.New(80,71), CPos.New(80,72), CPos.New(80,73)
}

TeslaTankGate =
{
CPos.New(76,62), CPos.New(77,62), CPos.New(77,63), 
}

EnemyHPadCamera = 
{
--[[Top]]
CPos.New(52,56), CPos.New(52,57), CPos.New(52,58), CPos.New(52,59),
--[[Bottom]]
CPos.New(54,70), CPos.New(54,71), CPos.New(54,72), CPos.New(46,79), CPos.New(46,80), CPos.New(46,81)
}

EnemyBaseTopCamera = 
{
CPos.New(27,53), CPos.New(28,53), CPos.New(29,53), CPos.New(29,54), CPos.New(29,55), CPos.New(29,56), CPos.New(29,57)
}

EnemyBaseBotCamera =
{
CPos.New(32,78), CPos.New(32,79), CPos.New(31,79), CPos.New(35,71), CPos.New(35,72), CPos.New(35,73), CPos.New(35,74)
}

InitAtk = {InitEUnit1, InitEUnit2, InitEUnit3}

player1Added = false

ArtyArrivalCoords = { CPos.New(94,84), CPos.New(94,85), CPos.New(94,86) }
ArtyGiven = false

SecondTranAttack = {CPos.New(76,48), CPos.New(76,50), CPos.New(76,51), CPos.New(76,52), CPos.New(76,53)}
TranAttackSent = false

SandbagsP2 = {sb1,sb2,sb3,sb4,sb5,sb6,sb7,sb8,sb9,sb10,sb11,sb12,sb13,sb14}
UnitsP2 = {P2Inf1,P2Inf2,P2Inf3,P2Inf4,P2Inf5,P2Inf6,P2Inf7,P2Inf8,P2Inf9,P2Inf10,P2Inf11,P2Inf12,P2Inf13,P2Inf14}
BuildingsP2 = {P2Radar,P2Tent,P2Power1,P2Power2}

EnemyBuildings = 
{
EPower1,EPower2,EPower3,EPower4,EPower5,EPower6,EPower7,ESam1,ESam2,EFtur1,EFtur2,EDome,EFact,EWeap,EBarr,ETesla,EFix
}

TeslaPower = {TeslaPower1, TeslaPower2}

BeachDefences = {BeachDefences1, BeachDefences2, BeachDefences3, BeachDefences4, BeachDefences5, BeachDefences6}

InfAttack = {}

--Error inespecífico que resolver

SetupCameras = function()
	Trigger.OnEnteredFootprint(TeslasGate, function(a, id)
		if a.Owner == player1 or a.Owner == player2 then
			Trigger.RemoveFootprintTrigger(id)
			
			if not cam1 then
				cam1 = Actor.Create("camera", true, { Owner = player1, Location = TeslaGateCamera.Location } )
			end
		end
	end)

	Trigger.OnEnteredFootprint(SnipersGate, function(a, id)
		if a.Owner == player1 or a.Owner == player2 then
			Trigger.RemoveFootprintTrigger(id)
			
			if not cam2 then
				cam2 = Actor.Create("camera", true, { Owner = player1, Location = SnipersCamera.Location } )
			end
		end
	end)

	Trigger.OnEnteredFootprint(TeslaTankGate, function(a, id)
		if a.Owner == player1 or a.Owner == player2 then
			Trigger.RemoveFootprintTrigger(id)
			
			if not cam3 then
				cam3 = Actor.Create("camera", true, { Owner = player1, Location = TeslaTankGateCamera.Location } )
			end
		end
	end)

	Trigger.OnEnteredFootprint(EnemyHPadCamera, function(a, id)
		if a.Owner == player1 or a.Owner == player2 then
			Trigger.RemoveFootprintTrigger(id)
			
			if not cam4 then
				cam4 = Actor.Create("camera", true, { Owner = player1, Location = EnemyHpadCamera.Location } )
			end
		end
	end)

	Trigger.OnEnteredFootprint(EnemyBaseTopCamera, function(a, id)
		if a.Owner == player1 or a.Owner == player2 then
			Trigger.RemoveFootprintTrigger(id)
			
			if not cam5 then
				cam5 = Actor.Create("camera", true, { Owner = player1, Location = EnemyBaseTopCamera.Location } )
			end
		end
	end)

	Trigger.OnEnteredFootprint(EnemyBaseBotCamera, function(a, id)
		if a.Owner == player1 or a.Owner == player2 then
			Trigger.RemoveFootprintTrigger(id)
			
			if not cam6 then
				cam6 = Actor.Create("camera", true, { Owner = player1, Location = EnemyBaseBotCamera.Location } )
			end
		end
	end)
end

--Player 1 Reinforcements
SendReinforcements = function(units, dst)
	Reinforcements.ReinforceWithTransport(player1, "lst", units, dst, {dst[1]})
end

GiveUnitsToPlayer = function(p)
	Utils.Do(SandbagsP2, function(a)
		a.Owner=p
	end)
	Utils.Do(UnitsP2, function(a)
		a.Owner=p
	end)
	Utils.Do(BuildingsP2, function(a)
		a.Owner=p
	end)
	
	Media.DisplayMessage("Co-Commander is missing. Unit Control Reassigned.", "Command")
end

Trigger.OnKilled(DestroyerBarrel1, function()
	Utils.Do(TeslaPower, function(t)
		if not t.IsDead then
			t.Kill()
		end
	end)
end)

Trigger.OnKilled(DestroyerBarrel2, function()
	Utils.Do({teslaTank1, teslaTank2}, function(t)
		if not t.IsDead then
			t.Kill()
		end
	end)
end)

Trigger.OnAllKilled(EnemyBuildings, function()
	Media.Debug("Game OVer - You Win")
end)

SendEnemyTranAttack = function(units, path)
	local atkUnits = Reinforcements.ReinforceWithTransport(enemy, "tran", units, path, {path[1]})[2]
	
	Utils.Do(atkUnits, function(u)
		Trigger.OnAddedToWorld(u, function()
			IdleHunt(u)
		end)
	end)
end

Trigger.OnEnteredFootprint(SecondTranAttack, function(a, id)
	if a.Owner == player1 or a.Owner == player2 then
		Trigger.RemoveFootprintTrigger(id)
		
		if TranAttackSent == false then
			TranAttackSent = true
			SendEnemyTranAttack({"e1","e1","e1","e2","e2"},{TranAttackEntry2.Location, TranAttackDst2.Location})
		end
	end
end)

Trigger.OnEnteredFootprint(ArtyArrivalCoords, function(a, id)
	if a.Owner == player1 or a.Owner == player2 then
		Trigger.RemoveFootprintTrigger(id)
		
		if ArtyGiven == false then
			ArtyGiven = true
			SendReinforcements({"e1","e1","e1","arty","arty"}, {GreeceReinforce3Entry.Location, GreeceReinforce3Dst.Location})
		end
	end
end)

Trigger.OnAllKilled(BeachDefences, function()
	SendReinforcements({"e1","e1","e1","e1","jeep"},{GreeceReinforce2Entry.Location, GreeceReinforce2Dst.Location})
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		if BeachCamera then
			BeachCamera.Destroy()
		end
	end)
end)

RunAI = function()
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		SendEnemyTranAttack({"e4","e4","e4","e4","e4"},{TranAttackEntry1.Location, TranAttackDst1.Location})
	end)
	
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		ProduceArmor()
		ProduceInfantry()
	end)
	
	Utils.Do(InitAtk, function(u)
		IdleHunt(u)
	end)
end

ProduceInfantry = function()
	if EBarr.IsDead or EBarr.Owner ~= enemy then
		return		
	end
	
	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(5))
	local toBuild = { Utils.Random({"e1","e2"}) }

	enemy.Build(toBuild, function(unit)
		
		InfAttack[#InfAttack + 1] = unit[1]
		if #InfAttack >= 5 then
			Utils.Do(InfAttack, function(i)
				if not i.IsDead then 
					IdleHunt(i)
				end
			end)
			InfAttack = { }
			Trigger.AfterDelay(DateTime.Minutes(1), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceArmor = function()
	if EWeap.IsDead or EWeap.Owner ~= enemy then
		return		
	end
	
	local toBuild = {"3tnk"}
	enemy.Build(toBuild, function(unit)
		Utils.Do(unit, function(u)
			IdleHunt(u)

		end)
		Trigger.AfterDelay(DateTime.Minutes(2), function()
			ProduceArmor()
		end)
	end)
end

Flavor = function()
	EHind.ReturnToBase(EHpad)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

Tick = function()
	if player1Added == true then
		if player2 then
			if player1.HasNoRequiredUnits() or player2.HasNoRequiredUnits() then
				enemy.MarkCompletedObjective(DenyAllies)
			end
		elseif player1.HasNoRequiredUnits() then
			enemy.MarkCompletedObjective(DenyAllies)
		end
	end
end

InitTriggers = function()
	Camera.Position = StartingP2CameraLoc.CenterPosition

	SendReinforcements({"e1","e1","medi","e1","e1"}, {GreeceReinforce1Entry.Location, GreeceReinforce1Dst.Location})
	
	enemy.Cash = 5000
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		player1Added = true
	end)
	
	RunAI()

	Flavor()
	
	if AllowCameras then
		SetupCameras()
	end
	
	if not player2 then
		player1.Cash = StartingCashP2
		GiveUnitsToPlayer(player1)
	else
		player2.Cash = StartingCashP2
		GiveUnitsToPlayer(player2)
	end

end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player1, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
    
    DestroyBase = player1.AddPrimaryObjective("Destroy the soviet Base.")
	--DestroyBase = player2.AddPrimaryObjective("Destroy the soviet Base.")

	DenyAllies = enemy.AddPrimaryObjective("Deny the allies!")
    
	Trigger.OnObjectiveCompleted(player1, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player1, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	Trigger.OnPlayerLost(player1, function()
		Media.PlaySpeechNotification(player1, "MissionFailed")
	end)
	Trigger.OnPlayerWon(player1, function()
		Media.PlaySpeechNotification(player1, "MissionAccomplished")
	end)
end
	
WorldLoaded = function()
	player1 = Player.GetPlayer("Greece")
	player2 = Player.GetPlayer("England")
	enemy = Player.GetPlayer("USSR")
	
	humans = { player1, player2 }
	
	InitTriggers()
	InitObjectives()
end