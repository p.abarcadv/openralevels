if Map.LobbyOption("difficulty") == "easy" then
	PlayerCash = 7000

elseif Map.LobbyOption("difficulty") == "normal" then
	PlayerCash = 5000

else
	PlayerCash = 5000
	
end

RunAI = function()
	ProduceInfantry()
	ProduceArmor()
	
	OtherAttackProcedures()
end

Tick = function()
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(alliesObj)
	end
end

InitTriggers = function()
	Player.Cash = PlayerCash
	
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	ProtectCivs = player.AddPrimaryObjective("Protect the civilian evacuation.")
	--DestroyOuposts = player.AddSecondaryObjective("Destroy outposts to prevent further soviet aggresion.")
	
	DenySoviets = enemy.AddPrimaryObjective("Deny the Soviets.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")

	InitTriggers()
	InitObjectives()
end