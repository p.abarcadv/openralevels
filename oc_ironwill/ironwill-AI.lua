if Map.LobbyOption("difficulty") == "easy" then
	infCount = 7
	tankCount = 7
	
	ChronoTankCount = 2
elseif Map.LobbyOption("difficulty") == "normal" then
	infCount = 8
	tankCount = 9
	
	ChronoTankCount = 3
else
	infCount = 9
	tankCount = 10
	
	ChronoTankCount = 4
end

SeaAttackGroup1 = {"e1", "e3", "1tnk", "1tnk", "1tnk"}
SeaAttackGroup2 = {"e1", "e1", "2tnk", "2tnk", "1tnk"}
SeaAttackGroup3 = {"e1", "e3", "2tnk", "arty", "arty"}

ChopperUnits =
{
	{"e3","e3","e3","e3","e3"},
	{"e6","e6","e6","e6","e6"},
	{"e1","e1","e1","e3","e3"}
}

infGroup = { }
armorGroup = { }

AttackPaths = 
{ 

}

AttackTranDelay = DateTime.Seconds(30)

--Add this to map
RightAtkTranEntry = {RightTranEntry1.Location, RightTranEntry2.Location, RightTranEntry3.Location}
LeftAtkTranEntry = {LeftTranEntry1.Location, LeftTranEntry2.Location, LeftTranEntry3.Location}

--Add this to map
RightAtkTranDst = {RightTranDst1.Location, RightTranDst2.Location, RightTranDst3.Location}
LeftAtkTranDst = {LeftTranDst1.Location, LeftTranDst2.Location, LeftTranDst3.Location}

ChronoTankDeployZones = { ChronoDst1.Location, ChronoDst2.Location, ChronoDst3.Location }

OtherAttackProcedures = function()
	Triggeer.AfterDelay(DateTime.Minutes(5), function()
		ChronoTankAttack()
	end)
end

SendTrans = function()
	local dir = Utils.Do("left", "right")
	
	local entry = Utils.Do()
	local dst = Utils.Do(.Location, .Location)
	
	local units = Utils.Random(ChopperUnits)
	local transOccupant = Reinforcements.ReinforceWithTransport(enemy, Chopper, units, ChopperPath, Entry.Location )
	
	Utils.Do(transOccupant, function()
		if not u.IsDead then
			if u.Type == "e6" then
				
			else
				IdleHunt(u)
			end
		end
	end)
	
	Actor.Create("tran", true, { Owner = player, Locaiton = .Locaiton})
	
	tran.Move(ASDF.Location)
	
end

ChronoTankAttack = function()
	Media.PlaySound("chrono2.aud")
	local count = ChronoTankCount
	--local delay = Utils.RandomInteger(0,61)
	local Loc = Utils.Random(ChronoTankDeployZones)
	
	for i = 1,ChronoTankCount,1 do
		local posModX = Utils.RandomInteger(-2,3)
		local posModY = Utils.RandomInteger(-2,3)
		
		local c = Actor.Create("ctnk", true, { Owner = enemy, Location = Loc + CVec.New(posModX,posModY) })
		
		IdleHunt(c)
	end	
	Trigger.AfterDelay(DateTime.Seconds(1 + delay), ChronoTankAttack)
end

ChronoCruiserAttack = function()
	if ChronoCruiserCreated == true then
		Trigger.AfterDelay(DateTime.Minutes(7), function()
			ChronoCruiserAttack()
		end)
	else
		return
	end
	
	local radius = WDist.FromCells(3)
	local cruiser = Utils.Where(Map.ActorsInCircle(ChronoCruiserInit.CenterPosition, radius), function(a) return a.Type == "ca" and a.Owner == enemy end)
	
	local Loc = Utils.Do(CruiserChronoDst1.Location, CruiserChronoDst2.Location, CruiserChronoDst3.Location)
	
	Trigger.OnKilled(cruiser, function()
		ProduceCruiser()
	end)
end

TeleportCruiser = function(ship, targetPos)
	local oldPos = ship.Location
	local newPos = targetPos
	
	ship.Teleport(newPos)
	
	Trigger.AfterDelay(DateTime.Seconds(60), function()
		if not ship.IsDead then
			ship.Teleport(oldPos)
		end
	end)
end

LandingAttacks = function()
	local Loc = Utils.Do(LandingDst1.Location, LandingDst2.Location, LandingDst3.Location)
	local group = Utils.Do(SeaAttackGroup1, SeaAttackGroup2, SeaAttackGroup3)
	
	local units = Reinforcements.ReinforceWithTransport(enemy, Chopper, units, ChopperPath, Entry.Location )
	
	Utils.Do(units, function(u)
		if not u.IsDead then
			Trigger.OnAddedToWorld(u, function()
				IdleHunt(u)
			end)
		end
	end)
end

BuildBase = function(buildings, fact)
	for i,v in ipairs(buildings) do
		if not v.exists then
			
			BuildBuilding(v, fact, buildings)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		BuildBase(buildings, fact)
	end)
end

BuildBuilding = function(building, fact, allbuildings)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if fact.IsDead or fact.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = fact.Location+CVec.New(1,1) + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), function()
			BuildBase(allbuildings, fact)
		end)
	end)
end

ProduceInfantry = function()
	
end

ProduceArmor = function()
	
end

ProduceCruiser = function()

end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end