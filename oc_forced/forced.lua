if Map.LobbyOption("difficulty") == "easy" then
	EvacueesRemaining = false
	PlayerCash = 5000
	MainCivMax = 80
	ExtraCivMax = 40
	
	RemoveHelpersUnits = false
	RemoveHelpersBuildings = false
elseif Map.LobbyOption("difficulty") == "normal" then
	EvacueesRemaining = true
	PlayerCash = 4000
	MainCivMax = 100	--100
	ExtraCivMax = 50	--50
	
	RemoveHelpersUnits = true
	RemoveHelpersBuildings = false
else
	EvacueesRemaining = true
	PlayerCash = 3000
	
	MainCivMax = 125
	ExtraCivMax = 75
	
	RemoveHelpersUnits = true
	RemoveHelpersBuildings = true
end

CivilianUI = 0
CivCount = 0
MainCivBatchEvacuated = false

civilianBuildings = 
{ 
civbuild1, civbuild2, civbuild3, civbuild4, civbuild5, civbuild6, civbuild7, civbuild8, civbuild9, 
civbuild10, civbuild11, civbuild12, civbuild13, civbuild14, civbuild15, civbuild16, civbuild17,
civbuild18, civbuild19, civbuild20, civbuild21, civbuild22, civbuild23, civbuild24, civbuild25,
civbuild26, civbuild27, civbuild28
}

LSTEntryPoints = { LstEntry1.Location, LstEntry2.Location, LstEntry3.Location }

RemoveOnHardUnits = 
	{
	HelpingUnit1, HelpingUnit2, HelpingUnit3, HelpingUnit4, HelpingUnit5, 
	HelpingUnit6, HelpingUnit7, HelpingUnit8, HelpingUnit9, HelpingUnit10, 
	HelpingUnit10, HelpingUnit11, HelpingUnit12, HelpingUnit13, HelpingUnit14,
	}
	
RemoveOnHardBuildings =
	{
	HelpingPBox1, HelpingPBox2
	}

Airfields = {AField1, AField2, AField3}
SovietInitPlanes = {Yak1, Yak2, Mig1}

AirfieldsDestroyed = false

Flavor = function()
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		local landings = Airfields
		local aircrafts = SovietInitPlanes
		
		for i = 1, #aircrafts, 1 do
			aircrafts[i].ReturnToBase(landings[i])
		end
	end)
end

Trigger.OnAllKilled(civilianBuildings, function()
	if MainCivBatchEvacuated == false then
		enemy.MarkCompletedObjective(DenyAllies)
	end
end)

SetCivBuildingToUse = function()
	local civBuild = Utils.Random(civilianBuildings)
	
	if not civBuild.IsDead then
		SendEvacuationLST(civBuild)
	else
		SetCivBuildingToUse()
	end
end

SendEvacuationLST = function(building)

	local entryPoint = Utils.Random(LSTEntryPoints)
	local lst = Reinforcements.ReinforceWithTransport(england, "lst.extraction", nil, { entryPoint, LstDst.Location})[1]
	
	local civilians = Reinforcements.Reinforce(england, {"c1","c2","c3","c4"}, {building.Location, building.Location+CVec.New(-1,1)},10)
	
	Utils.Do(civilians, function(civ)
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			if not civ.IsDead then
				civ.Move(CivWaypoint1.Location)
				civ.Move(CivsDst.Location)
				Trigger.OnIdle(civ, function()
					if not civ.IsDead then
						civ.EnterTransport(lst)
					end
				end)
				Trigger.OnRemovedFromWorld(civ, function()
					CivCount = CivCount + 1
				end)
			end
		end)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		if not lst.IsDead then
			lst.Move(entryPoint)
			lst.Destroy()
		end
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(45), function()
		SetCivBuildingToUse()
	end)
end

Trigger.OnAllKilled(Airfields, function()
	player.MarkCompletedObjective(DestroyAirfields)
	if AirfieldsDestroyed == false then
		AirAttacks = false
		AirfieldsDestroyed = true
	end
end)

StartEvacuation = function()
	Media.DisplayMessage("Start the evacuation.","Command")
	SetCivBuildingToUse()
	
	Trigger.AfterDelay(DateTime.Seconds(3), function()
		CivilianUI = 1
	end)
end

CombatCivilians = function()
	Media.DisplayMessage("We are ready to fight commander.","Civilian")
end

RunAI = function()
	enemy.Cash = 15000
	BuildBase()
	
	ParatroopersControl()

	Trigger.AfterDelay(DateTime.Minutes(2), function()
		OutOfMapAtks(OoMMainBattleGroup, OoMBadGuyAttackPath,OoMMainDelay)
	end)
	
	Trigger.AfterDelay(DateTime.Minutes(10), function()
		OutOfMapAtks(OoMExtraBattleGroup, MainBadGuyAttackPath,OoMExtraDelay)
	end)
	
	Trigger.AfterDelay(MainAttacksDelay, function()
		ProduceInfantry(enemy,MainUSSRAttackPath,InfCount)
		ProduceArmor(enemy,MainUSSRAttackPath,TankCount)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(60), function()
		GiveCashToAI()
	end)
end

GiveCashToAI = function()
	Trigger.AfterDelay(DateTime.Seconds(60), function()
		GiveCashToAI()
	end)
end

Tick = function()
	if CivilianUI == 1 then
		UserInterface.SetMissionText("Civilians evacuated: " .. CivCount, MessageColor)
	end
	
	if player.HasNoRequiredUnits() then
		if MainCivBatchEvacuated == false then
			enemy.MarkCompletedObjective(DenyAllies)
		else
			if EvacueesRemaining == false then
				player.MarkFailedObjective(EvacuateMore)
				if not AirfieldsDestroyed then
					player.MarkFailedObjective(DestroyAirfields)
				end
			end
			player.MarkCompletedObjective(LastStand)
		end
	end

	if CivCount >= MainCivMax then
		if EvacueesRemaining == true then
			EvacueesRemaining = false
			MainCivBatchEvacuated = true
			MainCivMax = MainCivMax + ExtraCivMax
			EvacuateMore = player.AddSecondaryObjective("Evacuate more civilians.")
			LastStand = player.AddPrimaryObjective("Defend the evacuation process for as long as you can.")
			player.MarkCompletedObjective(ProtectCivs)
		else
			if MainCivBatchEvacuated == true then
				player.MarkCompletedObjective(EvacuateMore)
				player.MarkCompletedObjective(LastStand)
			end
			player.MarkCompletedObjective(ProtectCivs)
		end
	end
end

InitTriggers = function()
	player.Cash = PlayerCash
	Camera.Position = StartingCamera.CenterPosition

	RunAI()

	Flavor()
	
	Trigger.AfterDelay(DateTime.Seconds(5), StartEvacuation)
	
	if RemoveHelpersUnits == true then
		Utils.Do(RemoveOnHardUnits, function(u)
			u.Destroy()
		end)
	end
	
	if RemoveHelpersBuildings == true then
		Utils.Do(RemoveOnHardBuildings, function(u)
			u.Destroy()
		end)
	end
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	ProtectCivs = player.AddPrimaryObjective("Protect the civilian evacuation.")
	DestroyAirfields = player.AddSecondaryObjective("Destroy the airfields to prevent air attacks.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the Allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)

end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	badguy = Player.GetPlayer("BadGuy")
	england = Player.GetPlayer("England")
	
	MessageColor = HSLColor.White
	
	InitTriggers()
	InitObjectives()
end