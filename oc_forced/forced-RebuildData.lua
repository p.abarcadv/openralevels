
Power1 = {type = "powr", pos = CVec.New(-3,-1), cost = 500, exists = true}
Power2 = {type = "apwr", pos = CVec.New(-6,-1), cost = 500, exists = true}
Power3 = {type = "apwr", pos = CVec.New(-4,-4), cost = 500, exists = true}
Power4 = {type = "apwr", pos = CVec.New(-1,-4), cost = 500, exists = true}
Power5 = {type = "apwr", pos = CVec.New(2,0), cost = 500, exists = true}

Kenn = {type = "kenn", pos = CVec.New(1,3), cost = 150, exists = true}

Barr = {type = "barr", pos = CVec.New(2,3), cost = 500, exists = true}
Weap = {type = "weap", pos = CVec.New(6,-1), cost = 2000, exists = true}


Ref1 = {type = "proc", pos = CVec.New(-4,11), cost = 1500, exists = true}
Ref2 = {type = "proc", pos = CVec.New(0,10), cost = 1500, exists = true}

Silo1 = {type = "silo", pos = CVec.New(-2,11), cost = 150, exists = true}
Silo2 = {type = "silo", pos = CVec.New(2,10), cost = 150, exists = true}
Silo3 = {type = "silo", pos = CVec.New(-5,16), cost = 150, exists = true}
Silo4 = {type = "silo", pos = CVec.New(-4,16), cost = 150, exists = true}

Dome = {type = "dome", pos = CVec.New(2,-3), cost = 1000, exists = true}

Ftur = {type = "ftur", pos = CVec.New(4,6), cost = 500, exists = true}

Tesla1 = {type = "tsla", pos = CVec.New(0,6), cost = 1000, exists = true}
Tesla2 = {type = "tsla", pos = CVec.New(11,0), cost = 1000, exists = true}

Sam1 = {type = "sam", pos = CVec.New(1,-5), cost = 1000, exists = true}
Sam2 = {type = "sam", pos = CVec.New(9,-1), cost = 1000, exists = true}
Sam3 = {type = "sam", pos = CVec.New(-3,8), cost = 1000, exists = true}

Fix = {type = "fix", pos = CVec.New(-3,5), cost = 1000, exists = true}

STek = {type = "stek", pos = CVec.New(-5,2), cost = 1000, exists = true}

BaseBuildings =
	{
		Power1,
		Barr,
		Ref1,
		Weap,
		Power3,
		Power2,
		Ftur,
		Ref2,
		Power4,
		Dome,
		Tesla1,
		Power5,
		Sam1,
		Kenn,
		Fix,
		Tesla2,
		Sam2,
		Sam3,
		STek,
		Silo1,
		Silo2,
		Silo3,
		Silo4
	}

Trigger.OnKilled(BasePower1, function()
	Power1.exists = false
end)

Trigger.OnKilled(BasePower2, function()
	Power2.exists = false
end)

Trigger.OnKilled(BasePower3, function()
	Power3.exists = false
end)

Trigger.OnKilled(BasePower4, function()
	Power4.exists = false
end)

Trigger.OnKilled(BasePower5, function()
	Power5.exists = false
end)

Trigger.OnKilled(BaseWeap, function()
	Weap.exists = false
end)

Trigger.OnKilled(BaseBarr, function()
	Barr.exists = false
end)

Trigger.OnKilled(BaseKenn, function()
	Kenn.exists = false
end)

Trigger.OnKilled(BaseTesla1, function()
	Tesla1.exists = false
end)

Trigger.OnKilled(BaseTesla2, function()
	Tesla2.exists = false
end)

Trigger.OnKilled(BaseFtur, function()
	Ftur.exists = false
end)

Trigger.OnKilled(BaseSam1, function()
	Sam1.exists = false
end)

Trigger.OnKilled(BaseSam2, function()
	Sam2.exists = false
end)

Trigger.OnKilled(BaseSam3, function()
	Sam3.exists = false
end)

Trigger.OnKilled(BaseFix, function()
	Fix.exists = false
end)

Trigger.OnKilled(BaseRef1, function()
	Ref1.exists = false
end)

Trigger.OnKilled(BaseRef2, function()
	Ref2.exists = false
end)

Trigger.OnKilled(BaseSilo1, function()
	Silo1.exists = false
end)

Trigger.OnKilled(BaseSilo2, function()
	Silo2.exists = false
end)

Trigger.OnKilled(BaseSilo3, function()
	Silo3.exists = false
end)

Trigger.OnKilled(BaseSilo4, function()
	Silo4.exists = false
end)

Trigger.OnKilled(BaseDome, function()
	Dome.exists = false
end)

Trigger.OnKilled(BaseStek, function()
	STek.exists = false
end)
