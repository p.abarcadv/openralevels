if Map.LobbyOption("difficulty") == "easy" then
	ParatroopDelay = DateTime.Minutes(3)
	MainAttacksDelay = DateTime.Minutes(10)
	BigWaveCount = 1
	
	OoMMainDelay = DateTime.Minutes(3)
	OoMExtraDelay = DateTime.Minutes(5)
elseif Map.LobbyOption("difficulty") == "normal" then
	ParatroopDelay = DateTime.Minutes(2)
	MainAttacksDelay = DateTime.Minutes(8.5)
	BigWaveCount = 2
	
	OoMMainDelay = DateTime.Minutes(2)
	OoMExtraDelay = DateTime.Minutes(4)
else
	ParatroopDelay = DateTime.Minutes(2)
	MainAttacksDelay = DateTime.Minutes(7)
	BigWaveCount = 2
		
	OoMMainDelay = DateTime.Minutes(2)
	OoMExtraDelay = DateTime.Minutes(4)
end

--USSR
SovietInfantryTypes = {"e1", "e1", "e2", "e4"}
InfCount = 9
InfAtk = {}

SovietVehicleTypes = {"3tnk", "3tnk", "3tnk", "ftrk", "v2rl", "v2rl", "ttnk", "4tnk"}
TankCount = 5
TankAtk = {}

--BadGuy
BadGuyInfantryTypes = {"e1", "e1", "e2", "e4"}
BadGuyInfCount = 6
BadGuyInfAtk = {}

BadGuyVehicleTypes = {"3tnk", "ftrk","v2rl"}
BadGuyTankCount = 3
BadGuyTankAtk = {}

OoMMainBattleGroup = {"3tnk","v2rl", "e1","e1","e2"}

OoMExtraBattleGroup = {"3tnk", "3tnk", "v2rl", "e1","e1","e2","e2"}

MainUSSRAttackPath = 
{
{MainAttackPath1.Location,MainAttackPath2.Location,MainAttackPathDst.Location}
}

MainBadGuyAttackPath = 
{
{TopBaseAtkPathEntry.Location, TopBaseAtkPath.Location},
{BotBaseAtkPathEntry.Location, BotBaseAtkPath.Location},
{TopBaseAtkPathEntry.Location, --[[TopBaseAtkPath.Location,]] AtkTopPath1.Location},
{BotBaseAtkPathEntry.Location, --[[BotBaseAtkPath.Location,]] AtkBotPath1.Location}
}

OoMBadGuyAttackPath = 
{
{EntryAtkTop.Location,AtkTopPath1.Location},
{EntryAtkTop.Location,AtkTopPath2.Location},
{EntryAtkBot.Location,AtkBotPath1.Location},
{EntryAtkBot.Location,AtkBotPath2.Location}
}

AirAttacks = true
ParatroopDestinies = {paratrooplz1, paratrooplz2, paratrooplz3, paratrooplz4}

DecreaseAttackDelay = function()
	
	OoMExtraDelay = DateTime.Seconds(1)
	OoMEMainDelay = DateTime.Seconds(1)
end

ParatroopersControl = function()
	Trigger.AfterDelay(ParatroopDelay, function()
		ConstantSingleParatroopers()
	end)
	Trigger.AfterDelay(DateTime.Minutes(5), function()
		ConstantSingleShocktroopers()
	end)
	Trigger.AfterDelay(DateTime.Minutes(15), function()
		GreatParatroopersAttack()
	end)
end

GreatParatroopersAttack = function()
	if AirAttacks == true then
		
		for x=1,BigWaveCount,1 do
			Trigger.AfterDelay(DateTime.Seconds(2*x), function()
				for i=1, 4,1 do
					local xPos = Utils.RandomInteger(-3,3)
					local yPos = Utils.RandomInteger(-3,3)
					local finalPos = ParatroopDestinies[i].CenterPosition + WVec.New(xPos * 1024, yPos * 1024, 0)
					
					Trigger.AfterDelay(DateTime.Seconds(2*i), function()
						SendParatroopers("normal",finalPos)
					end)
				end
			end)
		end
	
		Trigger.AfterDelay(DateTime.Minutes(7), function()
			GreatParatroopersAttack()
		end)
	end
end

ConstantSingleParatroopers = function()
	if AirAttacks == true then
		SendParatroopers("normal")
	end
	Trigger.AfterDelay(ParatroopDelay, function()
		ConstantSingleParatroopers()
	end)
end

ConstantSingleShocktroopers = function()
	if AirAttacks == true then
		SendParatroopers("shock",paratroopersLZBase.CenterPosition)
	end
	Trigger.AfterDelay(DateTime.Minutes(5), function()
		ConstantSingleParatroopers()
	end)
end

SendParatroopers = function(atktype,dst)
	if AirAttacks == true then

		if not dst then
			dst = (Utils.Random(ParatroopDestinies)).CenterPosition
		end
		
		if atktype == "shock" then
			PowerProxy = Actor.Create("powerproxy.parashocktroopers", false, { Owner = enemy })
		else
			PowerProxy = Actor.Create("powerproxy.paratroopers", false, { Owner = enemy })
		end
		
		local aircraft = PowerProxy.TargetParatroopers(dst, Angle.SouthWest)
		
		Utils.Do(aircraft, function(a)
			Trigger.OnPassengerExited(a, function(t, p)
				IdleHunt(p)
			end)
		end)

	else
		return
	end
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if BaseFact.IsDead or BaseFact.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = CYardCenterLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
				
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function(owner,paths,amount)
	if owner == enemy then
		if not ( Barr.exists or Barr.Owner ~= owner ) then
		--[[	return
		elseif baseharv.IsDead and enemy.Resources <= 299 then]]
			return
		end
	else
		--if not ( TopBarr.IsDead or TopBarr.Owner ~= owner ) or not ( BotBarr.IsDead or BotBarr.Owner ~= owner )) then 
		
		--end
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(SovietInfantryTypes) }
	local Path = Utils.Random(paths)
	enemy.Build(toBuild, function(unit)
		
		InfAtk[#InfAtk + 1] = unit[1]
		if #InfAtk >= amount then
			SendUnits(InfAtk, Path)
			InfAtk = { }
			Trigger.AfterDelay(DateTime.Seconds(30), function()
				ProduceInfantry(enemy,MainUSSRAttackPath,InfCount)
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceInfantry(enemy,MainUSSRAttackPath,InfCount)
			end)
		end
	end)
end

ProduceArmor = function(owner,paths,amount)
	if not (Weap.exists or Weap.Owner ~= enemy) then
		return
	--elseif baseharv.IsDead and enemy.Resources <= 599 then
	--	return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(SovietVehicleTypes) }
	local Path = Utils.Random(paths)
	enemy.Build(toBuild, function(unit)
		TankAtk[#TankAtk + 1] = unit[1]

		if #TankAtk >= amount then
			SendUnits(TankAtk, Path)
			TankAtk = { }
			Trigger.AfterDelay(DateTime.Minutes(1), function()
				ProduceArmor(enemy,MainUSSRAttackPath,TankCount)
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceArmor(enemy,MainUSSRAttackPath,TankCount)
			end)
		end
	end)
end

ProduceAircraft = function(owner,paths,amount)
	if not (Afld1 or Afld1.Owner ~= enemy) and (Afld2 or Afld2.Owner ~= enemy) and (Afld3 or Afld3.Owner ~= enemy) then
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(SovietVehicleTypes) }
	local Path = Utils.Random(paths)
	enemy.Build(toBuild, function(unit)
		TankAtk[#TankAtk + 1] = unit[1]

		if #TankAtk >= amount then
			SendUnits(TankAtk, Path)
			TankAtk = { }
			Trigger.AfterDelay(DateTime.Minutes(1), function()
				ProduceArmor(enemy,MainUSSRAttackPath,TankCount)
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceArmor(enemy,MainUSSRAttackPath,TankCount)
			end)
		end
	end)
end

OutOfMapAtks = function(group, waypoints, delay)
	
	local path = Utils.Random(waypoints)
	local units = Reinforcements.Reinforce(badguy, group, path, 10)
	
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		Utils.Do(units, function(u)
			if not u.IsDead then
				IdleHunt(u)
			end
		end)
	end)
	
	Trigger.AfterDelay(delay, function()
		OutOfMapAtks(group, waypoints, delay)
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint)
			end)
			IdleHunt(unit)
		end
	end)
end

IdleHunt = function(unit) if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end end