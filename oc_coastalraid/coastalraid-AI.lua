EnemyCompositions = {Comp1,Comp2,Compe3}
Comp1Inf= {"e1","e1","e3"}
Comp1Tank= {"2tnk","jeep"}
Comp2Inf= {"e1","e1","e1","e3","e3"}
Comp2Tank= {"2tnk","jeep"}
Comp3Inf= {"e1","e1","e3","e3","jeep","jeep"}
Comp3Tank= {"2tnk","2tnk"}
AICash = 100000

--Patrol1Group = { "jeep", "jeep", "2tnk", "2tnk" }
--Patrol2Group = { "jeep", "1tnk", "1tnk", "1tnk" }
AlliedInfantryTypes = { "e1", "e3" }
AlliedArmorTypes = { "jeep", "jeep", "2tnk", "2tnk", "1tnk", "1tnk", "1tnk", "arty" }

InfAttack = { }
ArmorAttack = { }

--SovietStartToBasePath = { StartPoint.Location, SovietBasePoint.Location }
--InfReinfPath = { SWRoadPoint.Location, InVillagePoint.Location }
--ArmorReinfPath = { NRoadPoint.Location, CrossroadsNorthPoint.Location }
--Patrol1Path = { NearRadarPoint.Location, ToRadarPoint.Location, InVillagePoint.Location, ToRadarPoint.Location }
--Patrol2Path = { BridgeEntrancePoint.Location, NERoadTurnPoint.Location, CrossroadsEastPoint.Location, BridgeEntrancePoint.Location }


if Map.LobbyOption("difficulty") == "easy" then
    AlliedArmorNumbers = 5
    AlliedInfantryNumbers = 6
    --ProductionInterval = 
elseif Map.LobbyOption("difficulty") == "normal" then
	AlliedInfantryNumbers = 8
    AlliedArmorNumbers = 6
    --ProductionInterval = 
else
    AlliedInfantryNumbers = 10
    AlliedArmorNumbers = 7
    --ProductionInterval = 
end

AttackPaths = 
{
	{ attackPath1, attackPath2},
	{ attackPath3, attackPath4}
}

ExtraAttackPaths = 
{
	{ attackPath1, attackPath2}
}

IdleHunt = function(unit) if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end end

IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			--Media.PlaySoundNotification(player, "AlertBuzzer")
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

	
BuildBuilding = function(building)
	--Media.PlaySpeechNotification(player, "ReinforcementsArrived") 
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if EBaseFact.IsDead or EBaseFact.Owner ~= enemy then
			--Media.PlaySpeechNotification(player, "ReinforcementsArrived") 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = EBaseFactLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
				
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function()
	if not Tent1.exists and not Tent2.exists then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		return
	end

	
	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(AlliedInfantryTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		--Media.PlaySoundNotification(player, "AlertBuzzer")
		InfAttack[#InfAttack + 1] = unit[1]
		if #InfAttack >= 10 then
			--Media.PlaySpeechNotification(player, "ReinforcementsArrived") 
			SendUnits(InfAttack, Path)
			InfAttack = { }
			Trigger.AfterDelay(DateTime.Seconds(30), ProduceInfantry)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceArmor = function()
	if not Weap.exists then
		return
	--elseif baseharv.IsDead and enemy.Resources <= 599 then
	--	return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(AlliedArmorTypes) }
	local Path = Utils.Random(AttackPaths)
	enemy.Build(toBuild, function(unit)
		ArmorAttack[#ArmorAttack + 1] = unit[1]

		if #ArmorAttack >= AlliedArmorNumbers then
			SendUnits(ArmorAttack, Path)
			ArmorAttack = { }
			Trigger.AfterDelay(DateTime.Minutes(1), ProduceArmor)
		else
			Trigger.AfterDelay(delay, ProduceArmor)
		end
	end)
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

--Trigger.AfterDelay(DateTime.Seconds(5), BuildBase) end