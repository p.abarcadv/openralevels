
if Map.LobbyOption("difficulty") == "easy" then
	playerInitialCash = 5000
elseif Map.LobbyOption("difficulty") == "normal" then
    playerInitialCash = 4000
else
    playerInitialCash = 3000
end

CondDefeat = false

BeachTurrets = {beachTurr1,beachTurr2,beachTurr3,beachTurr4,beachTurr5,beachTurr6,beachTurr7}
EnemyBase = 
	
	{
	EBasePwr1,EBasePwr2,EBasePwr3,EBasePwr4,EBasePwr5,EBasePwr6,EBasePwr7,EBasePwr8,EBasePwr9,
	EBaseTent1,EBaseTent2,EBaseWeap,EBaseDome,EBaseFact,EBaseRef,EBaseNaval
	--EExtraBasePwr1,EExtraBasePwr2,EExtraBaseBarr
	}

InsertionTransport = "lst.in"

SovietReinforcements1 = { "e1", "e1", "e1", "e2", "e2" }
SovietReinforcements2 = { "3tnk", "3tnk", "v2rl", "mcv" }
SovietReinforcements3 = { "e1", "e1", "e1", "e2", "e2" }
SovietReinforcements4 = { "3tnk", "3tnk", "e1", "e1", "e2" }

SovietReinforcements1Waypoints = {reinforcespawn1.Location, reinforcewaypoint1.Location}
SovietReinforcements2Waypoints = {reinforcespawn2.Location, reinforcewaypoint2.Location}
SovietReinforcements3Waypoints = {reinforcespawn3.Location, reinforcewaypoint3.Location}

RunInitialActivities = function()
	--baseharv.FindResources()
	IdlingUnits()
	Trigger.AfterDelay(10, function()
		BuildBase()
	end)

	Utils.Do(Map.NamedActors, function(actor)
		if actor.Owner == enemy and actor.HasProperty("StartBuildingRepairs") then
			Trigger.OnDamaged(actor, function(building)
				if building.Owner == enemy and building.Health < 3/4 * building.MaxHealth then
					building.StartBuildingRepairs()
				end
			end)
		end
	end)
	

	Trigger.AfterDelay(DateTime.Seconds(1), ProduceInfantry)
	Trigger.AfterDelay(DateTime.Seconds(2), ProduceArmor)
	
end

Trigger.OnCapture(EBaseAtek, function()
    player.MarkCompletedObjective(CaptureTech)
end)

Trigger.OnKilled(EBaseAtek, function()
    player.MarkFailedObjective(CaptureTech)
end)

Trigger.AfterDelay(DateTime.Seconds(10), function() 
	CondDefeat = true  
end)


SendAdditionalReinforcements = function()
	Reinforcements.ReinforceWithTransport(player, InsertionTransport, SovietReinforcements4, SovietReinforcements2Waypoints, { SovietReinforcements2Waypoints[1]})
	Media.PlaySpeechNotification(player, "ReinforcementsArrived") 
end

Trigger.OnAllKilled(BeachTurrets, function() 
	player.MarkCompletedObjective(BeachTurrs)
	SendAdditionalReinforcements()
end)

--[[
Trigger.OnAllKilled(EnemyBase, function() 
	player.MarkCompletedObjective(DestroyAllies)
end)
]]


Tick = function()
	
	if enemy.Resources >= enemy.ResourceCapacity * 0.75 then
		enemy.Cash = enemy.Cash + enemy.Resources - enemy.ResourceCapacity * 0.25
		enemy.Resources = enemy.ResourceCapacity * 0.25
	end
	
	
	if CondDefeat == true then
		if player.HasNoRequiredUnits() then
			enemy.MarkCompletedObjective(DestroySoviets)
		end
	end
	
	if enemy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(DestroyAllies)
	end
end

InitTriggers = function()
	player.Cash = playerInitialCash
    
    Camera.Position = initial_camera.CenterPosition
	
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		Reinforcements.ReinforceWithTransport(player, InsertionTransport, SovietReinforcements1, SovietReinforcements1Waypoints, { SovietReinforcements1Waypoints[1] })
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	end)
	Trigger.AfterDelay(DateTime.Seconds(20), function()
		Reinforcements.ReinforceWithTransport(player, InsertionTransport, SovietReinforcements2, SovietReinforcements2Waypoints, { SovietReinforcements2Waypoints[1] })
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	end)
	Trigger.AfterDelay(DateTime.Seconds(15), function()
		Reinforcements.ReinforceWithTransport(player, InsertionTransport, SovietReinforcements3, SovietReinforcements3Waypoints, { SovietReinforcements3Waypoints[1] })
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	end)
	
end 

InitObjectives = function()
	DestroyAllies = player.AddPrimaryObjective("Destroy the enemy base.")
	BeachTurrs = player.AddSecondaryObjective("Clear the beach defences.")
	CaptureTech = player.AddSecondaryObjective("Find and capture allied tech center.")
	DestroySoviets = enemy.AddPrimaryObjective("Destroy the invaders.")
	
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionFailed")
		end)
	end)
	Trigger.OnPlayerWon(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionAccomplished")
		end)
	end)

end


WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	
	InitObjectives()
	InitTriggers()
	
	enemy.Cash = 10000
	
	RunInitialActivities()
	
	

end