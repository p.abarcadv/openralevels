--POWER
Pwr1 =	{ type = "apwr", pos = CVec.New(-9, 4), cost = 500, exists = true}
Pwr2 =	{ type = "powr", pos = CVec.New(-6, 3), cost = 300, exists = true }
Pwr3 =	{ type = "apwr", pos = CVec.New(-4, 3), cost = 500, exists = true }
Pwr4 =	{ type = "powr", pos = CVec.New(-5, 0), cost = 300, exists = true }
Pwr5 =	{ type = "apwr", pos = CVec.New(-5,-3), cost = 500, exists = true }
Pwr6 =	{ type = "powr", pos = CVec.New(-7, -5), cost = 300, exists = true }
Pwr7 =	{ type = "apwr", pos = CVec.New(-5, -6), cost = 500, exists = true }
Pwr8 =	{ type = "apwr", pos = CVec.New(8, 1), cost = 500, exists = true }
Pwr9 =	{ type = "apwr", pos = CVec.New(10, 4), cost = 500, exists = true }
--PRODUCTION
Tent1 = { type = "tent", pos = CVec.New(-11, 10), cost = 500, exists = true }
Tent2 = { type = "tent", pos = CVec.New(8, 3), cost = 500, exists = true }
Ref =  { type = "proc", pos = CVec.New(5, -2), cost = 1400, exists = true }
Weap = { type = "weap", pos = CVec.New(3, 4), cost = 2000, exists = true }
--G. DEFENCES
Gun1 = { type = "gun", pos = CVec.New(-17, 14), cost = 800, exists = true }
Pbox1 = { type = "pbox", pos = CVec.New(-16, 14), cost = 600, exists = true }
Gun2 = { type = "gun", pos = CVec.New(-11, 15), cost = 800, exists = true }
Pbox2 = { type = "pbox", pos = CVec.New(-12, 15), cost = 600, exists = true }

Gun3 = { type = "gun", pos = CVec.New(15, 10), cost = 800, exists = true }
Pbox5 = { type = "pbox", pos = CVec.New(16, 10), cost = 600, exists = true }
Gun4 = { type = "gun", pos = CVec.New(15, 6), cost = 800, exists = true }
Pbox6 = { type = "pbox", pos = CVec.New(16, 6), cost = 600, exists = true }
Gun5 = { type = "gun", pos = CVec.New(-12, 0), cost = 800, exists = true }

--G. DEFENCES 2
Pbox3 = { type = "pbox", pos = CVec.New(-11, 5), cost = 600, exists = true }
Pbox4 = { type = "pbox", pos = CVec.New(4, 9), cost = 600, exists = true }
Hbox = { type = "hbox", pos = CVec.New(0, 2), cost = 750, exists = true }
--A. DEFENCES
AA1 = { type = "agun", pos = CVec.New(-19, 13), cost = 800, exists = true }
AA2 = { type = "agun", pos = CVec.New(-9, 14), cost = 800, exists = true }
AA3 = { type = "agun", pos = CVec.New(-2, 7), cost = 800, exists = true }
AA4 = { type = "agun", pos = CVec.New(1, -4), cost = 800, exists = true }
AA5 = { type = "agun", pos = CVec.New(-10,-1), cost = 800, exists = true }
AA6 = { type = "agun", pos = CVec.New(12,8), cost = 800, exists = true }
--EXTRAS
Dome = { type = "dome", pos = CVec.New(-15, 3), cost = 1400, exists = true }
Silo1 = { type = "silo", pos = CVec.New(4, -2), cost = 150, exists = true }
Silo2 = { type = "silo", pos = CVec.New(5, -3), cost = 150, exists = true }
Fix = { type = "fix", pos = CVec.New(0, -8), cost = 1200, exists = true}

BaseBuildings =
{
	Ref,
	Tent1,
	Weap,
    Fix,
    Dome,
    Tent2,
	Pwr1,
	Pwr2,
	Pwr3,
	Pwr4,
	Pwr5,
	Pwr6,
	Pwr7,
    Pwr8,
    Pwr9,
	Gun1,
	Gun2,
    Gun3,
    Gun4,
    Gun5,
    Pbox1,
	Pbox2,
	Pbox3,
	Pbox4,
    Pbox5,
	Pbox6,
	AA1,
	AA2,
	AA3,
	AA4,
	AA5,
    AA6,
	Silo1,
	Silo2
}

-----------------------------------------------------------------------------
--ON KILLED STRUCTURES
--POWER

Trigger.OnKilled(EBasePwr1, function(building)
    Pwr1.exists = false
end)

Trigger.OnKilled(EBasePwr2, function(building)
    Pwr2.exists = false
end)

Trigger.OnKilled(EBasePwr3, function(building)
    Pwr3.exists = false
end)
Trigger.OnKilled(EBasePwr4, function(building)
    Pwr4.exists = false
end)

Trigger.OnKilled(EBasePwr5, function(building)
    Pwr5.exists = false
end)

Trigger.OnKilled(EBasePwr6, function(building)
    Pwr6.exists = false
end)

Trigger.OnKilled(EBasePwr7, function(building)
    Pwr7.exists = false
end)

Trigger.OnKilled(EBasePwr8, function(building)
    Pwr8.exists = false
end)

Trigger.OnKilled(EBasePwr9, function(building)
    Pwr9.exists = false
end)

Trigger.OnKilled(EBaseTent1, function(building)
    Tent1.exists = false
end)

Trigger.OnKilled(EBaseTent2, function(building)
    Tent2.exists = false
end)

Trigger.OnKilled(EBaseRef, function(building)
    Ref.exists = false
end)

Trigger.OnKilled(EBaseWeap, function(building)
    Weap.exists = false
end)

Trigger.OnKilled(EBaseSilo1, function(building)
    Silo1.exists = false
end)

Trigger.OnKilled(EBaseSilo2, function(building)
    Silo2.exists = false
end)

Trigger.OnKilled(EBaseDome, function(building)
    Dome.exists = false
end)

Trigger.OnKilled(EBaseGun1, function(building)
    Gun1.exists = false
end)

Trigger.OnKilled(EBaseGun2, function(building)
    Gun2.exists = false
end)

Trigger.OnKilled(EBaseGun3, function(building)
    Gun3.exists = false
end)

Trigger.OnKilled(EBaseGun4, function(building)
    Gun4.exists = false
end)

Trigger.OnKilled(EBaseGun5, function(building)
    Gun5.exists = false
end)

Trigger.OnKilled(EBasePbox1, function(building)
    Pbox1.exists = false
end)

Trigger.OnKilled(EBasePbox2, function(building)
    Pbox2.exists = false
end)

Trigger.OnKilled(EBasePbox3, function(building)
    Pbox3.exists = false
end)

Trigger.OnKilled(EBasePbox4, function(building)
    Pbox4.exists = false
end)

Trigger.OnKilled(EBasePbox5, function(building)
    Pbox5.exists = false
end)

Trigger.OnKilled(EBasePbox6, function(building)
    Pbox6.exists = false
end)

Trigger.OnKilled(EBaseHbox, function(building)
    Hbox.exists = false
end)


Trigger.OnKilled(EBaseAA1, function(building)
    AA1.exists = false
end)

Trigger.OnKilled(EBaseAA2, function(building)
    AA2.exists = false
end)

Trigger.OnKilled(EBaseAA3, function(building)
    AA3.exists = false
end)

Trigger.OnKilled(EBaseAA4, function(building)
    AA4.exists = false
end)

Trigger.OnKilled(EBaseAA5, function(building)
    AA5.exists = false
end)

Trigger.OnKilled(EBaseAA6, function(building)
    AA6.exists = false
end)

Trigger.OnKilled(EBaseFix, function(building)
    Fix.exists = false
end)
