if Map.LobbyOption("difficulty") == "easy" then
	AdditionalUnit	 = {"2tnk"}
	ApcCombatType = {"apc.combat"}
	ApcSupportType = {"apc.support"}
	CivilianAidCoordsAvailable = 2
	EnemyReinforcements = 0
	EnemyReinforceDelay = DateTime.Minutes(6)
	StormClearTime = {DateTime.Minutes(16), DateTime.Minutes(18)}
	MessageToDisplay = "I'll show you where are the soviet air bases."
elseif Map.LobbyOption("difficulty") == "normal" then
	AdditionalUnit	 = {"1tnk"}
	ApcCombatType = {"apc.combat"}
	ApcSupportType = {"apc.support"}
	CreateTesla1 = true
	CivilianAidCoordsAvailable = 1
	EnemyReinforcements = 2
	EnemyReinforceDelay = DateTime.Minutes(6)
	StormClearTime = {DateTime.Minutes(13), DateTime.Minutes(15)}
	MessageToDisplay = "I'll show you one of the soviet air bases."
else
	AdditionalUnit	 = {}
	ApcCombatType = {"apc.combathard"}
	ApcSupportType = {"apc.supporthard"}
	CreateTesla1 = true
	CreateTesla2 = true
	CivilianAidCoordsAvailable = 0
	EnemyReinforcements = 4
	EnemyReinforceDelay = DateTime.Minutes(5)
	StormClearTime = {DateTime.Minutes(10), DateTime.Minutes(12)}
	MessageToDisplay = ""
end

ActualStormClearTime = {}
StormCleared = false

TransitionInEffect = false
ThunderActive = true
RedTransition = 0.00025
GreenTransition = 0.00015
BlueTransition = -0.0005
AmbientTransition = 0.00055
transition = 0	

civAlert = false
CivilianAidCoords = { CPos.New(79,98), CPos.New(80,98), CPos.New(81,98) }

LstInsertion = "lst.in"

SovAirBuildings = {Afld1, Afld2, Afld3, Afld4, Afld5, Afld6, Afld7, HPad1, HPad2, HPad3, HPad4, HPad5, HPad6 }
SovAir = { Plane1, Plane2, Plane3, Plane4, Plane5, Plane6, Plane7, Hind1, Hind2, Hind3, Hind4, Hind5, Hind6 }

AttackAircraftTargets = { }

SovUnitsEntry = { OoMUnitsEntry.Location, OoMUnitsDst.Location }

SovReinforcementsLoc =
{
{LstEntry1.Location, LstDst1.Location},
{LstEntry2.Location, LstDst2.Location}
}

Trigger.OnAllKilled(SovAirBuildings, function()
	player.MarkCompletedObjective(DestroyAir)
	player.MarkCompletedObjective(BeforeStorm)
end)

SendInitialUnits = function()
	Jeep1.Move(AlliedDst1.Location + CVec.New(-5,0))
	Jeep2.Move(AlliedDst3.Location + CVec.New(-5,0))
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")

		Reinforcements.Reinforce(player, {"1tnk"}, {AlliedEntry1.Location, AlliedDst1.Location + CVec.New(-3,0)}, 1)
		Reinforcements.Reinforce(player, {"1tnk"}, {AlliedEntry3.Location, AlliedDst3.Location + CVec.New(-3,0)}, 1)
		
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Reinforcements.Reinforce(player, {"2tnk"}, {AlliedEntry1.Location, AlliedDst1.Location + CVec.New(-2,-1)}, 1)
			Reinforcements.Reinforce(player, AdditionalUnit, {AlliedEntry2.Location, AlliedDst2.Location + CVec.New(-2,0)}, 1)
			Reinforcements.Reinforce(player, {"2tnk"}, {AlliedEntry3.Location, AlliedDst3.Location + CVec.New(-2,1)}, 1)
			Trigger.AfterDelay(DateTime.Seconds(1), function()
				Reinforcements.Reinforce(player, {"2tnk"}, {AlliedEntry1.Location, AlliedDst1.Location + CVec.New(-1,0)}, 1)
				
				Reinforcements.Reinforce(player, {"2tnk"}, {AlliedEntry3.Location, AlliedDst3.Location + CVec.New(-1,0)}, 1)
			
				Trigger.AfterDelay(DateTime.Seconds(1), function()
					Reinforcements.Reinforce(player, ApcCombatType, {AlliedEntry1.Location + CVec.New(0,-1), AlliedDst1.Location + CVec.New(1,-1)}, 1)
					Reinforcements.Reinforce(player, ApcSupportType, {AlliedEntry3.Location + CVec.New(0,1), AlliedDst3.Location + CVec.New(1,1)}, 1)
					Reinforcements.Reinforce(player, AdditionalUnit, {AlliedEntry2.Location, AlliedDst2.Location + CVec.New(0,0)}, 1)
					Trigger.AfterDelay(DateTime.Seconds(2), function()
						Reinforcements.Reinforce(player, {"arty"}, {AlliedEntry1.Location, AlliedDst1.Location + CVec.New(3,0)}, 1)
						Reinforcements.Reinforce(player, {"arty"}, {AlliedEntry3.Location, AlliedDst3.Location + CVec.New(3,0)}, 1)
					end)
				end)
			end)
		end)
	end)
end

SendEnemyReinforcements = function()
	if EnemyReinforcements > 0 then
		EnemyReinforcements = EnemyReinforcements - 1
		local origin = Utils.Random({"land", "water"})
		local force = { "e1", "e1", "3tnk", "3tnk", "ftrk"}
		
		if origin == "water" then
			local pos = Utils.RandomInteger(1,#SovReinforcementsLoc+1)
			local units = Reinforcements.ReinforceWithTransport(enemy, LstInsertion, force, SovReinforcementsLoc[1], {SovReinforcementsLoc[1][1]})[2]
			Utils.Do(units, function(a)
				Trigger.OnAddedToWorld(a, function()
					IdleHunt(a)
				end)
			end)
		else
			local units = Reinforcements.Reinforce(enemy, force, SovUnitsEntry, 10)
			Utils.Do(units, function(a)
				IdleHunt(a)
			end)
		end
		
		Trigger.AfterDelay(EnemyReinforceDelay, function()
			SendEnemyReinforcements()
		end)
	end
end

Trigger.OnEnteredFootprint(CivilianAidCoords, function(a, id)
	if a.Owner == player and civAlert == false then
		civAlert = true
		Trigger.RemoveFootprintTrigger(id)
		
		if CivilianAidCoordsAvailable == 2 then
			local cam1 = Actor.Create("camera", true, { Owner = player, Location = AidCamera1.Location })
			local cam2 = Actor.Create("camera", true, { Owner = player, Location = AidCamera2.Location })
			local cam3 = Actor.Create("camera", true, { Owner = player, Location = AidCamera3.Location })
			local cam4 = Actor.Create("camera", true, { Owner = player, Location = AidCamera4.Location })
			
			local cams = {cam1, cam2, cam3, cam4}
			
			Utils.Do(cams, function(c)
				Trigger.AfterDelay(DateTime.Seconds(10), function()
					c.Destroy()
				end)
			end)
		else if CivilianAidCoordsAvailable == 1 then
				local cam2 = Actor.Create("camera", true, { Owner = player, Location = AidCamera2.Location })
			
				Trigger.AfterDelay(DateTime.Seconds(10), function()
					cam2.Destroy()
				end)
			else
				return
			end
		end
		Media.DisplayMessage(MessageToDisplay,"Civilian")
	end
end)

SetupDifficulty = function()
	if CreateTesla1 == true then
		Actor.Create("tsla", true, { Owner = enemy, Location = RemovableBrik1.Location })
		RemovableBrik1.Destroy()
	end
	
	if CreateTesla2 == true then
		Actor.Create("tsla", true, { Owner = enemy, Location = RemovableBrik2.Location })
		RemovableBrik2.Destroy()
	end
end

Flavor = function()
	Trigger.AfterDelay(DateTime.Seconds(4), function()
		local landings = SovAirBuildings
		local aircrafts = SovAir
		
		for i = 1, #aircrafts, 1 do
			aircrafts[i].ReturnToBase(landings[i])
		end
	end)
end

StartClearStorm = function(delay)
	Trigger.AfterDelay(delay, function()
		TransitionInEffect = true
		Trigger.AfterDelay(DateTime.Seconds(15), function()
			ThunderActive = false
		end)
	end)
end

InitiateAirAttacks = function()
	local aircrafts = Utils.Where(Map.ActorsInWorld, function(a) return a.Type == "yak" or a.Type == "mig" or a.Type == "hind" end)
	
	for i=1,#aircrafts,1 do
		Trigger.AfterDelay(DateTime.Seconds(i*20), function()
			if not aircrafts[i].IsDead then 
				InitializeAttackAircraft(aircrafts[i], player)
			end
		end)
	end
end

InitializeAttackAircraft = function(aircraft, enemyPlayer)
	Trigger.OnIdle(aircraft, function()
		local actorId = tostring(aircraft)
		local target = AttackAircraftTargets[actorId]
		
		local aflds = Utils.Where(Map.ActorsInWorld, function(a) return a.Type == "afld" end)
		local hpads = Utils.Where(Map.ActorsInWorld, function(h) return h.Type == "hpad" end)

		if not target or not target.IsInWorld then
			target = ChooseRandomTarget(aircraft, enemyPlayer)
		end

		if target then
			AttackAircraftTargets[actorId] = target
			if aircraft.AmmoCount() > 0 then
				aircraft.Attack(target)
			else
				RemoveAircraft(aircraft)
			end
		else
			AttackAircraftTargets[actorId] = nil
			if aircraft.Type == "mig" or aircraft.Type == "yak" then
				Media.Debug("1")
				if #aflds > 0 then
					Media.Debug("2")
					aircraft.ReturnToBase()
				else
					RemoveAircraft(aircraft)
				end
			else
				if #hpads > 0 then
					aircraft.ReturnToBase()
				else
					RemoveAircraft(aircraft)
				end
			end
		end
	end)
end

ChooseRandomTarget = function(unit, enemyPlayer)
	local target = nil
	local enemies = Utils.Where(enemyPlayer.GetActors(), function(self)
		return self.HasProperty("Health") and unit.CanTarget(self) and not Utils.Any({ "sbag", "fenc", "brik", "cycl", "barb" }, function(type) return self.Type == type end)
	end)
	if #enemies > 0 then
		target = Utils.Random(enemies)
	end
	return target
end

RemoveAircraft = function(aircraft)
	aircraft.Move(RemovalPoint.Location)
	aircraft.Destroy(RemovalPoint.Location)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

Tick = function()
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(DenyAllies)
	end
	
	if ThunderActive == true then
		if (Utils.RandomInteger(1, 200) == 10) then
			local delay = Utils.RandomInteger(1, 10)
			Lighting.Flash("LightningStrike", delay)
			Trigger.AfterDelay(delay, function()
				Media.PlaySound("thunder" .. Utils.RandomInteger(1,6) .. ".aud")
			end)
		end
		if 	(Utils.RandomInteger(1, 200) == 10) then
			Media.PlaySound("thunder-ambient.aud")
		end
	end
	
	if TransitionInEffect == true then
		transition=transition+1
		
		Lighting.Red 		= Lighting.Red 		+ RedTransition/2
		Lighting.Green 		= Lighting.Green 	+ GreenTransition/2
		Lighting.Blue 		= Lighting.Blue 	+ BlueTransition/2
		Lighting.Ambient 	= Lighting.Ambient 	+ AmbientTransition/2
		
		transition= transition+1
		if transition >= 4000 then
			TransitionInEffect = false
			transition = 0
			StormCleared = true
			
			Lighting.Red 		= 1.0
			Lighting.Green 		= 1.0
			Lighting.Blue 		= 1.0
			Lighting.Ambient 	= 1.0
		end
	end
	
	if StormCleared == true then
		StormCleared = false
		player.MarkFailedObjective(BeforeStorm)
		InitiateAirAttacks()
	end
end

InitTriggers = function()
	Camera.Position = StartingCamera.CenterPosition
	
	MessageColor = HSLColor.White
	
	SetupDifficulty()
	
	Trigger.AfterDelay(EnemyReinforceDelay, function()
		SendEnemyReinforcements()
	end)
	
	SendInitialUnits()
	Flavor()
	
	ActualStormClearTime = Utils.RandomInteger(StormClearTime[1], StormClearTime[2])
	StartClearStorm(ActualStormClearTime)
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DestroyAir = player.AddPrimaryObjective("Destroy all airields and helipads.")
	BeforeStorm = player.AddSecondaryObjective("Complete primary objective before the storm abates.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the Allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)

end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	
	InitTriggers()
	InitObjectives()
end