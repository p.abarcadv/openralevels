
--Main
Proc = {type = "proc", pos = CVec.New(12, 2), cost = 1400, exists = true }
--Production
Tent = {type = "tent", pos = CVec.New(-3, 10), cost = 400, exists = true}
Weap = {type = "weap", pos = CVec.New(11, 9), cost = 2000, exists = true}
--Power
Pwr1 =	{ type = "powr", pos = CVec.New(-4, 4), cost = 300, exists = true }
Pwr2 =	{ type = "apwr", pos = CVec.New(-7, 3), cost = 500, exists = true }
Pwr3 =	{ type = "apwr", pos = CVec.New(-7, 6), cost = 500, exists = true }
Pwr4 =	{ type = "powr", pos = CVec.New(-4, 7), cost = 300, exists = true }
Pwr5 =	{ type = "apwr", pos = CVec.New(-7, 9), cost = 500, exists = true }
Pwr6 =	{ type = "apwr", pos = CVec.New(3, 2), cost = 500, exists = true }
Pwr7 =	{ type = "apwr", pos = CVec.New(6, 2), cost = 500, exists = true }
--Defences
Gun1 = { type = "gun", pos = CVec.New(9, 14), cost = 800, exists = true }
Gun2 = { type = "gun", pos = CVec.New(2, 14), cost = 800, exists = true }
Gun3 = { type = "gun", pos = CVec.New(-7, 14), cost = 800, exists = true }
Gun4 = { type = "gun", pos = CVec.New(15, 14), cost = 800, exists = true }
Gun5 = { type = "gun", pos = CVec.New(15, 9), cost = 800, exists = true }
Gun6 = { type = "gun", pos = CVec.New(15, 3), cost = 800, exists = true }
Gun7 = { type = "gun", pos = CVec.New(-7, 2), cost = 800, exists = true }
PBox1 = { type = "pbox", pos = CVec.New(0, 15), cost = 600, exists = true }
PBox2 = { type = "pbox", pos = CVec.New(11, 15), cost = 600, exists = true }
PBox3 = { type = "pbox", pos = CVec.New(2, 7), cost = 600, exists = true }
PBox4 = { type = "pbox", pos = CVec.New(9, 7), cost = 600, exists = true }
HBox1 = { type = "hbox", pos = CVec.New(0, -2), cost = 750, exists = true }
AGun1 = { type = "agun", pos = CVec.New(-3, 0), cost = 800, exists = true }
AGun2 = { type = "agun", pos = CVec.New(3, 0), cost = 800, exists = true }
AGun3 = { type = "agun", pos = CVec.New(-6, 13), cost = 800, exists = true }
AGun4 = { type = "agun", pos = CVec.New(14, 13), cost = 800, exists = true }
--Extras
Silo1 = { type = "silo", pos = CVec.New(10, 2), cost = 150, exists = true }
Silo2 = { type = "silo", pos = CVec.New(10, 3), cost = 150, exists = true }
Silo3 = { type = "silo", pos = CVec.New(10, 4), cost = 150, exists = true }
Silo4 = { type = "silo", pos = CVec.New(10, 5), cost = 150, exists = true }
Dome = { type = "dome", pos = CVec.New(5, 5), cost = 1400, exists = true }
Fix = { type = "fix", pos = CVec.New(-1, 3), cost = 1200, exists = true}

BaseBuildings =
{
Proc,

Pwr1,
Pwr2,

Tent,
Weap,

Pwr3,

Dome,
Fix,

Pwr4,

Silo1,
Silo2,

Gun1,
Gun2,

PBox1,
PBox2,

Pwr5,
Pwr6,
Pwr7,

Gun3,
Gun4,

AGun1,
AGun2,
AGun3,
AGun4,

HBox1,

Gun5,
Gun6,
Gun7,

PBox3,
PBox4,

Silo3,
Silo4
}

Trigger.OnKilled(BaseRef, function()
	Proc.exists = false
end)

Trigger.OnKilled(BasePower1, function()
	Pwr1.exists = false
end)

Trigger.OnKilled(BasePower2, function()
	Pwr2.exists = false
end)

Trigger.OnKilled(BasePower3, function()
	Pwr3.exists = false
end)

Trigger.OnKilled(BasePower4, function()
	Pwr4.exists = false
end)

Trigger.OnKilled(BasePower5, function()
	Pwr5.exists = false
end)

Trigger.OnKilled(BasePower6, function()
	Pwr6.exists = false
end)

Trigger.OnKilled(BasePower7, function()
	Pwr7.exists = false
end)

Trigger.OnKilled(BaseTent, function()
	Tent.exists = false
end)

Trigger.OnKilled(BaseWeap, function()
	Weap.exists = false
end)

Trigger.OnKilled(BaseGun1, function()
	Gun1.exists = false
end)

Trigger.OnKilled(BaseGun2, function()
	Gun2.exists = false
end)

Trigger.OnKilled(BaseGun3, function()
	Gun3.exists = false
end)

Trigger.OnKilled(BaseGun4, function()
	Gun4.exists = false
end)

Trigger.OnKilled(BaseGun5, function()
	Gun5.exists = false
end)

Trigger.OnKilled(BaseGun6, function()
	Gun6.exists = false
end)

Trigger.OnKilled(BaseGun7, function()
	Gun7.exists = false
end)

Trigger.OnKilled(BasePBox1, function()
	PBox1.exists = false
end)

Trigger.OnKilled(BasePBox2, function()
	PBox2.exists = false
end)

Trigger.OnKilled(BasePBox3, function()
	PBox3.exists = false
end)

Trigger.OnKilled(BasePBox4, function()
	PBox4.exists = false
end)

Trigger.OnKilled(BaseHBox1, function()
	HBox1.exists = false
end)

Trigger.OnKilled(BaseAGun1, function()
	AGun1.exists = false
end)

Trigger.OnKilled(BaseAGun2, function()
	AGun2.exists = false
end)

Trigger.OnKilled(BaseAGun3, function()
	AGun3.exists = false
end)

Trigger.OnKilled(BaseAGun4, function()
	AGun4.exists = false
end)

Trigger.OnKilled(BaseSilo1, function()
	Silo1.exists = false
end)

Trigger.OnKilled(BaseSilo2, function()
	Silo2.exists = false
end)

Trigger.OnKilled(BaseSilo3, function()
	Silo3.exists = false
end)

Trigger.OnKilled(BaseSilo4, function()
	Silo4.exists = false
end)

Trigger.OnKilled(BaseFix, function()
	Fix.exists = false
end)

Trigger.OnKilled(BaseDome, function()
	Dome.exists = false
end)