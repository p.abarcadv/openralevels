if Map.LobbyOption("difficulty") == "easy" then
	SupplyCash = 4000
	InfAtkCount = 6
	TankAtkCount = 3
elseif Map.LobbyOption("difficulty") == "normal" then
	SupplyCash = 4000
	InfAtkCount = 8
	TankAtkCount = 3
else	
	SupplyCash = 4000
	InfAtkCount = 10
	TankAtkCount = 4
end

AlliedVehlTypes = {"arty", "1tnk", "2tnk", "jeep"}
AlliedInfTypes = {"e1", "e3"}

WeapInfiltrated = false
BarrInfiltrated = false

MainAtkPaths = 
{
{PathA1, PathA2, PathA3},
{PathA1, PathA2, PathA3, PathA4},
{PathA1, PathB1, PathB2, PathB3},
{PathA1, PathB1, PathB2, PathB3, PathB4},
{PathA1}
}

SpyPath =
{
{PathA1, PathA2, PathA3},
{PathB1, PathB2, PathB3},
{PathA1}
}

InfAtk = {}
TankAtk = {}

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

--BaseStuff
BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if BaseFact.IsDead or BaseFact.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = CYardBuildLoc.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
				
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

PrepareInfantryAtk = function()
	local rngInf = Utils.RandomInteger(1,8)
	
	if rngInf ~= 7 then 
		ProduceInfantry()
	else
		ProduceSpy()
	end
end

PrepareBuildQueue = function()
	--local
end

ProduceInfantry = function()
	if not Tent.exists then
		--Media.PlaySoundNotification(enemy, "AlertBuzzer")
		return
	--elseif BaseHarv.IsDead and enemy.Resources <= 299 then
	--	return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(AlliedInfTypes) }
	local Path = Utils.Random(MainAtkPaths)
	enemy.Build(toBuild, function(unit)
		
		InfAtk[#InfAtk + 1] = unit[1]
		if #InfAtk >= InfAtkCount then
			Media.PlaySpeechNotification(enemy, "ReinforcementsArrived") 
			SendUnits(InfAtk, Path)
			InfAtk = { }
			Trigger.AfterDelay(DateTime.Seconds(30), PrepareInfantryAtk)
		else
			Trigger.AfterDelay(delay, ProduceInfantry)
		end
	end)
end

ProduceSpy = function()
	if not Tent.exists then
		--Media.PlaySoundNotification(enemy, "AlertBuzzer")
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		return
	end

	local toBuild = { "spy" }
	local Path = Utils.Random(SpyPath)
	
	enemy.Build(toBuild, function(unit)
		unit[1].DisguiseAsType("e1",player)
		SendSpy(unit[1],Path)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		PrepareInfantryAtk()
	end)
end

SendSpy = function(spyUnit, waypoints)
	local infiltratable = Utils.Where(player.GetActors(), function(building)
		return building.Type == "powr" or building.Type == "barr" or building.Type == "proc" or building.Type == "weap"
	end)
	local target = Utils.Random(infiltratable)
	
	if not spyUnit.IsDead then
		Utils.Do(waypoints, function(waypoint)
			spyUnit.Move(waypoint.Location)
		end)
		spyUnit.Infiltrate(target)
	end
end

ProduceArmor = function()
	if not Weap.exists then
		--return
	--elseif baseharv.IsDead and enemy.Resources <= 599 then
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(12), DateTime.Seconds(17))
	local toBuild = { Utils.Random(AlliedVehlTypes) }
	local Path = Utils.Random(MainAtkPaths)
	enemy.Build(toBuild, function(unit)
		TankAtk[#TankAtk + 1] = unit[1]

		if #TankAtk >= TankAtkCount then
			SendUnits(TankAtk, Path)
			TankAtk = { }
			Trigger.AfterDelay(DateTime.Minutes(1), ProduceArmor)
		else
			Trigger.AfterDelay(delay, ProduceArmor)
		end
	end)
end
