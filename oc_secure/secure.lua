if Map.LobbyOption("difficulty") == "easy" then
	
elseif Map.LobbyOption("difficulty") == "normal" then
	
else	
	
end

UnitsArrived = false

CivBuildings = {CivBuilding1, CivBuilding2, CivBuilding3, CivBuilding4, CivBuilding5, CivChurch}

InitiateAI = function()
	BuildBase()
	Trigger.AfterDelay(DateTime.Minutes(3), function()
		ProduceArmor()
		PrepareInfantryAtk()
	end)
end

IntroSequence = function()
	Reinforcements.Reinforce(player, {"e1", "e1", "e1"}, {SovEntry1.Location, SovDst1.Location+CVec.New(0,-2)}, 0)
	Reinforcements.Reinforce(player, {"e1", "e1", "e1"}, {SovEntry2.Location, SovDst2.Location+CVec.New(0,-2)}, 0)
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Reinforcements.Reinforce(player, {"e2", "e2"}, {SovEntry1.Location, SovDst1.Location}, 0)
		Reinforcements.Reinforce(player, {"e2", "e2"}, {SovEntry2.Location, SovDst2.Location}, 0)
		
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Reinforcements.Reinforce(player, {"mcv"}, {SovMCVEntry.Location, SovMCVDst.Location}, 10)
			UnitsArrived = true
		end)
	end)
end

Trigger.OnAllKilled(CivBuildings, function()
	player.MarkCompletedObjective(KillCivs)
end)

Tick = function()
	if player.HasNoRequiredUnits() and UnitsArrived == true then
		UnitsArrived = false
	end
	
	if enemy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(DenyAllies)
	end
end

InitTriggers = function()
	Camera.Position = SovMCVDst.CenterPosition
	
	player.Cash = 4000
	enemy.Cash = 10000
	
	InitiateAI()
	IntroSequence()	
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DenyAllies = player.AddPrimaryObjective("Deny the allies.")
	KillCivs = player.AddSecondaryObjective("Destroy civilians.")
	
	DenySoviets = enemy.AddPrimaryObjective("Deny the soviets.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")

	InitTriggers()
	InitObjectives()
end