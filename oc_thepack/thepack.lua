if Map.LobbyOption("difficulty") == "easy" then
	--TimerTicks = DateTime.Seconds(540) -- 9 mins
	StartingVehicles = {"jeep", "stnk"}
	RescuablesTotal = 4
	SuppTrucksToDestroy = 6
elseif Map.LobbyOption("difficulty") == "normal" then
	--TimerTicks = DateTime.Seconds(450) -- 7 mins
	StartingVehicles = {"jeep"}
	RescuablesTotal = 4
	SuppTrucksToDestroy = 7
else
	--TimerTicks = DateTime.Seconds(330) -- 5.5 mins
	StartingVehicles = {}
	RescuablesTotal = 3
	SuppTrucksToDestroy = 9
end

MainObjective = 0

DestroyedSuppTrucks = 0

--Other
DisplayInterface = false
unitsArrived = false

--For Secondary Objectives
located = 0

--For Intro sequence
Guerrillas = { Guerrilla1, Guerrilla2, Guerrilla3, Guerrilla4, Guerrilla5, Guerrilla6, Guerrilla7, Guerrilla8 }
IntroState = 0 --Added to prevent duplicates

--For Spy intel
PrisonersGuards = { PGuard1, PGuard2, PGuard3, PGuard4 }
Prisoners = { Prisoner1, Prisoner2, Prisoner3 }
BARR_HEALTH = 60000

SpyPossLocs = 
	{ 
	{	SpyPoint1,	CVec.New(1,3)	, "You can find the spy in the northeast woods."},
	{	SpyPoint2,	CVec.New(-3,1)	, "You can find the spy west from the repair station."}, 
	{	SpyPoint3,	CVec.New(0,4)	, "You can find the spy nearby the cave."}, 
	{	SpyPoint4,	CVec.New(-1,0)	, "You can find the spy nearby the lighthouse."},
	{	SpyPoint5,	CVec.New(-1,-3)	, "You can find the spy in the southeast woods."},
	{	SpyPoint6,	CVec.New(-1,0)	, "You can find the spy nearby the mill."},
	{	SpyPoint7,	CVec.New(-3,-1)	, "You can find the spy nearby the plane crash site."},
	{	SpyPoint8,	CVec.New(1,2)	, "You can find the spy nearby the ruined building remains."}
	}

CamerasBig = { CreationCam1.Location, CreationCam2.Location,CreationCam3.Location }
CamerasSmall = { CreationCam4.Location, CreationCam5.Location}

meeting = Utils.Random(SpyPossLocs)

RescuableGuerrillas = { HiddenInf1, HiddenInf2, HiddenInf3, HiddenTent }

--ROADS
MainRoad = 
{
Road02.Location, Road03.Location, Road04.Location, Road05.Location, Road06.Location,
Road07.Location, Road08.Location, Road09.Location, Road10.Location, Road11.Location,
Road12.Location, Road13.Location, Road14.Location, Road15.Location, Road16.Location,
Road17.Location, Road18.Location, Road19.Location, Road20.Location, Road21.Location,
Road22.Location, Road23.Location
}

--GUARDS
GuardsBase1 = {Base1Guard1,Base1Guard2,Base1Guard3,Base1Guard4,Base1Guard5,Base1Guard6}
GuardsBase2 = {Base2Guard1,Base2Guard2,Base2Guard3,Base2Guard4,Base2Guard5,Base2Guard6,Base2Guard7,Base2Guard8,Base2Guard9}
GuardsBase3 = {}
GuardsBase4 = {Base4Guard1,Base4Guard2,Base4Guard3,Base4Guard4,Base4Guard5,Base4Guard6}
GuardsBase5 = {Base5Guard1,Base5Guard2,Base5Guard3,Base5Guard4,Base5Guard5}

Barr1Loaded = false
Barr2Loaded = false
Barr3Loaded = false
Barr4Loaded = false
Barr5Loaded = false
NightOps = false

--PATROLS
Patrol1 = {Patrol1Unit1, Patrol1Unit2, Patrol1Unit3, Patrol1Unit4, Patrol1Unit5}
Patrol2 = {Patrol2Unit1, Patrol2Unit2, Patrol2Unit3, Patrol2Unit4, Patrol2Unit5}
Patrol3 = {Patrol3Unit1, Patrol3Unit2, Patrol3Unit3, Patrol3Unit4, Patrol3Unit5}
Patrol4 = {Patrol4Unit1, Patrol4Unit2, Patrol4Unit3, Patrol4Unit4, Patrol4Unit5}
Patrol5 = {Patrol5Unit1, Patrol5Unit2, Patrol5Unit3, Patrol5Unit4, Patrol5Unit5}
Patrol6 = {Patrol6Unit1, Patrol6Unit2, Patrol6Unit3, Patrol6Unit4, Patrol6Unit5}
Patrol1Path = {Patrol1Way1.Location, Patrol1Way2.Location, Patrol1Way3.Location, Patrol1Way2.Location}
Patrol2Path = {Patrol2Way1.Location, Patrol2Way2.Location, Patrol2Way3.Location, Patrol2Way2.Location}
Patrol3Path = {Patrol3Way1.Location, Patrol3Way2.Location, Patrol3Way3.Location, Patrol3Way2.Location}
Patrol4Path = {Patrol4Way1.Location, Patrol4Way2.Location, Patrol4Way3.Location, Patrol4Way2.Location}
Patrol5Path = {Patrol5Way1.Location, Patrol5Way2.Location, Patrol5Way3.Location, Patrol5Way2.Location}
Patrol6Path = {}
PatrolDelay = DateTime.Seconds(5)

Base1 = {Base1Power1, Base1Power2, Base1Power3, Base1Barr, Base1Dome, Base1Sam}
Base2 = {Base2Power1, Base2Power2, Base2Weap, Base2Fix, Base2Barr, Base2Sam1, Base2Sam2, Base2Kenn}
Base3 = 
{Base3Fact, Base3Power1, Base3Power2, Base3Power3, Base3Power4, Base3Dome, Base3Silo1, Base3Silo2, Base3Silo3, Base3Kenn, Base3Sam1, Base3Sam2, Base3Sam3, Base3Sam4, Base3Tesla, Base3Barr, Base3Weap, Base3Ftur1, Base3Ftur2, Base3Ref}
Base4 = {Base4Power1, Base4Power2, Base4Power3, Base4Hpad, Base4Sam, Base4Barr}
Base5 = {Base5Power, Base5Barr, Base5Kenn}

ConvoyArrived = false

ActivatePatrols = function()
	GroupPatrol(Patrol1, Patrol1Path, PatrolDelay, PatrolStation1.Location)
	GroupPatrol(Patrol2, Patrol2Path, PatrolDelay, PatrolStation2.Location)
    GroupPatrol(Patrol3, Patrol3Path, PatrolDelay, PatrolStation3.Location)
	GroupPatrol(Patrol4, Patrol4Path, PatrolDelay, PatrolStation4.Location)
	GroupPatrol(Patrol5, Patrol5Path, PatrolDelay, PatrolStation5.Location)
	--GroupPatrol(Patrol6, Patrol6Path, PatrolDelay, PatrolStation6)
end

SendGuerrillas = function()
	--GuerrillasEntry
end

SetAsObjective = function(structures)
	Trigger.OnAllKilled(structures, function()
		MainObjective = MainObjective + 1
	end)
end

GroupPatrol = function(units, waypoints, delay, restLoc)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Trigger.OnIdle(unit, function()
				if stop then
					return
				end
				if unit.Location == waypoints[i] then
					local bool = Utils.All(units, function(actor) return actor.IsIdle end)
					if bool then
						stop = true

						i = i + 1
						if i > #waypoints then
							i = 1
						end
						Trigger.AfterDelay(delay, function() stop = false end)
					end
				else
					if DayState ~= "Night" then
						unit.AttackMove(waypoints[i])
					else
						unit.AttackMove(restLoc)
						Trigger.ClearAll(unit)
					end
				end
			end)
		end
	end)
end

CreateBaseCameras = function()
	Utils.Do(CamerasBig, function(cam)
		Actor.Create( "camera", true,{ Owner = player, Location = cam })
	end)
	
	Utils.Do(CamerasSmall, function(cam)
		Actor.Create( "camera.small", true,{ Owner = player, Location = cam })
	end)
end

Trigger.OnAllKilled(PrisonersGuards, function()
	Media.DisplayMessage(meeting[3],"Civilian")
	SpyHintAcquired()
	Utils.Do(Prisoners, function(p)
		if not p.IsDead then
			p.Move(GuerrillasDst.Location)
			if not CivilianChurch.IsDead then
				p.EnterTransport(CivilianChurch)
			end
		end
	end)
end)

SendInitUnits = function()
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")

	Camera.Position = StartingCameraLoc.CenterPosition

	Reinforcements.Reinforce(player,{"e1","e1","e1"},{PlayerUnitsEntry1.Location, PlayerUnitsDst1.Location}, 1)
	Reinforcements.Reinforce(player,{"e1","e1","e1"},{PlayerUnitsEntry3.Location, PlayerUnitsDst3.Location}, 1)

	Reinforcements.Reinforce(player,{"e3","e3"},{PlayerUnitsEntry1.Location, PlayerUnitsDst1.Location+CVec.New(-1,0)}, 1)
	Reinforcements.Reinforce(player,{"e3","e3"},{PlayerUnitsEntry3.Location, PlayerUnitsDst3.Location+CVec.New(-1,0)}, 1)

	Reinforcements.Reinforce(player,StartingVehicles,{PlayerUnitsEntry2.Location, StartingCameraLoc.Location+CVec.New(-1,0)}, 3)

	Trigger.AfterDelay(DateTime.Seconds(3), function()
		unitsArrived = true
		DisplayInterface = true
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			DayClock()
		end)
	end)
end

SpyHintAcquired = function()
	Trigger.OnEnteredProximityTrigger(meeting[1].CenterPosition, WDist.FromCells(3), function(discoverer, id)
		if discoverer.Owner == player and discoverer.Type ~= "waypoint" then
			Trigger.RemoveProximityTrigger(id)
			local spy = Actor.Create( "spy", true,{ Owner = goodguy, Location = meeting[1].Location })
			player.MarkCompletedObjective(FindSpy)
			--spy.DisguiseAsType("e1", enemy)
			spy.Move(meeting[1].Location + meeting[2])
			Media.DisplayMessage("I will keep you updated on the soviet movements.","Saboteur")
			CreateBaseCameras()
			Trigger.AfterDelay(DateTime.Seconds(15), function()
				spy.Move(meeting[1].Location)
				spy.Destroy()
			end)
		end
	end)
end 

--Hidden Tent
Trigger.OnEnteredProximityTrigger(HiddenTent.CenterPosition, WDist.FromCells(6), function(discoverer, id)
	if discoverer.Owner == player then
		Trigger.RemoveProximityTrigger(id)
		player.Cash = player.Cash + 500
		Utils.Do(RescuableGuerrillas, function(u)
			u.Owner = player
		end)
		located = located + 1
	end
end)
--Hidden Jammer
Trigger.OnEnteredProximityTrigger(HiddenJammer.CenterPosition, WDist.FromCells(6), function(discoverer, id)
	if discoverer.Owner == player then
		Trigger.RemoveProximityTrigger(id)
		HiddenJammer.Owner = player
		located = located+1
	end
end)
--Hidden Dome
Trigger.OnEnteredProximityTrigger(HiddenRadar.CenterPosition, WDist.FromCells(6), function(discoverer, id)
	if discoverer.Owner == player and discoverer.Type ~= "waypoint" then
		Trigger.RemoveProximityTrigger(id)
		HiddenRadar.Owner = player
		located = located+1
	end
end)

Trigger.OnEnteredProximityTrigger(HiddenStealthTank.CenterPosition, WDist.FromCells(6), function(discoverer, id)
	if discoverer.Owner == player then
		Trigger.RemoveProximityTrigger(id)
		if Map.LobbyOption("difficulty") ~= "hard" then
			HiddenStealthTank.Owner = player
			located = located+1
		else
			HiddenStealthTank.Destroy()
		end
	end
end)

--ChurhcMessage
Trigger.OnEnteredProximityTrigger(CivilianChurch.CenterPosition, WDist.FromCells(6), function(discoverer, id)
	if discoverer.Owner == player and discoverer.Type ~= "waypoint"  then
		Trigger.RemoveProximityTrigger(id)
		Media.DisplayMessage("Some soldiers were taken prisoners west of here.","Civilian")
	end
end)

LoadIntoBarracks = function(units, barr)
	if not barr.IsDead then
		Utils.Do(units, function(a)
			if not a.IsDead then
				a.EnterTransport(barr)
			end
		end)
	end
end

UnloadFromBarracks = function(units, locs, barr)
	if not barr.IsDead then
		barr.UnloadPassengers()
	end
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Utils.Do(units, function(a)
			if not a.IsDead then
				--a.Move()
			end
		end)
	end)
end


MoveThroughRoad = function(unit)
	--Utils.Do(units, function(u)
		if not unit.IsDead then
			Utils.Do(MainRoad, function(way)
				unit.Move(way)
			end)
		end
		if not unit.IsDead then
			unit.Destroy()
		end
	--end)
end

PassingConvoy = function(units)
	Media.PlaySpeechNotification(player, "ConvoyApproaching")
	
	local convoy = Reinforcements.Reinforce(enemy,units,{Road01.Location, Road02.Location}, 30)
	Utils.Do(convoy, function(c)
		MoveThroughRoad(c)
	end)

end

GuardingBase = function()
	Trigger.OnDamaged(Base4Barr, function(self, attacker)
		if attacker.Owner == player then
			
		end
	end)
end

--[[	INTRO SEQUENCE	-	START	]]
IntroSequence = function()
	OutCam = Actor.Create( "camera.small", true,{ Owner = player, Location = CinematicOutpost.Location })
	Utils.Do(Guerrillas, function(u)
		if not u.IsDead then
			u.AttackMove(CinematicOutpost.Location)
		end
	end)
	IntroState = 1
	IntroPart1()
end

IntroPart1 = function()
	if IntroState == 1 then
		IntroState = 2
		Trigger.OnDamaged(OutpostBarr, function(self, attacker)
			if self.Health <= BARR_HEALTH/2 and attacker.Owner == goodguy then
				Media.PlaySoundNotification(player, "AlertBuzzer")
				Trigger.ClearAll(self)
				Trigger.AfterDelay(2, function()
					IntroPart2()
				end)
			end
		end)
	end
end

IntroPart2 = function()
	if IntroState == 2 then
		IntroState = 3
		Trigger.OnDamaged(OutpostBarr, function(self)
			if self.Health <= BARR_HEALTH/4 then
				Trigger.ClearAll(self)
				OutpostBarr.UnloadPassengers()
				Trigger.AfterDelay(2, function()
					IntroPart3()
				end)
			end
		end)
	end
end

IntroPart3 = function()
	if IntroState == 3 then
		IntroState = -1
		Trigger.OnKilled(OutpostBarr, function()
			Utils.Do(Guerrillas, function(a)
				if not a.IsDead then
					a.Stop()
					a.Move(SpyPoint8.Location+CVec.New(0,3))
					a.Destroy()
				end
			end)
			Trigger.AfterDelay(DateTime.Seconds(2), function()
				OutCam.Destroy()
				Trigger.AfterDelay(DateTime.Seconds(2), function()
					SendInitUnits()
					local unitsToRemove = Map.ActorsInBox(RemoveFromHere.CenterPosition, ToHere.CenterPosition, function(s)
						return s.Owner == enemy and s.Type == "e1" or s.Type == "e2"
					end)
					if #unitsToRemove > 0 then
						Utils.Do(unitsToRemove, function(d) 
							d.Stop()
							d.Destroy()
						end)
					end
				end)
			end)
		end)
	end
end

--[[	INTRO SEQUENCE	-	END	]]
Tick = function() 
	if player.HasNoRequiredUnits() and unitsArrived == true then
		enemy.MarkCompletedObjective(DenyAllies)
		unitsArrived = false
	end
	
	if MainObjective >= 4 then
		MainObjective = -1
		player.MarkCompletedObjective(DestroyBuildings)
	end
	
	if located >= RescuablesTotal then
		player.MarkCompletedObjective(LocateForces)
		located = -1
	end
	
	if DestroyedSuppTrucks >= SuppTrucksToDestroy then
		DestroyedSuppTrucks = -1
		player.MarkCompletedObjective(DestroyBuildings)
	end
	
	if NightOps == false and TimeHour == 23 then
		NightOps = true
		LoadIntoBarracks(GuardsBase1, Base1Barr)
		LoadIntoBarracks(GuardsBase2, Base2Barr)
		LoadIntoBarracks(GuardsBase4, Base4Barr)
		LoadIntoBarracks(GuardsBase5, Base5Barr)
	end
	
	if NightOps == true and TimeHour == 6 then
		ActivatePatrols()
		NightOps = false
		UnloadFromBarracks(GuardsBase1, 1, Base1Barr)
		UnloadFromBarracks(GuardsBase2, 2, Base2Barr)
		UnloadFromBarracks(GuardsBase4, 3, Base4Barr)
		UnloadFromBarracks(GuardsBase5, 4, Base5Barr)
	end
	
	--DayCycle
	if TransitionInEffect == true then
		transition=transition+1
		
		Lighting.Red 		= Lighting.Red 		- RedTransition
		Lighting.Blue 		= Lighting.Blue 	- BlueTransition
		Lighting.Green 		= Lighting.Green 	- GreenTransition
		Lighting.Ambient 	= Lighting.Ambient 	- AmbientTransition
		
		transition= transition+1
		if transition >= 4000 then
			--Media.Debug("Transition Completed")
			--Media.Debug("Transition Value 1: " .. transition)
			TransitionInEffect = false
			Lighting.Red 		= TimeOfDay[n].R
			Lighting.Green 		= TimeOfDay[n].G
			Lighting.Blue 		= TimeOfDay[n].B
			Lighting.Ambient 	= TimeOfDay[n].A
			transition = 0
			--Media.Debug("Transition Value 2: " .. transition)
		end
	end
	
	if TimeHour == 12 and ConvoyArrived == true then
		ConvoyArrived = false
		PassingConvoy({"ftrk","truk","truk","truk","ftrk"})
	end
	
	if TimeHour == 0 then
		ConvoyArrived = true
	end
	
	if DisplayInterface == true then 
		UserInterface.SetMissionText("Day: " .. DayN .. " " .."Time: " .. TimeHour .. ":" .. TimeMin .. "0", HUDColor)
	end
end

InitTriggers = function()
	TimeHour = INITHOUR
	Camera.Position = CinematicOutpost.CenterPosition

	IntroSequence()
	
	--Manage patrol in day and night time

	--Media.Debug("Set Objective 1")
	SetAsObjective(Base1)
	SetAsObjective(Base2)
	SetAsObjective(Base4)
	SetAsObjective(Base5)
	
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DestroyBuildings = player.AddPrimaryObjective("Destroy all soviet structures.")
	LocateForces = player.AddSecondaryObjective("Locate your forces in the area.")
	FindSpy = player.AddSecondaryObjective("Make contact with the saboteur.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

SetupMood = function()
	Media.PlayMusic("backstab")
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	goodguy = Player.GetPlayer("GoodGuy")

	InitObjectives()
	InitTriggers()
	SetupMood()
	
	HUDColor = player.Color
end
