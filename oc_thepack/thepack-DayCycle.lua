if Map.LobbyOption("speed") == "slowest" then

elseif Map.LobbyOption("speed") == "slow" then

elseif Map.LobbyOption("speed") == "normal" then
	
elseif Map.LobbyOption("speed") == "fast" then

elseif Map.LobbyOption("speed") == "faster" then

else
	
end

DayState = ""				--Current state of day (Morning, Day, Evening, Night)
TransitionInEffect = false	--Transition switch
transition = 0				--Transition "timer" value
DayN = 1					--Day number
INITHOUR = 22
TimeHour = 0				--InitialHour
TimeMin = 0
MINUTETICK = 5				--Delay between minute increments

n = 1						--Initial day state

Morning = { state = "Morning"	, R = 1.65	, G = 1.15	, 	B = 0.95, 	A = 0.65}
Day 	= { state = "Day"		, R = 1.0	, G = 1.0	, 	B = 1.0 , 	A = 1.0}
Evening = { state = "Evening"	, R = 1.65	, G = 1.15	, 	B = 0.95, 	A = 0.65}
Night 	= { state = "Night"		, R = 0.75	, G = 0.85	, 	B = 1.5	, 	A = 0.45}

--Cada estado del día ha de durar 7.5 mins
TimeOfDay = { Evening, Night, Morning, Day }

DayCycle = function()
	--Media.Debug("Value of n: " .. n)
	if n < #TimeOfDay then
		n = n + 1
		--Media.Debug("N + 1")
	else
		n = 1
		--Media.Debug("N = " .. n)
	end
	
	RedTransition 		= (Lighting.Red 	- TimeOfDay[n].R)/2000
	GreenTransition 	= (Lighting.Green 	- TimeOfDay[n].G)/2000
	BlueTransition 		= (Lighting.Blue 	- TimeOfDay[n].B)/2000
	AmbientTransition 	= (Lighting.Ambient - TimeOfDay[n].A)/2000

	TransitionInEffect = true
	DayState = TimeOfDay[n].state
	Media.Debug("Time: " .. DayState .. " - Value of N: - " .. n)
	--[[
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		DayCycle()
	end)]]
end

DayClock = function()
	TimeMin = TimeMin + 1
	if TimeMin >= 6 then
		TimeMin = 0
		TimeHour = TimeHour + 1
		if TimeHour >= 24 then
			DayN = DayN+1
			TimeHour = 0
		end
	end

	Trigger.AfterDelay(DateTime.Seconds(MINUTETICK), function()
		DayClock()
	end)
	
	if (TimeHour == 0 and TimeMin == 0) or (TimeHour == 6 and TimeMin == 0) or (TimeHour == 12 and TimeMin == 0) or (TimeHour == 18 and TimeMin == 0) then
		DayCycle()
	end
end

--each 6 hours the day state changes