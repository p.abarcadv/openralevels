
ProductionInterval =
{
	easy = DateTime.Seconds(30),
	normal = DateTime.Seconds(30),
	hard = DateTime.Seconds(30)
}

ParatrooperDelay = DateTime.Minutes(2)

SovietAircraftType = { "mig", "mig", "yak" }
AttackAircraft = { }
Migs = { }

ArmorCount = 1

SovInfantryTypes = { "e1", "e1", "e2", "e4", "e4" }

LowTierArmorTypes = {"3tnk", "3tnk", "v2rl", "ftrk"}
HighTierArmorTypes = {"3tnk", "3tnk", "3tnk", "v2rl", "v2rl", "ttnk", "ttnk", "ftrk"}

USSRInfAttack = { }
USSRArmorAttack = { }

BadGuyInfAttack = { }
BadGuyArmorAttack = { }

ExtraEnemyInfAttack = { }
ExtraEnemyArmorAttack = { }

Paratroops = {}
ParatrooperLZs = 
{
ParatrooperLZ1, ParatrooperLZ2, ParatrooperLZ3
}
ParatrooperEntryPoints = {ParatrooperEntry1, ParatrooperEntry2}

--Left outpost data
LeftLstPath = { LeftOoMLstEntry.Location, LeftOoMLstDst.Location }

AttackPhase = 0

--[[
PrimaryAttackPaths = 
	{
		--PathA - Removable once bridge destroyed
		--{PathA1, PathA2, PathA3, PathA4, PathA5}, 
		--{PathA1, PathA2, PathA3, PathB4, PathB5},
		--{PathA1, PathA2, PathA3, PathB4, PathC6B},
		{PathA1},
		--PathB
		--{PathB1, PathB2, PathB3, PathB4, PathB5},
		--{PathB1, PathB2, PathB3, PathB4, PathC6A},
		--{PathB1, PathB2, PathB3, PathB4, PathC6B},
		{PathB1},
		--PathC		
		--{PathC1, PathC2, PathC3, PathC4, PathC5},
		--{PathC1, PathC2, PathC3, PathC4, PathC6A},
		--{PathA1, PathA2, PathA3, PathB4, PathC6B},
		{PathC1},
		--PathD	
		--{PathD1, PathD2, PathC3, PathC4, PathC5, PathC6A},
		--{PathD1, PathD2, PathC3, PathC4, PathC5, PathC6A},
		{PathD1}
	}]]
	
MainBaseAtkTargets =
{
	{PathA1},
	{PathB1},
	{PathC1, PathC2},
	{PathD1, PathD2}
}

RightBaseAtkTargets =
{
	{PathA3, PathA4},
	{PathA3, PathA4, PathA5},
	{PathA3, PathB4}
}

LeftBaseAtkTargets =
{
	{},
	{},
	{}
}

PrimaryAttackPaths =
	{
		{PathA1, PathA1},
		{PathB1, PathB1},
		{PathC1, PathC1},
		{PathD1, PathD1},
	}

SecondaryAttackPaths =
	{
		{PathA1, PathA2, PathA3},
		{PathA1, PathA2, PathA3},
		{PathA1, PathA2, PathA3}
	}

SetupAttackPhase = function()
	AttackPhase = AttackPhase + 1
	
	if AttackPhase == 1 then
		ProduceInfantry(badguy)
		ProduceArmor(badguy)
	end
	
	if AttackPhase == 2 then
		ProduceInfantry(enemy)
		ProduceArmor(enemy)
	end
	
	if AttackPhase == 3 then
		ProduceInfantry(enemy)
		ProduceArmor(enemy)
	end
end

SendParatroopers = function(spawn, drop, units)	
	local entry = spawn.CenterPosition + WVec.New(0, 0, Actor.CruiseAltitude("badr"))	
	local transport = Actor.Create("badr", true, {CenterPosition = entry, Owner = enemy, Facing = (drop.CenterPosition - entry).Facing, Health = 100})		

	Utils.Do(units, function(type)			
		local a = Actor.Create(type, false, {Owner = enemy})			
		transport.LoadPassenger(a)
		Trigger.OnAddedToWorld(a, function()
			IdleHunt(a)
		end)
	end)

	transport.Paradrop(drop.Location)
end

ParatrooperController = function()
	local entry = Utils.RandomInteger(1,3)
	local target = Utils.RandomInteger(1,4)
	local units = Utils.Random({{"e4","e4","e4","e4","e4"}, {"e1","e1","e1","e2","e2"}})

	SendParatroopers(ParatrooperEntryPoints[entry], ParatrooperLZs[target], units)
	
	Trigger.AfterDelay(ParatrooperDelay, ParatrooperController)
end

AircraftTypes = {"yak"}
AircraftAtk = {}

ProduceAircraft = function(controller)
	if (Outpost3Afld1.IsDead or Outpost3Afld1.Owner ~= controller) or (Outpost3Afld2.IsDead or Outpost3Afld1.Owner ~= controller) then
		Media.Debug("air 1")
		return
	end
	Media.Debug("air 2")

	controller.Build(AircraftTypes, function(units)
		local planes = units[1]
		AircraftAtk[#AircraftAtk + 1] = planes
			Trigger.OnKilled(planes, function(units)
			if #AircraftAtk == 1 then
				Trigger.AfterDelay(DateTime.Seconds(10), function()
					ProduceAircraft(controller)
					Media.Debug("air 3")
				end)
			end
		end)
		InitializeAttackAircraft(planes, player)
		Media.Debug("air 4")
	end)
end

RecheckCash = function()
	enemy.Cash = 100000
	badguy.Cash = 100000
	
	Trigger.AfterDelay(DateTime.Seconds(10), RecheckCash)
end

BuildBase = function()
	for i,v in ipairs(BaseBuildings) do
		if not v.exists then
			BuildBuilding(v)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
end

BuildBuilding = function(building)
	Trigger.AfterDelay(Actor.BuildTime(building.type), function()
		if BaseFact.IsDead or BaseFact.Owner ~= enemy then 
			return
		end
		
		local actor = Actor.Create(building.type, true, { Owner = enemy, Location = BaseFactLocation.Location + building.pos })
		enemy.Cash = enemy.Cash - building.cost

		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == enemy and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
				
			end
		end)

		Trigger.AfterDelay(DateTime.Seconds(10), BuildBase)
	end)
end

ProduceInfantry = function(controller, path, groupCol)
	--if baseharv.IsDead and enemy.Resources <= 299 then
	--	return
	--end
	if controller == enemy then
		if (not Barr1.exists or Barr1.Owner ~= enemy) and (not Barr2.exists and Barr2.Owner ~= enemy) then
			Media.Debug("2 Barr enemy return")
			return
		end
	else if controller == badguy then
			if PrisonBaseBarr1.IsDead or PrisonBaseBarr1.Owner ~= badguy then
				Media.Debug("2 Barr badguy return")
				return
			end
		else
			if (Outpost1Barr.IsDead or Outpost1Barr.Owner ~= extraenemy) and (Outpost2Barr.IsDead or Outpost2Barr.Owner ~= extraenemy) and (Outer1Barr.IsDead or Outer1Barr.Owner ~= extraenemy) then
				Media.Debug("2 Barr extraenemy return")
				return
			end
		end
	end
	Media.Debug("building inf " .. tostring(controller))
	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(SovInfantryTypes) }
	local Path = Utils.Random(path)
	controller.Build(toBuild, function(unit)
		groupCol[#groupCol + 1] = unit[1]
		if #groupCol >= 3 then
			SendUnits(groupCol, Path)
			groupCol = { }
			Trigger.AfterDelay(DateTime.Seconds(30), function()
				ProduceInfantry(controller, path, groupCol)
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceInfantry(controller, path, groupCol)
			end)
		end
	end)
end

--[[
controller = player
path = attack path to follow if any (Location collection)
amount = number of units that compose the attack force
groupCol = collection to use
min_delay = time col before next production (min)
max_delay = time col before next production (max)
]]
ProduceArmor = function(controller, path, amount, groupCol, min_delay, max_delay)
	if controller == enemy then
		if (not Weap1.exists or Weap1.Owner ~= enemy) and (not Weap2.exists and Weap1.Owner ~= enemy) then
			Media.Debug("2 weap enemy return")
			return
		end
	else if controller == badguy then
			if PrisonBaseWeap.IsDead or PrisonBaseWeap.Owner ~= badguy then
				Media.Debug("3 weap badguy return")
				return
			end
		else
			if Outpost1Weap.IsDead or Outpost1Weap.Owner ~= extraenemy then
				Media.Debug("4 weap extraenemy return")
				return
			end
		end
	end
	Media.Debug("building tanks " .. tostring(controller))
	local delay = Utils.RandomInteger(min_delay, max_delay)
	local AvailableArmor = {}
	if controller == enemy then
		AvailableArmor = HighTierArmorTypes
	else
		AvailableArmor = LowTierArmorTypes
	end
	local toBuild = { Utils.Random(AvailableArmor) }
	local Path = Utils.Random(path)
	controller.Build(toBuild, function(unit)
		groupCol[#groupCol + 1] = unit[1]

		if #groupCol >= amount then
			SendUnits(groupCol, Path)
			groupCol = { }
			Trigger.AfterDelay(DateTime.Minutes(1), function()
				ProduceArmor(controller, path, amount, groupCol, min_delay, max_delay)
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceArmor(controller, path, amount, groupCol, min_delay, max_delay)
			end)
		end
	end)
end

LeftOoMAtk = function()
	local units = Utils.Random({{"v2rl","ftrk","3tnk","e1","e1"},{"3tnk","3tnk","3tnk","e1","e1"},{"ftrk","v2rl","v2rl","e1","e1"}})

	local attackGroup = Reinforcements.ReinforceWithTransport(enemy, LstInsertion, units, LeftLstPath, {LeftLstPath[1]})[2]

	Utils.Do(attackGroup, function(a)
		Trigger.OnAddedToWorld(a, function()
			if not a.IsDead then
				IdleHunt(a)
			end
		end)
	end)
	
	Trigger.AfterDelay(DateTime.Minutes(1), function()
		LeftOoMAtk()
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

RepopulateBaseUnits = function()
	for i=1,#BaseUnits,1 do
		Trigger.AfterDelay(DateTime.Seconds(i*10), function()
			Media.Debug("Sending units to repopulate base")
			SendBaseUnits(BaseUnits[i])
		end)
	end
	
	Trigger.AfterDelay(DateTime.Minutes(2), function()
		local hind1 = Reinforcements.Reinforce(enemy, {"hind"}, LSTPath1, 60)[1]
		local hind2 = Reinforcements.Reinforce(enemy, {"hind"}, LSTPath2, 60)[1]
		
		local hpads = Utils.Where(Map.ActorsInWorld, function(self) return self.Owner == enemy and self.Type == "hpad" end)
		
		Trigger.AfterDelay(DateTime.Seconds(2),function()
			if not hind1.IsDead then
				hind1.ReturnToBase(hpads[1])
			end
			if not hind2.IsDead then
				hind2.ReturnToBase(hpads[2])
			end
		end)
	end)
	--SendBaseUnits(BaseUnits[4])
end

SendBaseUnits = function(defenceGroup)
	local group = {}
	local initPos = {}
	local endPos = {}
	
	for i=1,#defenceGroup,1 do
		table.insert(	group,	defenceGroup[i].Utype)
		table.insert(	initPos,defenceGroup[i].firstPos)
		table.insert(	endPos,	defenceGroup[i].finalPos)
	end
	
	local units = Reinforcements.ReinforceWithTransport(enemy, LstInsertion, group, LSTPath1, {LSTPath1[1]})[2]
	
	for x=1,#units,1 do
		Trigger.OnAddedToWorld(units[x], function()
			units[x].Move(initPos[x])
			units[x].Move(endPos[x])
		end)
	end
end

RefOutpost0 = {Outer0Ref, Outer0Silo1, Outer0Silo2, Outer0Harv}
RefOutpost1 = {Outer1Ref, Outer1Silo1, Outer1Silo2, Outer1Harv}
RefOutpost2 = {Outer2Ref, Outer2Silo1, Outer2Silo2, Outer2Harv}
RefOutpost3 = {Outer3Ref, Outer3Silo1, Outer3Silo2, Outer3Silo3, Outer3Harv}
RefOutpost4 = {Outer4Ref, Outer4Silo1, Outer4Silo2, Outer4Silo3, Outer4Harv}
RefOutpost5 = {Outer5Ref, Outer5Silo1, Outer5Silo2, Outer5Silo3, Outer5Silo4, Outer5Harv}

OutpostRefs = {RefOutpost0, RefOutpost1, RefOutpost2, RefOutpost3, RefOutpost4, RefOutpost5}

SetupRefineries = function(collection)
	Utils.Do(collection, function(actor)
		if actor.Type == "proc.alterated" then
			Trigger.OnCapture(actor, function()
				SetSellableRefinery(collection)
				--Trigger.ClearAll(actor)
			end)
			Trigger.OnInfiltrated(actor, function()
				SetSellableRefinery(collection)
			end)
		end
	end)
end

SetSellableRefinery = function(collection)
	Utils.Do(collection, function(c)
		if not c.IsDead and c.Owner == turkey then
			if c.HasProperty("StartBuildingRepairs") then
				c.Sell()
			else
				c.Stop()
			end
		end
	end)
end