if Map.LobbyOption("difficulty") == "easy" then
	StartingCash = 5000
	IncreaseAtkPhaseDelay = DateTime.Minutes(7)
	--HoldAITime = DateTime.Minutes(3)
elseif Map.LobbyOption("difficulty") == "normal" then
	StartingCash = 5000
	IncreaseAtkPhaseDelay = DateTime.Minutes(7)
	--HoldAITime = DateTime.Minutes(2)
else	
	StartingCash = 3000
	IncreaseAtkPhaseDelay = DateTime.Minutes(6)
	--HoldAITime = DateTime.Minutes(1) + DateTime.Seconds(30)
end

ComenceTimer = false
TroopsReady = false

SpyType = "spy.electric"
FlareType = "flare"

--START INSERTION
SpyPath = { SpyEntry.Location, StartingCameraPos.Location }
SubWay2Coords = { CPos.New(125,118) }
EndSubPath = { CPos.New(109,128) }
BaseEntry = { CPos.New(122,106)}

PrisonWay1 = {Spawn1Way1.Location, Spawn1Way2.Location}
CivPrisonersWay = { Spawn1Way1.Location, VillagePoint.Location }

PrisonEntrance = { CPos.New(117,96), CPos.New(117,97), CPos.New(117,98) }

LSTPath1 = {USSRNavalBaseReinforceEntry1.Location, USSRNavalBaseReinforceDst1.Location}
LSTPath2 = {USSRNavalBaseReinforceEntry2.Location, USSRNavalBaseReinforceDst2.Location}

LstInsertion = "lst.in"

--[[
BaseEscape = 
	{ 
	CPos.New(97,59),CPos.New(97,58), 
	CPos.New(73,77), CPos.New(73,76), 
	CPos.New(57,94), CPos.New(57,95), 
	CPos.New(68,108), CPos.New(68,109) 
	}
]]
--NEARBY PLAYERBASE COORDS (Maybe this won't be necessary)
--[[
PlayerBaseZone = 
	{
	CPos.New(7,19), CPos.New(7,20), CPos.New(7,21), CPos.New(8,21), CPos.New(9,21), CPos.New(10,21), CPos.New(11,21),
	CPos.New(14,22), CPos.New(15,22), CPos.New(16,22), CPos.New(17,22), CPos.New(17,23),
	CPos.New(32,16), CPos.New(32,17), CPos.New(32,18),
	CPos.New(32,1), CPos.New(32,2), CPos.New(32,3), CPos.New(32,4), CPos.New(32,5), CPos.New(32,6), CPos.New(32,7),
	CPos.New(32,8), CPos.New(32,9), CPos.New(32,10), CPos.New(32,11), CPos.New(32,12), CPos.New(32,13), CPos.New(32,14),
	}
]]
--[[
EnemyMainBase = 
	{
	--Power
	BasePower1,BasePower2,BasePower3,BasePower4,BasePower5,BasePower6,
	BasePower7,BasePower8,BasePower9,BasePower10,BasePower11,BasePower12,
	BasePower12,BasePower13,BasePower14,BasePower15,BasePower16,BasePower17,
	BasePower18,BasePower19,BasePower20,BasePower21,
	--Defs1
	BaseTesla1,BaseTesla2,BaseTesla3,BaseTesla4,BaseTesla5,BaseTesla6,
	BaseTesla7,BaseTesla8,BaseTesla9,BaseTesla10,BaseTesla11,BaseTesla12,
	--Defs2
	BaseFtur1,BaseFtur2,BaseFtur3,BaseFtur4,
	--Refs
	BaseProc1,BaseProc2,BaseProc3,BaseProc4,
	--Production
	BaseBarr1,BaseBarr2,
	BaseWeap1,BaseWeap2,
	--Silos
	BaseSilo1,BaseSilo2,BaseSilo3,BaseSilo4,BaseSilo5,BaseSilo6,
	BaseSilo7,BaseSilo8,BaseSilo9,
	--Aflds
	BaseAfld1,BaseAfld2,BaseAfld3,BaseAfld4,
	--Sams
	BaseSams1,BaseSams2,BaseSams3,BaseSams4,BaseSams5,BaseSams6,BaseSams7,
	BaseSams8,BaseSams9,BaseSams10,BaseSams11,BaseSams12,
	--Etc
	BaseTech,BaseIron,BaseDome,
	--Kennels
	BaseKenn1, BaseKenn2,
	--Etc2
	BaseFix, BaseSpen1, BaseSpen2, BaseSpen3
	}
]]

AfterExtraction = false

ExtractionHelicopterType = "tran.extraction"
ExtractionPath = { ExtractionEntry.Location, ExtractionDestiny.Location }

TimerTicks = DateTime.Seconds(160) --Time of Outage. 
ticked = TimerTicks

InfiltrationSuccessful = false

DestroyablePrisonWalls = {ExpWall1, ExpWall2, ExpWall3, ExpWall4, ExpWall5, ExpWall6}

baseDiscovered = false

AlliedBase = 
	{ 
	PlayerPbox1, PlayerPbox2, PlayerPbox3, PlayerPbox4, PlayerGun1, PlayerGun2, PlayerGun3, PlayerGun4,
	PlayerTent, PlayerWeap, PlayerDome, PlayerPower1, PlayerPower2, PlayerPower3, PlayerPower4, PlayerFact,
	PlayerAAgun1, PlayerAAgun2, PlayerAAgun3	
	}
	
AlliedWalls =
	{
	Sbag1, Sbag2, Sbag3, Sbag4, Sbag5, Sbag6, Sbag7, Sbag8, Sbag9, Sbag10, Sbag11, Sbag12, Sbag13, Sbag14,
	Sbag15, Sbag16, Sbag17, Sbag18, Sbag19, Sbag20, Sbag21, Sbag22, Sbag23, Sbag24, Sbag25, Sbag26, Sbag27, Sbag28, Sbag29, Sbag30, Sbag31, Sbag32
	}

--AI ATTACKING START
StartAIRoutine = function()
	RunBaseManagement()
	
	SetupAttackPhase()
	--ProduceInfantry()
	--ProduceArmor()
	--ProduceAircraft()
	
	Trigger.AfterDelay(IncreaseAtkPhaseDelay, function()
		SetupAttackPhase()
		Trigger.AfterDelay(IncreaseAtkPhaseDelay, function()
			SetupAttackPhase()
		end)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(10), ParatrooperController)
	
	Trigger.AfterDelay(1, function()
		BuildBase()
	end)
	
	RepopulateBaseUnits()
	Barr1.exist = true
	Barr2.exist = true
	Weap1.exist = true
	Weap2.exist = true
	
	LeftOoMAtk()
end

--AI REBUILDING START
RunBaseManagement = function()
	--baseharv.FindResources()
	Utils.Do(Map.NamedActors, function(actor)
		if actor.Owner == enemy and actor.HasProperty("StartBuildingRepairs") then
			Trigger.OnDamaged(actor, function(building)
				if building.Owner == enemy and building.Health < 3/4 * building.MaxHealth then
					building.StartBuildingRepairs()
				end
			end)
		end
	end)
end

Checkpoint1 = 
{
CPos.New(54,105), CPos.New(54,106), CPos.New(54,107), CPos.New(54,108), CPos.New(54,109), 
CPos.New(54,110), CPos.New(54,111), CPos.New(52,111), CPos.New(53,111)
}

--[[
Checkpoint2 = 
{
CPos.New(54,105), CPos.New(54,106), CPos.New(54,107), CPos.New(54,108), CPos.New(54,109), CPos.New(54,110),CPos.New(54,111), CPos.New(52,111), CPos.New(53,111)
}
]]

SetRescuableBase = function()
	Utils.Do(AlliedBase, function(actor)
		Trigger.OnEnteredProximityTrigger(actor.CenterPosition, WDist.FromCells(10), function(discoverer, id)
			DiscoveredAlliedBase(actor, discoverer)
		end)
	end)
end

DiscoveredAlliedBase = function(actor, discoverer)
	if (not baseDiscovered and discoverer.Owner == player) then
		baseDiscovered = true  
		--Media.PlaySpeechNotification(player, "ObjectiveReached")
		
		Utils.Do(AlliedBase, function(building)
			building.Owner = player
		end)
		Utils.Do(AlliedWalls, function(walls)
			walls.Owner = player
		end)
		
		player.Cash = StartingCash
		
		ExtractGeneral()
		--ChangeEnemyBaseActors()
		
		--Need to delay this so we don't fail mission before obj added
		--Trigger.AfterDelay(DateTime.Seconds(1), function()
			--SetupTimeNotifications()
		--end)
	end
end

-----------------------
---GENERAL OBJ START---
-----------------------
GeneralMustSurvive = function()
	ReachExtraction =  player.AddPrimaryObjective("Extract the General from the combat area.")
	
	Trigger.OnKilled(General[1], function()
		player.MarkFailedObjective(ReachExtraction)
	end)
end

ExtractGeneral = function()
	flare = Actor.Create(FlareType, true, { Owner = player, Location = ExtractionDestiny.Location+CVec.New(0,1) })
	heli = Reinforcements.ReinforceWithTransport(player, ExtractionHelicopterType, nil, ExtractionPath)[1]
	if not General[1].IsDead then
		Trigger.OnRemovedFromWorld(General[1], EvacuateHelicopter)
	end
	--Trigger.OnKilled(heli, RescueFailed)
	Trigger.OnRemovedFromWorld(heli, HelicopterGone)
end

EvacuateHelicopter = function()
	if heli.HasPassengers then
		heli.Move(ExtractionEntry.Location)
		heli.Destroy()
	end
end

HelicopterGone = function()
	if not heli.IsDead then
		Media.PlaySpeechNotification(player, "TargetRescued")
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			flare.Destroy()
			EliminateSoviets = player.AddPrimaryObjective("Eliminate all soviet presence in the area.")
			player.MarkCompletedObjective(ReachExtraction)
			AfterExtraction = true
			
			StartAIRoutine()
		end)
	end
end
-----------------------
----GENERAL OBJ END----
-----------------------

Prison3Escapees = {"e1", "e1", "e1", "e1", "e3", "e3", "e3", "e3", "e3"}
--Prison3Path = {Prison3Exit.Location, Prison3Dst.Location}

-----------------------------
-----SECOND PRISON START-----
-----------------------------
Trigger.OnInfiltrated(SecondPrison, function()
	SecondJailbreak()
	player.MarkCompletedObjective(ExtraPrison)
end)

SecondJailbreak = function()
	Trigger.AfterDelay(DateTime.Seconds(1), function()Reinforcements.Reinforce(player, Prison3Escapees, Prison3Path, 10) end)
end

---------------------------
-----SECOND PRISON END-----
---------------------------
FinishTimer = function()
	for i = 0, 5, 1 do
		local c = TimerColor
		if i % 2 == 0 then
			c = HSLColor.White
		end
		
		Trigger.AfterDelay(DateTime.Seconds(i), function() UserInterface.SetMissionText("Power is back online.", c) end)
	end
	Trigger.AfterDelay(DateTime.Seconds(6), function() UserInterface.SetMissionText("") end)
end

BridgeSetup = function()
	local bridgeA = Map.ActorsInCircle(BridgeAPointer.CenterPosition, WDist.FromCells(1), function(self) return self.Type == "bridge2" end)[1]
	local bridgeB = Map.ActorsInCircle(BridgeBPointer.CenterPosition, WDist.FromCells(1), function(self) return self.Type == "bridge2" end)[1]
	local bridgeC = Map.ActorsInCircle(BridgeCPointer.CenterPosition, WDist.FromCells(1), function(self) return self.Type == "bridge2" end)[1]
	local bridgeD = Map.ActorsInCircle(BridgeDPointer.CenterPosition, WDist.FromCells(1), function(self) return self.Type == "bridge2" end)[1]
	local bridgeE = Map.ActorsInCircle(BridgeEPointer.CenterPosition, WDist.FromCells(1), function(self) return self.Type == "bridge2" end)[1]
	local bridgeF = Map.ActorsInCircle(BridgeFPointer.CenterPosition, WDist.FromCells(1), function(self) return self.Type == "bridge1" end)[1]
	
	local bridges = {bridgeC, bridgeD}
	
	Trigger.OnAnyKilled(bridges, function()
		
	end)
	
	Trigger.OnKilled(bridgeB, function()
		
	end)
end

DebugBridges = function()
	bridgeA[3].Kill()
	bridgeA[4].Kill()
end

CheckMainAtkPaths = function()
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		CheckMainAtkPaths()
	end)
end

AirplaneFlavor = function()
	BadGuyYak1.ReturnToBase(Outpost3Afld1)
	BadGuyYak2.ReturnToBase(Outpost3Afld2)
	
end

Tick = function()
	turkey.Resources = turkey.Resources - (0.01 * turkey.ResourceCapacity / 25)
	extraenemy.Cash = 3000
	
	
	if enemy.HasNoRequiredUnits() and badguy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(EliminateSoviets)
	end
	
	if AfterExtraction == true then
		if player.HasNoRequiredUnits() then
			enemy.MarkCompletedObjective(DenyAllies)
		end
	end
	
	if ComenceTimer == true then
		if ticked > 0 then
			UserInterface.SetMissionText("Power restored in " .. Utils.FormatTime(ticked), TimerColor)
			 ticked = ticked - 1
		elseif ticked == 0 then
			FinishTimer()
			ticked = ticked - 1
		end
	end
end

InitTriggers = function()
	Camera.Position = StartingCameraPos.CenterPosition
	Trigger.AfterDelay(DateTime.Seconds(3), function()
		ActivateInitialPatrols()
		EnterSpy()
	end)
	
	--Not giving a delay before mission starts causes an error
	Trigger.AfterDelay(1, function()
		BridgeSetup()
		PrisonBaseBarr1.IsPrimaryBuilding = true
	end)
	
	RecheckCash()
	DebugCode()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	SpySurvive = player.AddPrimaryObjective("The spy must survive.")
	FreePrisoners = player.AddPrimaryObjective("Infiltrate the southern prison complex.")
	ExtraPrison = player.AddSecondaryObjective("Clear the northren prison complex.")
	
	DenyAllies = enemy.AddPrimaryObjective("Eliminate all allied presence in the area.")

	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

DebugRefGroup = {"spy", "e6", "e7"}

Pos0 = CPos.New(48,30)
Pos1 = CPos.New(55,57)
Pos2 = CPos.New(90,20)
Pos3 = CPos.New(109,52)
Pos4 = CPos.New(47,86)
Pos5 = CPos.New(72,88)

DebugRefGroupPos = {Pos0, Pos1, Pos2, Pos3, Pos4, Pos5}

DebugCode = function()
	DebugWall1.Destroy()
	DebugWall2.Destroy()
	--[[
	Utils.Do(DebugRefGroupPos, function(a)
		Actor.Create(DebugRefGroup[1], true, { Owner = player, Location = a})
		Actor.Create(DebugRefGroup[2], true, { Owner = player, Location = a})
		Actor.Create(DebugRefGroup[3], true, { Owner = player, Location = a})
	end)]]
	
	
	--Attacks
	
	--Extraenemy
	ProduceInfantry(extraenemy, {{}}, ExtraEnemyInfAttack)
	ProduceArmor(extraenemy, {{}}, 3, ExtraEnemyArmorAttack, DateTime.Seconds(12), DateTime.Seconds(17))
	
	--BadGuy
	--LeftOoMAtk()
	ProduceInfantry(badguy, RightBaseAtkTargets, BadGuyInfAttack)
	ProduceArmor(badguy, RightBaseAtkTargets, 5, BadGuyArmorAttack, DateTime.Seconds(12), DateTime.Seconds(17))
	--ProduceAircraft(badguy)
	--[[]]
	
	--Enemy
	ProduceInfantry(enemy, MainBaseAtkTargets, USSRInfAttack)
	ProduceArmor(enemy, MainBaseAtkTargets, 7, USSRArmorAttack, DateTime.Seconds(0), DateTime.Seconds(0))
	--ProduceAircraft(enemy)
	
	SetupRefineries(RefOutpost0)
	SetupRefineries(RefOutpost1)
	SetupRefineries(RefOutpost2)
	SetupRefineries(RefOutpost3)
	SetupRefineries(RefOutpost4)
	SetupRefineries(RefOutpost5)
	--ExtractionDestiny.Position = CPos.New(123,98)
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	extraenemy = Player.GetPlayer("ExtraUSSR")
	badguy = Player.GetPlayer("BadGuy")
	turkey = Player.GetPlayer("Turkey")
	prisoners = Player.GetPlayer("Prisoners")
	
	InitTriggers()
	InitObjectives()
	
	AirplaneFlavor()
	
	TimerColor = player.Color
end