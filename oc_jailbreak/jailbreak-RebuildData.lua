
changeActors = false

--[[TONS OF REFERENCES]]
--SMOKE AND MIRRORS


BaseUnits =
	{
		{
			{Utype 	= "3tnk", firstPos = CPos.New(89,80)	,finalPos = CPos.New(89,79)		},
			{Utype 	= "3tnk", firstPos = CPos.New(93,80)	,finalPos = CPos.New(92,81)		},
			{Utype 	= "3tnk", firstPos = CPos.New(110,71)	,finalPos = CPos.New(109,70)	},
			{Utype 	= "3tnk", firstPos = CPos.New(114,71)	,finalPos = CPos.New(115,70)	},
			{Utype 	= "3tnk", firstPos = CPos.New(114,75)	,finalPos = CPos.New(113,76)	}
		},
		{
			{Utype 	= "3tnk", firstPos = CPos.New(99,84)	,finalPos = CPos.New(115,81)	},
			{Utype 	= "3tnk", firstPos = CPos.New(103,84)	,finalPos = CPos.New(117,83)	},
			{Utype 	= "3tnk", firstPos = CPos.New(100,92)	,finalPos = CPos.New(90,93)		},
			{Utype 	= "3tnk", firstPos = CPos.New(102,92)	,finalPos = CPos.New(90,99)		},
			{Utype 	= "3tnk", firstPos = CPos.New(102,92)	,finalPos = CPos.New(103,97)	}
		},
		{	--INFANTRY UNSOLVED
			{Utype 	= "e1",	firstPos = CPos.New(99,84)		,finalPos = CPos.New(115,81)	},
			{Utype 	= "e2", firstPos = CPos.New(103,84)		,finalPos = CPos.New(117,83)	},
			{Utype 	= "e2", firstPos = CPos.New(100,92)		,finalPos = CPos.New(90,93)		},
			{Utype 	= "e1", firstPos = CPos.New(102,92)		,finalPos = CPos.New(90,99)		},
			{Utype 	= "e1", firstPos = CPos.New(102,92)		,finalPos = CPos.New(103,97)	}
		},
		{
			{Utype 	= "3tnk", firstPos = CPos.New(114,96)	,finalPos = CPos.New(114,95)	},
			{Utype	= "3tnk", firstPos = CPos.New(105,110)	,finalPos = CPos.New(105,111)	},
			{Utype	= "v2rl", firstPos = CPos.New(86,51)	,finalPos = CPos.New(86,52)		},
			{Utype	= "v2rl", firstPos = CPos.New(88,89)	,finalPos = CPos.New(88,90)		},
			{Utype 	= "v2rl", firstPos = CPos.New(109,75)	,finalPos = CPos.New(109,76)	}
			
		},
		{	--INFANTRY UNSOLVED
			{Utype 	= "e1",	firstPos = CPos.New(99,84)		,finalPos = CPos.New(115,81)	},
			{Utype 	= "e2", firstPos = CPos.New(103,84)		,finalPos = CPos.New(117,83)	},
			{Utype 	= "e2", firstPos = CPos.New(100,92)		,finalPos = CPos.New(90,93)		},
			{Utype 	= "e1", firstPos = CPos.New(102,92)		,finalPos = CPos.New(90,99)		},
			{Utype 	= "e1", firstPos = CPos.New(102,92)		,finalPos = CPos.New(103,97)	}
		},
		{
			{Utype 	= "v2rl", firstPos = CPos.New(99,102)	,finalPos = CPos.New(99,103)	},
			{Utype 	= "ttnk", firstPos = CPos.New(88,79)	,finalPos = CPos.New(87,78)		},
			{Utype 	= "ttnk", firstPos = CPos.New(112,79)	,finalPos = CPos.New(113,78)	},
			{Utype 	= "ttnk", firstPos = CPos.New(94,94)	,finalPos = CPos.New(93,94)		},
			{Utype 	= "ttnk", firstPos = CPos.New(113,98)	,finalPos = CPos.New(114,99)	}		
			
		},
		{	--INFANTRY UNSOLVED
			{Utype 	= "e1",	firstPos = CPos.New(99,84)		,finalPos = CPos.New(115,81)	},
			{Utype 	= "e2", firstPos = CPos.New(103,84)		,finalPos = CPos.New(117,83)	},
			{Utype 	= "e2", firstPos = CPos.New(100,92)		,finalPos = CPos.New(90,93)		},
			{Utype 	= "e1", firstPos = CPos.New(102,92)		,finalPos = CPos.New(90,99)		},
			{Utype 	= "e1", firstPos = CPos.New(102,92)		,finalPos = CPos.New(103,97)	}
		},
		{
			{Utype 	= "4tnk", firstPos = CPos.New(99,84)	,finalPos = CPos.New(98,85)		},
			{Utype 	= "4tnk", firstPos = CPos.New(103,84)	,finalPos = CPos.New(104,85)	},
			{Utype 	= "4tnk", firstPos = CPos.New(100,92)	,finalPos = CPos.New(100,93)	},
			{Utype 	= "4tnk", firstPos = CPos.New(102,92)	,finalPos = CPos.New(102,93)	}
		}
	}

ChangeEnemyBaseActors = function()
	
	BasePower1 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower1.Location })
	BasePower2 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower2.Location })
	BasePower3 = Actor.Create ("powr", true, { Owner = enemy, Location = RemovableBasePower3.Location })
	BasePower4 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower4.Location })
	BasePower5 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower5.Location })
	BasePower6 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower6.Location })
	BasePower7 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower7.Location })
	BasePower8 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower8.Location })
	BasePower9 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower9.Location })
	BasePower10 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower10.Location })
	BasePower11 = Actor.Create ("apwr", true, { Owner = enemy, Location = RemovableBasePower11.Location })
	BasePower12 = Actor.Create ("powr", true, { Owner = enemy, Location = RemovableBasePower12.Location })
	
	BaseTesla1 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla1.Location })
	BaseTesla2 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla2.Location })
	BaseTesla3 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla3.Location })
	BaseTesla4 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla4.Location })
	BaseTesla5 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla5.Location })
	BaseTesla6 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla6.Location })
	BaseTesla7 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla7.Location })
	BaseTesla8 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla8.Location })
	BaseTesla9 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla9.Location })
	BaseTesla10 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla10.Location })
	BaseTesla11 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla11.Location })
	BaseTesla12 = Actor.Create ("tsla", true, { Owner = enemy, Location = RemovableBaseTesla12.Location })
	
	BaseSams1 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams1.Location })
	BaseSams2 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams2.Location })
	BaseSams3 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams3.Location })
	BaseSams4 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams4.Location })
	BaseSams5 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams5.Location })
	BaseSams6 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams6.Location })
	BaseSams7 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams7.Location })
	BaseSams8 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams8.Location })
	BaseSams9 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams9.Location })
	BaseSams10 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams10.Location })
	BaseSams11 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams11.Location })
	BaseSams12 = Actor.Create ("sam", true, { Owner = enemy, Location = RemovableBaseSams12.Location })
	
	
	Utils.Do(PowerForInfiltration, function(actor)
		actor.Destroy()
	end)
	Utils.Do(DefencesForEscape, function(actor)
		actor.Destroy()
	end)
	changeActors = true
end

Proc1 = {type = "proc", pos = CVec.New(-16,-15), cost = 1400, exists = true}
Proc2 = {type = "proc", pos = CVec.New(-10,-15), cost = 1400, exists = true}
Proc3 = {type = "proc", pos = CVec.New(14,-14), cost = 1400, exists = true}
Proc4 = {type = "proc", pos = CVec.New(17,-13), cost = 1400, exists = true}
Barr1 = {type = "barr", pos = CVec.New(-7,-10), cost = 400, exists = true}
Barr2 = {type = "barr", pos = CVec.New(5,-10), cost = 400, exists = true}
Weap1 = {type = "weap", pos = CVec.New(-3,-9), cost = 2000, exists = true}
Weap2 = {type = "weap", pos = CVec.New(1,-9), cost = 2000, exists = true}
		
Power1 = {type = "apwr", pos = CVec.New(-7,3), cost = 500, exists = true}
Power2 = {type = "apwr", pos = CVec.New(-7,11), cost = 500, exists = true}
Power3 = {type = "powr", pos = CVec.New(-7,14), cost = 300, exists = true}
Power4 = {type = "apwr", pos = CVec.New(-4,11), cost = 500, exists = true}
Power5 = {type = "apwr", pos = CVec.New(-5,14), cost = 500, exists = true}
Power6 = {type = "apwr", pos = CVec.New(5,3), cost = 500, exists = true}
Power7 = {type = "apwr", pos = CVec.New(14,3), cost = 500, exists = true}
Power8 = {type = "apwr", pos = CVec.New(14,11), cost = 500, exists = true}
Power9 = {type = "apwr", pos = CVec.New(15,14), cost = 500, exists = true}
Power10 = {type = "apwr", pos = CVec.New(17,3), cost = 500, exists = true}
Power11 = {type = "apwr", pos = CVec.New(17,11), cost = 500, exists = true}
Power12 = {type = "powr", pos = CVec.New(18,14), cost = 300, exists = true}
Power13 = {type = "powr", pos = CVec.New(-15,-2), cost = 300, exists = true}
Power14 = {type = "apwr", pos = CVec.New(-13,-2), cost = 500, exists = true}
Power15 = {type = "powr", pos = CVec.New(-7,-2), cost = 300, exists = true}
Power16 = {type = "apwr", pos = CVec.New(-5,-2), cost = 500, exists = true}
Power17 = {type = "apwr", pos = CVec.New(3,-2), cost = 500, exists = true}
Power18 = {type = "powr", pos = CVec.New(6,-2), cost = 300, exists = true}
Power19 = {type = "powr", pos = CVec.New(12,-2), cost = 300, exists = true}
Power20 = {type = "apwr", pos = CVec.New(14,-2), cost = 500, exists = true}
Power21 = {type = "powr", pos = CVec.New(17,-2), cost = 300, exists = true}
		
Dome = {type = "dome", pos = CVec.New(5,-14), cost = 1400, exists = true }
		
Silo1 = {type = "silo", pos = CVec.New(-6,-14), cost = 150, exists = true }
Silo2 = {type = "silo", pos = CVec.New(-5,-14), cost = 150, exists = true }
Silo3 = {type = "silo", pos = CVec.New(-4,-14), cost = 150, exists = true }
Silo4 = {type = "silo", pos = CVec.New(-6,-12), cost = 150, exists = true }
Silo5 = {type = "silo", pos = CVec.New(-5,-12), cost = 150, exists = true }
Silo6 = {type = "silo", pos = CVec.New(-4,-12), cost = 150, exists = true }
Silo7 = {type = "silo", pos = CVec.New(17,-14), cost = 150, exists = true }
Silo8 = {type = "silo", pos = CVec.New(18,-14), cost = 150, exists = true }
Silo9 = {type = "silo", pos = CVec.New(19,-14), cost = 150, exists = true }

Hpad1 = {type = "hpad", pos = CVec.New(-4,3), cost = 100, exists = false }
Hpad2 = {type = "hpad", pos = CVec.New(3,3), cost = 100, exists = false }
Fix = {type = "fix", pos = CVec.New(-1,-5), cost = 100, exists = true }
Kenn1 = {type = "kenn", pos = CVec.New(-5,-9), cost = 100, exists = true }
Kenn2 = {type = "kenn", pos = CVec.New(7,-9), cost = 100, exists = true }
Tech = {type = "stek", pos = CVec.New(-1,14), cost = 100, exists = true }

Tesla1 = {type = "tsla", pos = CVec.New(-16,-11), cost = 1500, exists = true }
Tesla2 = {type = "tsla", pos = CVec.New(-3,-15), cost = 1500, exists = true }
Tesla3 = {type = "tsla", pos = CVec.New(6,-15), cost = 1500, exists = true }
Tesla4 = {type = "tsla", pos = CVec.New(16,-15), cost = 1500, exists = true }
		
Tesla5 = {type = "tsla", pos = CVec.New(-16,0), cost = 1500, exists = true } 
Tesla6 = {type = "tsla", pos = CVec.New(-6,1), cost = 1500, exists = true } 
Tesla7 = {type = "tsla", pos = CVec.New(6,1), cost = 1500, exists = true } 
Tesla8 = {type = "tsla", pos = CVec.New(13,1), cost = 1500, exists = true } 
		
Tesla9 = {type = "tsla", pos = CVec.New(-8,11), cost = 1500, exists = true } 
Tesla10 = {type = "tsla", pos = CVec.New(0,19), cost = 1500, exists = true } 
Tesla11 = {type = "tsla", pos = CVec.New(6,17), cost = 1500, exists = true } 
Tesla12 = {type = "tsla", pos = CVec.New(12,16), cost = 1500, exists = true } 
		
Ftur1 = {type = "ftur", pos = CVec.New(-19,-3), cost = 750, exists = true } 
Ftur2 = {type = "ftur", pos = CVec.New(-19,-9), cost = 750, exists = true } 
Ftur3 = {type = "ftur", pos = CVec.New(8,-18), cost = 750, exists = true } 
Ftur4 = {type = "ftur", pos = CVec.New(14,-18), cost = 750, exists = true } 

Afld1 = {type = "afld", pos = CVec.New(-2,-15), cost = 750, exists = false }
Afld2 = {type = "afld", pos = CVec.New(1,-15), cost = 750, exists = false }
Afld3 = {type = "afld", pos = CVec.New(-2,-13), cost = 750, exists = false }
Afld4 = {type = "afld", pos = CVec.New(1,-13), cost = 750, exists = false }

Sams1 = {type = "sam", pos = CVec.New(-15,-9), cost = 750, exists = true }
Sams2 = {type = "sam", pos = CVec.New(-8,-15), cost = 750, exists = true }
Sams3 = {type = "sam", pos = CVec.New(4,-15), cost = 750, exists = true }
Sams4 = {type = "sam", pos = CVec.New(17,-15), cost = 750, exists = true }
Sams5 = {type = "sam", pos = CVec.New(-15,-3), cost = 750, exists = true }
Sams6 = {type = "sam", pos = CVec.New(-15,1), cost = 750, exists = true }
Sams7 = {type = "sam", pos = CVec.New(-4,1), cost = 750, exists = true }
Sams8 = {type = "sam", pos = CVec.New(3,1), cost = 750, exists = true }
Sams9 = {type = "sam", pos = CVec.New(16,1), cost = 750, exists = true }
Sams10 = {type = "sam", pos = CVec.New(-7,10), cost = 750, exists = true }
Sams11 = {type = "sam", pos = CVec.New(-2,19), cost = 750, exists = true }
Sams12 = {type = "sam", pos = CVec.New(13,16), cost = 750, exists = true }

Spen1 = {type = "spen", pos = CVec.New(-3,-28), cost = 750, exists = true }
Spen2 = {type = "spen", pos = CVec.New(12,-24), cost = 750, exists = true }
Spen3 = {type = "spen", pos = CVec.New(17,-22), cost = 750, exists = true }

BaseBuildings = 
	{
	--First Batch
	Power12,
	Barr2,
	Power21,
	Weap2,
	Proc4,
	Power11,
	Kenn2,
	
	--Second Batch
	Dome,
	Power21,
	Tesla3,
	Power10,
	Tesla6,
	Sams7,
	
	--Third Batch
	Power9,
	Tesla5,
	Sams3,
	Proc3,
	Silo7,
	Ftur3,
	
	--Fourth Batch
	Power17,
	Sams8,
	Power18,
	Power19,
	Silo8,
	Silo9,
	
	--Fifth Batch
	Tesla7,
	Power6,
	Tesla11,
	Power7,
	Sams12,
	Ftur1,
	Power8,
	
	--Sixth Batch
	Power1,
	Sams10,
	Power2,
	Proc2,
	Power3,
	Barr1,
	Weap1,
	
	--Seventh Batch
	Ftur2,
	Power4,
	Tesla1,
	Power5,
	Tesla2,
	Tesla4,
	
	--Eighth Bacth
	Ftur4,
	Power13,
	Tesla9,
	Sams1,
	Power14,
	Tesla8,
	Sams5,
	Sams9,
	
	--Ninth Batch
	Fix,
	Power15,
	Afld1,
	Afld2,
	Tech,
	Sams4,
	Kenn1,
	Power16,
	
	--Tenth Batch
	Hpad1,
	Hpad2,
	Tesla10,
	Sams2,
	Tesla12,
	Sams11,
	Afld3,
	Afld4,
	Sams6,
	
	--Eleventh Batch
	Silo1,
	Silo2,
	Silo3,
	Proc1,
	Silo4,
	Silo5,
	Silo6,
	
	Spen1,
	Spen2,
	Spen3
	}

--[[TONS OF REFERENCES]] --POWER
Trigger.OnKilled(BasePower13, function()
	Power13.exists = false
end)
Trigger.OnKilled(BasePower14, function()
	Power14.exists = false
end)
Trigger.OnKilled(BasePower15, function()
	Power15.exists = false
end)
Trigger.OnKilled(BasePower16, function()
	Power16.exists = false
end)
Trigger.OnKilled(BasePower17, function()
	Power17.exists = false
end)
Trigger.OnKilled(BasePower18, function()
	Power18.exists = false
end)
Trigger.OnKilled(BasePower19, function()
	Power19.exists = false
end)
Trigger.OnKilled(BasePower20, function()
	Power20.exists = false
end)
Trigger.OnKilled(BasePower21, function()
	Power21.exists = false
end)

if changeActors == true then
	Trigger.OnKilled(BasePower1, function()
		Power1.exists = false
	end)
	Trigger.OnKilled(BasePower2, function()
		Power2.exists = false
	end)
	Trigger.OnKilled(BasePower3, function()
		Power3.exists = false
	end)
	Trigger.OnKilled(BasePower4, function()
		Power4.exists = false
	end)
	Trigger.OnKilled(BasePower5, function()
		Power5.exists = false
	end)
	Trigger.OnKilled(BasePower6, function()
		Power6.exists = false
	end)
	Trigger.OnKilled(BasePower7, function()
		Power7.exists = false
	end)
	Trigger.OnKilled(BasePower8, function()
		Power8.exists = false
	end)
	Trigger.OnKilled(BasePower9, function()
		Power9.exists = false
	end)
	Trigger.OnKilled(BasePower10, function()
		Power10.exists = false
	end)
	Trigger.OnKilled(BasePower11, function()
		Power11.exists = false
	end)
	Trigger.OnKilled(BasePower12, function()
		Power12.exists = false
	end)

	--[[TONS OF REFERENCES]]--DEFENSES

	Trigger.OnKilled(BaseTesla1, function()
		Tesla1.exists = false
	end)
	Trigger.OnKilled(BaseTesla2, function()
		Tesla2.exists = false
	end)
	Trigger.OnKilled(BaseTesla3, function()
		Tesla3.exists = false
	end)
	Trigger.OnKilled(BaseTesla4, function()
		Tesla4.exists = false
	end)
	Trigger.OnKilled(BaseTesla5, function()
		Tesla5.exists = false
	end)
	Trigger.OnKilled(BaseTesla6, function()
		Tesla6.exists = false
	end)
	Trigger.OnKilled(BaseTesla7, function()
		Tesla7.exists = false
	end)
	Trigger.OnKilled(BaseTesla8, function()
		Tesla8.exists = false
	end)
	Trigger.OnKilled(BaseTesla9, function()
		Tesla9.exists = false
	end)
	Trigger.OnKilled(BaseTesla10, function()
		Tesla10.exists = false
	end)
	Trigger.OnKilled(BaseTesla11, function()
		Tesla11.exists = false
	end)
	Trigger.OnKilled(BaseTesla12, function()
		Tesla12.exists = false
	end)
	Trigger.OnKilled(BaseSams1, function()
		Sams1.exists = false
	end)
	Trigger.OnKilled(BaseSams2, function()
		Sams2.exists = false
	end)
	Trigger.OnKilled(BaseSams3, function()
		Sams3.exists = false
	end)
	Trigger.OnKilled(BaseSams4, function()
		Sams4.exists = false
	end)
	Trigger.OnKilled(BaseSams5, function()
		Sams5.exists = false
	end)
	Trigger.OnKilled(BaseSams6, function()
		Sams6.exists = false
	end)
	Trigger.OnKilled(BaseSams7, function()
		Sams7.exists = false
	end)
	Trigger.OnKilled(BaseSams8, function()
		Sams8.exists = false
	end)
	Trigger.OnKilled(BaseSams9, function()
		Sams9.exists = false
	end)
	Trigger.OnKilled(BaseSams10, function()
		Sams10.exists = false
	end)
	Trigger.OnKilled(BaseSams11, function()
		Sams11.exists = false
	end)
	Trigger.OnKilled(BaseSams12, function()
		Sams12.exists = false
	end)
end

Trigger.OnKilled(BaseFtur1, function()
	Ftur1.exists = false
end)
Trigger.OnKilled(BaseFtur2, function()
	Ftur2.exists = false
end)
Trigger.OnKilled(BaseFtur3, function()
	Ftur3.exists = false
end)
Trigger.OnKilled(BaseFtur4, function()
	Ftur4.exists = false
end)

--[[TONS OF REFERENCES]]--PRODUCTION
Trigger.OnKilled(BaseBarr1, function()
	Barr1.exists = false
end)
Trigger.OnKilled(BaseBarr2, function()
	Barr2.exists = false
end)
Trigger.OnKilled(BaseWeap1, function()
	Weap1.exists = false
end)
Trigger.OnKilled(BaseWeap2, function()
	Weap2.exists = false
end)
Trigger.OnKilled(BaseProc1, function()
	Proc1.exists = false
end)
Trigger.OnKilled(BaseProc2, function()
	Proc2.exists = false
end)
Trigger.OnKilled(BaseProc3, function()
	Proc3.exists = false
end)
Trigger.OnKilled(BaseProc4, function()
	Proc4.exists = false
end)
Trigger.OnKilled(BaseKenn1, function()
	Kenn1.exists = false
end)
Trigger.OnKilled(BaseKenn2, function()
	Kenn2.exists = false
end)

--[[TONS OF REFERENCES]]--EXTRAS
Trigger.OnKilled(BaseDome, function()
	Dome.exists = false
end)
Trigger.OnKilled(BaseTech, function()
	Tech.exists = false
end)
Trigger.OnKilled(BaseFix, function()
	Fix.exists = false
end)
Trigger.OnKilled(BaseSpen1, function()
	Spen1.exists = false
end)
Trigger.OnKilled(BaseSpen2, function()
	Spen2.exists = false
end)
Trigger.OnKilled(BaseSpen3, function()
	Spen3.exists = false
end)

--[[TONS OF REFERENCES]]--SILOS
Trigger.OnKilled(BaseSilo1, function()
	Silo1.exists = false
end)
Trigger.OnKilled(BaseSilo2, function()
	Silo2.exists = false
end)
Trigger.OnKilled(BaseSilo3, function()
	Silo3.exists = false
end)
Trigger.OnKilled(BaseSilo4, function()
	Silo4.exists = false
end)
Trigger.OnKilled(BaseSilo5, function()
	Silo5.exists = false
end)
Trigger.OnKilled(BaseSilo6, function()
	Silo6.exists = false
end)
Trigger.OnKilled(BaseSilo7, function()
	Silo7.exists = false
end)
Trigger.OnKilled(BaseSilo8, function()
	Silo8.exists = false
end)
Trigger.OnKilled(BaseSilo9, function()
	Silo9.exists = false
end)