-----------------------------------
--Code related to the first phase--
-----------------------------------

--PRISON ESCAPE
Prisoners1 = { "e1", "e1", "e1", "e1"}
Prisoners2 = { "e3", "e3", "medi" }	
Prisoners3 = { "thf", "thf" }							--North-Western Prison
GeneralPrisoner = { "gnrl", "spy" }						--South-Western Prison
PrisonCivilians = { "c1", "c2", "c3", "c4", "c5", "e1" }

OuterPrisonGuards = { ExpGuard1, ExpGuard2, ExpGuard3, ExpGuard4, ExpGuardKenn }
InnerPrisonGuards = { InsidePrisonGuard1, InsidePrisonGuard2, InsidePrisonGuard3, InsidePrisonGuard4 }
BaseGuards = { Guarding1, Guarding2, Guarding3, Guarding4, Guarding5, Guarding6, Guarding7 }

PowerForInfiltration = 
	{
	RemovableBasePower1,RemovableBasePower2,RemovableBasePower3,RemovableBasePower4,RemovableBasePower5,RemovableBasePower6,
	RemovableBasePower7,RemovableBasePower8,RemovableBasePower9,RemovableBasePower10,RemovableBasePower11,RemovableBasePower12
	}
	
DefencesForEscape =
	{
	RemovableBaseTesla1,RemovableBaseTesla2,RemovableBaseTesla3,RemovableBaseTesla4,RemovableBaseTesla5,RemovableBaseTesla6,
	RemovableBaseTesla7,RemovableBaseTesla8,RemovableBaseTesla9,RemovableBaseTesla10,RemovableBaseTesla11,RemovableBaseTesla12,
	RemovableBaseSams1,RemovableBaseSams2,RemovableBaseSams3,RemovableBaseSams4,RemovableBaseSams5,RemovableBaseSams6,RemovableBaseSams7,
	RemovableBaseSams8,RemovableBaseSams9,RemovableBaseSams10,RemovableBaseSams11,RemovableBaseSams12
	}

InfiltratableBuildings = 
	{ 
	RemovableBasePower1, RemovableBasePower2, RemovableBasePower3, RemovableBasePower4,
	RemovableBasePower5, RemovableBasePower6, RemovableBasePower7, RemovableBasePower8,
	RemovableBasePower9, RemovableBasePower10, RemovableBasePower11, RemovableBasePower12
	}

-----------
--BARRELS--
-----------
FirstBarrels = {FirstBarrels1, FirstBarrels2, FirstBarrels3}
SecondBarrels = {SecondBarrels1, SecondBarrels2, SecondBarrels3, SecondBarrels4, SecondBarrels5, SecondBarrels6}
ThirdBarrels = {ThirdBarrels1, ThirdBarrels2, ThirdBarrels3}
FourthBarrels = {FourthBarrels1, FourthBarrels2, FourthBarrels3}
FifthBarrels = {FifthBarrels1, FifthBarrels2, FifthBarrels3}
SixthBarrels = {SixthBarrels1, SixthBarrels2, SixthBarrels3, SixthBarrels4}

PrisonBarrels = {PrisonBarrels1, PrisonBarrels2, PrisonBarrels3, PrisonBarrels4 }
-----------
-----------

----------------
--PATROLS DATA--
----------------
BeachPatrol1 = { BeachPatrol2Inf1, BeachPatrol2Inf2, BeachPatrol2Inf3}
BeachPatrolPath1 = { BeachPatrolWay1.Location, BeachPatrolWay2.Location }
Patrol1 = { Patrol1Inf1, Patrol1Inf2, Patrol1Inf3 }
Patrol1Path = { DogPatrol1.Location, DogPatrol2.Location, DogPatrol3.Location, DogPatrol4.Location }

SecondPrisonUnitsA 	= { SecondPrisonUnitA1, SecondPrisonUnitA2 }
SecondPrisonPathA 	= { SecondPrisonPathA1, SecondPrisonPathA2 }
SecondPrisonUnitsB 	= { SecondPrisonUnitB1, SecondPrisonUnitB2 }
SecondPrisonPathB 	= { SecondPrisonPathB1, SecondPrisonPathB2 }

CP1PatrolUnits = {PatrolUnit1CP1, PatrolUnit2CP1}
CP1PatrolPaths = {PatrolPath1CP1.Location, PatrolPath2CP1.Location}

CP2PatrolUnits = {PatrolUnit1CP2, PatrolUnit2CP2}
CP2PatrolPaths = {PatrolPath1CP2.Location, PatrolPath2CP2.Location}

CP3PatrolUnits = {PatrolUnit1CP3, PatrolUnit2CP3}
CP3PatrolPaths = {PatrolPath1CP3.Location, PatrolPath2CP3.Location, PatrolPath3CP3.Location, PatrolPath2CP3.Location}

CP4PatrolUnits = {PatrolUnit1CP4, PatrolUnit2CP4, PatrolUnit3CP4}
CP4PatrolPaths = {PatrolPath1CP4.Location, PatrolPath2CP4.Location}

CP5PatrolUnits = {PatrolUnit1CP5, PatrolUnit2CP5}
CP5PatrolPaths = {PatrolPath1CP5.Location, PatrolPath2CP5.Location, PatrolPath3CP5.Location, PatrolPath2CP5.Location}

PatrolTime = DateTime.Seconds(3)

GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end

			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)

				if bool then
					stop = true

					i = i + 1
					if i > #waypoints then
						i = 1
					end

					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end

ActivateInitialPatrols = function()
	GroupPatrol(Patrol1, Patrol1Path, PatrolTime)
	GroupPatrol(BeachPatrol1, BeachPatrolPath1, PatrolTime)
end

ActivateEscapePatrols = function()
	GroupPatrol(CP1PatrolUnits,CP1PatrolPaths,PatrolTime)
	GroupPatrol(CP2PatrolUnits,CP2PatrolPaths,PatrolTime)
	GroupPatrol(CP3PatrolUnits,CP3PatrolPaths,PatrolTime)
	GroupPatrol(CP4PatrolUnits,CP4PatrolPaths,PatrolTime)
end

ActivateSecondPrisonPatrols = function()
	--GroupPatrol(SecondPrisonUnitsA, SecondPrisonPathA, PatrolTime)
	--GroupPatrol(SecondPrisonUnitsB, SecondPrisonPathB, PatrolTime)
end

---------------------------
---INTRO SEQUENCE START----
---------------------------
EnterSpy = function()
	local SubInf = Actor.Create("ss.infiltrator", true, { Owner = player, Location = SubWay1.Location } )
	SubInf.Move(SubWay2.Location)

	Trigger.OnEnteredFootprint(SubWay2Coords, function(a, id)
		Trigger.RemoveFootprintTrigger(id)
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			SpyInsert = Actor.Create(SpyType, true, { Owner = player, Location = SpyEntry.Location } )
			SpyInsert.Move(StartingCameraPos.Location)
			SpyMustSurvive()
		end)
		
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			SubInf.Move(SubWay3.Location)
		end)
		
	end)	
	Trigger.OnEnteredFootprint({CPos.New(109,128)}, function(a, id)
		Trigger.RemoveFootprintTrigger(id)
		SubInf.Destroy()
	end)
end
---------------------------
----INTRO SEQUENCE END-----
---------------------------

---------------------------
----INFILTRATION START-----
---------------------------
SpyMustSurvive = function()
	Trigger.OnKilled(SpyInsert, function()
		player.MarkFailedObjective(SpySurvive)
	end)
end

Trigger.OnEnteredFootprint(BaseEntry, function(a,id)	
	Trigger.RemoveFootprintTrigger(DateTime.Seconds(50))
	beachCamera = Actor.Create ("camera", true, { Owner = player, Location = BeachCameraLoc.Location })
	beachCameraActivation = true
	if not InitialCamera.IsDead then
		InitialCamera.Destroy()
	end
end)

Trigger.OnEnteredFootprint(PrisonEntrance, function(a,id)	
	Trigger.RemoveFootprintTrigger(id)
	if beachCameraActivation == true then
		beachCamera.Destroy()
	end
end)	

Trigger.OnAnyKilled(PrisonBarrels, function() 
	Utils.Do(OuterPrisonGuards, function(guard)
		if not guard.IsDead then
			guard.Kill()
		end
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()Media.PlaySoundNotification(player, "AlertBuzzer")end)
	Trigger.AfterDelay(DateTime.Seconds(3), function()Media.PlaySoundNotification(player, "AlertBuzzer")end)
	Trigger.AfterDelay(DateTime.Seconds(5), function()Media.PlaySoundNotification(player, "AlertBuzzer")end)
	
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		PreventEscape() 
	end)
end)

--EXPLODING EFFECTS START
PreventEscape = function()
	Utils.Do(BaseGuards, function(guard)
		if not guard.IsDead then
			guard.AttackMove(CPos.New(121,98))
			guard.AttackMove(CPos.New(124,98))
		end
	end)
	ActivateEscapePatrols()
end

Trigger.OnInfiltrated(BasePrison, function()
	JailBreakStart()
	OutageInitiationSetup()
	
	player.MarkCompletedObjective(SpySurvive)
end)

JailBreakStart = function()
	InfiltrationSuccessful = true
	
	Utils.Do(InnerPrisonGuards, function(actor)
		if not actor.IsDead then
			actor.Health = 1
		end
	end)

	Trigger.AfterDelay(DateTime.Seconds(3), function()Reinforcements.Reinforce(player, Prisoners1, PrisonWay1, 10) end)
	Trigger.AfterDelay(DateTime.Seconds(4), function()Reinforcements.Reinforce(player, Prisoners2, PrisonWay1, 10) end)
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		General = Reinforcements.Reinforce(player, GeneralPrisoner, PrisonWay1, 1) 
		GeneralMustSurvive() 
		player.MarkCompletedObjective(FreePrisoners)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()Reinforcements.Reinforce(prisoners, PrisonCivilians, CivPrisonersWay, 10) end)
	Trigger.AfterDelay(DateTime.Seconds(2), function()Reinforcements.Reinforce(prisoners, PrisonCivilians, CivPrisonersWay, 10) end)
	Trigger.AfterDelay(DateTime.Seconds(6), function()Reinforcements.Reinforce(prisoners, PrisonCivilians, CivPrisonersWay, 10) end)
	Trigger.AfterDelay(DateTime.Seconds(7), function()Reinforcements.Reinforce(prisoners, PrisonCivilians, CivPrisonersWay, 10) end)
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()Media.PlaySoundNotification(player, "AlertBuzzer")end)
	Trigger.AfterDelay(DateTime.Seconds(3), function()Media.PlaySoundNotification(player, "AlertBuzzer")end)
	Trigger.AfterDelay(DateTime.Seconds(5), function()Media.PlaySoundNotification(player, "AlertBuzzer")end)
	
	SetRescuableBase()
end
---------------------------
------INFILTRATION END-----
---------------------------

---------------------------
--EXPLODING BARRELS START--
---------------------------
Trigger.OnAnyKilled(SecondBarrels, function() 
	if not SecondBarrelsFtur.IsDead then
		SecondBarrelsFtur.Kill()
	end
end)

Trigger.OnAnyKilled(ThirdBarrels, function() 
	if not ThirdBarrelsFtur.IsDead then
		ThirdBarrelsFtur.Kill()
	end
end)

Trigger.OnAnyKilled(FourthBarrels, function() 
	if not ExpWallNew.IsDead then
		ExpWallNew.Kill() -- cambiar nombre a muro 
	end
end)

Trigger.OnAnyKilled(FifthBarrels, function()
	if not FifthBarrelsFtur.IsDead then
		FifthBarrelsFtur.Kill()
	end
end)

Trigger.OnAnyKilled(SixthBarrels, function() 
	if not SixthBarrelsFtur.IsDead then
		SixthBarrelsFtur.Kill()
	end
end)
---------------------------
---EXPLODING BARRELS END---
---------------------------
OutageInitiationSetup = function()
	Utils.Do(InfiltratableBuildings, function(building)
		Trigger.OnInfiltrated(building, function()
			if ComenceTimer == false then
				ComenceTimer = true
			end
		end)
	end)
end