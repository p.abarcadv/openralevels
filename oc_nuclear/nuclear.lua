if Map.LobbyOption("difficulty") == "easy" then
	--AttackIntervals = {2.5, 3}
	alertThreshold = 6
	totalMods = 2
elseif Map.LobbyOption("difficulty") == "normal" then
	--AttackIntervals = {2, 2.5}
	alertThreshold = 5
	totalMods = 3
else
	--AttackIntervals = {1, 1.5, 2}
	alertThreshold = 5
	totalMods = 4
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		AddAlertTick("We should make haste. They know of our arrival.")
	end)
end

alertValue = 0

--INFO
AddAlertTick = function(text)
	if not text then
		DisplayMsg(SelectRandomText())
	else
		DisplayMsg(text)
	end
	HUDColor = HSLColor.Yellow
	alertValue = alertValue + 1
	SelectModifier()
	Media.PlaySoundNotification(player, "AlertBuzzer")
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		HUDColor = player.Color
	end)
end

SelectRandomText = function()
	local rngText = Utils.Random(TextBarks)
	return rngText
end

DisplayMsg = function(msg, locutor)
	if not locutor then
		locutor = "Soldier"
	end
	Media.DisplayMessage(msg,locutor)
end
--INFO

Trigger.OnInfiltrated(radar2, function()
	CheckSecurityMeasures()
end)

Trigger.OnInfiltrated(radar3, function()
	CheckSecurityMeasures()
end)

tranPath1 = {tranReinforceEntry1.Location, tranReinforcePoint1.Location }
tranPath2 = {tranReinforceEntry2.Location, tranReinforcePoint2.Location }
tranPath3 = {tranReinforceEntry3.Location, tranReinforcePoint3.Location }
tranPath4 = {tranReinforceEntry4.Location, tranReinforcePoint4.Location }

SendSnipers = function()
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		local batch1 = Reinforcements.ReinforceWithTransport(enemy, "tran", {"sniper", "sniper", "sniper"}, tranPath1, {tranPath1[1]})
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			local batch2 = Reinforcements.ReinforceWithTransport(enemy, "tran", {"sniper", "sniper", "sniper"}, tranPath2, {tranPath2[1]})
			Trigger.AfterDelay(DateTime.Seconds(2), function()
				local batch3 = Reinforcements.ReinforceWithTransport(enemy, "tran", {"sniper", "sniper", "sniper"}, tranPath3, {tranPath3[1]})
				Trigger.AfterDelay(DateTime.Seconds(2), function()
					local batch4 = Reinforcements.ReinforceWithTransport(enemy, "tran", {"sniper", "sniper", "sniper"}, tranPath4, {tranPath4[1]})
				end)
			end)
		end)
	end)
end

SendTransports = function()
	
end

ScientistPath = 
{
FSPathA1.Location,
FSPathA2.Location,
FSPathA3.Location
}

MoveThroughWaypoints = function(a, waypoints)
	Utils.Move(waypoints, function(wy)
		a.Move(wy)
	end)
end

Tick = function()
	if alertValue >= alertThreshold then
		alertValue = -1
		HUDText = "The enemy is in full alert."
	end
	
	if player.HasNoRequiredUnits() then
		enemy.MarkCompletedObjective(DenyAllies)
	end
	
	UserInterface.SetMissionText("\n" .. HUDText .. alertValue .. 
	"\nSNIPERMOD ".. tostring(SniperMod.InEffect)..
	"\nPATROLMOD ".. tostring(PatrolMod.InEffect)..
	"\nTESLADMOD ".. tostring(TeslaDMod.InEffect)..
	"\nGUARDSMOD ".. tostring(GuardsMod.InEffect)..
	"\nVOLKOVMOD ".. tostring(VolkovMod.InEffect)
	, HUDColor)
	
	if Night == true then
		Lighting.Red = Lighting.Red-0.000125
		Lighting.Green = Lighting.Green-0.000075
		Lighting.Blue = Lighting.Blue+0.00025
		Lighting.Ambient = Lighting.Ambient-0.000275
		transition= transition+1
		if transition >= 2000 then
			Night = false
			transition = 0
			Lighting.Red = 0.75
			Lighting.Green = 0.85
			Lighting.Blue = 1.5
			Lighting.Ambient = 0.45
			Media.Debug("Night on time")
		end
	end
end

Night = true
transition = 0

HUDText = "Alert Status: "

--[[
Night = function()
	Lighting.Ambient = 1.0
	Lighting.Red = 1.0
	Lighting.Blue = 1.0
	Lighting.Green = 1.0
end]]

InitTriggers = function()
	Camera.Position = StartingCamera.CenterPosition
	AddAlertTick()
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		AddAlertTick()
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			AddAlertTick()
			Trigger.AfterDelay(DateTime.Seconds(2), function()
				AddAlertTick()
				Trigger.AfterDelay(DateTime.Seconds(2), function()
					AddAlertTick()
					
				end)
			end)
		end)
	end)
	--MoveThroughWaypoints(ScientistPath)
	
	FriendlyScientist.Move(FSPathA1.Location)
	FriendlyScientist.Move(FSPathA2.Location)
	FriendlyScientist.Move(FSPathA3.Location)
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	LocateNukes = player.AddPrimaryObjective("Locate Nuclear Facilities.") --x
	--BribeTheCivs = player.AddSecondaryObjective("Avoid attacking inhabitants of the area.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

SetupMood = function()
	Media.PlayMusic("search")
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	goodguy = Player.GetPlayer("GoodGuy")
	
	Media.Debug(Map.LobbyOption("difficulty"))
	
	InitTriggers()
	InitObjectives()
	SetupMood()
	
	HUDColor = player.Color
end