--LUA AI

SniperMod = {inEffect = false, message = "Snipers have been dispatched."}
GuardsMod = {inEffect = false, message = "Increased enemy squads patrolling the area."}
TeslaDMod = {inEffect = false, message = "Prototype 'Tesla Dreadnought' has been activated."}
PatrolMod = {inEffect = false, message = "Guard division are patrolling the area."}
VolkovMod = {inEffect = false, message = "Super soldier Volkov is surveilling the area."}

ModifiersList =
{
SniperMod,
GuardsMod,
TeslaDMod,
PatrolMod,
VolkovMod
}

ActiveModifiers = {}

TextBarks = 
{
"The soviets are looking for us.",
"The enemy knows of our presence.",
"They'll not be idle while we are here.",
"They are prepared for our arrival.",
"We must hurry. They know we are here.",
}
--[[]]
TextBarks2 =
{
{alert=1,{"The soviets are looking for us.","The enemy knows of our presence.","They'll not be idle while we are here.","They are prepared for our arrival.","We must hurry. They know we are here."}},
{alert=2,{"The soviets are looking for us.","The enemy knows of our presence.","They'll not be idle while we are here.","They are prepared for our arrival.","We must hurry. They know we are here."}},
{alert=3,{"The soviets are looking for us.","The enemy knows of our presence.","They'll not be idle while we are here.","They are prepared for our arrival.","We must hurry. They know we are here."}},
{alert=4,{"The soviets are looking for us.","The enemy knows of our presence.","They'll not be idle while we are here.","They are prepared for our arrival.","We must hurry. They know we are here."}},
{alert=5,{"The soviets are looking for us.","The enemy knows of our presence.","They'll not be idle while we are here.","They are prepared for our arrival.","We must hurry. They know we are here."}}
}

SelectModifier = function()
	local mod = Utils.Random(ModifiersList)
	--local mod = "Snipers"
	for i,m in ipairs(ModifiersList) do
		if m == mod then
			m.InEffect = true
			Media.Debug(m.message)
			table.insert(ActiveModifiers,m)
			table.remove(ModifiersList,i)
			--ActiveModifiers[] = m
		end
	end
end

CheckSecurityMeasures = function()
	for o, c in ipairs(ActiveModifiers) do
		--if c.inEffect == true then
			Media.DisplayMessage(c.message)
		--end
	end
end