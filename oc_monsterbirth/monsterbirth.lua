if Map.LobbyOption("difficulty") == "easy" then
	TimerTicks = DateTime.Seconds(600) -- 10 mins
	StartingUnits = { "shok", "shok", "shok", "shok" }
	AtkGroup = 
	{
	{"2tnk","jeep"}
	{"2tnk","1tnk"}
	} 
elseif Map.LobbyOption("difficulty") == "normal" then
	TimerTicks = DateTime.Seconds(540) -- 9 mins
	StartingUnits = { "shok", "shok", "shok", "shok" }
	AtkGroup = 
	{
	{"1tnk","1tnk","jeep"}
	{"2tnk","2tnk","1tnk"}
	} 
else
	TimerTicks = DateTime.Seconds(480) -- 8 mins
	StartingUnits = { "shok", "shok", "shok" }
	AtkGroup = 
	{
	{"2tnk","2tnk","jeep","jeep"}
	{"2tnk","1tnk","1tnk","arty"}
	} 
end

--540 - 9
--420 - 7
--360 - 6

ticked = TimerTicks

timerStarted = false
timerEnded = false

lstSent = false
lstType = "lst.special"

madTanks = {}
toBuild = {"qtnk"}

countT = 0
objectivelost = false

BaseCams = {Cam1, Cam2, Cam3, Cam4, Cam5, Cam6, Cam7}

MonsterBasePath = { MonsterPath2.Location, MonsterPath3.Location, MonsterPath4.Location, MonsterPath5.Location, MonsterPath6.Location }

MonsterTargets = { Destroyable1tnk, Destroyable2tnk, Destroyable3tnk, DestroyableGun }
CurrentWayPoint = 0

MonsterMapPath = 
{ 
MonsterPath5, MonsterPath7, MonsterAddPath1, MonsterAddPath2, MonsterAddPath3, MonsterPath8, MonsterPath9, MonsterPath10, MonsterPath11, MonsterPath12, MonsterPath13, MonsterPath14, MonsterPath15, MonsterPath16, MonsterPath17, MonsterPath18, MonsterPath19
}

Jammers = 
{
jammer1, jammer2, jammer3, jammer4
}

CameraSpawn1Coords = { CPos.New(48,39), CPos.New(48,38), CPos.New(49,38), CPos.New(49,37), CPos.New(50,37), CPos.New(52,35) }
CameraSpawn2Coords = {  }
CameraSpawn3Coords = {  }

NavalEntry = { NavalEntry1.Location, NavalEntry2.Location, NavalEntry3.Location }
NavalDst = { NavalDst1.Location, NavalDst2.Location, NavalDst3.Location }

StartingUnitsPath1 = { StartUnitsEntry.Location, StartUnitsDst.Location}
StartingUnitsPath2 = { StartUnitsEntry.Location, StartUnitsDst.Location + CVec.New(-1,0) }

DemitriPath = { DemitriEntry.Location, DemitriDst.Location }

TruksNum = 3
EvacuationTrucks = {truk1, truk2, truk3}

--[[VEHICLE ATTACKS]]
AtkHelis = { "heli.outofmap", "heli.outofmap" }

VehlAtkGroup = 
{
{"jeep", "2tnk"},
{"jeep", "1tnk"},
{"1tnk", "2tnk"},
{"2tnk", "2tnk"}
}
VehlAtkPaths =
{
{NEEntry.Location, NEDst.Location},
{SEEntry.Location, SEDst.Location},
{NWEntry.Location, NWDst.Location}
}

EntryMGG = 
{ 
{ EntryNorthWestMGG.Location, DstNorthWestMGG.Location },
{ EntrySouthWestMGG.Location, DstSouthWestMGG.Location },
{ EntryNorthEastMGG.Location, DstNorthEastMGG.Location }
}

AttackWave1 = 
{
atk1attacker1, atk1attacker2, atk1attacker3, atk1attacker4, atk1attacker5, atk1attacker6, atk1attacker9, atk1attacker10, atk1attacker11, atk1attacker12, atk1attacker13
}
AttackWave2 = {atk2attacker1, atk2attacker2, atk2attacker3, atk2attacker4, atk2attacker5, atk2attacker6, atk2attacker7}
AttackWave3 = {atk3attacker1, atk3attacker2}

Wave3Dst = { atk3location1, atk3location2 }

LSTActivationCoords = { CPos.New(10,19), CPos.New(19,10) }
LstTriggerCoords = { CPos.New(98,43), CPos.New(99,43), CPos.New(100,43), CPos.New(101,43) }

CreateBaseCameras = function()
	Utils.Do(BaseCams, function(camera_waypoint)
		Actor.Create ("camera", true, { Owner = player, Location = camera_waypoint.Location })
	end)
end

SetupTrucks = function()
	Utils.Do(EvacuationTrucks, function(t)
		Trigger.OnKilled(t, function()
			CountTrucks()
		end)
	end)
end

CountTrucks = function()
	countT = countT + 1
	if countT >= TruksNum then
		Media.PlaySpeechNotification(player, "ObjectiveNotMet")
		player.MarkFailedObjective(ProtectTrucks)
	end
end

Trigger.OnEnteredFootprint(CameraSpawn1Coords, function(a,id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		local camera = Actor.Create ("camera", true, { Owner = player, Location = CameraSpawn1.Location })
		Trigger.AfterDelay(DateTime.Seconds(10), function() camera.Destroy() end)
	end
end)

Trigger.OnEnteredFootprint(LstTriggerCoords, function(a,id)
	if a.Owner == player then
		Trigger.RemoveFootprintTrigger(id)
		SendEvacutationLst()
	end
end)

SendEvacutationLst = function()
	if lstSent == false then
		lstSent = true
		Reinforcements.Reinforce(player, {"lst.special"}, {NavalEntry2.Location, LstDestiny.Location}, 10)
	end
end

AlliedNavalEntry = function()
	local dst1 = Reinforcements.Reinforce(enemy, {"dd"}, {NavalEntry1.Location, NavalDst1.Location}, 10)[1]
	local dst2 = Reinforcements.Reinforce(enemy, {"dd"}, {NavalEntry2.Location, NavalDst2.Location}, 10)[1]
	local dst3 = Reinforcements.Reinforce(enemy, {"dd"}, {NavalEntry3.Location, NavalDst3.Location}, 10)[1]
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Reinforcements.Reinforce(badguy, {"tran.destroyable"}, {TranEntry.Location, TranDst.Location}, 10)
	end)
end

Trigger.OnEnteredProximityTrigger(evacuatePoint.CenterPosition, WDist.FromCells(4), function(a,id)
	if a.Owner == player and a.Type == "demitri" then
		Trigger.RemoveProximityTrigger(id)
		if TruksNum >= 1 then
			player.MarkCompletedObjective(EvacuateTrucks)
		end
		player.MarkCompletedObjective(EvacuateDemitri)
	end
end)

--INTRO SEQUENCE PART 1 -- SPY INFILTRATION
MonsterTankShowcase = function()

	local mtPath = MonsterBasePath 
	local targets = MonsterTargets
	
	MonsterTank.Move(MonsterPath1.Location)	
	Utils.Do(targets, function(target)
		MonsterTank.Attack(target, true, true)
	end)
	Utils.Do(mtPath, function(waypoint)
		MonsterTank.Move(waypoint)
	end)

	Trigger.OnKilled(DestroyableGun, function()
		if not ExplodableBarrel.IsDead then
			ExplodableBarrel.Kill()
			Media.DisplayMessage("Impressive! With this technology the Allies don't stand a chance.", "General")
			Trigger.AfterDelay(DateTime.Seconds(10), function()
				RemovableCam.Destroy()
				Media.DisplayMessage("Dr. Demitri has done a good job. Stalin will be pleased.", "General")
			CreateBaseCameras()
			end)
		end
	end)
	
	Trigger.OnEnteredFootprint({CPos.New(64,56)}, function(a, id)
		if a.Owner == supertank then
			Trigger.RemoveFootprintTrigger(id)
			Trigger.AfterDelay(DateTime.Seconds(2), function()
				SetMonsterTankState("off")
				SpyInfiltration() 
			end)
		end
	end)
end

--INTRO SEQUENCE PART 2 --SPY INFILTRATION
SpyInfiltration = function()
	Spy2.Infiltrate(Power4)
		
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Media.PlaySoundNotification(player, "AlertBuzzer")
		--Spy1.Infiltrate()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.DisplayMessage("The Alarm! What's the meaning of this!", "General")
		end)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(6), function()
		Media.DisplayMessage("The complex security has been breached sir.", "Soldier")
		Spy1.Attack(Guard1)
		Spy1.Attack(General)
		Media.PlaySoundNotification(player, "AlertBuzzer")
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(5), function()	
		Demitri = Reinforcements.Reinforce(player, {"demitri"}, DemitriPath, 10)[1]
		EvacuateDemitri = player.AddPrimaryObjective("Evacuate Dr. Demitri.")
		EvacuateTrucks = player.AddSecondaryObjective("Evacuate convoy with development data.")
		player.MarkCompletedObjective(OverseeDevelopment)

			if not Demitri.IsDead then
				Trigger.OnKilled(Demitri, function()
					player.MarkFailedObjective(EvacuateDemitri)
				end)
			else	
				player.MarkFailedObjective(EvacuateDemitri)
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(5), function()
			AlliedAttack()
			Media.DisplayMessage("Quickly! Protect the prototype!", "General")
		end)
	
	Trigger.AfterDelay(DateTime.Seconds(10), function()
		Media.DisplayMessage("Quickly! Protect the prototype!", "General")
		Media.PlaySoundNotification(player, "AlertBuzzer")
		local unitsA = Reinforcements.Reinforce(player, StartingUnits, StartingUnitsPath1, 10)
		local unitsB = Reinforcements.Reinforce(player, StartingUnits, StartingUnitsPath2, 10)
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Utils.Do(unitsA, function(unit)
				unit.AttackMove(ShokWaypoint1.Location)
				unit.Flash(20)
			end)
			Utils.Do(unitsB, function(unit)
				unit.AttackMove(ShokWaypoint2.Location)
				unit.Flash(20)
			end)
			Utils.Do(EvacuationTrucks, function(t)
				t.Owner = player
				t.Flash(20)
			end)
		end)
	end)
end

--INTRO SEQUENCE PART 3 -- ALLIED ATTACK
AlliedAttack = function()

	Reinforcements.Reinforce(enemy, {"mgg"}, {EntryNorthWestMGG.Location, DstNorthWestMGG.Location}, 1)
	Reinforcements.Reinforce(enemy, {"mgg"}, {EntrySouthWestMGG.Location, DstSouthWestMGG.Location}, 1)
	Reinforcements.Reinforce(enemy, {"mgg"}, {EntryNorthEastMGG.Location, DstNorthEastMGG.Location}, 1)
	
	AlliedNavalEntry()
	
	Utils.Do(AttackWave1, function(actor)
		actor.AttackMove(AttackBottom1.Location)
		actor.AttackMove(AttackBottom2.Location)
		actor.AttackMove(AttackFinalTarget.Location)
	end)
	Utils.Do(AttackWave2, function(actor)
		actor.AttackMove(AttackLeft1.Location)
		actor.AttackMove(AttackLeft2.Location)
	end)

	AttackWave3[1].Move(Wave3Dst[1].Location)
	AttackWave3[2].Move(Wave3Dst[2].Location)
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		StartMonsterTankEscort()
		Trigger.AfterDelay(DateTime.Seconds(30), function()
			CommenceOoMAttacks()
		end)
	end)
end

CommenceOoMAttacks = function()
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		SendGroundAtk(VehlAtkGroup[1], VehlAtkPaths[1])
	end)
	Trigger.AfterDelay(DateTime.Seconds(60), function()
		SendGroundAtk(VehlAtkGroup[2], VehlAtkPaths[1])
	end)
	Trigger.AfterDelay(DateTime.Seconds(120), function()
		SendGroundAtk(VehlAtkGroup[3], VehlAtkPaths[2])
	end)
	Trigger.AfterDelay(DateTime.Seconds(150), function()
		SendGroundAtk(VehlAtkGroup[1], VehlAtkPaths[2])
	end)
	Trigger.AfterDelay(DateTime.Seconds(180), function()
		SendGroundAtk(VehlAtkGroup[2], VehlAtkPaths[2])
	end)
	Trigger.AfterDelay(DateTime.Seconds(210), function()
		SendGroundAtk(VehlAtkGroup[3], VehlAtkPaths[3])
	end)
	Trigger.AfterDelay(DateTime.Seconds(240), function()
		SendGroundAtk(VehlAtkGroup[1], VehlAtkPaths[3])
	end)
	Trigger.AfterDelay(DateTime.Seconds(270), function()
		SendGroundAtk(VehlAtkGroup[2], VehlAtkPaths[3])
	end)
end


--INTRO SEQUENCE PART 4 --ALLIED ATTACK
StartMonsterTankEscort = function()
	timerStarted = true
	SetMonsterTankState("on")
	SetupMonsterTankMovement(MonsterMapPath)
	SetAndRemoveWaypoints()
end

--MonsterTank "AI"
--[[SET MOVE ROUTE]]
SetupMonsterTankMovement = function(waypoints)
	--mtPath = waypoints

	Utils.Do(waypoints, function(waypoint)
		MonsterTank.AttackMove(waypoint.Location)
	end)
end

SetAndRemoveWaypoints = function()
	Utils.Do(MonsterMapPath, function(coll)
		Trigger.OnEnteredProximityTrigger(coll.CenterPosition, WDist.FromCells(2), function(a, id)
			if a.Type == "5tnk" then
				table.remove(MonsterMapPath,1)
				Trigger.RemoveProximityTrigger(id)
			end
		end)
	end)
end

--PROBLEM WHEN JAMMERS ARE DESTROYED EARLY
SetupDisablers = function()
	Utils.Do(Jammers, function(actor)
		if actor == jammer3 then
			Trigger.OnEnteredProximityTrigger(actor.CenterPosition, WDist.FromCells(12), function(a, id)
				if a.Type == "5tnk" then
					Trigger.RemoveProximityTrigger(id)
					cam1 = Actor.Create ("camera", true, { Owner = player, Location = jamcam1.Location })
					Trigger.OnKilled(jammer3, function()
						Trigger.AfterDelay(DateTime.Seconds(10), function()
							cam1.Destroy()
						end)
					end)
				end
			end)
		end
		Trigger.OnEnteredProximityTrigger(actor.CenterPosition, WDist.FromCells(12), function(a, id)
			if a.Type == "5tnk" then
				SetMonsterTankState("off")
				Trigger.RemoveProximityTrigger(id)
				if Map.LobbyOption("difficulty") ~= "hard" then
					local camJam = Actor.Create ("camera.small", true, { Owner = player, Location = actor.Location })
					Trigger.OnKilled(actor, function()
						SetMonsterTankState("on")
						if camJam then
							camJam.Destroy()
						end
					end)
				else
					Trigger.OnKilled(actor, function()
						SetMonsterTankState("on")
					end)
				end
				
			end
		end)
	end)
end

--LOCATION OF MONSTER TANK DETECTION
Trigger.AfterDelay(DateTime.Seconds(10), function()
	--SendGroundAtk(VehlAtkGroup[1], VehlAtkPaths[1])
end)

--on
--off
SetMonsterTankState = function(cond)
	if not MonsterTank.IsDead then
		if cond == "on" then
			Media.DisplayMessage("--- Signal Acquired ---", "Computer")
			MonsterTank.Stance = "Defend"
			SetupMonsterTankMovement(MonsterMapPath)
		elseif cond == "off" then
			Media.DisplayMessage("--- Signal Lost ---", "Computer")
			MonsterTank.Stop()
			MonsterTank.Stance = "HoldFire"
		end
	end
end

IntroSequence = function()
	-- Monster Tank Showcase of Prowess
	-- Spy Attack
	-- Alarm
	-- Allied Attack
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		MonsterTankShowcase()
	end)
end

SendGroundAtk = function(units, path)
	local mobileGap = Reinforcements.Reinforce(enemy, {"mgg"}, path, 10)[1]
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		local tanks = Reinforcements.Reinforce(enemy, units, path, 10)
		Trigger.AfterDelay(DateTime.Seconds(4), function()
			Utils.Do(tanks, function(unit)
				unit.Guard(mobileGap)
			end)
		end)
	end)

	Trigger.AfterDelay(DateTime.Seconds(10), function()
		TrackSuperTank(mobileGap)
	end)
end

TrackSuperTank = function(unit)
	Trigger.OnIdle(unit, function()
		if not MonsterTank.IsDead then
			unit.Move(MonsterTank.Location)
		end
	end)
	Trigger.AfterDelay(DateTime.Seconds(5), function()
		if not unit.IsDead then
			TrackSuperTank(unit)
		end
	end)
end

CreateEnemyDefences = function(dir, origin)
	local x = 0
	local y = 0
	local limitX = 0
	local posOrigin = origin
	
	if dir == "top" then
		posOrigin = origin+CVec.New(-1,-1)
		limitX = 2
	elseif dir == "bot" then
		posOrigin = origin+CVec.New(-1,0)
		limitX = 2
	elseif dir == "left" then
		posOrigin = origin+CVec.New(-1,-1)
		limitX = 1
	else
		posOrigin = origin+CVec.New(0,-1)
		limitX = 1
	end
	
	for i=1, 6, 1 do 
		local iterator = CVec.New(x, y)
		if origin == posOrigin+iterator then
			Actor.Create ("pbox", true, { Owner = enemy, Location = origin })
		else
			Actor.Create ("sbag", true, { Owner = enemy, Location = posOrigin + iterator })
		end
		x=x+1
		if x>limitX then
			x=0
			y=y+1
		end
		--Actor.Create()
	end
end

FinishTimer = function()
	timerEnded = true
	for i = 0, 5, 1 do
		local c = TimerColor
		if i % 2 == 0 then
			c = HSLColor.White
		end
		
		Trigger.AfterDelay(DateTime.Seconds(i), function() UserInterface.SetMissionText("Super Tank has expired.", c) end)
	end
	Trigger.AfterDelay(DateTime.Seconds(6), function() UserInterface.SetMissionText("") end)
end

--[[]]
ProduceInfantry = function()
	local toBuild = Utils.Random("e1", "e2", "e4")

	Actor.Build(toBuild, function()
		
	end)

end

--ProduceMAD()

ProduceMAD = function()
	--Produce("qtnk", string factionVariant = nil, string productionType = nil)
	SWeap.Produce("qtnk")
	--Trigger.AfterDelay(DateTime.Seconds(10), function()
	--SWeap.Produce("qtnk", nil, "weap")
	--end)
	--SWeap.Produce("qtnk", nil, "weap")
	Media.Debug("Starting")
end

Tick = function()
	if timerStarted == true then
		if ticked > 0 then
			UserInterface.SetMissionText("Super Tank will self-destruct in " .. Utils.FormatTime(ticked), TimerColor)
			ticked = ticked - 1
		elseif ticked == 0 then
			FinishTimer()
			ticked = ticked - 1
			Trigger.AfterDelay(DateTime.Seconds(1), function() MonsterTank.Kill() end)
		end
	end
end

InitTriggers = function()
	Camera.Position = RemovableCam.CenterPosition
	badguy.Cash = 300
	Spy1.DisguiseAsType("e1",player)
	Spy2.DisguiseAsType("e1",player)
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		CreateEnemyDefences("right", GenPoint1.Location)
		CreateEnemyDefences("top", GenPoint2.Location)
		CreateEnemyDefences("bot", GenPoint3.Location)
		CreateEnemyDefences("bot", GenPoint4.Location)
		CreateEnemyDefences("bot", GenPoint5.Location)
		CreateEnemyDefences("bot", GenPoint6.Location)
	end)
	
	SetupDisablers()
	
	IntroSequence()

end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	OverseeDevelopment = player.AddPrimaryObjective("Oversee super tank prototype development.")
	
	DenySoviets = enemy.AddPrimaryObjective("Deny the soviets.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)

	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("USSR")
	enemy = Player.GetPlayer("Greece")
	badguy = Player.GetPlayer("BadGuy")
	supertank = Player.GetPlayer("Turkey")

	TimerColor = player.Color

	InitObjectives()
	InitTriggers()
end