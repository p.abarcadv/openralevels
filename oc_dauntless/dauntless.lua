if Map.LobbyOption("difficulty") == "easy" then
	--maybe have all 3 choppers
	TanyaType="e7"
elseif Map.LobbyOption("difficulty") == "normal" then
    TanyaType="e7.noautotarget"
	--maybe have all 3 choppers
else
	TanyaType="e7.noautotarget"
    --maybe have all 2 choppers
	--extraction is necessary
end

--For inf/veh production
ProgressValue = 0

Barr1Disabled = false
Barr2Disabled = false
Barr3Disabled = false
Barr4Disabled = false
Weap1Disabled = false

--defeatCond = false
tran = "tran.infiltration"

--Patrols
PatrolUnitsA = {PatrolAUnit1, PatrolAUnit2, PatrolAUnit3}
PatrolUnitsB = {PatrolBUnit1, PatrolBUnit2}
PatrolUnitsC = {PatrolCUnit1, PatrolCUnit2, PatrolCUnit3, PatrolCUnit4}
PatrolUnitsD = {PatrolDUnit1, PatrolDUnit2, PatrolDUnit3, PatrolDUnit4}
PatrolUnitsE = {PatrolEUnit1, PatrolEUnit2, PatrolEUnit3}

PatrolPathA = {PatrolAPath1.Location, PatrolAPath2.Location, PatrolAPath3.Location, PatrolAPath4.Location, PatrolAPath5.Location}
PatrolPathB = {PatrolBPath1.Location, PatrolBPath2.Location, PatrolBPath3.Location, PatrolBPath2.Location}
PatrolPathC = {PatrolCPath1.Location, PatrolCPath2.Location}
PatrolPathD = {PatrolDPath1.Location, PatrolDPath2.Location, PatrolDPath3.Location, PatrolDPath4.Location, PatrolDPath3.Location, PatrolDPath2.Location}
PatrolPathE = {PatrolEPath1.Location, PatrolEPath2.Location}

GuardingUnitsA = {AlertAUnit1, AlertAUnit2, AlertAUnit3}
GuardingUnitsB1 = {AlertBUnit1, AlertBUnit2, AlertBUnit3}
GuardingUnitsB2 = {AlertBUnit4, AlertBUnit5, AlertBUnit6}
GuardingUnitsC = {AlertCUnit1, AlertCUnit2, AlertCUnit3, AlertCUnit4}
GuardingUnitsD = {AlertDUnit1, AlertDUnit2, AlertDUnit3}

HeliAtk = {"mh60.attack", "heli.attack"}
HeliAtkPath1 = {Heli1Spawn.Location, Heli1Spawn.Location+CVec.New(-1,-1)}
HeliAtkPath2 = {Heli2Spawn.Location, Heli2Spawn.Location+CVec.New(-1,-1)}

Camera0Area = { CPos.New(52,72), CPos.New(52,73), CPos.New(52,74), CPos.New(52,81), CPos.New(52,82)}
Camera1Area = 
{
CPos.New(32,55), CPos.New(33,55), CPos.New(34,55), CPos.New(35,55), CPos.New(36,55), CPos.New(37,55), CPos.New(38,55), CPos.New(39,55), CPos.New(39,54)
}
Camera2Area = {CPos.New(60,36), CPos.New(60,37), CPos.New(60,50), CPos.New(60,51)}
Camera3Area = {CPos.New(82,50), CPos.New(82,51), CPos.New(77,54), CPos.New(78,54), CPos.New(79,54)}
LastBaseCoords = {CPos.New(110,57), CPos.New(111,57), CPos.New(95,55), CPos.New(95,56), CPos.New(95,57), CPos.New(95,58), CPos.New(95,59), CPos.New(95,60), CPos.New(95,61), CPos.New(95,62), CPos.New(95,63), CPos.New(95,64)}

TransportedUnits =
{
{"e1", "e1",  "e3",  "e3",  "e3"},
{"e1", "e1",  "e3",  "e3",  "e3"},
{TanyaType, "spy",  "spy",  "mech",  "mech"}
}

TransportPath =
{
{transpawn.Location, landingpoint1.Location},
{transpawn.Location+CVec.New(-2,0), landingpoint2.Location},
{transpawn.Location+CVec.New(0,-2), landingpoint3.Location}
}

BarrSquad =
{
{"e1","e1","e1"},		--squad easy
{"e1","e1","e2","e2"},	--squad normal
{"e4","e4","e4","e4"}	--squad hard
}

InfGuard = {}
VehGuard = {}

IntroSequence = function()
	Trigger.AfterDelay(DateTime.Seconds(1), function() 
		SendInitialHeliAtk(HeliAtk, HeliAtkPath1)
		SendInitialHeliAtk(HeliAtk, HeliAtkPath2)
	end)

	Trigger.AfterDelay(DateTime.Seconds(6), function() 
		SquadInsertion(TransportedUnits[1],TransportPath[1])
		SquadInsertion(TransportedUnits[2],TransportPath[2])
		SquadInsertion(TransportedUnits[3],TransportPath[3])
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(15), function() 
		local col_tanya = Utils.Where(Map.ActorsInWorld, function(a)
			return a.Type == "e7" or a.Type == "e7.noautotarget"
		end)
		
		Tanya = col_tanya[1]
		
		Trigger.AfterDelay(DateTime.Seconds(2), function()
			Trigger.OnKilled(Tanya, function()
				player.MarkFailedObjective(TanyaSurvive)
			end)
		end)
	end)
end

SendInitialHeliAtk = function(units, path)
	local helis = Reinforcements.Reinforce(player, units, path )
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		Utils.Do(helis, function(unit)
			unit.AttackMove(startsam3.Location)
		end)
	end)
end

DispatchHelis = function(target, units)
	local helis = Reinforcements.Reinforce(player, units, {Heli1Spawn.Location, Heli1Spawn.Location+CVec.New(-1,-1)} ) 
	
	Utils.Do(helis, function(unit)
		unit.Attack(target)
	end)
	Trigger.OnKilled(target, function()
		Utils.Do(helis, function(unit)
			if not unit.IsDead then
				unit.Move(Heli1Spawn.Location)
			end
		end)
	end)
end

Trigger.AfterDelay(DateTime.Seconds(60), function()
	
end)

Trigger.OnDamaged(startsam1, function(a)
	if a.Health <= 40000 and not a.IsDead then
		a.Kill()
	end
end)

Trigger.OnInfiltrated(Weap1, function()
	--Weap1Disabled = true
end)

SecondaryObj = function()
	local structures = Utils.Where(Map.ActorsInWorld, function(b)
		return b.HasProperty("StartBuildingRepairs") and b.Owner==enemy
	end)
	
	Trigger.OnAllKilled(structures, function()
		player.MarkCompletedObjective(DestroyBuildings)
	end)
end

--CAMERAS CREATION

Base0Switch = 0 -- 0 = waiting for activation, 1 = active -1 = inactive
Base1Switch = 0
Base2Switch = 0
Base3Switch = 0
Base3Switch = 0
--BASE 0
Trigger.OnEnteredFootprint(Camera0Area, function(a, id)
	if a.Owner == player then
		if a.Type ~= "spy" then
			Trigger.RemoveFootprintTrigger(id)
			if Base0Switch == 0 then
				Base0Switch = 1
			end
		end
		if not cam0 then
			cam0 = Actor.Create("camera", true, { Owner = player, Location = spawncam0.Location })
		end
	end
end)
--To Tick

--
Trigger.OnEnteredFootprint(Camera1Area, function(a, id)
	if a.Owner == player --[[and a.Type ~= "spy"]] then
		if a.Type ~= "spy" then
			Trigger.RemoveFootprintTrigger(id)
			if Base1Switch == 0 then
				Base1Switch = 1
			end
		end
		
		if not cam1 then
			cam1 = Actor.Create("camera", true, { Owner = player, Location = spawncam1.Location })
		end
	end
end)

Trigger.OnEnteredFootprint(Camera2Area, function(a, id)
	if a.Owner == player --[[and a.Type ~= "spy"]] then
		Trigger.RemoveFootprintTrigger(id)
		local cam1 = Actor.Create("camera", true, { Owner = player, Location = spawncam2.Location })
	end
end)
Trigger.OnEnteredFootprint(Camera3Area, function(a, id)
	if a.Owner == player --[[and a.Type ~= "spy"]] then
		Trigger.RemoveFootprintTrigger(id)
		local cam1 = Actor.Create("camera", true, { Owner = player, Location = spawncam3.Location })
		
		Barr3.IsPrimaryBuilding = true
		InfCreation(Barr3,BarrSquad[2])
	end
end)

Trigger.OnEnteredFootprint(LastBaseCoords, function(a, id)
	if a.Owner == player --[[and a.Type ~= "spy"]] then
		Trigger.RemoveFootprintTrigger(id)
		
		Barr4.IsPrimaryBuilding = true
		InfCreation(Barr4,BarrSquad[3])
	end
end)

--CAMERAS CREATION
Trigger.OnKilled(barrelFtur1, function()
	if not explodingFtur.IsDead then
		explodingFtur.Kill()
	end
end)

Trigger.OnKilled(ATek, function()
	player.MarkCompletedObjective(DestroyATek)
	if not Tanya.IsDead then
		player.MarkCompletedObjective(TanyaSurvive)
	end
end)

SquadInsertion = function(units, path)
	Trigger.AfterDelay(DateTime.Seconds(1), function() 
		--Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		Reinforcements.ReinforceWithTransport(player, tran, units, path, { path[1] })		
	end)
end

SendGuards = function(units)
	Utils.Do(units, function(unit)
		IdleHunt(unit)
	end)
end

InfCreation = function(barr, units)
	if barr.IsDead or barr.IsPrimaryBuilding == false then
		return
	end
	local toBuild = { Utils.Random(units) }
	local delay = DateTime.Seconds(3)
		
	enemy.Build(toBuild, function(trained)
		InfGuard[#InfGuard + 1] = trained[1]
		if #InfGuard >= 3 then
			Trigger.AfterDelay(DateTime.Seconds(1), function()
				Utils.Do(InfGuard, function(unit) IdleHunt(unit) end)
				InfGuard = {}
			end)
		else
			Trigger.AfterDelay(delay, function()
				InfCreation(barr, units)
			end)
		end
	end)
	Trigger.AfterDelay(DateTime.Seconds(45), function()
		InfCreation(barr, units)
	end)
end


GroupPatrol = function(units, waypoints, delay)
	local i = 1
	local stop = false

	Utils.Do(units, function(unit)
		Trigger.OnIdle(unit, function()
			if stop then
				return
			end

			if unit.Location == waypoints[i] then
				local bool = Utils.All(units, function(actor) return actor.IsIdle end)

				if bool then
					stop = true

					i = i + 1
					if i > #waypoints then
						i = 1
					end

					Trigger.AfterDelay(delay, function() stop = false end)
				end
			else
				unit.AttackMove(waypoints[i])
			end
		end)
	end)
end

ActivatePatrols = function()
	GroupPatrol(PatrolUnitsA, PatrolPathA, DateTime.Seconds(3))
	GroupPatrol(PatrolUnitsB, PatrolPathB, DateTime.Seconds(3))
	GroupPatrol(PatrolUnitsC, PatrolPathC, DateTime.Seconds(3))
    GroupPatrol(PatrolUnitsD, PatrolPathD, DateTime.Seconds(3))
    GroupPatrol(PatrolUnitsE, PatrolPathE, DateTime.Seconds(3))
end

IdleHunt = function(unit) if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end end

Tick = function()
	enemy.Cash = 10000
	
	if Base0Switch == 1 then
		Base0Switch = -1
		SendGuards(GuardingUnitsA)
	
		Barr1.IsPrimaryBuilding = true
		InfCreation(Barr1,BarrSquad[1])
	end
	
	if Base1Switch == 1 then
		Base1Switch = -1
		DispatchHelis(AttackableTesla1, {"heli.attack", "mh60.attack"})
		SendGuards(GuardingUnitsB1)
		Trigger.AfterDelay(DateTime.Seconds(12), function()
			SendGuards(GuardingUnitsB2)
		end)
		
		Barr2.IsPrimaryBuilding = true
		InfCreation(Barr2,BarrSquad[2])
	end
	
end

InitTriggers = function()
	Camera.Position = startcamspawn1.CenterPosition	
	
	local cam1 = Actor.Create("camera.medium", true, { Owner = player, Location = startcamspawn1.Location })
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		local cam2 = Actor.Create("camera.medium", true, { Owner = player, Location = startcamspawn2.Location })
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(1), function()
		IntroSequence()
	end)
	
	SecondaryObj()

	Trigger.AfterDelay(DateTime.Seconds(6), function()
		ActivatePatrols()
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(30), function()
		--ProductionFacilities()
	end)
end

InitObjectives = function()
	DestroyATek = player.AddPrimaryObjective("Find and destroy the Technology Center.")
	TanyaSurvive = player.AddPrimaryObjective("Tanya must survive.")
	DestroyBuildings = player.AddSecondaryObjective("Destroy all enemy buildings.")

	DenyAllies = enemy.AddPrimaryObjective("Deny the Allies.")
	
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionFailed")
		end)
	end)
	Trigger.OnPlayerWon(player, function()
		Trigger.AfterDelay(DateTime.Seconds(1), function()
			Media.PlaySpeechNotification(player, "MissionAccomplished")
		end)
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	
	InitTriggers()
	InitObjectives()
end