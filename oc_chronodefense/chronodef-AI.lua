if Map.LobbyOption("difficulty") == "easy" then
	SendMCVUSSR = true		--left
	SendMCVBadguy = false	--right
	
	sellDelay = DateTime.Minutes(5)
	
	--First Phase
	FirstPhaseAtkPlane = "yak"
	AtkAircraftDelay = DateTime.Seconds(180)
	OutpostAtkInterval = DateTime.Minutes(4)
	
elseif Map.LobbyOption("difficulty") == "normal" then
	SendMCVUSSR = true		--left
	SendMCVBadguy = false	--right
	
	sellDelay = DateTime.Minutes(1)
	
	--First Phase
	FirstPhaseAtkPlane = "mig"
	AtkAircraftDelay = DateTime.Seconds(140)
	OutpostAtkInterval = DateTime.Minutes(3)
	
else
	SendMCVUSSR = true		--left
	SendMCVBadguy = true	--right
	
	sellDelay = DateTime.Minutes(1)
	
	--First Phase
	FirstPhaseAtkPlane = "mig"
	AtkAircraftDelay = DateTime.Seconds(100)
	OutpostAtkInterval = DateTime.Minutes(2)
	
end

----------------------
--General Attack Start
----------------------
SovietInfantryTypes = { "e1", "e2", "e4" }
SovietVehicleTypes = { "ftrk", "ftrk", "3tnk" }
SovietAircraftTypes = { "yak", "mig" }

InfAtkCount = 7
ArmorAtkCount = 2

----------------------
--General Attack End
----------------------

----------------------
--Outpost Attack Start
----------------------
MidAtkPaths = 
	{
	{},
	{MidAtkPath2},
	{MidAtkPath2, AtkChronoDst}
	}

InitialInfAttack = { }
InitialArmorAttack = { }

AtkAircraftEntry = {AirAtkEntry1, AirAtkEntry2, AirAtkEntry3}

OoMAir = true
--------------------
--Outpost Attack End
--------------------

------------------------
--Main Base Attack Start
------------------------
InfLeftAttack = { }
InfRightAttack = { }

ArmorLeftAttack = { }
ArmorRightAttack = { }

PlaneLeftAttack = { }
PlaneRightAttack = { }
----------------------
--Main Base Attack End
----------------------

--------------------
--Paratroopers Start
--------------------
ParatroopDestinies = { ParatroopLz1, ParatroopLz2, ParatroopLz3 }
ParatroopDelay = 4
-------------------
--Paratroopers End
-------------------

ProductionInterval = 30
	--{
	--30,
	--0,--normal = DateTime.Seconds(15),
	--0--hard = DateTime.Seconds(5)
	--}


LeftAtkPaths = 
	{
		{LeftAtkPath1, LeftAtkPath2, MidAtkPath2},
		{LeftAtkPath1, LeftAtkPath2, AtkChronoDst},
		{LeftAtkPath1, LeftAtkPath2},
		{LeftAtkPath1, MidAtkPath1, MidAtkPath2},
		{LeftAtkPath1, MidAtkPath1},
		{LeftAtkPath1},
		{}
	}

RightAtkPaths = 
	{
		{RightAtkPath1, RightAtkPath2, MidAtkPath2},
		{RightAtkPath1, RightAtkPath2, AtkChronoDst},
		{RightAtkPath1, MidAtkPath1, AtkChronoDst},
		{MidAtkPath1, MidAtkPath2, AtkChronoDst},
		{MidAtkPath1, MidAtkPath1},
		{RightAtkPath1},
		{}
	}

OoMPlaneAtk = function()	--Out of map Plane Atk
	if OoMAir == true then
		Media.Debug("In OoMPlaneAtk")
		local pos = Utils.Random(AtkAircraftEntry)
		
		local atkPlane = Reinforcements.Reinforce(badguy, {FirstPhaseAtkPlane}, {pos.Location, pos.Location + CVec.New(0,2)}, 1)
		--Actor.Create( "mig", false, { Owner = badguy, Location = AirAtkEntry1.Location + CVec.New(0,5) } )
		Trigger.AfterDelay(1, function()
			TargetForPlanes(atkPlane[1])
		end)
		Trigger.AfterDelay(AtkAircraftDelay, OoMPlaneAtk)
	else
		Media.Debug("Out of OoMPlaneAtk")
		return
	end
end

TargetForPlanes = function(plane, target)
	if plane.IsDead then
		return
	end
	if not target or target.IsDead or (not target.IsInWorld) then
		local enemies = Utils.Where(Map.ActorsInWorld, function(u) 
			return u.Owner == player and u.HasProperty("Health") and plane.CanTarget(u) and u.Type ~= "sbag" and u.Type ~= "brik"
		end)
		if #enemies > 0 then
			target = Utils.Random(enemies)
		end
	end
	if target and plane.AmmoCount() > 0 and plane.CanTarget(target) then
		plane.Attack(target)
	else
		--if plane.Owner
		plane.ReturnToBase()
	end
	plane.CallFunc(function()
		Trigger.AfterDelay(1, function()
			TargetForPlanes(plane, target)
		end)
	end)
end

--[[
IdlingUnits = function()
	local lazyUnits = Utils.Where(Map.ActorsInWorld, function(actor)
		return actor.HasProperty("Hunt") and (actor.Owner == enemy) end)

	Utils.Do(lazyUnits, function(unit)
		Trigger.OnDamaged(unit, function()
			Trigger.ClearAll(unit)
			Trigger.AfterDelay(0, function() IdleHunt(unit) end)
		end)
	end)
end
]]

SellOutpost = function()
	Utils.Do(OutpostBuildings, function(building)
		if not building.IsDead and building.Owner ~= player then
			building.Sell()
			if building.Type == "weap" then
				MidWeap.exists = false
			elseif building.Type == "barr" then
				MidBarr.exists = false
			end
		end
	end)
end

SendParatroopers = function()
	Media.Debug("Paratroopers Sent.")
	local powerproxy = Actor.Create("powerproxy.paratroopers", false, { Owner = badguy })
	
	local destiny = Utils.Random(ParatroopDestinies)
	local plane = powerproxy.TargetParatroopers(destiny.CenterPosition, Angle.SouthWest)
	
	Media.Debug(tostring(destiny))
	
	Utils.Do(plane, function(a)
		Trigger.OnPassengerExited(a, function(t, p)
			IdleHunt(p)
		end)
	end)
end

BuildBase = function(collection, cyard, owner)
	for i,v in ipairs(collection) do
		if not v.exists then
			BuildBuilding(v, collection, cyard, owner)
			return
		end
	end
	Trigger.AfterDelay(DateTime.Seconds(10), function() 
		BuildBase(collection, cyard, owner)
	end)
end

BuildBuilding = function(building, collection, cyard, owner)
	--Media.Debug(tostring(Actor.BuildTime(building.type)) .. " seconds")
	Trigger.AfterDelay(DateTime.Seconds(10)--[[Actor.BuildTime(building.type)]], function()
		if cyard.IsDead or cyard.Owner ~= owner then
			return
		end
		local actor = Actor.Create(building.type, true, { Owner = owner, Location = cyard.Location + CVec.New(1,1) + building.pos })
		Media.Debug("S: " .. tostring(actor) .. "P: " .. tostring(owner))
		owner.Cash = owner.Cash - building.cost
		building.exists = true
		Trigger.OnKilled(actor, function() building.exists = false end)
		if building.type == "weap" then
			Trigger.AfterDelay(DateTime.Seconds(5), function()
				--Media.Debug("Weap Built")
				if owner == enemy then
					ProduceArmor( building, enemy, LeftAtkPaths, ArmorLeftAttack)
				else
					ProduceArmor( building, badguy, RightAtkPaths, ArmorRightAttack)
				end
			end)
		end
		if building.type == "barr" then
			Trigger.AfterDelay(DateTime.Seconds(5), function()
				--Media.Debug("Barr Built")
				if owner == enemy then
					ProduceInfantry( building, enemy, LeftAtkPaths, InfLeftAttack)
				else
					ProduceInfantry( building, badguy, RightAtkPaths, InfRightAttack)
				end
			end)
		end
		Trigger.OnDamaged(actor, function(building)
			if building.Owner == owner and building.Health < building.MaxHealth * 3/4 then
				building.StartBuildingRepairs()
			end
		end)
		Trigger.AfterDelay(DateTime.Seconds(10), function()
			BuildBase(collection, cyard, owner)
		end)
	end)
end

ProduceArmor = function(producer, owner, paths, col, intervalDelay)
	if not producer.exists --[[or producer.Owner ~= owner]] then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		--Media.Debug("ProduceArmor - Exit")
		return
	end
	--Media.Debug("1")
	if not intervalDelay then 
		intervalDelay = DateTime.Minutes(2)
	end
	
	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(SovietVehicleTypes) }
	local Path = Utils.Random(paths)
	--Media.Debug("2")
	owner.Build(toBuild, function(unit)
		--Media.Debug("3")
		
		col[#col + 1] = unit[1]
		if #col >= ArmorAtkCount then
			SendUnits(col, Path)
			col = { }
			Trigger.AfterDelay(intervalDelay, function()
				Media.Debug("Armor Sent." .. tostring(owner))
				ProduceArmor(producer, owner, paths, col, intervalDelay)
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceArmor(producer, owner, paths, col)
			end)
		end
	end)
end

ProduceInfantry = function(producer, owner, paths, col)
	if not producer.exists --[[or producer.Owner ~= owner]] then
	--[[	return
	elseif baseharv.IsDead and enemy.Resources <= 299 then]]
		--Media.Debug("ProduceInfantry - Exit")
		return
	end

	local delay = Utils.RandomInteger(DateTime.Seconds(3), DateTime.Seconds(9))
	local toBuild = { Utils.Random(SovietInfantryTypes) }
	local Path = Utils.Random(paths)
	owner.Build(toBuild, function(unit)
		col[#col + 1] = unit[1]
		if #col >= InfAtkCount then
			SendUnits(col, Path)
			col = { }
			Trigger.AfterDelay(DateTime.Seconds(30), function()
				Media.Debug("Infantry Sent." .. tostring(owner))
				ProduceInfantry(producer, owner, paths, col)
			end)
		else
			Trigger.AfterDelay(delay, function()
				ProduceInfantry(producer, owner, paths, col)
			end)
		end
	end)
end

--[[THIS IS REQUIRED FOR PLANES TO WORK]]
ProducePlanes = function(owner)
	--if not Airfield.exist or Airfield.Owner ~= owner then
		--return
	--end
	owner.Build(SovietAircraftType, function(units)
		local yak = units[1]
		Trigger.OnKilled(yak, ProduceAircraft)
		InitializeAttackAircraft(yak, Greece)
	end)
end
--[[THIS IS REQUIRED FOR PLANES TO WORK]]

SendUnits = function(units, waypoints)
	Utils.Do(units, function(unit)
		if not unit.IsDead then
			Utils.Do(waypoints, function(waypoint)
				unit.AttackMove(waypoint.Location)
			end)
			IdleHunt(unit)
		end
	end)
end

IdleHunt = function(unit) 
	if not unit.IsDead then Trigger.OnIdle(unit, unit.Hunt) end 
end