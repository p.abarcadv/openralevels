--[[	RIGHT BASE	]]
RightPower1 = 	{type = "powr"	, pos = CVec.New(-4,-5)	, cost = 300	, 	exists = false}
RightPower2 = 	{type = "apwr"	, pos = CVec.New(-7, -5), cost = 500	, 	exists = false}
RightPower3 = 	{type = "apwr"	, pos = CVec.New(2, 1)	, cost = 300	, 	exists = false}
RightPower4 = 	{type = "apwr"	, pos = CVec.New(2, -2)	, cost = 500	, 	exists = false}
RightPower5 = 	{type = "apwr"	, pos = CVec.New(2, -2)	, cost = 500	, 	exists = false}
RightPower6 = 	{type = "apwr"	, pos = CVec.New(2, 8)	, cost = 500	, 	exists = false}
RightPower7 = 	{type = "powr"	, pos = CVec.New(5, 8)	, cost = 300	, 	exists = false}

RightDome 	= 	{type = "dome"	, pos = CVec.New(5,2)	, cost = 2000	, 	exists = false}

RightProc1 	= 	{type = "proc"	, pos = CVec.New(-8,-2)	, cost = 1400	, 	exists = false}
RightProc2 	= 	{type = "proc"	, pos = CVec.New(-4,9)	, cost = 1400	, 	exists = false}

RightBarr 	= 	{type = "barr"	, pos = CVec.New(0,7)	, cost = 500	, 	exists = false}
RightWeap 	= 	{type = "weap"	, pos = CVec.New(-9,5)	, cost = 2000	, 	exists = false}

RightFix 	= 	{type = "fix"	, pos = CVec.New(-9,2)	, cost = 1000	, 	exists = false}

RightTesla1 = 	{type = "tsla"	, pos = CVec.New(-10,-2), cost = 1500	, 	exists = false}
RightTesla2 = 	{type = "tsla"	, pos = CVec.New(-11,5)	, cost = 1500	, 	exists = false}
RightTesla3 = 	{type = "tsla"	, pos = CVec.New(-5,10)	, cost = 1500	, 	exists = false}
RightTesla4 = 	{type = "tsla"	, pos = CVec.New(-1,12)	, cost = 1500	, 	exists = false}
--NO READY FROM HERE ONWARDS
RightFtur1 	= 	{type = "ftur"	, pos = CVec.New(-10,2)	, cost = 700	, 	exists = false}
RightFtur2 	= 	{type = "ftur"	, pos = CVec.New(-4,5)	, cost = 700	, 	exists = false}
RightFtur3 	= 	{type = "ftur"	, pos = CVec.New(-6,11)	, cost = 700	, 	exists = false}
RightFtur4 	= 	{type = "ftur"	, pos = CVec.New(4,12)	, cost = 700	, 	exists = false}

RightSam1 	= 	{type = "sam"	, pos = CVec.New(3, 2)	, cost = 1200	, 	exists = false}
RightSam2 	= 	{type = "sam"	, pos = CVec.New(15, 1)	, cost = 1200	, 	exists = false}

RightStek 	= 	{type = "stek"	, pos = CVec.New(4,-5)	, cost = 1500	, 	exists = false}

RightKenn 	= 	{type = "kenn"	, pos = CVec.New(1,10)	, cost = 150	, 	exists = false}

--[[	LEFT BASE	]]
LeftPower1 	= 	{type = "powr"	, pos = CVec.New(-4,-1)	, cost = 300	, 	exists = false}
LeftPower2 	= 	{type = "apwr"	, pos = CVec.New(-4, -4), cost = 500	, 	exists = false}
LeftPower3 	= 	{type = "powr"	, pos = CVec.New(-6, -4), cost = 300	, 	exists = false}
LeftPower4 	= 	{type = "apwr"	, pos = CVec.New(-11, 0), cost = 500	, 	exists = false}
LeftPower5 	= 	{type = "apwr"	, pos = CVec.New(5, -1)	, cost = 500	, 	exists = false}
LeftPower6 	= 	{type = "apwr"	, pos = CVec.New(8, -1)	, cost = 500	, 	exists = false}
LeftPower7 	= 	{type = "powr"	, pos = CVec.New(14, -2), cost = 300	, 	exists = false}

LeftDome 	= 	{type = "dome"	, pos = CVec.New(3,-1)	, cost = 2000	, 	exists = false}
LeftProc1 	= 	{type = "proc"	, pos = CVec.New(-8,2)	, cost = 1400	, 	exists = false}
LeftProc2 	= 	{type = "proc"	, pos = CVec.New(-12,3)	, cost = 1400	, 	exists = false}

LeftBarr 	= 	{type = "barr"	, pos = CVec.New(0,3)	, cost = 500	, 	exists = false}
LeftWeap 	= 	{type = "weap"	, pos = CVec.New(3,3)	, cost = 2000	, 	exists = false}
LeftFix 	= 	{type = "fix"	, pos = CVec.New(7,3)	, cost = 1000	, 	exists = false}

LeftKenn 	= 	{type = "kenn"	, pos = CVec.New(2,4)	, cost = 150	, 	exists = false}

LeftTesla1 	= 	{type = "tsla"	, pos = CVec.New(-1, 6)	, cost = 1500	, 	exists = false}
LeftTesla2 	= 	{type = "tsla"	, pos = CVec.New(-8, 6)	, cost = 1500	, 	exists = false}
LeftTesla3 	= 	{type = "tsla"	, pos = CVec.New(6, 6)	, cost = 1500	, 	exists = false}
LeftTesla4 	= 	{type = "tsla"	, pos = CVec.New(14, 6)	, cost = 1500	, 	exists = false}

LeftFtur1 	= 	{type = "ftur"	, pos = CVec.New(-7, 7)	, cost = 700	, 	exists = false}
LeftFtur2 	= 	{type = "ftur"	, pos = CVec.New(0, 7)	, cost = 700	, 	exists = false}
LeftFtur3 	= 	{type = "ftur"	, pos = CVec.New(-1, 3)	, cost = 700	, 	exists = false}
LeftFtur4 	= 	{type = "ftur"	, pos = CVec.New(11, 5)	, cost = 700	, 	exists = false}

LeftSam1 	= 	{type = "sam"	, pos = CVec.New(3, 2)	, cost = 1200	, 	exists = false}
LeftSam2 	= 	{type = "sam"	, pos = CVec.New(15, 1)	, cost = 1200	, 	exists = false}

LeftStek 	= 	{type = "stek"	, pos = CVec.New(-9, -5), cost = 1500	, 	exists = false}

--[[	MID BASE	]]
MidWeap 	= 	{type = "weap"	, pos = CVec.New(15, 1)	, cost = 1500	, 	exists = true, Owner = "USSR"}
MidBarr 	= 	{type = "barr"	, pos = CVec.New(-9, -6), cost = 500	, 	exists = true, Owner = "USSR"}

LeftBaseBuildings = 
{
	LeftPower1,
	LeftPower2,

	--Fourth Batch
	
	LeftProc1,
	LeftFtur1,
	LeftBarr,
	LeftPower3,
	LeftFix,
	LeftPower4,
	
	LeftPower5,
	LeftWeap,
	LeftProc2,
	LeftPower6,
	
	LeftDome,
	LeftPower7,
	LeftKenn,
	LeftStek,
	
	--Fifth Batch
	LeftFix,
	LeftTesla1,
	LeftTesla2,
	LeftTesla3,
	LeftTesla4,
	LeftFtur2,
	LeftFtur3,
	LeftFtur4,
	LeftSam1,
	LeftSam2,
}

--[[RIGHT BASE]]
RightBaseBuildings = 
{
	RightPower1,
	RightPower2,
	RightProc1,
	RightFtur1,
	RightBarr,
	RightPower5,
	RightPower6,
	RightPower7,
	--Fourth Batch
	RightWeap,
	RightDome,
	RightPower3,
	RightPower4,
	RightProc2,
	RightFix,
	RightStek,
	--Fifth Batch
	RightFix,
	RightTesla1,
	RightTesla2,
	RightTesla3,
	RightTesla4,

	RightFtur2,
	RightFtur3,
	RightFtur4,
	RightSam1,
	RightSam2,
}

Trigger.OnKilled(OutWeap, function()
	MidWeap.exists = false
end)

Trigger.OnKilled(OutBarr, function()
	MidBarr.exists = false
end)
