if Map.LobbyOption("difficulty") == "easy" then
	PlayerInitCash = 4000
	PlayerExtraCash = 3000
	ChronoForces = true
	remainingTime = DateTime.Seconds(900) --15 mins
elseif Map.LobbyOption("difficulty") == "normal" then
	PlayerInitCash = 3000
	PlayerExtraCash = 2000
	ChronoForces = true
	remainingTime = DateTime.Seconds(720) --12 mins
else
	PlayerInitCash = 3000
	PlayerExtraCash = 2000
	ChronoForces = false
	remainingTime = DateTime.Seconds(510) --9 mins
end

OutpostPower 		= { OutPower1, OutPower2, OutPower3, OutPower4, OutPower5, OutPower6, OutPower7, OutPower8, OutPower9 }

ExtraPowerPlants 	= { extraPower1 }
OutpostBuildings 	= 
	{ 
	OutWeap, OutBarr, OutKenn, OutProc, OutFix, OutSilo1, OutSilo2, OutPower1, OutPower2, OutPower3, OutPower4, OutPower5, OutPower6, OutPower7, OutPower8, OutPower9, tesla1, tesla2, tesla3, tesla4, tesla5 
	}
BotOutpostUnits 	= { BotFtur1, BotFtur2, BotFtur3, BotFtur4, BotTnk1, BotTnk2, BotTnk3, BotTnk4 }

AlliedMCVEscort = {"2tnk", "1tnk", "e1", "e1", "e3"}
SovietMCVEscort = {"3tnk", "3tnk", "v2rl", "ttnk", "ttnk"}

PSouthPath = {PSouthEntry.Location, PSouthDst.Location}
PSouthUnits = {"2tnk", "2tnk", "2tnk", "2tnk", "arty", "arty"}

EnglandAAGuns = { EnglandAAGun1, EnglandAAGun2, EnglandAAGun3, EnglandAAGun4 }

AddedAnnihilationObjective = false

reinforcementsP = false
SovietMCVSent = false

-------------------------------
--ENEMY BASE INITIATION START--
-------------------------------
SetupBaseVariables = function()
	--Media.Debug("Setup complete")
	Left	= {E1McvEntry, E1FactLoc, {E1McvEntry.Location, E1FactLoc.Location}, LeftBaseBuildings}
	Right	= {E2McvEntry, E2FactLoc, {E2McvEntry.Location, E2FactLoc.Location}, RightBaseBuildings}

	MCVPaths = { Left, Right }
end

SovietForcesSetup = function()
	rng = Utils.RandomInteger(1, 3)
	ArmorAtkCount = 4
	Media.PlaySpeechNotification(player, "SovietForcesApproaching")
	if SovietMCVSent == false and AddedAnnihilationObjective == false then
		SovietMCVSent = true
		AddedAnnihilationObjective = true
		OoMAir = false
		
		Trigger.AfterDelay(DateTime.Seconds(30), function()
			SendMCVCompanions()
		end)
		Trigger.AfterDelay(DateTime.Minutes(1), function()
			SendMCVs()
		end)
	end
end

SendMCVCompanions = function()
	Media.PlaySpeechNotification(player, "SovietReinforcementsArrived")
	if SendMCVUSSR == true and SendMCVBadguy == true then
		--Media.Debug("Send soviet reinforcements - both sides")
		Reinforcements.Reinforce(enemy, SovietMCVEscort, { E1McvEntry.Location, EBaseControl1.Location+CVec.New(0,1) }, 20)
		Reinforcements.Reinforce(badguy, SovietMCVEscort, { E2McvEntry.Location, EBaseControl2.Location+CVec.New(0,1) }, 20)
	else
		--Media.Debug("Send soviet reinforcements - one side")
		local path = MCVPaths[rng][3]
		Reinforcements.Reinforce(enemy, SovietMCVEscort, { path[1], path[2] + CVec.New(-3,3) }, 20)
	end
end

SendMCVs = function()
	Media.PlaySpeechNotification(player, "SovietReinforcementsArrived")
	if SendMCVUSSR == true and SendMCVBadguy == true then
		--Media.Debug("SendMCV USSR and BadGuy")
		local mcv1 = Reinforcements.Reinforce(enemy, 	{"mcv"}, MCVPaths[1][3], 0)[1]
		local mcv2 = Reinforcements.Reinforce(badguy, 	{"mcv"}, MCVPaths[2][3], 0)[1]
		DeployMCV(MCVPaths[1], mcv1)
		DeployMCV(MCVPaths[2], mcv2)
		enemy.Cash = enemy.Cash + 10000
		badguy.Cash = badguy.Cash + 10000
	else
		--Media.Debug("SendMCV USSR")
		local path = MCVPaths[rng][3]
		local mcv = Reinforcements.Reinforce(enemy, {"mcv"}, path, 0)[1]
		DeployMCV(MCVPaths[rng], mcv)
		enemy.Cash = enemy.Cash + 10000
	end
end

DeployMCV = function(data, mcv)
	local coord_1 = data[1]
	local coord_2 = data[2]
	local toBuild = data[4]
	Trigger.OnEnteredFootprint({coord_2.Location}, function(a, id)
		if ( a.Owner == enemy or a.Owner == badguy ) and a.Type == mcv.Type then
			Trigger.RemoveFootprintTrigger(id)
			mcv.Deploy()
			Trigger.AfterDelay(DateTime.Seconds(5), function()
				local facts = Map.ActorsInBox(coord_1.CenterPosition, coord_2.CenterPosition, function(actor)
				return actor.Type == "fact" and (actor.Owner == enemy or actor.Owner == badguy) end)
				
				if #facts > 0 then
					local fact = facts[1]
					local factOwner = fact.Owner

					Trigger.AfterDelay(DateTime.Seconds(2), function()
						BuildBase(toBuild, fact, factOwner)
					end)
				end
			end)
		end
	end)
end
-----------------------------
--ENEMY BASE INITIATION END--
-----------------------------

Trigger.OnAllKilled(BotOutpostUnits, function()
	player.MarkCompletedObjective(DestroyCheckpoint)
	Trigger.AfterDelay(DateTime.Seconds(4), function()
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		Reinforcements.Reinforce(player, PSouthUnits, PSouthPath, 20)
	end)
end)

Trigger.OnAllKilled(OutpostPower, function()
	player.MarkCompletedObjective(DisableTeslas)
	Utils.Do(ExtraPowerPlants, function(plant)
		if not plant.IsDead then
			plant.Destroy()
		end
	end)
	if AddedAnnihilationObjective == false then
		AddedAnnihilationObjective = true
		DenySoviets = player.AddPrimaryObjective("Eliminate soviet presence in the area.")
	end
end)

Trigger.OnKilled(ChronoTarget, function()
	enemy.MarkCompletedObjective(DenyAllies)
end)

CallReinforcements = function()
	Media.PlaySpeechNotification(player, "ReinforcementsArrived")
	
	Reinforcements.Reinforce(player, {"mcv"}, {PReinforceEntry2.Location, PReinforceRally2.Location}, 20)
	
	Trigger.AfterDelay(DateTime.Seconds(2), function()
		Reinforcements.Reinforce(player, {"jeep", "jeep"}, {PReinforceEntry2.Location, PReinforceRally2.Location}, 20)
	end)
	
	Trigger.AfterDelay(DateTime.Seconds(3), function()
		Reinforcements.Reinforce(player, AlliedMCVEscort, {PReinforceEntry1.Location, PReinforceRally1.Location}, 20)
		Reinforcements.Reinforce(player, AlliedMCVEscort, {PReinforceEntry3.Location, PReinforceRally3.Location}, 20)
	end)
end

AlliedChronoReinforce = function()
	if ChronoForces == true and not ChronoTarget.IsDead then
		Media.PlaySoundNotification(player, "ChronoshiftSound")	--Sound name missing
		Media.PlaySpeechNotification(player, "ReinforcementsArrived")
		Actor.Create("ctnk", true, { Owner = player, Location = ChronoTarget.Location + CVec.New(-2,1), Facing = 64 })
		Actor.Create("ctnk", true, { Owner = player, Location = ChronoTarget.Location + CVec.New(3,1), Facing = 64 })
	end
end

RunInitialAI = function()
	OutBarr.IsPrimaryBuilding = true
	
	ProduceArmor(MidWeap, enemy, MidAtkPaths, InitialArmorAttack, OutpostAtkInterval)
	ProduceInfantry(MidBarr, enemy, MidAtkPaths, InitialInfAttack, OutpostAtkInterval)

	Trigger.AfterDelay(DateTime.Seconds(30), function()
		OoMPlaneAtk()
	end)
end

DestroyExtraPowerPlants = function()
	Utils.Do({extraPower2,extraPower3,extraPower4,extraPower5}, function(p)
		p.Destroy()
	end)
end

Tick = function()   
    if player.HasNoRequiredUnits() then
        enemy.MarkCompletedObjective(DenyAllies)
    end
	
	if enemy.HasNoRequiredUnits() and badguy.HasNoRequiredUnits() then
		player.MarkCompletedObjective(DefendChrono)
        player.MarkCompletedObjective(DenySoviets)
    end
	
	if remainingTime == DateTime.Minutes(10) then
        Media.PlaySpeechNotification(player, "TenMinutesRemaining")
	elseif remainingTime == DateTime.Minutes(11) then
		SendParatroopers()
    elseif remainingTime == DateTime.Minutes(5) then
		SendParatroopers()
        Media.PlaySpeechNotification(player, "WarningFiveMinutesRemaining")
    elseif remainingTime == DateTime.Minutes(4) then
        Media.PlaySpeechNotification(player, "WarningFourMinutesRemaining")
    elseif remainingTime == DateTime.Minutes(3) then
        Media.PlaySpeechNotification(player, "WarningThreeMinutesRemaining")
		AlliedChronoReinforce()
    elseif remainingTime == DateTime.Minutes(2) then
		SendParatroopers()
		Trigger.AfterDelay(DateTime.Seconds(15), function()
			SovietForcesSetup()
		end)
        Media.PlaySpeechNotification(player, "WarningTwoMinutesRemaining")
    elseif remainingTime == DateTime.Minutes(1) then
        Media.PlaySpeechNotification(player, "WarningOneMinuteRemaining")
    end

    if remainingTime > 0 and timerStarted then
        UserInterface.SetMissionText("Reinforcements arriving in " .. Utils.FormatTime(remainingTime), player.Color)
        if not addtime then
            remainingTime = remainingTime - 1
        end
    elseif remainingTime == 0 then
		Trigger.AfterDelay(DateTime.Seconds(10), function()
			SellOutpost()
			DestroyExtraPowerPlants()
		end)
        UserInterface.SetMissionText("")
        --enemy.MarkCompletedObjective(DenyAllies)
		if reinforcementsP == false then
			reinforcementsP = true
			Trigger.AfterDelay(DateTime.Seconds(2), CallReinforcements)
		end
    end
end

InitTriggers = function()
	Camera.Position = CameraStartingPoint.CenterPosition
	enemy.Cash = 10000
	badguy.Cash = 10000
	player.Cash = PlayerInitCash
	
	timerStarted = true

	RunInitialAI()

	SetupBaseVariables()
end

InitObjectives = function()
	Trigger.OnObjectiveAdded(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "New " .. string.lower(p.GetObjectiveType(id)) .. " objective")
	end)

	DefendChrono = player.AddPrimaryObjective("Defend the Chronosphere.")
	DisableTeslas = player.AddPrimaryObjective("Destroy the power plants to disable the tesla coils.")
	DestroyCheckpoint = player.AddSecondaryObjective("Destroy south-west outpost for additional troops.")
	
	DenyAllies = enemy.AddPrimaryObjective("Deny the Allies.")
	
	Trigger.OnObjectiveCompleted(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective completed")
	end)
	Trigger.OnObjectiveFailed(player, function(p, id)
		Media.DisplayMessage(p.GetObjectiveDescription(id), "Objective failed")
	end)
	
	Trigger.OnPlayerLost(player, function()
		Media.PlaySpeechNotification(player, "Lose")
	end)
	Trigger.OnPlayerWon(player, function()
		Media.PlaySpeechNotification(player, "Win")
	end)
end

WorldLoaded = function()
	player = Player.GetPlayer("Greece")
	enemy = Player.GetPlayer("USSR")
	badguy = Player.GetPlayer("BadGuy")
	england = Player.GetPlayer("England")
	
	TimerColor = player.Color

	InitObjectives()
	InitTriggers()
end